*This program cleans the summary of deposits data and creates various
*measures of banking development at the county level
/*
if "`c(os)'" == "Unix" {
	global location "/san/RDS/Work/fif/b1mxp07/Matt"
	adopath ++ "/san/RDS/Work/fif/b1mxp07/my_ados"
}
else if "`c(os)'" == "Windows" {
	global location "//RB/B1/NYRESAN/RDS/Work/fif/b1mxp07/Matt"
	adopath ++ "//RB/B1/NYRESAN/RDS/Work/fif/b1mxp07/my_ados"
}
set more off
*/

if "`c(os)'" == "Unix" {
	global location "/home/rcemxp06/accomodation"
	global outreg "/home/rcemxp06/accomodation/output/regressions"
	global outsum "/home/rcemxp06/accomodation/output/summaries"
	global outint "/home/rcemxp06/accomodation/output/interactions"
	global outfig "/home/rcemxp06/accomodation/output/figures"
	global data "/home/rcemxp06/accomodation/data"
	global subcode "/home/rcemxp06/accomodation/code/subcode"
	adopath ++ "/home/rcemxp06/ado"
}
else if "`c(os)'" == "Windows" {
	global location "R:/accomodation"
	global outreg "R:/accomodation/output/regressions"
	global outsum "R:/accomodation/output/summaries"
	global outint "R:/accomodation/output/interactions"
	global outfig "R:/accomodation/output/figures"
	global data "R:/accomodation/data"
	global subcode "R:/accomodation/code/subcode"
	adopath ++ "R:/ado"
}

/*
use "$location/data/sod/sod.dta", clear
replace idrssd = id_rssd if idrssd==.
drop id_rssd

tostring(date), replace
gen date2 = date(date, "YMD")
drop date
ren date2 date
format %td date

label var idrssd "Entity Code"
label var hi_holder "High Holder"
label var br_name "Branch Name"
label var br_num "Branch Number"
label var br_street "Branch Street"
label var br_city "Branch City"
label var br_county_nm "Branch County Name"
label var br_county_cd "Branch County Code"
label var br_msa_cd "Branch MSA Code"
label var br_state_cd "Branch State Code"
label var br_stcnty_cd "Branch FIPS Code"
label var br_zip_cd "Branch Zip Code"
label var br_deposits "Branch Deposits"
label var norssdflag "No RSSD Flag"
label var bank "Commercial Bank Dummy"
label var bank_name "Bank Name"
label var br_serv_type "Branch Serv Type"
label var br_mainoffice "Branch Main Office Dummy"
label var thrift "Thrift Dummy"
label var br_cbsa_metro "CBSA Code"
label var br_cbsa_metro_name "CBSA Name"
label var br_feddistrict "Fed District" 
label var date "Date"

replace thrift = 0 if bank==1

sort date
order idrssd bank_name hi_holder date br_county_nm br_stcnty_cd br_deposits bank thrift br_num br_city br_county_cd br_msa_cd br_state_cd br_zip_cd ///
norssdflag rssd86 br_serv_type br_mainoffice year

save, replace
*/

*SOD? 

use "$location/data/sod/sod.dta", clear
//These are not real counties
drop if br_stcnty_cd<1001

gen st_cd=floor(br_stcnty_cd/1000)

gen cleanout=0
foreach num of numlist 60 64 66 68 69 70 72 75 78 90 {
replace cleanout=1 if st_cd==`num'
}
drop if cleanout==1
drop cleanout


//tabstat date, by(st_cd) stat(n)


//Getting Number of Branches by High Holder
gen numbr = 1
label var numbr "Number of branches"
//These banks do not have an entity code
drop if hi_holder==0

//Generating multistate dummy by branch
bys hi_holder date: egen stsd = sd(br_state_cd)
gen multist = (stsd>0 & stsd!=.)

collapse (sum) br_deposits numbr (first) br_county_nm multist, by(hi_holder br_stcnty_cd date)

// log assets  EW and DW  x x
// log branches EW x and DW x
// Percent multistate EW x and DW x
// Deposit share HHI x
// Percent top 5% EW and DW 

// Banks per unit population


/***********************************************/
/****BRANCH OUTSIDE COUNTY TO BRANCH WITHIN*****/
/***********************************************/
//Total number of branches by high holder
bys hi_holder date (br_stcnty_cd): egen tot_br_hh = total(numbr)
//Total number of branches by high holder that are in the county
bys br_stcnty_cd date: egen tot_cnty_br = total(numbr)
bys br_stcnty_cd date: egen tot_br = total(tot_br_hh)
//Percentage of branches that are outside the county
gen tot_ex_br = tot_br - tot_cnty_br
assert tot_ex_br>=0
gen rat_ex_cnty_br = tot_ex_br/tot_cnty_br
assert rat_ex_cnty_br!=.

/***********************************************/
/***DEPOSIT OUTSIDE COUNTY TO DEPOSIT WITHIN****/
/***********************************************/
bys hi_holder date (br_stcnty_cd): egen tot_dep_hh = total(br_deposits)
bys br_stcnty_cd date: egen tot_cnty_dep = total(br_deposits)
bys br_stcnty_cd date: egen tot_dep = total(tot_dep_hh)
gen tot_ex_dep = tot_dep - tot_cnty_dep
assert tot_ex_dep>=0
gen rat_ex_cnty_dep = tot_ex_dep/tot_cnty_dep
//assert rat_ex_cnty_dep!=.

/********************************************************/
/***PERCENTAGE OF BANKS AND BRANCHES IN COUNTY THAT ARE MULTI STATE***/
/********************************************************/

gen numbank = 1

bys br_stcnty_cd date: egen tot_cnty_bank = total(numbank)
by br_stcnty_cd date: egen multist_bank = total(multist)
gen pew_multi_bank = multist_bank/tot_cnty_bank
by br_stcnty_cd date: egen multist_bank_dep = total(multist*br_deposits)
gen pdw_multi_bank = multist_bank_dep/tot_cnty_dep

gen multistbr=multist*numbr
by br_stcnty_cd date: egen multist_br = total(multistbr)
gen pew_multi_br = multist_br/tot_cnty_br

/***Percent that are UNIT ****/
gen unitbank=(tot_br_hh==1)
by br_stcnty_cd date: egen unitbr_cnty = total(unitbank*numbr)
gen pew_unit_bank = unitbr_cnty/tot_cnty_br
by br_stcnty_cd date: egen unitbr_cnty_dep = total(unitbank*br_deposits)
gen pdw_unit_bank = unitbr_cnty_dep/tot_cnty_dep


/***total branches****/
by br_stcnty_cd date: egen totbr_cnty = total(tot_br_hh)
by br_stcnty_cd date: egen totbr_cnty_dw = total(tot_br_hh)
gen log_totbr_cnty=log(totbr_cnty)

/****HHI DEPOSITS****/
gen dep_share=br_deposits/tot_cnty_dep
by br_stcnty_cd date: egen hhi_dep = total(dep_share*dep_share)
replace hhi_dep=. if hhi_dep==0

drop multist_* unitbr_* totbr_cnty

gen year=year(date)
save "$location/data/sod/sodhh.dta", replace

/***Construct real assets****/
import excel using "$location/data/misc/CPILFESL_import.xls", firstrow clear
gen month=month(DATE)
gen year=year(DATE)
keep if month==6
drop DATE

rename CPILFESL cpi

save "$location/data/misc/cpi.dta", replace



use "$location/data/regdata/class_rawcore_annual.dta", clear

keep year hhrssd_dup assets
rename hhrssd_dup hi_holder
merge m:1 year using "$location/data/misc/cpi.dta"
drop if _merge!=3
drop _merge

sum cpi if year==2000
gen base=r(mean)
gen assets_real=assets/cpi*base



foreach var in assets_real{
foreach num in 100{
cap drop tag
gen tag = (`var'~=.)
sort year tag `var'
by year tag: gen p`num'_`var' = floor((_n-1)/_N * `num') + 1 if tag
drop tag
}
}

tempfile cpi_assets
save `cpi_assets', replace

//merge with sod//
use "$location/data/sod/sodhh.dta", replace

merge m:1 hi_holder year using `cpi_assets'
drop if _merge==2
drop _merge year

/***log_assets****/
bys br_stcnty_cd date: egen assets_cnty = mean(assets_real)
gen log_ew_assets_cnty=log(assets_cnty)

bys br_stcnty_cd date: egen total_assets = total(assets)
replace total_assets=. if total_assets==0
bys br_stcnty_cd date: egen total_assets_real = total(assets_real)
replace total_assets_real=. if total_assets_real==0

gen log_tot_assets=log(total_assets)
gen log_tot_assets_real=log(total_assets_real)

by br_stcnty_cd date: egen assets_cnty_dep = total(assets_real*br_deposits)
replace assets_cnty_dep=. if assets_cnty_dep==0
gen br_deposits_a=br_deposits if assets_real~=.

bys br_stcnty_cd date: egen tot_cnty_dep_a = total(br_deposits_a)
replace tot_cnty_dep_a=. if tot_cnty_dep_a==0

gen log_dw_assets_cnty = log(assets_cnty_dep /tot_cnty_dep_a)

gen pct_local_dep=br_deposits/tot_dep_hh
gen pct_local_at=br_deposits/assets/1000

bys br_stcnty_cd date: egen pct_local_cnty_dep = mean(pct_local_dep)
bys br_stcnty_cd date: egen pct_local_cnty_at = mean(pct_local_at)

by br_stcnty_cd date: egen temp = total(pct_local_dep*br_deposits)
gen pct_local_cnty_dep_dw = temp/tot_cnty_dep
drop temp

by br_stcnty_cd date: egen temp = total(pct_local_at*br_deposits)
gen pct_local_cnty_at_dw = temp/tot_cnty_dep_a

/**precent in top 1% and percent $1bn in real terms**/

gen large=(assets_real>1500000 & assets_real~=.)
bys br_stcnty_cd date: egen large_cnty=total(large)
gen numbank_a=numbank if assets_real~=.
bys br_stcnty_cd date: egen numbank_a_cnty=total(numbank_a)
replace numbank_a_cnty=. if numbank_a_cnty==0
gen pew_large=large_cnty/numbank_a_cnty

by br_stcnty_cd date: egen large_cnty_dep = total(large*br_deposits)
gen pdw_large=large_cnty_dep/tot_cnty_dep_a

gen top1=(p100_assets_real>99 & p100_assets_real~=.)
bys br_stcnty_cd date: egen top1_cnty=total(top1)
gen pew_top1=top1_cnty/numbank_a_cnty
by br_stcnty_cd date: egen top1_cnty_dep = total(top1*br_deposits)
gen pdw_top1=top1_cnty_dep/tot_cnty_dep_a


drop assets_cnty_dep numbank_a_cnty top1 large top1_cnty_dep large_cnty_dep

save "$location/data/sod/sodhh.dta", replace

collapse (sum) br_deposits br_deposits_a numbr numbank numbank_a (first) rat_ex_cnty_br rat_ex_cnty_dep pew* pdw* log_totbr_cnty hhi* log_ew* log_dw* log_tot_a* pct_local_cnty*, by(br_stcnty_cd date)


//Filling in missings after doing ts fill

gen year = year(date)
/*
egen temp = group(br_stcnty_cd year)
tsset temp local_hh
tsfill, full
foreach var in date br_stcnty_cd year{
	bys temp: egen t`var' = max(`var')
	replace `var' = t`var' if `var'==.
	drop t`var'
}
foreach var in br_deposits numbr {
	replace `var' = 0 if `var'==.
}

//Calculating total county deposits and local share
bys br_stcnty_cd date: egen cnty_dep = sum(br_deposits)
gen cnty_dep_local = br_deposits/cnty_dep if local_hh==1

//Calculating total number of branches in the county
bys br_stcnty_cd date: egen cnty_num_br = sum(numbr)

//Keep only one instance
keep if local_hh==1
keep year br_stcnty_cd cnty_dep_local cnty_num_br rat_ex_cnty_br rat_ex_cnty_dep perc_multi_cnty
*/

drop date
tostring br_stcnty_cd, gen(area)
drop br_stcnty_cd
replace area = "0" + area if length(area)==4

saveold "$location/data/sod/sodcnty.dta", replace


