global user Minh
set more off

if "${user}"=="Minh"{
	global location "C:/Users/Minh/OneDrive/frombank"
	global outreg "$location/output/regressions"
	global outsum "$location/output/summaries"
	global outint "$location/frombank/output/interactions"
	global outfig "$location/frombank/output/figures"
	global data "$location/data"
	global subcode "$location/code/subcode"
}
else{
	if "`c(os)'" == "Unix" {
		global location "/home/rcemxp06/frombank"
		global outreg "/home/rcemxp06/frombank/output/regressions"
		global outsum "/home/rcemxp06/frombank/output/summaries"
		global outint "/home/rcemxp06/frombank/output/interactions"
		global outfig "/home/rcemxp06/frombank/output/figures"
		global data "/home/rcemxp06/frombank/data"
		global subcode "/home/rcemxp06/frombank/code/subcode"
		adopath ++ "/home/rcemxp06/ado"
	}
	else if "`c(os)'" == "Windows" {
		global location "R:/frombank"
		global outreg "R:/frombank/output/regressions"
		global outsum "R:/frombank/output/summaries"
		global outint "R:/frombank/output/interactions"
		global outfig "R:/frombank/output/figures"
		global data "R:/frombank/data"
		global subcode "R:/frombank/code/subcode"
		adopath ++ "R:/ado"
	}
}
