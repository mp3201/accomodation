*This program cleans the raw qcew data and puts it in panel format

/***********/
/*National**/
/***********/

cd /san/RDS/Work/fif/b1mxp07/Matt/data/
global path "/san/RDS/Work/fif/b1mxp07/Matt"
use qcew_nat_1990_2011_cleaned, clear

//Dropping aggregate naics
drop if naics=="101" | naics=="102"
drop statq*

ren m3 emplq1
ren m6 emplq2
ren m9 emplq3
ren m12 emplq4

drop m1 m2 m4 m5 m7 m8 m10 m11

reshape long estabq emplq wageq awwq, i(area naics own agglvl year) j(quarter)

ren estabq estabq_nat 
ren emplq emplq_nat
ren wageq wageq_nat
ren awwq awwq_nat

gen date = yq(year,quarter)
format %tq date
drop year quarter

drop area

label variable estabq_nat "National Establishment"
label variable emplq_nat "National Employment"
label variable wageq_nat "National Wages"
label variable awwq_nat "National Average Weekly Wage (saved from original)"

sort naics date
save qcew_nat_1990_2011_panel, replace

/***********/
/**COUNTY***/
/***********/

cd /san/RDS/Work/fif/b1mxp07/Matt/data/
use qcew_county_1990_2011_cleaned, clear

//Dropping aggregate naics
drop if naics=="101" | naics=="102"


//Keeping 2 Digits
drop if agglvl==75

/*
//Keeping 3 Digits
drop if agglvl==74
*/


//Dropping fips with 999
gen non_cnty = substr(area, 3,3)
drop if non_cnty=="999"
drop non_cnty

gen avemplq1 = wageq1/(awwq1*13)
gen avemplq2 = wageq2/(awwq2*13)
gen avemplq3 = wageq3/(awwq3*13)
gen avemplq4 = wageq4/(awwq4*13)

//End of quarter employment
ren m3 emplq1
ren m6 emplq2
ren m9 emplq3
ren m12 emplq4

drop m1 m2 m4 m5 m7 m8 m10 m11

//Panel format
reshape long estabq emplq avemplq wageq awwq statq , i(area naics own agglvl year) j(quarter)

//Dropping non 50/DC
gen state = substr(area, 1,2)
drop if state=="72" | state=="78"

label variable estabq "Establishment"
label variable emplq "End of Quarter Employment"
label variable avemplq "Quarterly Average Employment"
label variable wageq "Total Wage"
label variable awwq "Average Weekly Wage"
label variable naics "NAICS Code"
label variable area "County Code"
label variable own "Ownership Code"
label variable agglvl "Aggregation Level"
label variable statq "Disclosure Status"

gen date = yq(year, quarter)
format %tq date
drop year quarter state 

order area naics date own agglvl estabq emplq avemplq wageq awwq

replace statq="1" if statq==""
replace statq="2" if statq=="-"
replace statq="3" if statq=="N"

destring statq, replace

label define status 1 "Disclosed" 2 "No Data Available" 3 "Nondisclosed"
label values statq status

save qcew_county_1990_2011_panel, replace

/*
preserve
keep if quarter(dofq(date))==1
keep area naics date own agglvl annstat annestab annemp annwage antxwage anncntrb annaww aap
save qcew_1990_2011_annual2, replace
restore
*/

//Creating Measures of Employment Growth
//Sort once since dataset is big
sort area date
by area date: gen temp = emplq if naics=="10"
by area date: egen empl_cnty = max(temp)
label variable empl_cnty "County Employment - Series 10"
by area date: gen temp2 = estabq if naics=="10"
by area date: egen estab_cnty = max(temp2)
label variable estab_cnty "County Establishments - Series 10"
drop temp*

drop if naics=="10"
by area date: egen empl_cnty_est = sum(emplq)
label variable empl_cnty_est "County Employment - Hand Summation"
by area date: egen estab_cnty_est = sum(estabq)
label variable estab_cnty_est "County Establishments - Hand Summation"

gen empl_cnty_rat = empl_cnty_est/empl_cnty
label variable empl_cnty_rat "Employment Ratio - Est/Actual"
gen estab_cnty_rat = estab_cnty_est/estab_cnty
label variable estab_cnty_rat "Establishments Ratio - Est/Actual"

by area date: gen num = _n
replace empl_cnty_rat = . if num!=1
replace estab_cnty_rat = . if num!=1

//Regressing Missing Percentage against log employment
gen empl_cnty_miss = 1 - empl_cnty_rat
gen log_emplq = log(emplq)
reg empl_cnty_miss log_emplq, robust

//Histogram of estimated empl to actual empl
hist empl_cnty_rat, fraction ///
title("Estimated employment to actual - Quarterly, All time")
graph export "$path/output/empl_quart_rat_cnty.png", replace
hist empl_cnty_rat if date>=tq(2001q1), fraction ///
title("Estimated employment to actual - Quarterly, Post 2001")
graph export "$path/output/empl_quartpost2001_rat_cnty.png", replace
//Histogram of estimated estabilishment to actual establishments
hist estab_cnty_rat, fraction ///
title("Estimated establishments to actual - Quarterly, All time")
graph export "$path/output/estab_quart_rat_cnty.png", replace
hist estab_cnty_rat if date>=tq(2001q1), fraction ///
title("Estimated establishments to actual - Quarterly, Post 2001")
graph export "$path/output/estab_quartpost2001_rat_cnty.png", replace

save, replace

collapse (mean) empl_cnty empl_cnty_est, by(area date)

keep area date empl_cnty empl_cnty_est if num==1

//Estimated county growth rates
sort area date
by area: gen empl_cnty_est_l4 = empl_cnty_est[_n-4]
//What happens if there are new entries in the industry
//This growth rate would be missing
by area: gen gr_cnty_est = (empl_cnty_est/empl_cnty_est_l4) - 1
br if empl_cnty_est_l4==. & empl_cnty_est!=.

//Actual county growth rates
by area: gen empl_cnty_l4 = empl_cnty[_n-4]
//What happens if there are new entries in the industry
//This growth rate would be missing
gen gr_cnty = (empl_cnty/empl_cnty_l4) - 1
br if empl_cnty_l4 ==. & empl_cnty != .

gen gr_est_act = gr_cnty_est/gr_cnty
label variable gr_est_act "Estimate GR/Actual GR"

hist gr_est_act if abs(gr_est_act-1)<20, fraction
graph export "/san/RDS/Work/fif/b1mxp07/Matt/output/gr_rat_cnty.png", replace

//Merging national with county panel
//Sort to merge with national series
sort naics date
use qcew_nat_1990_2011_panel, clear
merge 1:m naics date using qcew_county_1990_2011_panel.dta
format %tq date
order area naics date agglvl estabq estabq_nat emplq emplq_nat avemplq avemplq_nat wageq wageq_nat awwq awwq_nat




/***********/
/****MSA****/
/***********/

cd /san/RDS/Work/fif/b1mxp07/Matt/data/
use qcew_msa_1990_2011_cleaned, clear

//Dropping aggregate naics
drop if naics=="101" | naics=="102"

//Average monthly employment
gen avemplq1 = wageq1/(awwq1*13)
gen avemplq2 = wageq2/(awwq2*13)
gen avemplq3 = wageq3/(awwq3*13)
gen avemplq4 = wageq4/(awwq4*13)

//End of quarter employment
ren m3 emplq1
ren m6 emplq2
ren m9 emplq3
ren m12 emplq4

drop m1 m2 m4 m5 m7 m8 m10 m11

//Panel format
reshape long estabq emplq avemplq wageq awwq statq, i(area naics own agglvl year) j(quarter)

label variable estabq "Establishment"
label variable emplq "End of Quarter Employment"
label variable avemplq "Quarterly Average Employment"
label variable wageq "Total Wage"
label variable awwq "Average Weekly Wage"
label variable naics "NAICS Code"
label variable area "County Code"
label variable own "Ownership Code"
label variable agglvl "Aggregation Level"
label variable statq "Disclosure Status"

gen date = yq(year, quarter)
format %tq date
drop year quarter

order area naics date own agglvl estabq emplq avemplq wageq awwq 

replace statq="1" if statq==""
replace statq="2" if statq=="-"
replace statq="3" if statq=="N"

destring statq, replace

label define status 1 "Disclosed" 2 "No Data Available" 3 "Nondisclosed"
label values statq status

save qcew_msa_1990_2011_panel, replace

//Creating Measures of Employment Growth
//Sort once since dataset is big
sort area date
by area date: gen temp = emplq if naics=="10"
by area date: egen empl_msa = max(temp)
label variable empl_msa "MSA Employment - Series 10"
drop temp

drop if naics=="10"
by area date: egen empl_msa_est = sum(emplq)
label variable empl_msa_est "County Employment - Hand Summation"

gen empl_msa_rat = empl_msa_est/empl_msa
label variable empl_msa_rat "Employment Ratio - Est/Actual"

sort area date naics
by area date: gen num = _n
replace empl_msa_rat = . if num!=1

//Histogram of estimated empl to actual empl
hist empl_msa_rat, fraction
graph export "/san/RDS/Work/fif/b1mxp07/Matt/output/empl_rat_msa.png", replace

//Hist of estimated empl to actual empl post 2001
hist empl_msa_rat if date>=tq(2001q1), fraction
//Hist of estimated empl to actual empl pre 2001
hist empl_msa_rat if date<tq(2001q1), fraction

save, replace

keep if num==1
keep area date empl_msa empl_msa_est

gen greater = (empl_msa_est>empl_msa)
gen recent = (date>=tq(2001q1))

//Estimated MSA growth rates
sort area date
by area: gen empl_msa_est_l4 = empl_msa_est[_n-4]

by area: gen gr_msa_est = (empl_msa_est/empl_msa_est_l4) - 1
br if empl_msa_est_l4==. & empl_msa_est!=.

//Actual MSA growth rates
by area: gen empl_msa_l4 = empl_msa[_n-4]
//What happens if there are new entries in the industry
//This growth rate would be missing
gen gr_msa = (empl_msa/empl_msa_l4) - 1
br if empl_msa_l4 ==. & empl_msa != .

gen gr_est_act = gr_msa_est/gr_msa
label variable gr_est_act "Estimate GR/Actual GR"

hist gr_est_act if abs(gr_est_act-1)<20, fraction
graph export "/san/RDS/Work/fif/b1mxp07/Matt/output/gr_rat_msa.png", replace

save qcew_msa_emplq, replace



/*********/
/***OLD***/
/*********/

//National measures from MSA level data
use qcew_msa_1990_2011_panel, clear
collapse (sum) estabq emplq avemplq wageq, by(year quarter naics)

ren estabq estabq_nat 
ren emplq emplq_nat
ren avemplq avemplq_nat
ren wageq wageq_nat

gen awwq_nat = wageq_nat/(avemplq_nat*13)

label variable estabq_nat "National Establishment (Sum)"
label variable emplq_nat "National Employment (Sum)"
label variable wageq_nat "National Wages (Sum)"
label variable avemplq_nat "National Average Employment (sum)"
label variable awwq_nat "National Average Weekly Wage"

save qcew_natmsa_1990_2011_panel, replace


