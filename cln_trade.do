global user Minh
set more off

if "${user}"=="Minh"{
	global location "C:/Users/Minh/OneDrive/frombank"
	global outreg "$location/output/regressions"
	global outsum "$location/output/summaries"
	global outint "$location/frombank/output/interactions"
	global outfig "$location/frombank/output/figures"
	global data "$location/data"
	global subcode "$location/code/subcode"
}
else{
	if "`c(os)'" == "Unix" {
		global location "/home/rcemxp06/frombank"
		global outreg "/home/rcemxp06/frombank/output/regressions"
		global outsum "/home/rcemxp06/frombank/output/summaries"
		global outint "/home/rcemxp06/frombank/output/interactions"
		global outfig "/home/rcemxp06/frombank/output/figures"
		global data "/home/rcemxp06/frombank/data"
		global subcode "/home/rcemxp06/frombank/code/subcode"
		adopath ++ "/home/rcemxp06/ado"
	}
	else if "`c(os)'" == "Windows" {
		global location "R:/frombank"
		global outreg "R:/frombank/output/regressions"
		global outsum "R:/frombank/output/summaries"
		global outint "R:/frombank/output/interactions"
		global outfig "R:/frombank/output/figures"
		global data "R:/frombank/data"
		global subcode "R:/frombank/code/subcode"
		adopath ++ "R:/ado"
	}
}

// IMPORTS

foreach filetype in /*imp*/ exp {
	local filetype exp
	local filelist: dir "$location/data/trade/" files "`filetype'*_con.dta"
	local nfiles : word count `filelist'
	
	
	forval i=1/`nfiles' {
	  local curfile `: word `i' of `filelist''
	  use "$location/data/trade/`curfile'", clear
	  destring year, replace
	  save, replace
	}
	

	local first=1
	local curfile `: word `first' of `filelist''
	use "$location/data/trade/`curfile'", clear

	forval i=2/`nfiles' {
	  local curfile `: word `i' of `filelist''
	  append using "$location/data/trade/`curfile'", force
	}
	
	if "`filetype'"=="exp"{
		local varlabel Export
		keep hs ccode quantity value sic naics descrip_2 year
	}
	else{
		local varlabel Import
		keep hs ccode cquan gquan cvalue gvalue charge sic naics descrip_2 year
	}

	replace year = year + 1900

	// Missing naics, get them from the concordance table that Feenstra provided
	replace naics = "980000" if hs=="9801002000"
	replace naics = "990000" if hs=="9802002000"
	
	drop if naics==""
	
	// Generate different naics
	gen naics3 = substr(naics, 1, 3)
	gen naics4 = substr(naics, 1, 4)
	
	if "`filetype'"=="exp"{
		preserve
		collapse (sum) quantity value , by(naics3 year)

		ren quantity `filetype'_gquan
		ren value `filetype'_gvalue
		
		label var `filetype'_gquan "`varlabel' General Quantity"
		label var `filetype'_gvalue "`varlabel' General Value"

		save "$location/data/trade/`filetype'_agg_naics3.dta", replace
		restore
		
		preserve
		
		collapse (sum) quantity value , by(naics4 year)

		ren quantity `filetype'_gquan
		ren value `filetype'_gvalue
		
		label var `filetype'_gquan "`varlabel' General Quantity"
		label var `filetype'_gvalue "`varlabel' General Value"

		save "$location/data/trade/`filetype'_agg_naics4.dta", replace
		restore
		
	}
	else{
		// Generate 3 digit NAICS dataset
		preserve
		
		collapse (sum) cquan gquan cvalue gvalue , by(naics3 year)

		ren cquan `filetype'_cquan
		ren gquan `filetype'_gquan
		ren cvalue `filetype'_cvalue
		ren gvalue `filetype'_gvalue
		
		label var `filetype'_cquan "`varlabel' Consumption Quantity"
		label var `filetype'_gquan "`varlabel' General Quantity"
		label var `filetype'_cvalue "`varlabel' Consumption Value"
		label var `filetype'_gvalue "`varlabel' General Value"

		save "$location/data/trade/`filetype'_agg_naics3.dta", replace
		restore
		
	
		// Generate 4 digit NAICS dataset
		preserve

		collapse (sum) cquan gquan cvalue gvalue, by(naics4 year)

		ren cquan `filetype'_cquan
		ren gquan `filetype'_gquan
		ren cvalue `filetype'_cvalue
		ren gvalue `filetype'_gvalue

		label var `filetype'_cquan "`varlabel' Consumption Quantity"
		label var `filetype'_gquan "`varlabel' General Quantity"
		label var `filetype'_cvalue "`varlabel' Consumption Value"
		label var `filetype'_gvalue "`varlabel' General Value"
		
		save "$location/data/trade/`filetype'_agg_naics4.dta", replace
		restore	
	}
}


**************************************************
**************************************************
**************************************************

// GENERATE TRADABLE / NONTRADABLE

use "$location/data/national/nat_naics_sic_3.dta", clear
drop if codetype==2
gen year = year(dofq(date))
gen quarter = quarter(dofq(date))
keep if quarter==4
drop quarter
ren industry naics3

keep naics3 year nemplq
tempfile nat_empl
save `nat_empl', replace

use "$location/data/trade/imp_agg_naics3.dta", clear
merge 1:1 naics3 year using "$location/data/trade/exp_agg_naics3"
ren _merge in_imp

// Merge in employment data
merge 1:1 naics3 year using `nat_empl'
//drop if _merge!=3

// Use criteria set by Sufi and Mian
egen imp_exp_sum = rowtotal(imp_gvalue exp_gvalue)
gen imp_exp_percapita = imp_exp_sum/nempl
keep if year==1998


**************************************************
**************************************************
**************************************************
























