*This program tabulates the amount of observations we
*would drop if we begin at 1975, 1980, or 1985. The criteria for dropping
*an observation is if it is suppressed at any point in time at
*the county level. So increasing the year to begin the data
*at decreases the number of observations that would be dropped

foreach i in 2 3 {
	local i=3
	use "/san/RDS/Work/fif/b1mxp07/Matt/data/naics_sic_`i'.dta",replace
	//preserve
	drop if date<tq(1980q1) & codetype == 2 
	
	bys codetype area industry (date): egen supanyt = min(statq)
	label values supanyt status

	gen drpav = 1 if supanyt==0 & statq==0
	replace drpav = 2 if supanyt==0 & statq==1
	replace drpav = 3 if supanyt==1
	
	label define drptype 1 "Suppressed & Dropped" 2 "Not Suppressed & Dropped" 3 "Not Dropped"
	label values drpav drptype
	tab supanyt codetype
	
	gen cnt = 1
	collapse (sum) estabq emplq wageq cnt, by(codetype drpav date)
	save "/san/RDS/Work/fif/b1mxp07/Matt/data/sumstatsdrop80.dta",replace
	
		
	preserve 
		collapse (mean) supanyt, by(codetype date)
		gen dropt = (1 - supanyt)*100
		tw (line dropt date if codetype==1) ///
		(line dropt date if codetype==2), ///
		legend(label(1 "NAICS") label(2 "SIC")) ///
		title("% drop at each time period -`i' Digits 80/95") ///
		ylabel(0(10)100,)
		graph export "/san/RDS/Work/fif/b1mxp07/Matt/output/todrop`i'_8095.png", replace
	restore 
}

tab supanyt date if codetype==1 & date<tq(2000q1)
tab supanyt date if codetype==1 & date>=tq(2000q1)
tab supanyt date if codetype==2 & date<tq(1990q1)
tab supanyt date if codetype==2 & date>=tq(1990q1)

foreach i in 80 85{
	use R:\Matt\data\sumstatsdrop`i'.dta, clear
	egen typedrop = group(codetype date)
	tsset typedrop drpav
	tsfill, full

	foreach x in estabq emplq wageq cnt{
		replace `x' = 0 if `x'==. 
	}	

	gsort typedrop -drpav
	by typedrop: carryforward codetype, gen(codetype2)
	by typedrop: carryforward date, gen(date2)
	drop date codetype

	ren date2 date
	ren codetype2 codetype

	order codetype date drpav

	bysort codetype date: egen e_totavail = sum(emplq)
	bysort codetype date: egen es_totavail = sum(estabq)
	bysort codetype date: egen w_totavail = sum(wageq)
	bysort codetype date: egen cnt_totavail = sum(cnt)
	bysort codetype date: egen sup = sum(cnt) if drpav==1
	bysort codetype date: egen nsup = sum(cnt) if drpav==1 | drpav==2
	bysort codetype date: egen ndrop = sum(cnt) if drpav==1 | drpav==2 | drpav==3

	gen psup = sup/cnt_totavail*100
	gen pnsup = nsup/cnt_totavail*100
	gen pndrop = ndrop/cnt_totavail*100

	gen emplperc = emplq/e_totavail*100
	gen estabperc = estabq/es_totavail*100
	gen wageperc = wageq/w_totavail*100
	gen cntperc = cnt/cnt_totavail*100

		
		twoway (line emplperc date if codetype==1 & drpav==2) ///
		(line emplperc date if codetype==2 & drpav==2), ///
		legend(size(vsmall)) ///
		title("Not Suppressed but Dropped Employment by Quarter", size(small)) ///
		ytitle("% of Available Employment", size (small)) xtitle("Date") ///
		graphregion(color(white)) ///
		legend(rows(1) label(2 "SIC")  label(1 "NAICS")) ///
		xlabel(#16, angle(45) grid nogmin nogmax glwidth(thin) glcolor(gs14)) ///
		ylabel(0(2)16,angle(0))
		graph export "R:\Matt\output\emplq`i'.wmf", replace
		
		twoway (line estabperc date if codetype==1 & drpav==2) ///
		(line estabperc date if codetype==2 & drpav==2), ///
		legend(size(vsmall)) ///
		title("Not Suppressed but Dropped Establishments by Quarter", size(small)) ///
		ytitle("% of Available Establishments", size (small)) xtitle("Date") ///
		graphregion(color(white)) ///
		legend(rows(1) label(2 "SIC")  label(1 "NAICS")) ///
		xlabel(#16, angle(45) grid nogmin nogmax glwidth(thin) glcolor(gs14)) ///
		ylabel(0(2)16,angle(0))
		graph export "R:\Matt\output\estabq`i'.wmf", replace
		
		twoway (line wageperc date if codetype==1 & drpav==2) ///
		(line wageperc date if codetype==2 & drpav==2), ///
		legend(size(vsmall)) ///
		title("Not Suppressed but Dropped Wages by Quarter", size(small)) ///
		ytitle("% of Available Wages", size (small)) xtitle("Date") ///
		graphregion(color(white)) ///
		legend(rows(1) label(2 "SIC")  label(1 "NAICS")) ///
		xlabel(#16, angle(45) grid nogmin nogmax glwidth(thin) glcolor(gs14)) ///
		ylabel(0(2)16,angle(0))
		graph export "R:\Matt\output\wageq`i'.wmf", replace
				
		twoway (line cntperc date if codetype==1 & drpav==2) ///
		(line cntperc date if codetype==2 & drpav==2), ///
		legend(size(vsmall)) ///
		title("Not Suppressed but Dropped Observations (unweighted) by Quarter", size(small)) ///
		ytitle("% of Available Obs", size (small)) xtitle("Date") ///
		graphregion(color(white)) ///
		legend(rows(1) label(2 "SIC")  label(1 "NAICS")) ///
		xlabel(#16, angle(45) grid nogmin nogmax glwidth(thin) glcolor(gs14)) ///
		ylabel(,angle(0))
		graph export "R:\Matt\output\cntq`i'.wmf", replace
}	

	
