global user Minh
set more off

if "${user}"=="Minh"{
	global location "C:/Users/mphan21/OneDrive/frombank"
	global outreg "$location/output/regressions"
	global outsum "$location/output/summaries"
	global outint "$location/frombank/output/interactions"
	global outfig "$location/frombank/output/figures"
	global data "$location/data"
	global subcode "$location/code/subcode"
}
else{
	if "`c(os)'" == "Unix" {
		global location "/home/rcemxp06/frombank"
		global outreg "/home/rcemxp06/frombank/output/regressions"
		global outsum "/home/rcemxp06/frombank/output/summaries"
		global outint "/home/rcemxp06/frombank/output/interactions"
		global outfig "/home/rcemxp06/frombank/output/figures"
		global data "/home/rcemxp06/frombank/data"
		global subcode "/home/rcemxp06/frombank/code/subcode"
		adopath ++ "/home/rcemxp06/ado"
	}
	else if "`c(os)'" == "Windows" {
		global location "R:/frombank"
		global outreg "R:/frombank/output/regressions"
		global outsum "R:/frombank/output/summaries"
		global outint "R:/frombank/output/interactions"
		global outfig "R:/frombank/output/figures"
		global data "R:/frombank/data"
		global subcode "R:/frombank/code/subcode"
		adopath ++ "R:/ado"
	}
}



use "$data/county_level_qcew/clean_naics_sic_3.dta", clear
gen quarter = quarter(dofq(date))
keep if quarter==4 & codetype!=2
gen year = year(dofq(date))

//National employment is by industry

keep area year own industry agglvl statq estabq emplq wageq nemplq nestabq nwageq
gen cnty_empl_share = emplq/nemplq
gen cnty_empl_share_sq = cnty_empl_share^2

collapse (sum) cnty_empl_share_sq , by(year industry)
ren cnty_empl_share_sq industry_hhi

// Industry 491 is postal service and this industry has suppressed employment for every county except 1
// I'm dropping this industry because high HHI is due to the fact that we drop this industry for every other county

destring industry, replace
drop if industry==999 | industry==491

gsort year industry_hhi
egen industry_hhi_quartile = xtile(industry_hhi), by(year) nq(4)

// Are the industries in each quartile the same every year?
// More or less

preserve

keep if industry_hhi_quartile==4 | industry_hhi_quartile==1
reshape wide industry_hhi, i(industry industry_hhi_quartile) j(year)
gsort industry_hhi_quartile industry

restore

keep if year==1998 

// 4th quartile is the highest in HHI, designate as tradable

gen indcat = 1 if industry_hhi_quartile==4

// 1st quartile lowest in HHI, designate as non-tradable

replace indcat = 2 if industry_hhi_quartile==1

replace indcat = 3 if indcat==.

label define indtype 1 "Tradable" 2 "Non - Tradable" 3 "Other"
label val indcat indtype
drop year

save "$location/data/trade/trade_ind_def.dta", replace


*****************************************************
*****************************************************
*****************************************************

// CONSTRUCTING TRADABLE / NON - TRADABLE BARTIK

set more off
local i 3
use "$data/county_level_qcew/naics_sic_`i'.dta",clear
drop if date<tq(1979q1) & codetype == 2
	
merge m:1 date using "$data/misc_data/GDPDEF.dta"
assert _merge!=1
drop _merge
	
replace wageq = wageq/gdpdefl
	
bys codetype area industry (date): egen supanyt = min(statq)
label values supanyt status
	
gen drpav = 1 if supanyt==0 & statq==0
replace drpav = 2 if supanyt==0 & statq==1
replace drpav = 3 if supanyt==1
	
label define drptype 1 "Suppressed & Dropped" 2 "Not Suppressed & Dropped" 3 "Not Dropped"
label values drpav drptype
tab supanyt codetype
	
//Dropping county-quarter-industry observation that was suppressed at any time
drop if drpav!=3
drop if codetype==2

destring industry, replace

// Merge in tradable / non - tradable identifier
merge m:1 industry using "$location/data/trade/trade_ind_def.dta"
keep if indcat==1 | indcat==2
drop _merge

tostring industry, replace

local i 3
merge m:1 codetype industry date using "$data/national/nat_naics_sic_`i'.dta"
drop if _merge!=3
drop _merge
sort codetype date area industry

preserve
collapse (sum) cnty_estabq=estabq cnty_emplq=emplq cnty_wageq=wageq, by(area date indcat)
save "$data/county_level_qcew/trad_nontrad_cnty_naics_3", replace
restore

local i 3
merge m:1 area date indcat using "$data/county_level_qcew/trad_nontrad_cnty_naics_`i'.dta"
drop if _merge!=3
drop _merge

keep area date industry indcat statq estabq emplq wageq cnty_estabq cnty_emplq cnty_wageq grtnemplq grtnestabq grtnwageq

egen areaind = group(area industry)
tsset areaind date

foreach var in emplq estabq wageq{
	gen double p`var' = `var'/cnty_`var'
}

//Generating instruments
sort areaind date
foreach var in emplq estabq wageq{
	gen double ins`var' = L4.p`var'*grtn`var'
}
*

// Sum instruments 
collapse (sum) ins* , by(area date indcat)
sort area indcat date
save "$data/trade/trad_nontrad_instrument.dta", replace









