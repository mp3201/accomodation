*This program cleans the small busines loans variables and merge with the call reports/bhc
*data from class rawcore

if "`c(os)'" == "Unix" {
	global location "/san/RDS/Work/fif/b1mxp07/Matt"
	adopath ++ "/san/RDS/Work/fif/b1mxp07/my_ados"
}
else if "`c(os)'" == "Windows" {
	global location "//RB/B1/NYRESAN/RDS/Work/fif/b1mxp07/Matt"
	adopath ++ "//RB/B1/NYRESAN/RDS/Work/fif/b1mxp07/my_ados"
}
set more off

use "$location/data/regdata/sbl.dta", clear
tostring dt, replace
gen date = date(dt, "YMD")
format %td date
drop if quarter(date)!=2
drop if year(date)<1993
drop dt

gen date2 = qofd(date)
drop date
ren date2 date
format %tq date

ren ID_RSSD entity
ren REG_HH_1_ID hhrssd

foreach mnemonic in RCON5564 RCON5566 RCON5568 RCON5570 RCON5572 RCON5574 RCON5571 RCON5573 RCON5575 RCON5565 RCON5567 RCON5569{
	replace `mnemonic' = . if RCON6999==1
}

egen ln_sbl_ci_cnt = rowtotal(RCON5564 RCON5566 RCON5568)
replace ln_sbl_ci_cnt = . if (RCON5564==. & RCON5566==. & RCON5568==.) | (RCON5564==0 & RCON5566==0 & RCON5568==0)
egen ln_sbl_ci_amt = rowtotal(RCON5565 RCON5567 RCON5569)
replace ln_sbl_ci_amt = . if (RCON5565==. & RCON5567==. & RCON5569==.) | (RCON5565==0 & RCON5567==0 & RCON5569==0)
replace ln_sbl_ci_amt = RCON1763 if RCON6999==1 & RCON1763!=0 & RCON1763!=.

egen ln_sbl_cre_cnt = rowtotal(RCON5570 RCON5572 RCON5574)
replace ln_sbl_cre_cnt = . if (RCON5570==. & RCON5572==. & RCON5574==.) | (RCON5570==0 & RCON5572==0 & RCON5574==0)
egen ln_sbl_cre_amt = rowtotal(RCON5571 RCON5573 RCON5575)
replace ln_sbl_cre_amt = . if (RCON5571==. & RCON5573==. & RCON5575==.) | (RCON5571==0 & RCON5573==0 & RCON5575==0)
egen sbl_cre_temp = rowtotal(RCONF160 RCONF161)
replace sbl_cre_temp = . if RCONF160==. & RCONF161==.
replace ln_sbl_cre_amt = sbl_cre_temp if RCON6999==1 & sbl_cre_temp!=0 & sbl_cre_temp!=.

egen ln_sbl_amt = rowtotal(ln_sbl_ci_amt ln_sbl_cre_amt)
replace ln_sbl_amt = . if ln_sbl_cre_amt==. & ln_sbl_ci_amt==. 

ren RCON6999 sbl_dum

ren RCON5564 ln_sbl_cre_c1
ren RCON5566 ln_sbl_cre_c2
ren RCON5568 ln_sbl_cre_c3

ren RCON5570 ln_sbl_ci_c1
ren RCON5572 ln_sbl_ci_c2
ren RCON5574 ln_sbl_ci_c3

ren RCON5565 ln_sbl_cre_a1
ren RCON5567 ln_sbl_cre_a2
ren RCON5569 ln_sbl_cre_a3

ren RCON5571 ln_sbl_ci_a1
ren RCON5573 ln_sbl_ci_a2
ren RCON5575 ln_sbl_ci_a3

ren RCON5562 ln_cre_cnt
ren RCON5563 ln_ci_cnt

drop RCON*
order entity hhrssd date ln_*
drop if date>tq(2013q2)
save "$location/data/regdata/sbl_cleaned.dta", replace
