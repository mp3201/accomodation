*This program runs the regressions at the county level.
*The establishment/employment data from qcew to feed in is the combined 
*SIC and NAICS 3 digit with the bartik instrument created beforehand

if "`c(os)'" == "Unix" {
	global location "/san/RDS/Work/fif/b1mxp07/Matt"
	adopath ++ "/san/RDS/Work/fif/b1mxp07/my_ados"
}
else if "`c(os)'" == "Windows" {
	global location "//RB/B1/NYRESAN/RDS/Work/fif/b1mxp07/Matt"
	adopath ++ "//RB/B1/NYRESAN/RDS/Work/fif/b1mxp07/my_ados"
}
set more off

use "$location/data/census/county_population.dta", clear
drop if fipsco=="000"
drop state_fips county_fips areaname region division pop19904
reshape long pop, i(fips state_name county_name fipsst fipsco) j(year)
drop if pop==.
ren fips area
save "$location/data/census/county_population_long", replace

import excel "$location/data/misc.xlsx", firstrow sheet("fips") clear
merge 1:1 stabb using "$location/data/misc/dereg.dta"
drop if _merge!=3
drop _merge
tostring stfips, replace
replace stfips = "0" + stfips if length(stfips)!=2
tempfile dereg
save `dereg', replace

use "$location/data/naics_sic_inscnty_3.dta", clear
capture drop _merge
gen quarter = quarter(dofq(date))
gen year = year(dofq(date))
drop if quarter!=2
drop quarter date

merge m:1 area year using "$location/data/sod/sodcnty.dta"
drop if _merge!=3
drop _merge

gen stfips = substr(area, 1, 2)
gen stfipsn = stfips
destring stfipsn, replace
merge m:1 stfips using `dereg' 
assert _merge==3
drop _merge

merge m:1 area year using "$location/data/census/county_population_long"
// Some missing counties assert _merge==3
drop if _merge!=3
drop _merge

sort codetype area year
gen ysmadate = year - madate
gen ysintra = year - denovo
gen ysinter = year - inter
gen ysmbhc = year - mbhc

//Generating dummy variables that take the value 1 if it is X years after any deregulation event
forvalues i = -10(1)15 {
	if `i' < 0 {
		local temp = substr("`i'", 2,.)
		local concat n`temp'
	}
	else {
		local concat p`i'
	}
	gen ind_all_`concat' = ((ysmadate==`i') | (ysintra==`i') | (ysinter==`i') | (ysmbhc==`i'))
}

foreach var in madate intra inter mbhc{	
	forvalues i = -10(1)15 {
		if `i' < 0 {
			local temp = substr("`i'", 2,.)
			local concat n`temp'
		}
		else {
			local concat p`i'
		}
		gen ind_`var'_`concat' = (ys`var'==`i') 
	}
}

foreach var in ysmadate ysintra ysinter ysmbhc{
	gen ind`var' = (`var'>=0)
}

replace ysmadate = 0 if ysmadate<0
replace ysintra = 0 if ysintra<0
replace ysinter = 0 if ysinter<0
replace ysmbhc = 0 if ysmbhc<0

egen deregindex = rowtotal(indysmadate indysintra indysinter indysmbhc)

ren cnty_dep_local cnty_dep
ren perc_multi_cnty cnty_multi
ren rat_ex_cnty_br rat_br
ren rat_ex_cnty_dep rat_dep

replace cnty_dep = 1-cnty_dep
replace rat_br = log(rat_br+1)
replace rat_dep = log(rat_dep+1)
gen cnty_br = ln(cnty_num_br)


bys codetype year (area): egen uspop = sum(pop)
gen poprat = pop/uspop

gen smlcnty = .
levelsof year, local(years)
forvalue i = 1/3{
	foreach yr of local years{
		 summ poprat if codetype==`i' & year==`yr', d
		 capture replace smlcnty = (poprat<`r(p50)') if codetype==`i' & year==`yr'
	}
}

tab smlcnty, m
//dropping bottom half of all counties
bys codetype area (year): egen mxsmlcnty = max(smlcnty)
tab mxsmlcnty

bys codetype year (area): egen sumsmlcnty = sum(pop) if mxsmlcnty==1
gen popdropsh = sumsmlcnty/uspop*100
summ popdropsh
local avgpopdrop = `r(mean)'

//on average dropping below median county gets rid of counties totaling about 8% of the population
tabstat popdropsh,by(year) st(mean sd)
drop sumsmlcnty popdropsh

//Merge with population from counties
//If counties is below a certain cutoff at any time, exclude them from the sample all together
//Try different dates
drop if mxsmlcnty==1

forvalue i = 1/2{
	foreach grt in grtcemplq grtcestabq grtcwageq {
		 summ `grt' if codetype==`i', d
		 local p01 = `r(p1)'
		 local p99 = `r(p99)'
		 replace `grt' = . if `grt'<`p01'
		 replace `grt' = . if `grt'>`p99'
	}
}

//Creating interaction with above/below growth dummies
foreach pred in empl estab wage {
	if ("`pred'" == "empl"){
		local crhsti "Predicted Employment"
	}
	else if ("`pred'" == "estab"){
		local crhsti "Predicted Establishments"
	}
	else if ("`pred'" == "wage"){
		local crhsti "Predicted Wage"
	}

	gen ptemp = (ins`pred'q>=0 & ins`pred'q!=.)
	gen pos_`pred' = ins`pred'q*ptemp
	label var pos_`pred' "Positive `crhsti'"
	gen ntemp = (ins`pred'q<0 & ins`pred'q!=.)
	gen neg_`pred' = ins`pred'q*ntemp
	label var neg_`pred' "Negative `crhsti'"
	drop ptemp ntemp
}

/*
foreach pred in empl estab wage {
	if ("`pred'" == "empl"){
		local crhsti "Predicted Employment"
	}
	else if ("`pred'" == "estab"){
		local crhsti "Predicted Establishments"
	}
	else if ("`pred'" == "wage"){
		local crhsti "Predicted Wage"
	}
	summ ins`pred'q if codetype==1
	gen b_naics = (ins`pred'q<`r(mean)' & ins`pred'q!=.) if codetype==1
	gen a_naics = (ins`pred'q>=`r(mean)' & ins`pred'q!=.) if codetype==1
	summ ins`pred'q if codetype==2
	gen b_sic = (ins`pred'q<`r(mean)' & ins`pred'q!=.) if codetype==2
	gen a_sic = (ins`pred'q>=`r(mean)' & ins`pred'q!=.) if codetype==2
	
	gen btemp = b_naics if codetype==1
	replace btemp = b_sic if codetype==2
	
	gen atemp = a_naics if codetype==1
	replace atemp = a_sic if codetype==2
	
	gen pos_`pred' = ins`pred'q*atemp
	label var pos_`pred' "Above Avg `crhsti'"
	
	gen neg_`pred' = ins`pred'q*btemp
	label var neg_`pred' "Above Avg `crhsti'"
	
	drop atemp btemp b_naics a_naics b_sic a_sic
	
}
*/

local lhs grtcemplq grtcestabq grtcwageq
local rhs empl estab wage
local n : word count `lhs'

forvalues i = 1/`n' {
		local clhs : word `i' of `lhs'
		local crhs : word `i' of `rhs'
		foreach inst in cnty_br cnty_dep cnty_multi rat_br rat_dep {
			gen p`inst'_`crhs' = `inst'*pos_`crhs'
			gen n`inst'_`crhs' = `inst'*neg_`crhs'
			gen `inst'_`crhs' = `inst'*ins`crhs'q
			if ("`inst'" == "cnty_br") {
				local instti "# of Bank Branches"
			}
			else if ("`inst'" == "cnty_dep") {
				local instti "Local Bank Deposit Share"			
			}
			else if ("`inst'" == "cnty_multi") {
				local instti "Percent Multi State"			
			}
			else if ("`inst'" == "rat_br") {
				local instti "Log Branch Ratio"			
			}
			else if ("`inst'" == "rat_dep") {
				local instti "Log Deposit Ratio"			
			}
			
			if ("`crhs'" == "empl"){
				local crhsti "Predicted Employment"
			}
			else if ("`crhs'" == "estab"){
				local crhsti "Predicted Establishments"
			}
			else if ("`crhs'" == "wage"){
				local crhsti "Predicted Wage"
			}
			
			label var `inst'_`crhs' "`instti' X `crhsti'"
			label var p`inst'_`crhs' "`instti' X `crhsti' X Pos Pred. Growth"
			label var n`inst'_`crhs' "`instti' X `crhsti' X Neg Pred. Growth"
						
		}
		
		foreach instrument in ysmadate ysintra ysinter ysmbhc deregindex {
			if ("`instrument'" == "ysmadate") {
				local firstti "Years Since Intrastate M&A"
			}
			else if ("`instrument'" == "ysintra") {
				local firstti "Years Since Intrastate Denovo"
			}
			else if ("`instrument'" == "ysinter") {
				local firstti "Years Since Interstate Dereg"
			}
			else if ("`instrument'" == "ysmbhc") {
				local firstti "Years Since Multibank BHC"
			}
				
			gen `instrument'_`crhs' = `instrument'*ins`crhs'q
			gen p`instrument'_`crhs' = `instrument'*pos_`crhs'
			gen n`instrument'_`crhs' = `instrument'*neg_`crhs'
			label var `instrument'_`crhs' "`firstti' X `crhsti'"
			label var p`instrument'_`crhs' "`firstti' X `crhsti' X Pos Pred. Growth"
			label var n`instrument'_`crhs' "`firstti' X `crhsti' X Neg Pred. Growth"	
		}
}
*


//Label variables
label var cnty_br "# of Bank Branches"
label var cnty_dep "Local Bank Deposit Share"
label var cnty_multi "Percent Multi State"
label var rat_br "Log Branch Ratio"
label var rat_dep "Log Deposit Ratio"
label var ysmadate "Years Since Intrastate M&A"
label var ysintra "Years Since Intrastate Denovo"
label var ysinter "Year Since Inter State"
label var ysmbhc "Years Since Multibank BHC"
label var insemplq "Predicted Employment"
label var insestabq "Predicted Establishments"
label var inswageq "Predicted Wages"
label var codetype "NAICS/SIC/Combined"
label var grtcemplq "Actual Employment Growth Rate"
label var grtcestabq "Actual Establishment Growth Rate"
label var grtcwageq "Actual Wage Growth Rate"
label var cnty_num_br "Number of Branches Outside to Inside of County"
label var stfips "State FIPS Code"
label var stfipsn "State FIPS Code - Numeric"
label var stabb "State Abbreviation"
label var stnm "State Name"
label var madate "Year of IntraState M&A Deregulation"
label var denovo "Year of IntraState Denovo Deregulation"
label var inter "Year of Interstate Deregulation"
label var mbhc "Year of Multi Bank BHC Deregulation"
label var unit "Unit Banking Law Pre Instrastate Dereg Dummy"
label var pop "County Population"
label var uspop "US Population"
label var poprat "County to US Population Ratio"
label var smlcnty "Small County Dummy"
label var mxsmlcnty "Small County at Any Time"

save "$location/data/county_regression_data.dta", replace

//xi i.area
set more off
set matsize 4000

xtset typearea year
xi i.year

foreach inst in /*cnty_br cnty_dep cnty_multi rat_br*/ rat_dep {
	xtreg `inst' ind_madate_n10 ind_madate_n9 ind_madate_n8 ind_madate_n7 ind_madate_n6 ind_madate_n5 ind_madate_n4 ind_madate_n3 ind_madate_n2 ind_madate_n1 ind_madate_p0 ind_madate_p1 ind_madate_p2 ind_madate_p3 ind_madate_p4 ind_madate_p5 ind_madate_p6 ind_madate_p7 ind_madate_p8 ind_madate_p9 ind_madate_p10 ind_madate_p11 ind_madate_p12 ind_madate_p13 ind_madate_p14 ind_madate_p15 _I* if codetype==3 & mxsmlcnty==0, fe cluster(stfipsn)
	xtreg `inst' ind_intra_n10 ind_intra_n9 ind_intra_n8 ind_intra_n7 ind_intra_n6 ind_intra_n5 ind_intra_n4 ind_intra_n3 ind_intra_n2 ind_intra_n1 ind_intra_p1 ind_intra_p2 ind_intra_p3 ind_intra_p4 ind_intra_p5 ind_intra_p6 ind_intra_p7 ind_intra_p8 ind_intra_p9 ind_intra_p10 ind_intra_p11 ind_intra_p12 ind_intra_p13 ind_intra_p14 ind_intra_p15 _I* if codetype==3 & mxsmlcnty==0, fe cluster(stfipsn)
	xtreg `inst' ind_inter_n10 ind_inter_n9 ind_inter_n8 ind_inter_n7 ind_inter_n6 ind_inter_n5 ind_inter_n4 ind_inter_n3 ind_inter_n2 ind_inter_n1 ind_inter_p1 ind_inter_p2 ind_inter_p3 ind_inter_p4 ind_inter_p5 ind_inter_p6 ind_inter_p7 ind_inter_p8 ind_inter_p9 ind_inter_p10 ind_inter_p11 ind_inter_p12 ind_inter_p13 ind_inter_p14 ind_inter_p15 _I* if codetype==3 & mxsmlcnty==0, fe cluster(stfipsn)
	xtreg `inst' ind_mbhc_n10 ind_mbhc_n9 ind_mbhc_n8 ind_mbhc_n7 ind_mbhc_n6 ind_mbhc_n5 ind_mbhc_n4 ind_mbhc_n3 ind_mbhc_n2 ind_mbhc_n1 ind_mbhc_p1 ind_mbhc_p2 ind_mbhc_p3 ind_mbhc_p4 ind_mbhc_p5 ind_mbhc_p6 ind_mbhc_p7 ind_mbhc_p8 ind_mbhc_p9 ind_mbhc_p10 ind_mbhc_p11 ind_mbhc_p12 ind_mbhc_p13 ind_mbhc_p14 ind_mbhc_p15 _I* if codetype==3 & mxsmlcnty==0, fe cluster(stfipsn)	
}
s
set more off
local lhs grtcemplq grtcestabq grtcwageq
local rhs empl estab wage
local n : word count `lhs'

//Full Time Period
forvalues types = 3/3 {
	if (`types'==1) local indtype "NAICS"
	else if (`types'==2) local indtype "SIC"
	else if (`types'==3) local indtype "Combined"
	capture erase "$location/output/`indtype'_xt_index.xlm"
	capture erase "$location/output/`indtype'_xt_index.txt"

	forvalues i = 1/`n' {
		local clhs : word `i' of `lhs'
		local crhs : word `i' of `rhs'
		foreach inst in cnty_br cnty_dep cnty_multi rat_br rat_dep {

			xtivreg2 `clhs' ins`crhs'q  (`inst' `inst'_`crhs' = deregindex deregindex_`crhs' /*ysmadate ysintra ysinter ysmbhc ysmadate_`crhs' ysintra_`crhs' ysinter_`crhs' ysmbhc_`crhs'*/ ) _I* if codetype==`types' & mxsmlcnty==0, savefirst ffirst cluster(stfipsn) fe
			/*
			matrix first = e(first)
			local apfmain = first[7,1]
			local apfint = first[7,2]
			est store _xtivreg2_`clhs'
			est restore _xtivreg2_`inst'
			outreg2 using "$location/output/`indtype'_xt_index", cttop("`instti' FIRST STAGE") addtext(County FE, YES, Clustered SE, STATE) excel adjr2 label append drop(_I*)
			est restore _xtivreg2_`inst'_`crhs'
			outreg2 using "$location/output/`indtype'_xt_index", cttop("`instti'X`crhsti' FIRST STAGE") addtext(County FE, YES, Clustered SE, STATE) excel adjr2 label append drop(_I*)
			est restore _xtivreg2_`clhs'
			outreg2 using "$location/output/`indtype'_xt_index", cttop("SECOND STAGE") excel adjr2 label append drop(_I*) addtext(County FE, YES, Clustered SE, STATE, Hansen J Stat, `e(j)', Hansen J PValue, `e(jp)', First Stage F-Statistics, `e(widstat)', First Stage Main, `apfmain', First Stage Int., `apfint', Avg % of Population Dropped, `avgpopdrop' ) ///
			sortvar(cnty_br_empl cnty_dep_empl cnty_multi_empl rat_br_empl rat_dep_empl ///
			cnty_br_estab cnty_dep_estab cnty_multi_estab rat_br_estab rat_dep_estab  ///
			cnty_br_wage cnty_dep_wage cnty_multi_wage rat_br_wage rat_dep_wage ///
			cnty_br cnty_dep cnty_multi rat_br rat_dep ///
			deregindex_empl deregindex_estab deregindex_wage deregindex /*ysmadate_empl ysintra_empl ysinter_empl ysmbhc_empl ysmadate_estab ysintra_estab ysinter_estab ysmbhc_estab  ysmadate_wage ysintra_wage ysinter_wage ysmbhc_wage */ insemplq insestabq inswageq /*ysmadate ysintra ysinter ysmbhc*/)
	
			xtivreg2 `clhs' ins`crhs'q `inst' `inst'_`crhs' _I* if codetype==`types' & mxsmlcnty==0, cluster(stfipsn) fe
			outreg2 using "$location/output/`indtype'_xt_index", cttop("OLS") excel adjr2 label append drop(_I*) addtext(County FE, YES, Clustered SE, STATE)
			*/
			
			outreg2 using "$location/output/`indtype'_xt_index", cttop("SECOND STAGE") excel adjr2 label append drop(_I*) addtext(County FE, YES, Clustered SE, STATE, /*Hansen J Stat, `e(j)', Hansen J PValue, `e(jp)',*/ First Stage F-Statistics, `e(widstat)' /*, First Stage Main, `apfmain', First Stage Int., `apfint', Avg % of Population Dropped, `avgpopdrop'*/ ) ///
			sortvar(cnty_br_empl cnty_dep_empl cnty_multi_empl rat_br_empl rat_dep_empl ///
			cnty_br_estab cnty_dep_estab cnty_multi_estab rat_br_estab rat_dep_estab  ///
			cnty_br_wage cnty_dep_wage cnty_multi_wage rat_br_wage rat_dep_wage ///
			cnty_br cnty_dep cnty_multi rat_br rat_dep ///
			deregindex_empl deregindex_estab deregindex_wage deregindex /*ysmadate_empl ysintra_empl ysinter_empl ysmbhc_empl ysmadate_estab ysintra_estab ysinter_estab ysmbhc_estab  ysmadate_wage ysintra_wage ysinter_wage ysmbhc_wage */ insemplq insestabq inswageq /*ysmadate ysintra ysinter ysmbhc*/)
	
			xtivreg2 `clhs' ins`crhs'q `inst' `inst'_`crhs' _I* if codetype==`types' & mxsmlcnty==0, cluster(stfipsn) fe
			outreg2 using "$location/output/`indtype'_xt_index", cttop("OLS") excel adjr2 label append drop(_I*) addtext(County FE, YES, Clustered SE, STATE)
		}
	}
}
s
/*
//1990-2000
forvalues types = 3 {
	if (`types'==1) local indtype "NAICS"
	else if (`types'==2) local indtype "SIC"
	else if (`types'==3) local indtype "Combined"
	capture erase "$location/output/`indtype'_xt_time.xlm"
	capture erase "$location/output/`indtype'_xt_time.txt"
	forvalues i = 1/`n' {
		local clhs : word `i' of `lhs'
		local crhs : word `i' of `rhs'
		foreach inst in cnty_br cnty_dep cnty_multi rat_br rat_dep {
							
			xtivreg2 `clhs' ins`crhs'q  (`inst' `inst'_`crhs' = ysmadate ysintra ysinter /*ysmbhc*/ ysmadate_`crhs' ysintra_`crhs' ysinter_`crhs' /*ysmbhc_`crhs'*/ ) _I* if codetype==`types' & mxsmlcnty==0 & year>=1990 & year<=2000, savefirst ffirst cluster(stfipsn) fe
			matrix first = e(first)
			local apfmain = first[7,1]
			local apfint = first[7,2]
			est store _xtivreg2_`clhs'
			est restore _xtivreg2_`inst'
			outreg2 using "$location/output/`indtype'_xt_time", cttop("`instti' FIRST STAGE") addtext(County FE, NO, Clustered SE, STATE) excel adjr2 label append drop(_I*)
			est restore _xtivreg2_`inst'_`crhs'
			outreg2 using "$location/output/`indtype'_xt_time", cttop("`instti'X`crhsti' FIRST STAGE") addtext(County FE, NO, Clustered SE, STATE) excel adjr2 label append drop(_I*)
			est restore _xtivreg2_`clhs'
			outreg2 using "$location/output/`indtype'_xt_time", cttop("SECOND STAGE") excel adjr2 label append drop(_I*) addtext(County FE, NO, Clustered SE, STATE, Hansen J Stat, `e(j)', Hansen J PValue, `e(jp)', First Stage F-Statistics, `e(widstat)',  First Stage Main, `apfmain', First Stage Int., `apfint', Avg % of Population Dropped, `avgpopdrop' ) ///
			sortvar(cnty_br_empl cnty_dep_empl cnty_multi_empl rat_br_empl rat_dep_empl ///
			cnty_br_estab cnty_dep_estab cnty_multi_estab rat_br_estab rat_dep_estab  ///
			cnty_br_wage cnty_dep_wage cnty_multi_wage rat_br_wage rat_dep_wage ///
			cnty_br cnty_dep cnty_multi rat_br rat_dep ///
			ysmadate_empl ysintra_empl ysinter_empl ysmbhc_empl ysmadate_estab ysintra_estab ysinter_estab ysmbhc_estab  ysmadate_wage ysintra_wage ysinter_wage ysmbhc_wage insemplq insestabq inswageq ysmadate ysintra ysinter ysmbhc)
		
			xtivreg2 `clhs' ins`crhs'q `inst' `inst'_`crhs' _I* if codetype==`types' & mxsmlcnty==0 & year>=1990 & year<=2000, cluster(stfipsn) fe 
			outreg2 using "$location/output/`indtype'_xt_time", cttop("OLS") excel adjr2 label append drop(_I*) addtext(County FE, NO, Clustered SE, STATE)
			
			
		}
	}
}
*/


//Heterogeneous - Full Time Period
forvalues types = 3/3 {
	if (`types'==1) local indtype "NAICS"
	else if (`types'==2) local indtype "SIC"
	else if (`types'==3) local indtype "Combined"
	capture erase "$location/output/`indtype'_xt_het.xlm"
	capture erase "$location/output/`indtype'_xt_het.txt"
	forvalues i = 1/`n' {
		local clhs : word `i' of `lhs'
		local crhs : word `i' of `rhs'
		foreach inst in cnty_br cnty_dep cnty_multi rat_br rat_dep {			
			xtivreg2 `clhs' pos_`crhs' neg_`crhs' (`inst' p`inst'_`crhs' n`inst'_`crhs' = ysmadate ysintra ysinter /*ysmbhc*/ pysmadate_`crhs' pysintra_`crhs' pysinter_`crhs' /*pysmbhc_`crhs'*/ nysmadate_`crhs' nysintra_`crhs' nysinter_`crhs' /*nysmbhc_`crhs'*/ ) _I* if codetype==`types' & mxsmlcnty==0, savefirst ffirst cluster(stfipsn) fe
			matrix first = e(first)
			local apfmain = first[7,1]
			local apfneg = first[7,2]
			local apfpos = first[7,3]
			est store _xtivreg2_`clhs'
			est restore _xtivreg2_`inst'
			outreg2 using "$location/output/`indtype'_xt_het", cttop("`instti' FIRST STAGE") addtext(County FE, NO, Clustered SE, STATE) excel adjr2 label append drop(_I*)
			est restore _xtivreg2_p`inst'_`crhs'
			outreg2 using "$location/output/`indtype'_xt_het", cttop("`instti'X`crhsti'XPos Pred Growth FIRST STAGE") addtext(County FE, NO, Clustered SE, STATE) excel adjr2 label append drop(_I*)
			est restore _xtivreg2_n`inst'_`crhs'
			outreg2 using "$location/output/`indtype'_xt_het", cttop("`instti'X`crhsti'XNeg Pred Growth FIRST STAGE") addtext(County FE, NO, Clustered SE, STATE) excel adjr2 label append drop(_I*)
			est restore _xtivreg2_`clhs'
			outreg2 using "$location/output/`indtype'_xt_het", cttop("SECOND STAGE") excel adjr2 label append drop(_I*) addtext(County FE, NO, Clustered SE, STATE, Hansen J Stat, `e(j)', Hansen J PValue, `e(jp)', First Stage F-Statistics, `e(widstat)', First Stage Main, `apfmain', First Stage Positive, `apfpos', First Stage Negative, `apfneg', Avg % of Population Dropped, `avgpopdrop' ) ///
			sortvar(pcnty_br_empl ncnty_br_empl pcnty_dep_empl ncnty_dep_empl pcnty_multi_empl ncnty_multi_empl prat_br_empl nrat_br_empl prat_dep_empl nrat_dep_empl ///
			pcnty_br_estab ncnty_br_estab pcnty_dep_estab ncnty_dep_estab pcnty_multi_estab ncnty_multi_estab prat_br_estab nrat_br_estab prat_dep_estab nrat_dep_estab  ///
			cnty_br_wage cnty_dep_wage cnty_multi_wage rat_br_wage rat_dep_wage ///
			cnty_br cnty_dep cnty_multi rat_br rat_dep ///
			pys* nys* insemplq insestabq inswageq ysmadate ysintra ysinter ysmbhc )
	
			xtivreg2 `clhs' pos_`crhs' neg_`crhs' `inst' p`inst'_`crhs' n`inst'_`crhs' _I* if codetype==`types' & mxsmlcnty==0, cluster(stfipsn) fe 
			outreg2 using "$location/output/`indtype'_xt_het", cttop("OLS") excel adjr2 label append drop(_I*) addtext(County FE, NO, Clustered SE, STATE)
			
		}
	}
}

/*
//Heterogeneous - 1990-2000 Time Period
forvalues types = 3 {
	if (`types'==1) local indtype "NAICS"
	else if (`types'==2) local indtype "SIC"
	else if (`types'==3) local indtype "Combined"
	capture erase "$location/output/`indtype'_xt_het_time.xlm"
	capture erase "$location/output/`indtype'_xt_het_time.txt"
	forvalues i = 1/`n' {
		local clhs : word `i' of `lhs'
		local crhs : word `i' of `rhs'
		foreach inst in cnty_br cnty_dep cnty_multi rat_br rat_dep {
		
			xtivreg2 `clhs' pos_`crhs' neg_`crhs' (`inst' p`inst'_`crhs' n`inst'_`crhs' = ysmadate ysintra ysinter /*ysmbhc*/ pysmadate_`crhs' pysintra_`crhs' pysinter_`crhs' /*pysmbhc_`crhs'*/ nysmadate_`crhs' nysintra_`crhs' nysinter_`crhs' /*nysmbhc_`crhs'*/ ) _I* if codetype==`types' & mxsmlcnty==0 & year>=1990 & year<=2000 , savefirst ffirst cluster(stfipsn) fe
			matrix first = e(first)
			local apfmain = first[7,1]
			local apfneg = first[7,2]
			local apfpos = first[7,3]
			est store _xtivreg2_`clhs'
			est restore _xtivreg2_`inst'
			outreg2 using "$location/output/`indtype'_xt_het_time", cttop("`instti' FIRST STAGE") addtext(County FE, NO, Clustered SE, STATE) excel adjr2 label append drop(_I*)
			est restore _xtivreg2_p`inst'_`crhs'
			outreg2 using "$location/output/`indtype'_xt_het_time", cttop("`instti'X`crhsti'XPos Pred Growth FIRST STAGE") addtext(County FE, NO, Clustered SE, STATE) excel adjr2 label append drop(_I*)
			est restore _xtivreg2_n`inst'_`crhs'
			outreg2 using "$location/output/`indtype'_xt_het_time", cttop("`instti'X`crhsti'XNeg Pred Growth FIRST STAGE") addtext(County FE, NO, Clustered SE, STATE) excel adjr2 label append drop(_I*)
			est restore _xtivreg2_`clhs'
			outreg2 using "$location/output/`indtype'_xt_het_time", cttop("SECOND STAGE") excel adjr2 label append drop(_I*) addtext(County FE, NO, Clustered SE, STATE, Hansen J Stat, `e(j)', Hansen J PValue, `e(jp)', First Stage F-Statistics, `e(widstat)', First Stage Main, `apfmain', First Stage Positive, `apfpos', First Stage Negative, `apfneg', Avg % of Population Dropped, `avgpopdrop' ) ///
			sortvar(pcnty_br_empl ncnty_br_empl pcnty_dep_empl ncnty_dep_empl pcnty_multi_empl ncnty_multi_empl prat_br_empl nrat_br_empl prat_dep_empl nrat_dep_empl ///
			pcnty_br_estab ncnty_br_estab pcnty_dep_estab ncnty_dep_estab pcnty_multi_estab ncnty_multi_estab prat_br_estab nrat_br_estab prat_dep_estab nrat_dep_estab  ///
			cnty_br_wage cnty_dep_wage cnty_multi_wage rat_br_wage rat_dep_wage ///
			cnty_br cnty_dep cnty_multi rat_br rat_dep ///
			pys* nys* insemplq insestabq inswageq ysmadate ysintra ysinter ysmbhc)
	
			xtivreg2 `clhs' pos_`crhs' neg_`crhs' `inst' p`inst'_`crhs' n`inst'_`crhs' _I* if codetype==`types' & mxsmlcnty==0 & year>=1990 & year<=2000, cluster(stfipsn) fe 
			outreg2 using "$location/output/`indtype'_xt_het_time", cttop("OLS") excel adjr2 label append drop(_I*) addtext(County FE, NO, Clustered SE, STATE)
			
		}
	}
}
*/

/*
translator set smcl2pdf fontsize 9
translate "R:\Matt\output\ivreg.smcl" "R:\Matt\output\ivreg.pdf", replace t(smcl2pdf)
*/

