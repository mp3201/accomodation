

if "`c(os)'" == "Unix" {
	global location "/home/rcemxp06/accomodation"
	global outreg "/home/rcemxp06/accomodation/output/regressions"
	global outsum "/home/rcemxp06/accomodation/output/summaries"
	global outint "/home/rcemxp06/accomodation/output/interactions"
	global outfig "/home/rcemxp06/accomodation/output/figures"
	global outind "/home/rcemxp06/accomodation/output/industries"
	global data "/home/rcemxp06/accomodation/data"
	global subcode "/home/rcemxp06/accomodation/code/subcode"
	adopath ++ "/home/rcemxp06/ado"
}
else if "`c(os)'" == "Windows" {
	global location "R:/accomodation"
	global outreg "R:/accomodation/output/regressions"
	global outsum "R:/accomodation/output/summaries"
	global outint "R:/accomodation/output/interactions"
	global outfig "R:/accomodation/output/figures"
	global outind "R:/accomodation/output/industries"
	global data "R:/accomodation/data"
	global subcode "R:/accomodation/code/subcode"
	adopath ++ "R:/ado"
}

set more off

/**********************************************/
/*** Output                        ************/
/**********************************************/
cd $data
use panel_temp, clear 

*Sample conditions
global samp w1 
*g -- all, w1 -- 2% trim 2 5% trim
global popmin 33 
global popcond pctpop85>$popmin & pctemp85>$popmin & numbr85>0 
global ycond1 year>1985 & year<2006 & $popcond
global ycond2 year>1990 & year<2006 & $popcond
global ycond3 year>1990 & year<=2011 & $popcond
global ycond4 year>1985 & year<=2011 & $popcond
global type 3 

cd $outfig
set scheme lean1


global endoglist rdp lad lap dwl dwu dwld dwla
*global endoglist rbr ltbr lewa lad
*global endoglist rbr rdp ltbr lewa ldwa dwl 
global se_opt cluster(stfipsn) robust /*partial(yr_*)*/
global se_optfe cluster(stfipsn) robust ivar(pid) tvar(year) fe

cd $subcode
*include 1_regress.do

cd $subcode
*include 2_predictor.do

cd $subcode
include 3_asymmetry.do

cd $subcode
include 4_summary.do

cd $subcode
*include 5_maps.do

/**************************************************************/
// Industry level output
cd $data
use panel_temp_ind, clear

// Set akternative globals
global samp w2i 
global se_opt cluster(stfipsn) robust /*partial(yr_*)*/
global se_optfe cluster(stfipsn) robust absorb(pid_yr)

cd $subcode
include 6_industry.do

exit

stop

/*
Full time period -- 1985-2005

generate for one time period to start: 1985-2005 
and with and without county fixed effects
for two instruments
log_sumdates and deregindex
regressions without an instrument, regression with reduced form instrument, regressions instrumenting for size (something else) + 3 more with fixed effects
repeat with focus on asymmetries

consider below average asymmetry versus within year asymmetry

check robustness (repeat both) for following time periods: 1990-2005, 1990-2010

*/

/*
gen pub=pew_unit_bank


foreach var in wage empl {
areg pred_`var' if codetype==3 & year>1985 & year<=2011, absorb(area)
predict temp if e(sample)
gen sub  = (pred_`var'<temp & pred_`var'!=.)
gen sub_`var' = pred_`var'*sub
drop sub temp
}

foreach var in lad lsd drg ltbr lrbr lrdp {

foreach var in lap  {
foreach var2 in wage empl {
gen pred_`var2'_`var'=pred_`var2'*`var'
gen sub_`var2'_`var'=sub_`var2'*`var'
}
}




/* INSTURMENT VALIDITY
local var lad
reg `var' yr_* if codetype==1 & $ycond3
predict r_`var' if e(sample), r

graph box r_dwld if codetype==1 & $ycond3, over(ysintra)
graph box r_dwld if codetype==1 & $ycond3, over(ysmadate)
graph box r_dwld if codetype==1 & $ycond3, over(ysinter)
graph box r_dwld if codetype==1 & $ycond3, over(ysmbhc)
graph box r_dwld if codetype==1 & $ycond3, over(sd)

reg r_dwld sd rindex
areg lad c.lsd##rindex yr_* if codetype==1 & $ycond3, cluster(stfipsn) absorb(pid)

cd $outfig
foreach var in ysmadate ysinter ysintra ysmbhc {
preserve
collapse (mean) r_dwld (sd) r_dwld=sd, by(`var')


