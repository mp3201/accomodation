
/*Submitting through the SAS grid*/;
%include '/data/sasbench/frbny_signon_user.sas';
rsubmit  task1;


* CHECK HERE: the incall and outcall should be referencing the proper folders;
libname cin '/san/RDS/Derived/reg_data/data_c/call';
libname cout '/san/RDS/Derived/reg_data/data_c/call';
libname ncin '/san/RDS/Derived/reg_data_nc/call';
libname ncout '/san/RDS/Derived/reg_data_nc/call';



*options macrogen symbolgen;
%macro lowcase(dsn,ctype);
     %let dsid=%sysfunc(open(&dsn));
     %let num=%sysfunc(attrn(&dsid,nvars));
     %put &num;
     data &ctype.out.&dsn._&ctype;
           set &dsn(rename=(
        %do i = 1 %to &num;
        %let var&i=%sysfunc(varname(&dsid,&i));      /*function of varname returns the name of a SAS data set variable*/
        &&var&i=%sysfunc(lowcase(&&var&i))         /*rename all variables*/
        %end;));
        %let close=%sysfunc(close(&dsid));
  run;
    %mend lowcase;

%macro conftype(ctype);
    * CHECK HERE: the data step should reference the proper confidnetiailty statement in the incall and outcall;
    data call(rename=(pushdown=riad9106 ID_RSSD=entity dt=date nm_short=name));
    *data call_nc(rename=(pushdown=riad9106 ID_RSSD=entity dt=date nm_short=name));
        set &ctype.in.cuv_rcri_&ctype.;
        * CHECK HERE: any dates greater than the desired time frame should be deleted;
        if DT>20140630 then delete;

            /*********************************/;
            /* Schedule RI - Income Statment */;
            /*********************************/;
			/*NOTE: Many banks appear to only report income variables in q2 and q4 before 1983*/;

            /* Net Income */;
            /* Date range: 1969q4 to present */;
            ytdnetinc = .;
            ytdnetinc = riad4340;
            label ytdnetinc='Net Income';

                        /* Total Operating Income*/;
            /* Date range: 1969q4 to present */;
            ytdoperating_inc_tot = .;
            ytdoperating_inc_tot = riad4000;
            label ytdoperating_inc_tot='Total Operating Income';

			/* Total Operating Expense*/;
            /* Date range: 1969q4 to present */;
            ytdoperating_exp_tot = .;
            ytdoperating_exp_tot = riad4130;
            label ytdoperating_exp_tot='Total Operating Expense';

			/***********************/;
            /* 1. Interest income: */;
            /***********************/;
            /* Interest Income */;
            /* Date range: 1984q1 to present */;
            ytdint_inc = .;
            ytdint_inc = riad4107;
            label ytdint_inc='Interest Income';

                /* Interest Income From Trading Assets */
         /* Date range: 1984q1 to present */
                ytdint_trad_inc = .;
                ytdint_trad_inc = riad4069;
                label ytdint_trad_inc='Interest Income From Trading Assets';


            /* Interest & Fee Income on Loans in Domestic Offices */;
            /* Date Range: 1984q1 to present */;
            if dt < 19840331 then
            ytdint_inc_lndom = . ;
            else if dt>=19840331 & dt<=19861231 then
                    ytdint_inc_lndom = sum(riad4011, riad4019, riad4024, riad4012, riad4026, riad4054, riad4055, riad4056, riad4058, riad4057, riad4065);
            else if dt>=19870331 & dt<=20001231 then
                    ytdint_inc_lndom = sum(riad4011, riad4019, riad4024, riad4012, riad4026, riad4054, riad4055, riad4056, riad4058, riad4504, riad4503, riad4505, riad4307);
            else if dt>=20010331 & dt<20071231 then
                    ytdint_inc_lndom = sum(riad4011, riad4024, riad4012, riadb485, riadb486, riad4056, riadb487, riad4065);
            else ytdint_inc_lndom = sum(riad4435, riad4436, riad4024, riad4012, riadb485, riadb486, riad4056, riadb487, riad4065);
           	label ytdint_inc_lndom = 'Interest & Fee Income on Loans in Domestic Offices';

            /* Interest & Fee Income on Loans in Foreign Offices */;
	    /* Date Range: 1984q1 to present */;
            if dt < 19840331 then
                ytdint_inc_lnfor = . ;
            else ytdint_inc_lnfor = riad4059;
            label ytdint_inc_lnfor = 'Interest & Fee Income on Loans in Foreign Offices';

            /* Total Interest Income on Loans */;
            /* Date Range: 1969q4 to present */;
            if dt <19691231 then
                ytdint_inc_ln = . ;
            else ytdint_inc_ln = riad4010;
            label ytdint_inc_ln = 'Interest Income on Loans (Total)';


            /* Interest Income on Securities (Total) + Interest Income from Depository Institutions */;
            /* Date Range: 1984q1 to present */;
            if dt<19840331 then
                ytdint_inc_sec = . ;
            else if dt>=19840331 & dt<=19881231 then
                ytdint_inc_sec = sum(riad4027, riad4066, riad4068, riad4105, riad4106);
            else if dt>=19890331 & dt<=20001231 then
                ytdint_inc_sec = sum(riad4027, riad3657, riad4506, riad4507, riad3658, riad3659, riad4105, riad4106);
            else ytdint_inc_sec = sum(riadb488, riadb489, riad4060, riad4115);
            label ytdint_inc_sec = 'Interest Income on Securities (Total) + Interest Income from Depository Institutions';

			/* Interest Income on IBB only */;
            /* Date Range: 1984q1 to present */;
            if dt<19840331 then
                ytdint_inc_ibb = . ;
            else if dt>=19840331 & dt<=20001231 then
                ytdint_inc_ibb = sum(riad4105, riad4106);
            else ytdint_inc_ibb = sum(riad4115);
            label ytdint_inc_ibb = 'Interest Income on IBB only';

				/*Interest Income on IBB only*/;
				if dt<19840331 then
					ytdint_inc_ibb_bhc = . ;
				else if dt>=19840331 & dt<=20001231 then
					ytdint_inc_ibb_bhc = sum(riad4105, riad4106);
				else ytdint_inc_ibb_bhc = sum(riad4115);
				label ytdint_inc_ibb_bhc='Interest Income on Balances: BHCs Only';


				/*Interest Income on U.S. Treasury and agency securities BHCs Only*/;
				if dt <20010331 then
					ytdint_inc_sec_ust_bhc = .;
				else ytdint_inc_sec_ust_bhc = riadb488;
				label ytdint_inc_sec_ust_bhc = 'Interest Income on U.S. Treasury and Agency Securities: BHCs Only';

				/*Interest Income on MBS BHCs Only*/;
				if dt<20010331 then
					ytdint_inc_sec_mbs_bhc =.;
				else ytdint_inc_sec_mbs_bhc = riadb489;
				label ytdint_inc_sec_mbs_bhc = 'Interest Income on MBS: BHCs Only';

				/*Interest Income on MBS*/;
				if dt<20010331 then
					ytdint_inc_sec_mbs =.;
				else ytdint_inc_sec_mbs = riadb489;
				label ytdint_inc_sec_mbs = 'Interest Income on MBS';

				/*Interest Income on All Other Securities*/;
				if dt<20010331 then
					ytdint_inc_sec_oth_bhc =.;
				else ytdint_inc_sec_oth_bhc = riad4060;
				label ytdint_inc_sec_oth_bhc = 'Interest Income on All Other Securities: BHCs Only';

				/*Interest Income on Securities Less Interest Income on MBS*/;
  				if dt < 19860630 then
                     ytdint_inc_sec_nombs = .;
                 else ytdint_inc_sec_nombs = sum(ytdint_inc_sec, -ytdint_inc_sec_mbs);
                 label ytdint_inc_sec_nombs = 'Interest Income on Securities, No MBS';


			/****************************************/;


	    /* Interest Income From Trading Assets */;
	    /* Date Range: 1984q1 to present */;
            if dt<19840331 then
                ytdint_inc_trad = . ;
            else ytdint_inc_trad = riad4069;
            label ytdint_inc_trad = 'Interest Income From Trading Assets' ;

	    /* Interest Income on Federal Funds and Repos */;
            /* Date Range: 1984q1 to present */;
            if dt<19840331 then
                ytdint_inc_ffrepo = . ;
            else ytdint_inc_ffrepo = riad4020;
            label ytdint_inc_ffrepo = 'Interest Income on Federal Funds and Repos' ;

            /* All Other Interest Income */;
            /* Date Range: 1984q1 to present */;
            if dt<19840331 then
                ytdint_inc_alloth = . ;
            else ytdint_inc_alloth = riad4518;
            label ytdint_inc_alloth = 'All Other Interest Income' ;

            /***********************/;
            /* 2. Interest Expense */;
            /***********************/;

            /* Total Interest Expense */;
            /* Date range: 1984q1 to present */;
            ytdint_exp = .;
            ytdint_exp = riad4073;
            label ytdint_exp='Interest Expense';

            /* Interest Expense on Domestic Deposts */;
	    /* Date Range: 1984q1 to present */;
            if dt<19840331 then
                ytdint_exp_dom = . ;
            else if dt>=19840331 & dt<=19861231 then
                ytdint_exp_dom = sum(riad4174, riad4176);
            else if dt>=19870331 & dt<=19961231 then
                ytdint_exp_dom = sum(riad4174, riad4515, riad4511, riad4509, riad4508);
            else if dt>=19970331 & dt<=20001231 then
                ytdint_exp_dom = sum(riada517, riada518, riad4511, riad4509, riad4508);
            else ytdint_exp_dom = sum(riada517, riada518, riad0093, riad4508);
            label ytdint_exp_dom = 'Interest Expense on Domestic Deposts' ;

	    /* Interest Expense on Foreign Deposts */;
	    /* Date Range: 1984q1 to present */;
            if dt<19840331 then
                ytdint_exp_for = . ;
            else ytdint_exp_for = riad4172;
            label ytdint_exp_for = 'Interest Expense on Foreign Deposts' ;

	    /* Interest Expense on Federal Funds Purchased and Securities Sold Under Agreements to Repurchase */;
	    /* Date Range: 1984q1 to present */;
            if dt<19840331 then
                ytdint_exp_ffrepo= . ;
            else ytdint_exp_ffrepo= riad4180;
            label ytdint_exp_ffrepo= 'Interest Expense on Federal Purchased and Securities Sold Under Agreements to Repurchase' ;

            /* Interest on Trading Liabilities and Other Borrowed Money	*/;
            /* Date Range: 1984q1 to present */;
            if dt<19840331 then
                ytdint_exp_trad_othbor = . ;
            else ytdint_exp_trad_othbor = riad4185;
            label ytdint_exp_trad_othbor = 'Interest on Trading Liabilities and Other Borrowed Money' ;

	   /* Interest on Subordinated Notes and Debentures */;
	   /* Date Range: 1984q1 to present */;
           if dt<19840331 then
               ytdint_exp_subdebt = . ;
           else ytdint_exp_subdebt = riad4200;
           label ytdint_exp_subdebt = 'Interest on Subordinated Notes and Debentures' ;

           /* 3. Net Interest Income */;
           /* Date range: 1984q1 to present */;
           ytdint_inc_net = .;
           ytdint_inc_net = riad4074;
           label ytdint_inc_net='Net Interest Income';

           /* 4. Provision for Loan & Lease Losses (called Credit Provisions prior to 2001q1) */;
           /* Date range: 1969q4 to present */;
           ytdllprov = .;
           ytdllprov = riad4230;
           label ytdllprov='Provision for Loan & Lease Losses';

           /**************************/;
           /* 5. Noninterest income: */;
           /**************************/;

           /* Total Noninterest Income */;
           /* Date range:1984q1 to present */;
           ytdnonint_inc = .;
           ytdnonint_inc = riad4079;
           label ytdnonint_inc='Total Noninterest Income';

           /* Total Noninterest Income Excluding OREO since it started being reported in 1991q1*/;
           /* Date range:1984q1 to present */;
           ytdnonint_inc_no_oreo = .;
           ytdnonint_inc_no_oreo = sum(riad4079,-riad5415);
           label ytdnonint_inc_no_oreo='Total Noninterest Income, Excluding OREO since 1991q1';

			/*Net gains (losses) on sales of other real estate owned (oreo) as its own variable*/;
			if dt < 19910331 then
				ytdnonint_inc_oreo = . ;
			else ytdnonint_inc_oreo = riad5415;
			label ytdnonint_inc_oreo = 'Noninterest Income from OREO' ;

           /* Income from Fiduciary Activities */;
           /* Date range: 1969q4 to present */;
           ytdfiduc_inc = .;
           ytdfiduc_inc = riad4070;
           label ytdfiduc_inc='Income from Fiduciary Activities';

           /* Service Charges on Deposits	*/;
	   /* Date Range: 1984q1 to Present		*/;
           if dt<19840331 then
               ytdnonint_inc_srv_chrg_dep = . ;
           else ytdnonint_inc_srv_chrg_dep = riad4080;
           label ytdnonint_inc_srv_chrg_dep = 'Service Charges on Deposits' ;

           /* Trading Income	*/;
	   /* Date Range: 1984q1 to present		*/;
           if dt <19840331 then
               ytdtradrev_inc = . ;
           else if dt>=19840331 & dt<=19951231 then
               ytdtradrev_inc= sum(riad4077, riad4075);
           else ytdtradrev_inc = riada220;
           label ytdtradrev_inc = 'Trading Income' ;

           /* Net Servicing Fees	*/;
	   /* Date Range: 2001q1 to Present		*/;
           if dt<20010331 then
               ytdnonint_inc_net_srv_fees = . ;
           else ytdnonint_inc_net_srv_fees = riadb492;
           label ytdnonint_inc_net_srv_fees = 'Net Servicing Fees' ;

           /* Net Securitization Income */;
           /* Date range: 2001q1 to present */;
           if dt < 20010331 then
               ytdsectn_inc = . ;
           else ytdsectn_inc = riadb493 ;
           label ytdsectn_inc='Net Securitization Income' ;

	   /* Venture Capital Revenue */;
	   /* Date range: 2001q1 to present */;
           if dt < 20010331 then
               ytdvc_inc = . ;
           else ytdvc_inc = riadb491;
           label ytdvc_inc='Venture Capital Revenue';

           /* Investment Banking Service Charges, Comissions and Fees	*/;
	   /* Date Range: 2001q1 to present		*/;
           if dt<20010331 then
               ytdnonint_inc_fees_ibank = . ;
           else if dt >=20010331 & dt<=20061231 then
               ytdnonint_inc_fees_ibank = sum(riadb491,riadb490);
           else ytdnonint_inc_fees_ibank = sum(riadb491, riadc886,riadc888);
           label ytdnonint_inc_fees_ibank = 'Investment Banking Service Charges, Comissions and Fees' ;



	   /* Fiduciary, Annuity, and Insurance Fees	*/;
	   /* Date Range: 1984q1 to present		*/;
           if dt<19840331 then
               ytdnonint_inc_fiduc_insur_anu= . ;
           else if dt>=19840331 & dt<=19931231 then
               ytdnonint_inc_fiduc_insur_anu= riad4070;
           else if dt>=19940331 & dt<=20001231 then
               ytdnonint_inc_fiduc_insur_anu= sum(riad4070, riad8431);
           else if dt>=20010331 & dt<=20021231 then
               ytdnonint_inc_fiduc_insur_anu= sum(riad4070, riad8431, riadb494);
           else if dt>20030331 & dt<=20061231 then
               ytdnonint_inc_fiduc_insur_anu= sum(riad4070, riad8431, riadc386, riadc387);
           else ytdnonint_inc_fiduc_insur_anu= sum(riad4070, riadc887, riadc386, riadc387);
           label ytdnonint_inc_fiduc_insur_anu= 'Fiduciary, Annuity, and Insurance Fees' ;

	   /* All Other Noninterest Income */;
	   /* Date Range: 1991q1 to present		*/;
           if dt<19910331 then
               ytdnonint_inc_alloth2 = . ;
           else if dt>=19910331 & dt<=19961231 then
               ytdnonint_inc_alloth2 = sum(riad4076, riad5407, riad5408);
           else if dt>=19970331 & dt<=20001231 then
               ytdnonint_inc_alloth2 = sum(riad5407, riad5408);
           else ytdnonint_inc_alloth2 = sum(riadb497, riadb493, riad5415, riad5416, riadb496);
           label ytdnonint_inc_alloth2 = 'Other Noninterest Income 2' ;

	   /* All Other Noninterest Income Excluding OREO	*/;
	   /* Date Range: 1991q1 to present		*/;
           if dt<19910331 then
               ytdnonint_inc_alloth2_no_oreo = . ;
           else if dt>=19910331 & dt<=19961231 then
               ytdnonint_inc_alloth2_no_oreo = sum(riad4076, riad5407, riad5408, -riad5415);
           else if dt>=19970331 & dt<=20001231 then
               ytdnonint_inc_alloth2_no_oreo = sum(riad5407, riad5408, -riad5415);
           else ytdnonint_inc_alloth2_no_oreo = sum(riadb497, riadb493, riad5416, riadb496);
           label ytdnonint_inc_alloth2_no_oreo = 'Other Noninterest Income 2, Excluding OREO' ;

           /* Fees and Other Income */;
           /* Date range: 1969q4 to present */;
           ytdfeeOther_inc = sum(ytdnonint_inc, -ytdtradrev_inc, -ytdnonint_inc_srv_chrg_dep, -ytdfiduc_inc) ;
           label ytdfeeOther_inc='Fees and Other Income' ;

           /* 6. Realized gains (losses) on securities */;
           /* HTM Securities */;
           if dt<19940331 then
               ytdhtmsecur_inc = . ;
           else ytdhtmsecur_inc=riad3521;
           label ytdhtmsecur_inc='Gains (Losses) on Held-to-Maturity Securities';

           /* AFS Securities */;
           if dt<19940331 then
               ytdafssecur_inc = . ;
           else ytdafssecur_inc=riad3196;
           label ytdafssecur_inc='Gains (Losses) on AFS Securities';

           /* Gains (losses) on securities not held in trading accounts */;
	   /* Date range: 1984q1 to present */;
           ytdsecur_inc = . ;
           ytdsecur_inc = riad4091 ;
           label ytdsecur_inc='Gains and losses on securities' ;

           /**************************/;
           /* 7. Noninterest Expense */;
           /**************************/;

           /* Total Noninterest Expense */;
           /* Date range: 1984q1 to present */;
           ytdnonint_exp = .;
           ytdnonint_exp = riad4093;
           label ytdnonint_exp='Total Noninterest Expense';


           /* Noninterest Expense from Salaries and Employee Benefits      */;
           /* Date Range: 1969q4 to present        */;
           if dt<19691231 then
               ytdnonint_exp_comp = . ;
           else ytdnonint_exp_comp = riad4135;
           label ytdnonint_exp_comp = 'NIE of  Salaries and Employee Benefits';

           /* Noninterest Expense Associated with Fixed Assets     */;
           /* Date Range: 1969q4 to present        */;
           if dt<19691231 then
               ytdnonint_exp_fass = . ;
           else ytdnonint_exp_fass = riad4217;
           label ytdnonint_exp_fass = 'NIE of Premises and Fixed Assets';

           /* Goodwill Impairment Losses     */;
           /* Date Range: 2002q1 to present     */;
           if dt<20020331 then
               ytdnonint_exp_gwill = . ;
           else ytdnonint_exp_gwill = riadc216;
           label ytdnonint_exp_gwill = 'NIE Goodwill Impairment Losses';

           /* Amortization Expense and Impairment Losses for Other Intangible Assets     */;
           /* Date Range: 1984q1 to present     */;
           if dt<19840331 then
               ytdnonint_exp_amort = . ;
           else ytdnonint_exp_amort = riadc232;
           label ytdnonint_exp_amort = 'NIE Amortization and Impairment Losses for Other Intangible Assets';

           ytdnonint_exp_oth=riad4092;
           ytdnonint_exp_allother=sum(ytdnonint_exp, -ytdnonint_exp_comp, -ytdnonint_exp_fass);

           /* 9. Taxes */;
           /* Date range: 1984q1 to present */;
           ytdtaxes_inc = .;
           ytdtaxes_inc = riad4302;
           label ytdtaxes_inc='Taxes';

           /* 11. Extraordinary Items */;
           /* Date range: 1969q4 to present */;
           ytdextra_inc = .;
           ytdextra_inc = riad4320;
           label ytdextra_inc='Extraordinary Items';

           /* 13. Minority Interest */;
           if dt < 19810630 then
               ytdminor_int = . ;
           else if dt <20090331 then
               ytdminor_int = riad4484 ;
           else ytdminor_int = riadg103 ;
           label ytdminor_int='Minority interest' ;

           /* Schedule RI Memoranda Items */;

           /* Subcomponents of Trading Income (for revised CCAR) */;
           if dt < 19950331 then
               ytdtrad_ir_inc = . ;
           else ytdtrad_ir_inc = riad8757 ;
           label ytdtrad_ir_inc='Interest Rate Exposures' ;

           if dt < 19950331 then
               ytdtrad_fx_inc = . ;
           else ytdtrad_fx_inc = riad8758 ;
           label ytdtrad_fx_inc='Foreign Exchange Exposures' ;

           if dt < 19950331 then
               ytdtrad_eq_inc = . ;
           else ytdtrad_eq_inc = riad8759 ;
           label ytdtrad_eq_inc='Equity Security or Index Exposures' ;

           if dt < 19950331 then
               ytdtrad_com_oth_inc = . ;
           else ytdtrad_com_oth_inc = riad8760 ;
           label ytdtrad_com_oth_inc='Commodity and Other Exposures' ;

           if dt < 20070331 then
               ytdtrad_cred_inc = . ;
           else ytdtrad_cred_inc = riadf186 ;
           label ytdtrad_cred_inc='Credit Exposures' ;

           if dt < 19950331 then
               ytdtrad_allother_inc = . ;
           else if dt <20070331 then ytdtrad_allother_inc = riad8760 ;
           else ytdtrad_allother_inc = sum(riad8760,riadf186);
           label ytdtrad_allother_inc='Commodity and Other Exposures' ;


           /*************************************************/;
           /* SCHEDULE RI-A: Changes in Bank Equity Capital */;
           /*************************************************/;
           /* Sale, conversion, acquisition, or retirement of capital stock, net */;
           /* Date range: 2001q1 to present */;
           ytdstocksale = .;
           ytdstocksale = riadb509;
           label ytdstocksale = 'Sale, conversion, acquisition, retirement of capital stock, net';

           /* Treasury stock transactions */;
           /* Date range: 2001q1 to present */;
           ytdtreasurystock = .;
           ytdtreasurystock = riadb510;
           label ytdtreasurystock = 'Treasury stock transactions, net';

           /* Changes due to business combinations, net */;
           /* Date range: 1976 to present */;
           ytdbuschange = .;
           ytdbuschange = riad4356;
           label ytdbuschange = 'Capital changes due to business combinations, net';

           /* Cash dividends declared on preferred stock */;
           /* Date range: 1969q4 to present */;
           ytdprefdividend = .;
           ytdprefdividend = riad4470;
           label ytdprefdividend = 'Cash dividends declared on preferred stock';

           /* Cash dividends declared on common stock */;
           /* Date range: 1969q4 to present */;
           ytdcommdividend = .;
           ytdcommdividend = riad4460;
           label ytdcommdividend = 'Cash dividends declared on common stock';

           /* Other comprehensive income */;
           /* Date range: 2001q1 to present */;
           ytdcompinc_oth = .;
           ytdcompinc_oth = riadb511;
           label ytdcompinc_oth = 'Other comprehensive income';

           /* Other transactions with parent holding company */;
           /* Date range: 1996q4 to present */;
           ytdhctrans = .;
           ytdhctrans = riad4415;
           label ytdhctrans = 'Other transactions with parent holding company';



           /*********************************************/;
           /* SCHEDULE RI-B: Charge-offs and Recoveries */;
           /*********************************************/;

           /***************/;
           /* REAL ESTATE */;
           ****************/;

           /* Charge-offs on Real Estate Loans */;
           /* Date range (riad4613): 1984q1 to present */;
           /* Date range (riad4256): 1984q1 to 2000q4 */;
           ytdxoff_re = .;
           if dt > 20001231 then ytdxoff_re = riad4613 ;
           if dt <= 20001231 then ytdxoff_re = sum(riad4613,riad4256) ;
           label ytdxoff_re='Charge-offs on Real Estate Loans';

           /* Recoveries on Real Estate Loans */;
           /* Date range (riad4616): 1984q1 to present */;
           /* Date range (riad4257): 1984q1 to 2000q4 */;
           ytdrecov_re = .;
           if dt > 20001231 then ytdrecov_re = riad4616;
           if dt <= 20001231 then ytdrecov_re = sum(riad4616,riad4257)  ;
           label ytdrecov_re='Recoveries on Real Estate Loans';

           /* Net Charge-offs on Real Estate Loans */;
           /* Date range: 1984q1 to present */;
           ytdnetxoff_re = .;
           if ytdxoff_re=. & ytdrecov_re^=. then ytdnetxoff_re = 0 - ytdrecov_re;
           else if ytdrecov_re=. & ytdxoff_re^=. then ytdnetxoff_re = ytdxoff_re;
           else ytdnetxoff_re = ytdxoff_re - ytdrecov_re;
           label ytdnetxoff_re='Net Charge-offs on Real Estate Loans';

          /***************************/;
          /* RESIDENTIAL REAL ESTATE */;
          /***************************/;

          /* Charge-offs and Recoveries on 1-4 Family Residential Real Estate */;
          /* Date range (riad5411,riad5413,riad5449,riad5451): 1991q1 to present */;
          /* Date range (riadc235,riadc235): 2002q1 to present */;
          if dt < 19910331 then do ;
              ytdxoff_rre=. ;
              ytdxoff_heloc=. ;
              ytdrecov_heloc=. ;
              ytdxoff_closedlien=. ;
              ytdrecov_closedlien=. ;
              ytdxoff_firstlien=. ;
              ytdrecov_firstlien=. ;
              ytdxoff_jrlien=. ;
              ytdrecov_jrlien=. ;
              end ;
          if dt >= 19910331 & dt <= 20011231 then do ;
              ytdxoff_rre = sum(riad5411,riad5413,riad5449,riad5451) ;
              ytdxoff_heloc=riad5411 ;
              ytdrecov_heloc=riad5412 ;
              ytdxoff_closedlien= riad5413 ;
              ytdrecov_closedlien= riad5414 ;
              ytdxoff_firstlien=. ;
              ytdrecov_firstlien=. ;
              ytdxoff_jrlien=. ;
              ytdrecov_jrlien=. ;
              end ;
          else do ;
              ytdxoff_rre = sum(riadc234,riadc235,riad5411) ;
              ytdxoff_heloc=riad5411 ;
              ytdrecov_heloc=riad5412 ;
              ytdxoff_closedlien= sum(riadc234,riadc235);
              ytdrecov_closedlien= sum(riadc217,riadc218);
              ytdxoff_firstlien=riadc234 ;
              ytdrecov_firstlien=riadc217 ;
              ytdxoff_jrlien=riadc235 ;
              ytdrecov_jrlien=riadc218 ;
              end ;
          label ytdxoff_rre='Charge-offs on 1-4 Family Residential Real Estate';

          /* Recoveries on 1-4 Family Residential Real Estate */;
          /* Date range (riad5412,riad5414,riad5450,riad5452): 1991q1 to present */;
          /* Date range (riadc217,riadc218): 2002q1 to present */;
          if dt < 19910331 then do ;
              ytdrecov_rre=. ;
              end;
          if dt >= 19910331 & dt <= 20011231 then do ;
              ytdrecov_rre = sum(riad5412,riad5414,riad5450,riad5452) ;
              end;
          else do ;
              ytdrecov_rre = sum(riadc217,riadc218,riad5412) ;
              end;
          label ytdrecov_rre='Recoveries on 1-4 Family Residential Real Estate';

          /* Net Charge-offs on 1-4 Family Residential Real Estate */;
          /* Date range: 1991q1 to present */;
          ytdnetxoff_rre=sum(ytdxoff_rre,-ytdrecov_rre) ;
          ytdnetxoff_heloc=sum(ytdxoff_heloc, -ytdrecov_heloc) ;
          ytdnetxoff_closedlien=sum(ytdxoff_closedlien, -ytdrecov_closedlien) ;
          ytdnetxoff_firstlien=sum(ytdxoff_firstlien, -ytdrecov_firstlien) ;
          ytdnetxoff_jrlien=sum(ytdxoff_jrlien, -ytdrecov_jrlien) ;
          label ytdnetxoff_rre='Net Charge-offs on 1-4 Family Residential Real Estate';
          label ytdxoff_heloc='Charge-offs on 1-4 Family HELOCs';
          label ytdrecov_heloc='Recoveries on 1-4 Family HELOCs';
          label ytdnetxoff_heloc='Net Charge-offs on 1-4 Family HELOCs';
          label ytdxoff_closedlien='Charge-offs on 1-4 Family Closed Lien Mortgages';
          label ytdrecov_closedlien='Recoveries on 1-4 Family Closed Lien Mortgages';
          label ytdnetxoff_closedlien='Net Charge-offs on 1-4 Family Closed Lien Mortgages';
          label ytdxoff_firstlien='Charge-offs on 1-4 Family 1st Lien Closed Mortgages';
          label ytdrecov_firstlien='Recoveries on 1-4 Family 1st Lien Closed Mortgages';
          label ytdnetxoff_firstlien='Net Charge-offs on 1-4 Family 1st Lien Closed Mortgages';
          label ytdxoff_jrlien='Charge-offs on 1-4 Family Jr Lien Closed Mortgages';
          label ytdrecov_jrlien='Recoveries on 1-4 Family Jr Lien Closed Mortgages';
          label ytdnetxoff_jrlien='Net Charge-offs on 1-4 Family Jr Lien Closed Mortgages';

          /**************************/;
          /* COMMERCIAL REAL ESTATE */;
          /**************************/;

          /* Charge-offs on construction and land development, mutlifamily resid. properties, and nonfarm/nonres loans*/;
          /* Date range: 1991q1 to present */;
          if dt<19910331 then do;
              ytdxoff_const=.;
              ytdrecov_const=.;
              ytdxoff_multi=.;
              ytdrecov_multi=.;
              ytdxoff_nfnr=.;
              ytdrecov_nfnr=.;

              end;
          if dt>=19910331 & dt<20070331 then do;
              ytdxoff_const=riad3582;
              ytdrecov_const=riad3583;
              ytdxoff_multi=riad3588;
              ytdrecov_multi=riad3589;
              ytdxoff_nfnr=riad3590;
              ytdrecov_nfnr=riad3591;

              end;
          if dt>=20070331 then do;
              ytdxoff_const=riad3582;
              ytdrecov_const=riad3583;
              ytdxoff_multi=riad3588;
              ytdrecov_multi=riad3589;
              ytdxoff_nfnr=riad3590;
              ytdrecov_nfnr=riad3591;

              end;

          ytdnetxoff_const=sum(ytdxoff_const, -ytdrecov_const);
          ytdnetxoff_multi=sum(ytdxoff_multi, -ytdrecov_multi);
          ytdnetxoff_nfnr=sum(ytdxoff_nfnr, -ytdrecov_nfnr);


          ytdxoff_cre=sum(ytdxoff_const,ytdxoff_multi,ytdxoff_nfnr);
          ytdrecov_cre=sum(ytdrecov_const,ytdrecov_multi,ytdrecov_nfnr);
          ytdnetxoff_cre=sum(ytdxoff_cre, -ytdrecov_cre);

          label ytdxoff_const='Charge-offs on construction, land loans';
          label ytdrecov_const='Recoveries on construction, land loans';
          label ytdnetxoff_const='Net charge-offs on construction, land loans';
          label ytdxoff_multi='Charge-offs on multi-family property loans';
          label ytdrecov_multi='Recoveries on multi-family property loans';
          label ytdnetxoff_multi='Net charge-offs on multi-family property loans';
          label ytdxoff_nfnr='Charge-offs on nonfarm, nonres CRE loans';
          label ytdrecov_nfnr='Recoveries on nonfarm, nonres CRE loans';
          label ytdnetxoff_nfnr='Net charge-offs on nonfarm, nonres CRE loans';

          label ytdxoff_cre='Charge-offs on Commercial Real Estate Loans';
          label ytdrecov_cre='Recoveries on Commercial Real Estate Loans';
          label ytdnetxoff_cre='Net Charge-offs on Commercial Real Estate Loans';


          /***************************/;
          /* OTHER REAL ESTATE LOANS */;
          /***************************/;
          /* Charge-offs on Other Real Estate Loans */;
          /* Date range: 1984q1 to present */;
          ytdxoff_othre = .;
          ytdrecov_othre=.;
          ytdnetxoff_othre=.;

          ytdxoff_othre = sum(ytdxoff_re,-ytdxoff_rre,-ytdxoff_cre);
          ytdrecov_othre = sum(ytdrecov_re,-ytdrecov_rre,-ytdrecov_cre);
          ytdnetxoff_othre=sum(ytdxoff_othre,-ytdrecov_othre);

          label ytdxoff_othre='Charge-offs on Other Real Estate';
          label ytdrecov_othre='Recoveries on Other Real Estate';
          label ytdnetxoff_othre='Net Charge-offs on Other Real Estate (Non-Res, Non-Com)';

          ytdnetxoff_refor=sum(riad4652,-riad4662);
          ytdnetxoff_farm=sum(riad3584,-riad3585);


          /***************************/;
          /* DEPOSITORY INSTITUTIONS */;
          /***************************/;

          /* Charge-offs on Loans to Depository Institutions */;
          /* Date range: 1984q1 to present */;
          ytdxoff_dep = .;
          ytdxoff_dep = sum(riad4653,riad4654,riad4481);
          label ytdxoff_dep='Charge-offs on Loans to Depository Institutions';

          /* Recoveries on Loans to Depository Institutions */;
          /* Date range: 1984q1 to present */;
          ytdrecov_dep = .;
          ytdrecov_dep = sum(riad4663,riad4664,riad4482);
          label ytdrecov_dep='Recoveries on Loans to Depository Institutions';

          /* Net Charge-offs on Loans to Depository Institutions */;
          /* Date range: 1984q1 to present */;
          ytdnetxoff_dep = .;
          if ytdxoff_dep=. & ytdrecov_dep^=. then ytdnetxoff_dep = 0 - ytdrecov_dep;
          else if ytdrecov_dep=. & ytdxoff_dep^=. then ytdnetxoff_dep = ytdxoff_dep;
          else ytdnetxoff_dep = ytdxoff_dep - ytdrecov_dep;
          label ytdnetxoff_dep='Net Charge-offs on Loans to Depository Institutions';

          /***************************/;
          /* AGRICULTURAL PRODUCTION */;
          /***************************/;

          /* Charge-offs on Loans to Finance Agricultural Production */;
          /* Date range: 1984q1 to present */;
          ytdxoff_agr = .;
          ytdxoff_agr = riad4655 ;
          label ytdxoff_agr='Charge-offs on Loans to Finance Agricultural Production' ;

          /* Recoveries on Loans to Finance Agricultural Production */;
          /* Date range: 1984q1 to present */;
          ytdrecov_agr = .;
          ytdrecov_agr = riad4665;
          label ytdrecov_agr='Recoveries on Loans to Finance Agricultural Production' ;

          /* Net Charge-offs on Loans to Finance Agricultural Production */;
          /* Date range: 1984q1 to present */;
          ytdnetxoff_agr=sum(ytdxoff_agr, -ytdrecov_agr);
          label ytdnetxoff_agr='Net Charge-offs on Loans to Finance Agricultural Production' ;

          /*******/;
          /* C&I */;
          /*******/;

          /* Charge-offs on C&I Loans */;
          /* Date range (riad4638): 1984q1 to present */;
          /* Date range (riad4264): 1984q1 to present */;
          ytdxoff_ci = .;
          if dt > 20001231 then ytdxoff_ci = riad4638 ;
          if dt <= 20001231 then ytdxoff_ci = sum(riad4638,riad4264) ;
          label ytdxoff_ci='Charge-offs on C&I Loans';

          /* Recoveries on C&I Loans */;
          /* Date range (riad4608): 1984q1 to present */;
          /* Date range (riad4265): 1984q1 to present */;
          ytdrecov_ci = .;
          if dt > 20001231 then ytdrecov_ci = riad4608 ;
          if dt <= 20001231 then ytdrecov_ci = sum(riad4608,riad4265) ;
          label ytdrecov_ci='Recoveries on C&I Loans';

          /* Net Charge-offs on C&I Loans */;
          /* Date range: 1984q1 to present */;
          ytdnetxoff_ci = .;
          ytdnetxoff_ci =sum(ytdxoff_ci,-ytdrecov_ci);
          label ytdnetxoff_ci='Net Charge-offs on C&I Loans';

          /* Loans to individuals for household, family, and other personal expenditures: */;
          /************/;
          /* CONSUMER */;
          /************/;

          /* Charge-offs on Consumer Loans */;
          /* Date range (riadb514,riadb516): 2001q1 to present */;
          /* Date range (riad4656,riad4657,riad4262,riad4258): 1984q1 to present */;
          ytdxoff_cons = .;
          if dt <= 20001231 then ytdxoff_cons = sum(riad4656,riad4657,riad4262,riad4258);
          else if dt < 20110331 then ytdxoff_cons = sum(riadb514,riadb516);
          else ytdxoff_cons = sum(riadb514,riadk129,riadk205);
          label ytdxoff_cons='Charge-offs on Consumer Loans';

          /* Recoveries on Consumer Loans */;
          /* Date range (riadb515,riadb517): 2001q1 to present */;
          /* Date range (riad4666,riad4667,riad4263,riad4259): 1984q1 to present */;
          ytdrecov_cons = .;
          if dt <= 20001231 then ytdrecov_cons = sum(riad4666,riad4667,riad4263,riad4259);
          else if dt < 20110331 then ytdrecov_cons = sum(riadb515,riadb517);
          else ytdrecov_cons = sum(riadb515,riadk133,riadk206);
          label ytdrecov_cons='Recoveries on Consumer Loans';

          /* Net Charge-offs on Consumer Loans */;
          /* Date range: 1984q1 to present */;
          ytdnetxoff_cons=sum(ytdxoff_cons,-ytdrecov_cons);
          label ytdnetxoff_cons='Net Charge-offs on Consumer Loans';

          /****************/;
          /* CREDIT CARDS */;
          /****************/;

          /* Charge-offs on Credit Card Loans */;
          /* Date range (riadb514): 2001q1 to present */;
          /* Date range (riad4656,riad4262): 1984q1 to present */;
          ytdxoff_cc = .;
          ytdxoff_cc = riadb514;
          if dt <= 20001231 then ytdxoff_cc = sum(riad4656,riad4262);
          label ytdxoff_cc='Charge-offs on Credit Card Loans';

          /* Recoveries on Credit Card Loans */;
          /* Date range (riadb515): 2001q1 to present */;
          /* Date range (riad4666,riad4263): 1984q1 to present */;
          ytdrecov_cc = .;
          ytdrecov_cc = riadb515;
          if dt <= 20001231 then ytdrecov_cc = sum(riad4666,riad4263) ;
          label ytdrecov_cc='Recoveries on Credit Card Loans';

          /* Net Charge-offs on Credit Card Loans */;
          /* Date range: 1984q1 to present */;
          ytdnetxoff_cc = sum(ytdxoff_cc,-ytdrecov_cc);
          label ytdnetxoff_cc='Net Charge-offs on Credit Card Loans';

          /************************/;
          /* OTHER CONSUMER LOANS */;
          /************************/;

          /* Charge-offs on Other Loans to Consumers (Installment)*/;
          /* Date range (riad4657,riad4258): 1984q1 to 2000q4 */;
          /* Date range (riadb516): 2001q1 to present */;
          /* Date range (riadk129,k205): 2011q1 to present */;
          ytdxoff_othcons=.;
          if (dt >= 19840331 & dt <= 20001231)
              then ytdxoff_othcons=sum(riad4657,riad4258);
          else if dt < 20110331 then ytdxoff_othcons=riadb516;
          else ytdxoff_othcons = sum(riadk129,riadk205);
          label ytdxoff_othcons='Charge-offs on Other Loans to Consumers' ;

          /* Recoveries on Other Loans to Consumers (Installment)*/;
          /* Date range (riad4667,riad4259): 1984q1 to 2000q4 */;
          /* Date range (riadb517): 2001q1 to present */;
          if dt < 198403
              then ytdrecov_othcons=.;
          if (dt >= 19840331 & dt <= 20001231)
              then ytdrecov_othcons=sum(riad4667,riad4259);
          else if dt < 20110331 then ytdrecov_othcons=riadb517;
          else ytdrecov_othcons = sum(riadk129,riadk205);
          label ytdrecov_othcons='Recoveries on Other Loans to Consumers';

          /* Net Charge-offs on Other Loans to Consumers*/;
          /* Date range: 1984q1 to present */;
          ytdnetxoff_othcons=sum(ytdxoff_othcons, -ytdrecov_othcons) ;
          label ytdnetxoff_othcons='Net Charge-offs on Other Loans to Consumers' ;

          /***********************/;
          /* FOREIGN GOVERNMENTS */;
          /***********************/;

           /* Charge-offs on Loans to Foreign Governments */;
           /* Date range: 1984q1 to present */;
           ytdxoff_fgovt=.;
           ytdxoff_fgovt=riad4643;
           label ytdxoff_fgovt='Charge-offs on Loans to Foreign Governments' ;

           /* Recoveries on Loans to Foreign Governments */;
           /* Date range: 1984q1 to present */;
           ytdrecov_fgovt=.;
           ytdrecov_fgovt=riad4627;
           label ytdrecov_fgovt='Recoveries on Loans to Foreign Governments' ;

           /* Net Charge-offs on Loans to Foreign Governments */;
           /* Date range: 1984q1 to present */;
           ytdnetxoff_fgovt =sum(ytdxoff_fgovt, -ytdrecov_fgovt) ;
           label ytdnetxoff_fgovt='Net Charge-offs on Loans to Foreign Governments' ;

           /***************/;
           /* OTHER LOANS */;
           /***************/;

           /* Charge-offs on Other Loans */;
           /* Date range: 1984q1 to present */;
           ytdxoff_oth=.;
           ytdxoff_oth=riad4644;
           label ytdxoff_oth='Charge-offs on Other Loans' ;

           /* Recoveries on Other Loans */;
           /* Date range: 1984q1 to present */;
           ytdrecov_oth=.;
           ytdrecov_oth=riad4628;
           label ytdrecov_oth='Recoveries on Other Loans' ;

           /* Net Charge-offs on Other Loans */;
           /* Date range: 1984q1 to present */;
           ytdnetxoff_oth = sum(ytdxoff_oth, -ytdrecov_oth) ;
           label ytdnetxoff_oth='Net Charge-offs on Other Loans' ;


           /*******************/;
           /* ALL OTHER LOANS */;
           /*******************/;
           /* Charge-offs on All Other Loans */;
           /* Recoveries on All Other Loans */;
           if dt < 19910331 then do ;
               ytdxoff_allother = . ;
               ytdrecov_allother = . ;
               end ;
           else do ;
               ytdxoff_allother = sum(ytdxoff_lease,ytdxoff_oth,ytdxoff_fgovt,ytdxoff_agr,ytdxoff_dep,ytdxoff_othre) ;
               ytdrecov_allother = sum(ytdrecov_lease,ytdrecov_oth,ytdrecov_fgovt,ytdrecov_agr,ytdrecov_dep,ytdrecov_othre) ;
               end ;

           ytdnetxoff_allother = sum(ytdxoff_allother,-ytdrecov_allother) ;

           label ytdxoff_oth='Charge-offs on All Other Loans' ;
           label ytdrecov_oth='Recoveries on All Other Loans' ;
           label ytdnetxoff_oth='Net Charge-offs on All Other Loans' ;

           /*******************************/;
           /* LEASE FINANCING RECEIVABLES */;
           /*******************************/;

           /* Charge-offs on Lease Financing Receivables */;
           /* Date range: 1984q1 to present */;
           ytdxoff_lease=.;
           ytdxoff_lease=riad4266;
           label ytdxoff_lease='Charge-offs on Lease Financing Receivables' ;

           /* Recoveries on Lease Financing Receivables */;
           /* Date range: 1984q1 to present */;
           ytdrecov_lease=.;
           ytdrecov_lease=riad4267;
           label ytdrecov_lease='Recoveries on Lease Financing Receivables' ;

           /* Net Charge-offs on Lease Financing Receivables */;
           /* Date range: 1984q1 to present */;
           ytdnetxoff_lease=sum(ytdxoff_lease, -ytdrecov_lease) ;
           label ytdnetxoff_lease='Net Charge-offs on Lease Financing Receivables' ;

           /****************************************************************************
           Begining in 2001:2 write-downs arising from transfers to the held-for-sale
           account are included in riadnetxoffs.  For 2001, the variable (5523) was
           negative, so add back in.  2002:1 and after riad5523 is positive, so subtract
           out of netxoffs.
           *****************************************************************************/;

            /*********/;
            /* TOTAL */;
            /*********/;

            /* Charge-offs Total */;
            /* Date range (riad4635): 1976q1 to present */;
            /* Date range (riad5523): 2001q2 to present */;
            ytdxoff_tot = .;
            if dt <= 20010331 then ytdxoff_tot = riad4635 ;
            if (dt > 20010331 & dt <= 20011231) then ytdxoff_tot = sum(riad4635,riad5523) ;
            if dt > 20011231 then do;
                if riad4635=. & riad5523^=. then ytdxoff_tot = 0 - riad5523;
                else if riad5523=. & riad4635^=. then ytdxoff_tot = riad4635;
                else ytdxoff_tot = riad4635 - riad5523 ;
                end;
            label ytdxoff_tot='Charge-offs Total';

            /* Recoveries Total */;
            /* Date range: 1976q1 to present */;
            ytdrecov_tot = .;
            ytdrecov_tot = riad4605;
            label ytdrecov_tot='Recoveries Total';

            /* Net Charge-offs Total */;
            /* Date range: 1976q1 to present */;
            ytdnetxoff_tot = .;
            ytdnetxoff_tot = sum(ytdxoff_tot, -ytdrecov_tot);
            label ytdnetxoff_tot='Net Charge-offs Total';


   	/* Pushdown--> used to determine if push down accounting has occurred (so flow variables
   	   	       do NOT need to be quarterized) */;
               pushdown = .;
               pushdown = riad9106;

/* DROP ALL RIAD VARIABLES  */;
               drop riad:;




    pushdown=riad9106;
          drop rcon: rc: te: riad: ;


/************************************************************************
*************************************************************************
********************     BALANCE SHEET VARIABLES     ********************
*************************************************************************
************************************************************************/

	/****************************************
    **********     SCHEDULE RC     **********
    ****************************************/

	/**********************************
  	*********** ASSETS ****************
  	**********************************/

        /* Total Assets	*/
	/*	Date Range: 1969q2 to present		*/
            if dt<19690630 then
                assets = . ;
		else assets = rcfd2170;
                label assets = 'Total Assets';


    /* 	Cash and balances due from depository institutions */
    /* 	Date range: 1969q2 to present */
       	cash = .;
       	cash = rcfd0010;
       	label cash='Cash and balances due from depository institutions';

    /*	Federal Funds Sold in domestic offices */
    /* 	Date range (rcfd0276): 1988q1 to 1996q4 */
    /* 	Date range (rconb987): 2002q1 to present */
    /* 	Date range (rcfdb987): 2003q1 to present */
       	ffsold = .;
       	if dt <= 19961231 then ffsold=rcfd0276 ;
       	if dt >19961231 & dt<20030331 then ffsold=.;
       	if dt >= 20020331 then ffsold = rconb987 ;
       	label ffsold='Federal Funds Sold';

	/*	Federal Funds Sold, Securities Purchased under Agreements to Resell	*/
	/*	Date Range: 1988q1 to present		*/
        ffrepo_ass = rcfd1350 ;
        if (ffrepo_ass=. & dt>=20020331) then ffrepo_ass = sum(rconb987,rcfdb989) ;
		label ffrepo_ass = 'Federal Funds Sold, Securities Purchased under Agreements to Resell';

  	/* Securities Purchased Under Agreement to Resell */
    /* Date range:  1988q1 to 1996q4, 2002q1 to present */
       	repo_purch = .;
       	if dt <= 19961231 then repo_purch=rcfd0277 ;
		if dt>=20020331 then repo_purch=rcfdb989 ;
		label repo_purch='Securities Purchased Under Agreement, Etc';

    /* Allowance for Loan and Lease Losses (Loan Loss Reserve) */
    /* Date range: 1976q1 to present */
		llres = .;
		llres = rcfd3123;
		label llres='Loan Loss Reserve';

	/* Total Fixed Assets */
	/* Date range: 1969q2 to present */
        fixed_ass = .;
        fixed_ass = rcfd2145;
        label fixed_ass='Total Fixed Assets';

    /* Other Real estate owned */
        oreo = . ;
        oreo = rcfd2150;
        label oreo ='Other Real Estate Owned';

 	/* Intangible Assets */
	/* Date range: 1983q1 to present */
        intang = .;
        if dt <20010331 then intang = rcfd2143;
        else intang = sum(rcfd3163,rcfd0426);
        label intang='Intangible Assets';

	/* Goodwill */
	/* Date range: 1985q2 to present */
	    goodwill = .;
	    goodwill = rcfd3163;
	    label goodwill='Goodwill';

	/* Other Intangible Assets */
	/* Date range: 1985q2 to present */
	    intang_oth = .;
	    if dt <20010331 then intang_oth = sum(rcfd2143, -rcfd3163);
	    else intang_oth = rcfd0426;
	    label intang_oth ='Other Intangible Assets';

	/*	Total Investment Securities - Book Value	*/
	/*	Date Range: 1984q1 to present		*/
		if dt<19840331 then
			inv_sec = . ;
		else if dt>=19840331 & dt<=19931231 then
			inv_sec = rcfd0390;
		else inv_sec = sum(rcfd1754, rcfd1773);
		label inv_sec = 'Total Investment Securities - Book Value';

	/*	Interest Bearing Balances	*/
	/*	Date Range: 1984q1 to present		*/
		if dt<19840331 then
			ibb = . ;
		else ibb = rcfd0071;
		label ibb = 'Interest Bearing Balances';

		ibb_bhc=ibb;
		label ibb_bhc='Interest Bearing Balances - BHCs Only';


/*******************************************/;
/**********Schedule RC-B*********************/;
/*******************************************/;

	/*Schedule HC-B: HTM(Amortized Cost) */

	/*HTM U.S. Treasury Securities*/;
	if dt<19940331 then
		htm_ust = .;
	else htm_ust = rcfd0211;
	label htm_ust= 'HTM U.S. Treasury Securities';


	/* HTM U.S. government agency and corporation obligations (exclude mortgage-backed securities) Issued by U.S. government agencies*/;
	if dt<19940331 then
		htm_gov_obl=.;
	else htm_gov_obl=rcfd1289;
	label htm_gov_obl = 'U.S. government agency obligations (exclude mortgage-backed securities - Issued by U.S. government agencies';


	/* HTM U.S. government agency and corporation obligations (exclude mortgage-backed securities) Issued by U.S. government-sponsored agencies*/;
	if dt<19940331 then
		htm_gvsp_obl=.;
	else htm_gvsp_obl=rcfd1294;
	label htm_gvsp_obl = 'U.S. government agency obligations (exclude mortgage-backed securities - Issued by U.S. government-sponsored agencies';

	/* HTM Securities issued by states and political subdivisions in the U.S. */;
	if dt<19940331 then
            htm_sec_sts=.;
        else if dt>=19940331 & dt<20010331 then
            htm_sec_sts=sum(rcfd1676,rcfd1681,rcfd1694);
	else htm_sec_sts=rcfd8496;
	label htm_sec_sts = 'HTM securities issued by states and political subdivisions in the U.S.';

	/* Commercial MBS */

	/* Commercial Agency issued pass-through securities */
	if dt<20110331 then
		htm_copth_sag=.;
	else htm_copth_sag=rcfdk142;
	label htm_copth_sag = 'Commercial Pass-Through Securities - Issued by FNMA, GGHLMC, or GNMA';

	/* Commercial Agency issued MBS */
	if dt<20110331 then
		htm_cmbs_ag=.;
	else htm_cmbs_ag=rcfdk150;
	label htm_cmbs_ag = 'Other Commercial MBS - Issued or guaranteed by FNMA, FHLMC, or GNMA';

	/* Commercial non-agency pass through securities */
	if dt<20110331 then
		htm_copth_oth=.;
	else htm_copth_oth= rcfdk146;
	label htm_copth_oth='Commercial pass-through securities - other pass-through securities';

	/* Other non-agency commercial MBS */
	if dt<20110331 then
		htm_aoth_cmbs=.;
	else htm_aoth_cmbs=rcfdk154;
	label htm_aoth_cmbs = 'Other Commercial MBS - All Other MBS';

	/* Commercial pass through securities */
	if dt<20090630 then
		htm_copth=.;
	else if dt>=20090630 & dt<20110331 then
		htm_copth=rcfdg324;
	else htm_copth = sum(rcfdk142, rcfdk146);
	label htm_copth = 'Commercial pass through securities';

	/* Other Commercial MBS */
	if dt<20090630 then
		htm_commbs=.;
	else if dt>=20090630 & dt<20110331 then
		htm_commbs= rcfdg328;
	else htm_commbs = sum(rcfdk150, rcfdk154);
	label htm_commbs = 'Other Commercial MBS';

	/* Agency Pass Through MBS */
	if dt<20010331 then
		htm_agpth_mbs = .;
	else if dt>=20010331 & dt<20090630 then
		htm_agpth_mbs = sum(rcfd1698, rcfd1703);
	else if dt>=20090630 & dt<20110331 then
		htm_agpth_mbs = sum(rcfdg300, rcfdg304);
	else htm_agpth_mbs = sum(rcfdg300,rcfdg304, rcfdk142);
	label htm_agpth_mbs = 'Agency Pass Through MBS';


	/* Agency CMOs */
	if dt<20010331 then
		htm_ag_cmos=.;
	else if dt>=20010331 & dt<20090630 then
		htm_ag_cmos = sum(rcfd1714, rcfd1718);
	else if dt>=20090630 & dt<20110331 then
		htm_ag_cmos = sum(rcfdg312, rcfdg316);
	else htm_ag_cmos = sum(rcfdg312, rcfdg316, rcfdk150);
	label htm_ag_cmos = 'Agency CMOs';

	/* Non Agency MBS */
	if dt<20010331 then
		htm_nag_mbs=.;
	else if dt>=20010331 & dt<20090630 then
		htm_nag_mbs = sum(rcfd1709, rcfd1733);
	else if dt>=20090630 & dt<20110331 then
		htm_nag_mbs = sum(rcfdg308, rcfdg320, rcfdg324, rcfdg328);
	else htm_nag_mbs = sum(rcfdg308, rcfdg320, rcfdk146, rcfdk154);
	label htm_nag_mbs = 'Non Agency MBS';


	/* Asset Backed Securities */
	if dt<20010331 then
		htm_ab_sec=.;
	else if dt>=20010331 & dt<20060331 then
		htm_ab_sec = sum(rcfdb838, rcfdb842, rcfdb846, rcfdb850, rcfdb854, rcfdb858);
	else if dt>=20060331 & dt<20090630 then
		htm_ab_sec = rcfdc026;
	else htm_ab_sec = sum(rcfdc026, rcfdg336, rcfdg340, rcfdg344);
	label htm_ab_sec = 'Asset Backed Securities';

	/*Other Domestic Debt Securities */
	if dt<20010331 then
		htm_odom_dsec=.;
	else htm_odom_dsec =rcfd1737;
	label htm_odom_dsec = 'Other Domestic Debt Securities';

	/*Other Foreign Debt Securities */
	if dt<20010331 then
		htm_ofgn_dsec=.;
	else htm_ofgn_dsec = rcfd1742;
	label htm_ofgn_dsec = 'Other Foreign Debt Securities';


	/*Schedule HC-B: AFS (Fair Value) */

	/*AFS U.S. Treasury Securities*/;
	if dt<19940331 then
		afs_ust=.;
	else afs_ust=rcfd1287;
	label afs_ust= 'AFS U.S. Treasury Securities';

	/* AFS U.S. government agency and corporation obligations (exclude mortgage-backed securities) issued by U.S. government agencies*/;
	if dt<19940331 then
		afs_gov_obl=.;
	else afs_gov_obl=rcfd1293;
	label afs_gov_obl = 'U.S. government agency obligations (exclude mortgage-backed securities - Issued by U.S. government agencies';


	/* AFS U.S. government agency and corporation obligations (exclude mortgage-backed securities) issued by U.S. government-sponsored agencies*/;
	if dt<19940331 then
		afs_gvsp_obl=.;
	else afs_gvsp_obl=rcfd1298;
	label afs_gvsp_obl = 'U.S. government agency obligations (exclude mortgage-backed securities - Issued by U.S. government-sponsored agencies';

	/* AFS Securities issued by states and political subdivisions in the U.S. */;
	if dt<19940331 then
		afs_sec_sts=.;
	else if dt>=19940331 & dt<20010331 then
		afs_sec_sts=sum(rcfd1679,rcfd1691,rcfd1697);
	else afs_sec_sts= rcfd8499;
	label afs_sec_sts = 'AFS securities issued by states and political subdivisions in the U.S.';

	/* Commercial MBS */

	/* Commercial Agency issued pass-through securities */
	if dt<20110331 then
		afs_copth_sag=.;
	else afs_copth_sag=rcfdk145;
	label afs_copth_sag = 'Commercial Pass-Through Securities - Issued by FNMA, FHLMC, or GNMA';

	/* Commercial Agency issued MBS */
	if dt<20110331 then
		afs_cmbs_ag=.;
	else afs_cmbs_ag=rcfdk153;
	label afs_cmbs_ag = 'Other Commercial MBS - Issued or guaranteed by FNMA, FHLMC, or GNMA';

	/* Commercial non-agency pass through securities */
	if dt<20110331 then
		afs_copth_oth=.;
	else afs_copth_oth= rcfdk149;
	label afs_copth_oth = 'Commercial pass-through securities - other pass-through securities';

	/* Other non-agency commercial MBS */
	if dt<20110331 then
		afs_aoth_cmbs=.;
	else afs_aoth_cmbs=rcfdk157;
	label afs_aoth_cmbs = 'Other Commercial MBS - All Other MBS';

	/* Commercial pass through securities */
	if dt<20090630 then
		afs_copth=.;
	else if dt>=20090630 & dt<20110331 then
		afs_copth=rcfdg327;
	else afs_copth = sum(rcfdk145, rcfdk149);
	label afs_copth = 'Commercial pass through securities';

	/* Other Commercial MBS */
	if dt<20090630 then
		afs_commbs=.;
	else if dt>=20090630 & dt<20110331 then
		afs_commbs= rcfdg331;
	else afs_commbs = sum(rcfdk153, rcfdk157);
	label afs_commbs = 'Other Commercial MBS';


	/* Agency Pass Through MBS */
	if dt<20010331 then
		afs_agpth_mbs = .;
	else if dt>=20010331 & dt<20090630 then
		afs_agpth_mbs = sum(rcfd1702, rcfd1707);
	else if dt>=20090630 & dt<20110331 then
		afs_agpth_mbs = sum(rcfdg303, rcfdg307);
	else afs_agpth_mbs = sum(rcfdg303, rcfdg307, rcfdk145);
	label afs_agpth_mbs = 'Agency Pass Through MBS';


	/* Agency CMOs */
	if dt<20010331 then
		afs_ag_cmos=.;
	else if dt>=20010331 & dt<20090630 then
		afs_ag_cmos = sum(rcfd1717, rcfd1732);
	else if dt>=20090630 & dt<20110331 then
		afs_ag_cmos = sum(rcfdg315, rcfdg319);
	else afs_ag_cmos = sum(rcfdg315, rcfdg319, rcfdk153);
	label afs_ag_cmos ='Agency CMOs';

	/* Non Agency MBS */
	if dt<20010331 then
		afs_nag_mbs=.;
	else if dt>=20010331 & dt<20090630 then
		afs_nag_mbs = sum(rcfd1713, rcfd1736);
	else if dt>=20090630 & dt<20110331 then
		afs_nag_mbs = sum(rcfdg311, rcfdg323, rcfdg327, rcfdg331);
	else afs_nag_mbs = sum(rcfdg311, rcfdg323, rcfdk149, rcfdk157);
	label afs_nag_mbs = 'Non Agency MBS';


	/* Asset Backed Securities */
	if dt<20010331 then
		afs_ab_sec=.;
	else if dt>=20010331 & dt<20060331 then
		afs_ab_sec = sum(rcfdb841, rcfdb845, rcfdb849, rcfdb853, rcfdb857, rcfdb861);
	else if dt>=20060331 & dt<20090630 then
		afs_ab_sec = rcfdc027;
	else afs_ab_sec = sum(rcfdc027, rcfdg339, rcfdg343, rcfdg347);
	label afs_ab_sec = 'Asset Backed Securities';

	/*Other Domestic Debt Securities */
	if dt<20010331 then
		afs_odom_dsec=.;
	else afs_odom_dsec = rcfd1741;
	label afs_odom_dsec = 'Other Domestic Debt Securities';

	/*Other Foreign Debt Securities */
	if dt<20010331 then
		afs_ofgn_dsec=.;
	else afs_ofgn_dsec = rcfd1746;
	label afs_ofgn_dsec = 'Other Foreign Debt Securities';

	/* Investments in mutual funds and other equity securities */
	if dt<20010331 then
		afs_inv_mf_eqsec=.;
	else afs_inv_mf_eqsec = rcfda511;
	label afs_inv_mf_eqsec = 'Investments in mutual funds and other equity securities with readily determinable fair values';



/************************************/;
/*FOR GRANULAR SECURITIES MODELLING */;
/************************************/;


	/*Schedule HC-B: HTM (Amortized Cost) + AFS (Fair Value)*/;

	/*HTM and AFS U.S. Treasury and Agency Securities*/;
	if dt<19940331 then
		htmafs_ust = .;
	else htmafs_ust = sum(rcfd0211, rcfd1289, rcfd1294, rcfd1287, rcfd1293, rcfd1298);
	label htmafs_ust= 'HTM and AFS U.S. Treasury and Agency Securities';

	/*HTM and AFS U.S. Treasury and Agency Securities*/;
	if dt<19940331 then
		htmafs_ust_bhc = .;
	else htmafs_ust_bhc = sum(rcfd0211, rcfd1289, rcfd1294,  rcfd1287, rcfd1293, rcfd1298);
	label htmafs_ust_bhc = 'HTM and AFS U.S. Treasury and Agency Securities: BHCs Only';


	/*HTM and AFS MBS*/;
	if dt<20010331 then
		htmafs_mbs = .;
	else if dt>=20010331 & dt<20090630 then
		htmafs_mbs = sum(rcfd1698, rcfd1703, rcfd1709, rcfd1714, rcfd1718, rcfd1733, rcfd1702, rcfd1707, rcfd1713, rcfd1717, rcfd1732, rcfd1736);
	else if dt>=20090630 & dt<20110331 then
		htmafs_mbs = sum(rcfdg300, rcfdg304, rcfdg308, rcfdg312, rcfdg316, rcfdg320, rcfdg324, rcfdg328, rcfdg303, rcfdg307, rcfdg311, rcfdg315, rcfdg319, rcfdg323, rcfdg327, rcfdg331);
	else htmafs_mbs = sum(rcfdg300, rcfdg304, rcfdg308, rcfdg312, rcfdg316, rcfdg320, rcfdk142, rcfdk146, rcfdk150, rcfdk154, rcfdg303, rcfdg307, rcfdg311, rcfdg315, rcfdg319, rcfdg323, rcfdk145, rcfdk149, rcfdk153, rcfdk157);
	label htmafs_mbs = 'HTM and AFS MBS';

	/*HTM and AFS MBS*/;
	if dt<20010331 then
		htmafs_mbs_bhc = .;
	else if dt>=20010331 & dt<20090630 then
		htmafs_mbs_bhc = sum(rcfd1698, rcfd1703, rcfd1709, rcfd1714, rcfd1718, rcfd1733, rcfd1702, rcfd1707, rcfd1713, rcfd1717, rcfd1732, rcfd1736);
	else if dt>=20090630 & dt<20110331 then
		htmafs_mbs_bhc = sum(rcfdg300, rcfdg304, rcfdg308, rcfdg312, rcfdg316, rcfdg320, rcfdg324, rcfdg328, rcfdg303, rcfdg307, rcfdg311, rcfdg315, rcfdg319, rcfdg323, rcfdg327, rcfdg331);
	else htmafs_mbs_bhc = sum(rcfdg300, rcfdg304, rcfdg308, rcfdg312, rcfdg316, rcfdg320, rcfdk142, rcfdk146, rcfdk150, rcfdk154, rcfdg303, rcfdg307, rcfdg311, rcfdg315, rcfdg319, rcfdg323, rcfdk145, rcfdk149, rcfdk153, rcfdk157);
	label htmafs_mbs_bhc = 'HTM and AFS MBS: BHCs Only';

	/*HTM All Other Securities*/;
	/*MDRM definition for ABS is wrong, b838 - b861 only available up until 2005q4*/;
	if dt<20010331 then
		htmafs_oth = .;
	else if dt>=20010331 & dt<20060331 then
		htmafs_oth = sum(rcfd1737, rcfd1742, rcfdb838, rcfdb842, rcfdb846, rcfdb850, rcfdb854, rcfdb858, rcfd1741, rcfd1746, rcfdb841, rcfdb845, rcfdb849, rcfdb853, rcfdb857, rcfdb861, rcfda511, rcfd8496, rcfd8499);
	else if dt>=20060331 & dt<20090630 then
		htmafs_oth = sum(rcfd1737, rcfd1742, rcfdc026, rcfd1741, rcfd1746, rcfda511, rcfdc027, rcfd8496, rcfd8499);
	else htmafs_oth = sum(rcfd1737, rcfd1742, rcfdc026, rcfdg336, rcfdg340, rcfdg344, rcfd1741, rcfd1746, rcfda511, rcfdc027, rcfdg339, rcfdg343, rcfdg347, rcfd8496, rcfd8499);
	label htmafs_oth = 'HTM All Other Securities';


	/*HTM All Other Securities*/;
	/*MDRM definition for ABS is wrong, b838 - b861 only available up until 2005q4*/;
	if dt<20010331 then
		htmafs_oth_bhc = .;
	else if dt>=20010331 & dt<20060331 then
		htmafs_oth_bhc = sum(rcfd1737, rcfd1742, rcfdb838, rcfdb842, rcfdb846, rcfdb850, rcfdb854, rcfdb858, rcfd1741, rcfd1746, rcfdb841, rcfdb845, rcfdb849, rcfdb853, rcfdb857, rcfdb861, rcfda511, rcfd8496, rcfd8499);
	else if dt>=20060331 & dt<20090630 then
		htmafs_oth_bhc = sum(rcfd1737, rcfd1742, rcfdc026, rcfd1741, rcfd1746, rcfda511, rcfdc027, rcfd8496, rcfd8499);
	else htmafs_oth_bhc = sum(rcfd1737, rcfd1742, rcfdc026, rcfdg336, rcfdg340, rcfdg344, rcfd1741, rcfd1746, rcfda511, rcfdc027, rcfdg339, rcfdg343, rcfdg347, rcfd8496, rcfd8499);
	label htmafs_oth_bhc = 'HTM All Other Securities: BHCs Only';


            	/* HTM U.S. government agency and corporation obligations (exclude mortgage-backed securities) Issued by U.S. government-sponsored agencies*/;
	if dt<19940331 then
		htmafs_gvsp_obl=.;
	else htmafs_gvsp_obl=sum(rcfd1294,rcfd1298);
	label htmafs_gvsp_obl = 'U.S. government agency obligations (exclude mortgage-backed securities - Issued by U.S. government-sponsored agencies';


        if dt<19940331 then
            htmafs_gov_obl = .;
        else htmafs_gov_obl=sum(rcfd1293,rcfd1289);
        label htmafs_gov_obl = 'U.S. government agency obligations (exclude mortgage-backed securities - Issued by U.S. government agencies';

        if dt<19940331 then
            htmafs_sec_sts=.;
        else if dt>=19940331 & dt<20010331 then
            htmafs_sec_sts=sum(rcfd1676,rcfd1681,rcfd1694,rcfd1679,rcfd1691,rcfd1697);
        else htmafs_sec_sts=sum(rcfd8496,rcfd8499);
        label htmafs_sec_sts='HTM securities issued by states and political subdivisions in the U.S.';


        if dt<20010331 then
            htmafs_agpth_mbs=.;
        else if dt>=20010331 & dt<20090630 then
            htmafs_agpth_mbs = sum(rcfd1698, rcfd1703,rcfd1702, rcfd1707);
	else if dt>=20090630 & dt<20110331 then
		htmafs_agpth_mbs = sum(rcfdg300, rcfdg304,rcfdg303, rcfdg307);
	else htmafs_agpth_mbs = sum(rcfdg300,rcfdg304, rcfdk142,rcfdg303, rcfdg307, rcfdk145);
	label htmafs_agpth_mbs = 'Agency Pass Through MBS';


           if dt<20010331 then
		htmafs_ag_cmos=.;
	else if dt>=20010331 & dt<20090630 then
		htmafs_ag_cmos = sum(rcfd1714, rcfd1718,rcfd1717, rcfd1732);
	else if dt>=20090630 & dt<20110331 then
		htmafs_ag_cmos = sum(rcfdg312, rcfdg316,rcfdg315, rcfdg319);
	else htmafs_ag_cmos = sum(rcfdg312, rcfdg316, rcfdk150,rcfdg315, rcfdg319, rcfdk153);
	label htmafs_ag_cmos = 'Agency CMOs';



            if dt<20010331 then
		htmafs_nag_mbs=.;
	else if dt>=20010331 & dt<20090630 then
		htmafs_nag_mbs = sum(rcfd1709, rcfd1733,rcfd1713, rcfd1736);
	else if dt>=20090630 & dt<20110331 then
		htmafs_nag_mbs = sum(rcfdg308, rcfdg320, rcfdg324, rcfdg328,rcfdg311, rcfdg323, rcfdg327, rcfdg331);
	else htmafs_nag_mbs = sum(rcfdg308, rcfdg320, rcfdk146, rcfdk154,rcfdg311, rcfdg323, rcfdk149, rcfdk157);
	label htmafs_nag_mbs = 'Non Agency MBS';


/* Asset Backed Securities */
	if dt<20010331 then
		htmafs_ab_sec=.;
	else if dt>=20010331 & dt<20060331 then
		htmafs_ab_sec = sum(rcfdb838, rcfdb842, rcfdb846, rcfdb850, rcfdb854, rcfdb858,rcfdb841, rcfdb845, rcfdb849, rcfdb853, rcfdb857, rcfdb861);
	else if dt>=20060331 & dt<20090630 then
		htmafs_ab_sec = sum(rcfdc026,rcfdc027);
	else htmafs_ab_sec = sum(rcfdc026, rcfdg336, rcfdg340, rcfdg344,rcfdc027, rcfdg339, rcfdg343, rcfdg347);
	label htmafs_ab_sec = 'Asset Backed Securities';

	/*Other Domestic Debt Securities */
	if dt<20010331 then
		htmafs_odom_dsec=.;
	else htmafs_odom_dsec =sum(rcfd1737,rcfd1741);
	label htmafs_odom_dsec = 'Other Domestic Debt Securities';

/*Other Foreign Debt Securities */
	if dt<20010331 then
		htmafs_ofgn_dsec=.;
	else htmafs_ofgn_dsec = sum(rcfd1742,rcfd1746);
	label htmafs_ofgn_dsec = 'Other Foreign Debt Securities';






	if dt<20010331 then
		ibb_inv_sec_nombs = .;
	else ibb_inv_sec_nombs = sum(ibb, htmafs_ust, htmafs_oth);
	label ibb_inv_sec_nombs='IBB + Investment Securities - MBS';


	/*AMORTIZED COST OF AFS AND HTM SECURITIES TOGETHER, BHCs ONLY*/;
	/*Amortized Cost AFS+HTM U.S. Treasury and Agency Securities*/;
	if dt<19940331 then
		amort_cost_htmafs_ust_bhc = .;
	else amort_cost_htmafs_ust_bhc = sum(rcfd0211, rcfd1289, rcfd1294, rcfd1286, rcfd1291, rcfd1297);
	label amort_cost_htmafs_ust_bhc = 'Amortized Cost HTM and AFS U.S. Treasury and Agency Securities: BHCs Only';

	/*Amortized Cost AFS+HTM MBS*/;
	if dt<19940331 then
		amort_cost_htmafs_mbs_bhc = .;
	else if dt>=19940331 & dt<19940930 then
		amort_cost_htmafs_mbs_bhc = sum(rcfd1698, rcfd1703, rcfd1701, rcfd1706, rcfd1711, rcfd1716, rcfd1731, rcfd1735);
	else if dt>=19940930 & dt<20090630 then
		amort_cost_htmafs_mbs_bhc = sum(rcfd1698, rcfd1703, rcfd1709, rcfd1714, rcfd1718, rcfd1733, rcfd1701, rcfd1706, rcfd1711, rcfd1716, rcfd1731, rcfd1735);
	else if dt>=20090630 & dt<20110331 then
		amort_cost_htmafs_mbs_bhc = sum(rcfdg300, rcfdg304, rcfdg308, rcfdg312, rcfdg316, rcfdg320, rcfdg324, rcfdg328, rcfdg302, rcfdg306, rcfdg310, rcfdg314, rcfdg318, rcfdg322, rcfdg326, rcfdg330);
	else amort_cost_htmafs_mbs_bhc = sum(rcfdg300, rcfdg304, rcfdg308, rcfdg312, rcfdg316, rcfdg320, rcfdk142, rcfdk146, rcfdk150, rcfdk154, rcfdg302, rcfdg306, rcfdg310, rcfdg314, rcfdg318, rcfdg322, rcfdk144, rcfdk148, rcfdk152, rcfdk156);
	label amort_cost_htmafs_mbs_bhc = 'Amortized Cost HTM and AFS MBS: BHCs Only';

	/*Amortized Cost AFS+HTM All Other Securities*/;
	if dt<20010331 then
		amort_cost_htmafs_oth_bhc = .;
	else if dt>=20010331 & dt<20060331 then
		amort_cost_htmafs_oth_bhc = sum(rcfd1737, rcfd1742, rcfdb838, rcfdb842, rcfdb846, rcfdb850, rcfdb854, rcfdb858, rcfd1739, rcfd1744, rcfdb840, rcfdb844, rcfdb848, rcfdb852, rcfdb856, rcfdb860, rcfda510, rcfd8496, rcfd8498);
	else if dt>=20060331 & dt<20090630 then
		amort_cost_htmafs_oth_bhc = sum(rcfd1737, rcfd1742, rcfdc026, rcfd1739, rcfd1744, rcfda510, rcfdc989, rcfd8496, rcfd8498);
	else amort_cost_htmafs_oth_bhc =sum(rcfd1737, rcfd1742, rcfdc026, rcfdg336, rcfdg340, rcfdg344, rcfd1739, rcfd1744, rcfda510, rcfdc989, rcfdg338, rcfdg342, rcfdg346, rcfd8496, rcfd8498);
	label amort_cost_htmafs_oth_bhc = 'AMortized Cost HTM and AFS All Other Securities: BHCs Only';


	/*********************************/
	/*Schedule HC-B AND Schedule HC-D*/
	/*********************************/;


	/*US Treasury Securities*/;

	if dt < 19940331 then
	ust_trad = .;
	else if dt>=19940331 & dt <20080331 then
	ust_trad = rcon3531;
	else ust_trad = rcfd3531 ;
	label ust_trad='U.S. Treasury (trading) in domestic offices' ;

	if dt < 19940331 then
	ust_afs = .;
	else ust_afs = rcfd1287 ;
	label ust_afs='U.S. Treasury Securities (AFS)' ;

	if dt < 19940331 then
	ust_htm = .;
	else ust_htm = rcfd0211 ;
	label ust_htm='U.S. Treasury Securities (HTM)' ;

	/*HTM (Amortized Cost) + AFS (Fair Value) + Trading Securities*/;
	if dt<19940331 then
	ust_sec=.;
	ust_sec = sum(ust_trad, ust_afs, ust_htm);
	label ust_sec='U.S. Treasury Securities';

	/*US Agency Securities*/;

	if dt < 19940331 then
	agency_trad = .;
	else if dt>=19940331 & dt <20080331 then
	agency_trad = rcon3532;
	else agency_trad = rcfd3532 ;
	label agency_trad='U.S. Govt Agency Obligations (trading) in domestic offices' ;





        	/*US Muni Securities*/;

	if dt < 19940331 then
	muni_trad = .;
	else if dt>=19940331 & dt <20080331 then
	muni_trad = rcon3533;
	else muni_trad = rcfd3533 ;
	label muni_trad='U.S. Muni (trading)' ;

	if dt < 19940331 then
	muni_afs= .;
	else if dt>=19940331 & dt <20010331 then
	muni_afs = sum(rcfd1679, rcfd1691, rcfd1697);
	else muni_afs = rcfd8499 ;
	label muni_afs='Muni Securities (AFS)' ;

	if dt < 19940331 then
	muni_htm= .;
	else if dt>=19940331 & dt <20010331 then
	muni_htm=sum(rcfd1676, rcfd1681, rcfd1694);
	else muni_htm = rcfd8496 ;
	label muni_htm='Muni Securities (HTM)' ;

	/*HTM (Amortized Cost) + AFS (Fair Value) + Trading Securities*/;
	if dt<19940331 then
	muni_sec=.;
	else muni_sec = sum(muni_trad, muni_afs, muni_htm);
	label muni_sec='U.S. Muni Securities';


*****************************


        /*	Trading Assets		*/;
        /*	Date Range: 1984q1 to present		*/;
        if dt<19840331 then
            trad_ass = . ;
        else if dt>=19840331 & dt<=19931231 then
            trad_ass = rcfd2146;
        else trad_ass = rcfd3545;
        label trad_ass = 'Trading Assets';

	/*	Interest Earning Assets		*/
	/*	Date Range: 1984q1 to present		*/
		if dt<19840331 then
			earn_ass = . ;
		else if dt>=19840331 & dt<=19871231 then
		    earn_ass = sum(rcfd0071, rcfd0390,rcfd1350, rcfd2122, rcfd2146);
		else if dt>=19880331 & dt<=19931231 then
		    earn_ass = sum(rcfd0071, rcfd0390, rcfd1350, rcfd2122, rcfd2146);
		else if dt>=19940331 & dt<=19961231 then
		    earn_ass = sum(rcfd0071, rcfd1754, rcfd1773, rcfd1350, rcfd2122, rcfd3545);
		else if dt>=19970331 & dt<=20011231 then
		    earn_ass = sum(rcfd0071, rcfd1754, rcfd1773, rcfd1350, rcfd2122, rcfd3545);
		else earn_ass = sum(rcfd0071, rcfd1754, rcfd1773, rconb987, rcfdb989, rcfd2122, rcfd3545);
		label earn_ass = 'Interest Earning Assets';

	/* Investments in Unconsolidated subsidiaries (assets) */ ;
		unconsub_inv = . ;
		if dt <20090630 then unconsub_inv = sum(rcfd2130,-rcfd5374);
		else unconsub_inv = rcfd2130;

    /* Direct and Indirect Investments in RE Ventures (assets) */ ;
    /* Different from the BHCs*/ ;
		reven_inv = . ;
		if dt<20090630 then reven_inv = rcfd5374;
		else reven_inv = rcfd3656;

	/* Other Assets */;
		if dt < 19810630 then
        oth_assets = .;
        else if dt<20060331 then oth_assets = sum(rcfd2160, rcfd2155);
        else oth_assets = rcfd2160;
        label oth_assets = 'Other assets (consistant)';

        /*All Other Assets*/
	 alloth_assets=sum(fixed_ass, oreo, unconsub_inv, goodwill, intang_oth, oth_assets);

     /************************************************************
     **********     LIABILITIES  *********************************
     ************************************************************/

	/* Total Deposits */
	/* Date Range: 1984q1 to present */
		if dt<19840331 then
		tot_deposit = . ;
		else tot_deposit = sum(rcon6631, rcon6636, rcfn6631, rcfn6636);
		label tot_deposit = 'Total Deposits';

	/* Total Deposits Alternative Construction */
	/* Date Range: 1969q2 to present */
		if dt<19690630 then
		tot_deposit_alt = . ;
		else tot_deposit_alt = rcfd2200;
		label tot_deposit_alt = 'Total Deposits Alternative Construction';

    /* Total Trading Liabilities */
    /* Date Range: 1994q1 to present */
		if dt<19940331 then
		trad_liab = .;
		else trad_liab = rcfd3548;
		label trad_liab = 'Total Trading Liabilities';

    /* Other Borrowed Money */
    /* Date Range: 1984q1 to present */
		if dt < 19840331 then
		othbor_liab = .;
		else if dt >= 19840331 and dt <19940331 then
		othbor_liab = sum(rcfd2850,rcfd2910);
		else if dt >= 19940331 and dt <19970331 then
 		othbor_liab = sum(rcfd2332,rcfd2333,rcfd2910);
		else if dt >=19970331 and dt<19970630 then
		othbor_liab = sum(rcfd2332,rcfd2333);
		else if dt >= 19970630 and dt <20010331 then
		othbor_liab = sum(rcfd2332,rcfda547,rcfda548);
		else if dt >= 20010331 then
		othbor_liab = rcfd3190;
		label othbor_liab = 'Other Borrowed Money';

    /* Subordinated notes and debentures (including limited life preferred stock) */
    /* Date range: 1969q2 to present */
		subdebt = rcfd3200 ;
		if dt>=19840331 and dt<19970331 then
		subdebt= sum(rcfd3200,rcfd3282) ;
		label subdebt='Subordinated notes and debentures';

	/* Other Liabilities */
	/* Date Range: */
        if dt < 19900331 then
        liab_oth = . ;
        else if dt <20010331 then liab_oth = sum(rcfd2930, rcfd2920, -rcfd3000);
        else if dt <20060331 then liab_oth = sum(rcfd2930, rcfd2920);
        else liab_oth = rcfd2930;
        label liab_oth = 'Other Liabilities';


    /* Deposits, Domestic & Total */;
		if dt < 19810630 then do ;
		dom_deposit_ib= . ;
		dom_deposit_nib= . ;
		for_deposit = . ;
		for_deposit_ib = . ;
		for_deposit_nib = . ;
		end ;
		else do ;
		dom_deposit_ib= rcon6636 ;
		dom_deposit_nib= rcon6631 ;
	    for_deposit = sum(rcfn6631,rcfn6636) ;
		for_deposit_ib = rcfn6636 ;
		for_deposit_nib = rcfn6631 ;
	    end ;
        label for_deposit='Foreign Deposits' ;
        label dom_deposit_ib='Interest Bearing Domestic Deposits' ;
        label dom_deposit_nib='Non Interest Bearing Domestic Deposits' ;
        label for_deposit_ib='Interest Bearing Foreign Deposits' ;
        label for_deposit_nib='Non Interest Bearing Foreign Deposits' ;


		if dt < 19810630 then
		ib_deposit=.;
		else ib_deposit = sum(dom_deposit_ib, for_deposit_ib);
		label ib_deposit='Interest-bearing Deposits' ;

		if dt < 19810630 then
		nib_deposit=.;
		else nib_deposit = sum(dom_deposit_nib, for_deposit_nib);
	    label nib_deposit='Noninterest-bearing Deposits' ;


	/* "Unstable" and "Stable" Constructed Variables */;
	if dt < 19900331 then
		stable_deposits = . ;
		else stable_deposits = rcon2385 ;
		label stable_deposits = 'Total Nontransaction Accounts (including MMDAs)' ;

	if dt < 19900331 then
		unstable_deposits = . ;
		else unstable_deposits = sum(rcon2215, for_deposit_nib, for_deposit_ib) ;
		label unstable_deposits = 'Foreign Deposits and Total Transaction Accounts (including Total Demand Deposits)' ;



	/* Transaction Accounts */;
		transaction_dep=rcon2215;

	/* Money Market Deopsit and Other Saving Accounts (TOTAL) */;
		savings_dep=sum(rcon6810,rcon0352);

	/* Time Deposits of Less than 100k (TOTAL) */;
		time_lt100k_dep=rcon6648;

	/* Time Deposits of More than 100k (TOTAL) */;
        if dt <19970331 then
 		time_gt100k_dep=sum(rcon6645,rcon6646);
        else if dt <20100331 then
		time_gt100k_dep=rcon2604;
	    else time_gt100k_dep=sum(rconJ473,rconJ474);

	/* Brokered Deposits of less than 100k */;
		brokered_lt100k_dep=sum(rcon2343);

	/* Brokered Deposits of less than 250k */;
		brokered_lt250k_dep=sum(rcon2343,rconJ472);

/* Note that we think that minority interests used to be a part of other liabilities pre-2001, but was broken out as a separate item after 2001, hence the variable change */;
/* Total Liabilities */;
        if dt < 19840331 then
        liab_tot = . ;
        else if dt <19970331 then liab_tot = sum(rcfd2948, -rcfd3000, rcfd3282);
		else if dt <20010331 then liab_tot = sum(rcfd2948, -rcfd3000);
		else liab_tot = rcfd2948;
        label liab_tot ='Total Liabilities' ;

        /* Total Liabilities - unadjusted */;
        /* This series is just equal to the total liabilities line item */;
        if dt<19690630 then liab_tot_unadj = . ;
        else liab_tot_unadj=rcfd2948;
        label liab_tot_unadj='Total Liabilities (RCFD2948)';


        if dt<19690330 then liab_tot_2950=.;
        else if dt>=19690630 & dt<=19751231 then
            liab_tot=sum(rcfd2950,rcfd3000,rcfd3200);
        else if dt>=19760331 & dt<=19831231 then
            liab_tot_2950=sum(rcfd2950,rcfd3200);
        else if dt>=19840331 & dt<19970331 then
            liab_tot_2950=sum(rcfd2948,rcfd3282);
        else if dt>=19970331 & dt<200103331 then
            liab_tot_2950=rcfd2948;
        else liab_tot_2950=sum(rcfd2948,rcfd3000);
        label liab_tot_2950='Total Liabilitites (RCFD2950)';



	/* Federal Funds Purchased */
	/* Date range (rcfd0278): 1988q3 to 1996q4 */
	/* Date range (rconb993): 2002q1 to present */
	/***********Data missing 1997-2001***********/
		ffpurch = .;
       	if dt <= 19961231 then ffpurch = rcfd0278 ;
       	if dt > 19961231 & dt<20030331 then ffpurch=.;
       	if dt >=20020331 then ffpurch = rconb993 ;
       	label ffpurch='Federal Funds Purchased';

	/* Securities Sold Under Agreement to Repurchase */
	/* Date range (rcfd0279): 1988q3 to 1996q4 */
	/* Date range (rcfdb995): 2002q1 to present */
	/* **********Data missing 1997-2001***********/
		repo_sold = .;
		if dt <= 19961231 then repo_sold = rcfd0279 ;
		if dt >= 20020331 then repo_sold = rcfdb995 ;
		label repo_sold='Securities Sold Under Agreement, Etc';

    /* FF Purchased/Sec Sold Under Agreements to Repurchase */
    /* Date range : 1969q2 to present */
       	ffrepo_liab = rcfd2800;
       	if (dt>=20020331 & ffrepo_liab=.) then ffrepo_liab = sum(rconb993,rcfdb995);
       	label ffrepo_liab='FF Purchased/Sec Sold Under Agreements to Repurchase';

    /* Minority Interest in consolidated subsidiaries*/
    /* Date range: 1984q1 to present */
    /*  - reported under liabilities, 1984q1 - 2000q4 */
    /*  - reported as separate contra-asset, 2001q1 - present */
     	minorint = . ;
     	minorint = rcfd3000 ;
     	label minorint='Minority interest' ;

		ustdem_liab = rcon2840;
		minint_liab = rcfd3000;


          /**********************************
          *****      EQUITY CAPITAL     *****
          **********************************/

	/* Total Equity EXCLUDING noncontrolling (minority) interests in consolidated subsidiaries*/
	/* Date range: 1969q2 to present */
		equity = . ;
		equity  = rcfd3210 ;
		label equity ='Total Equity' ;

     /************************************************************
     **********     SCHEDULE RC-C: LOANS AND LEASES     **********
     ************************************************************/

	/* Total Loans and Leases Net of Unearned Income  */
	/* Date Range: 1976q1 to present  */
		if dt<19760331 then
		ln_tot = . ;
		else ln_tot = rcfd2122;
		label ln_tot = 'Total Loans and Leases Net of Unearned Income';

	/* Credit Cards		*/
	/* Date Range: 1984q1 to present		*/
		ln_cc = .;
    	if dt <= 20001231 then ln_cc = rcfd2008 ;
    	else ln_cc = rcfdb538;
		label ln_cc ='Credit Cards';

   /* REAL ESTATE */
   /* Real Estate Loans */
   /* Date range: 1978q4 to present */
		ln_re = .;
       	ln_re = rcfd1410;
       	label ln_re='Real Estate Loans';

		if dt <19910331 then do;
       	ln_heloc = .;
       	ln_closedlien = .;
       	ln_firstlien = .;
       	ln_jrlien = .;
   		end;
        else do;
        ln_heloc = rcon1797;
        ln_closedlien = sum(rcon5367,rcon5368);
        ln_firstlien = rcon5367;
        ln_jrlien = rcon5368;
        end;

		label ln_heloc = '1-4 Family HELOCs (Domestic)' ;
		label ln_closedlien = '1-4 Family Closed Lien Mortgages (Domestic)';
		label ln_firstlien = '1-4 Family 1st Lien Mortgages (Domestic)';
		label ln_jrlien = '1-4 Family Jr Lien Mortgages (Domestic)';

	/* Commercial Real Estate Loans */
    /* Construction and land development loans*/
      	if dt<19840331 then do;
        ln_const=.;
        end;
        if dt>=19840331 & dt<20070331 then do;
		ln_const=rcon1415;
        end;
        if dt>=20070331 then do;
        ln_const=rcon1415;
        end;
        label ln_const='Construction loans';

    /* Multi-family resid. property loans */
		if dt<19840331 then
		ln_multi=.;
		else ln_multi=rcon1460;
		label ln_multi='Multi-family property loans';

	/* Nonfarm, nonresidential real estate loans */
		if dt<19591231 then do;
		ln_nfnr=.;
		end;
		if dt>=19591231 & dt<20070331 then do;
		ln_nfnr=rcon1480;
		end;
		if dt>=20070331 then do;
		ln_nfnr=rcon1480;
		end;
		label ln_nfnr='Nonfarm, nonres CRE loans';

	/*	Domestic Deposits 	*/
	/*	Date Range: 1984q1 to present		*/
		if dt<19840331 then
			dom_deposit = . ;
		else dom_deposit = sum(rcon6631, rcon6636);
		label dom_deposit = 'Domestic Deposits';


	/* 1-4 Family Residential Real Estate (Domestic) */
    /* Date Range: 1984q1 to present */
		if dt<19840331 then
		ln_rre= . ;
        else if dt>=19840331 & dt<=19870930 then
      	ln_rre= rcon1430;
        else if dt>=19871231 & dt<=19901231 then
        ln_rre= sum(rcon1797, rcon1798);
        else ln_rre= sum(rcon1797, rcon5367, rcon5368) ;
        label ln_rre= '1-4 Family Residential Real Estate (Domestic)';

	/* Total Commercial Real Estate Loans */
    /* Date Range: 1984q1 to present */
        if dt<19840331 then
        ln_cre = . ;
        else if dt>=19840331 & dt<=20061231 then
        ln_cre = sum(rcon1415, rcon1460, rcon1480);
        else ln_cre = sum(rconf158, rconf159, rcon1460, rconf160, rconf161);
       	label ln_cre = 'Total Commercial Real Estate Loans';

	/* C&I Loans */
    /* Date Range: 1984q1 to present */
        if dt<19840331 then
		ln_ci = . ;
		else ln_ci = sum(rcfd1763,rcfd1764);
		label ln_ci ='C&I Loans';

	/* Other Real Estate loans */
	/* Date range: 1978q4 to present */
		ln_othre =sum(ln_re, -ln_rre, -ln_cre) ;
		label ln_othre='Other Real Estate loans' ;
		ln_farm=rcon1420;

   	/* Depository Institutions */
   	/* Loans to Other Depository Institutions */
    /* Date range: 1984q1 to present */
		ln_dep = . ;
		if dt >= 19840331 & dt <20010630 then ln_dep = sum(rcfd1489,rcfd1755);
       	if dt >= 20010630 then ln_dep = rcfd1288 ;
       	label ln_dep='Loans to Other Depository Institutions' ;

	/* Agriculture */
    /* Loans to Finance Agricultural Production */
    /* Date range: 1978q4 to present */
     	ln_agr = . ;
    	if dt >= 19781231 then ln_agr = rcfd1590;
   		label ln_agr='Loans to Finance Agricultural Production' ;

   	/* Individuals for household...*/
   	/* Consumer Loans */
	/* Date range: 1978q4 to present */
		ln_cons = .;
		ln_cons = rcfd1975;
		label ln_cons='Consumer Loans';

	/* Other Consumer Loans */
	/* Date range: 1984q1 to present */
       	ln_othcons = .;
       	if dt < 20110331 then ln_othcons = sum(rcfd2011,rcfdb539) ;
       	else ln_othcons = sum(rcfdb539,rcfdk137,rcfdk207);
       	label ln_othcons='Other Consumer loans (Installment loans)' ;

    /* Loans to foreign governments */
    /* Date range: 1978q4 to present */
       	ln_fgovt = . ;
        ln_fgovt = rcfd2081;
        label ln_fgovt='Loans to foreign governments';

	/* Loans to Non-Depository Financial Institutions and Other Loans */
    /* Date range: 1984q1 to present */
       	ln_oth=.;
       	ln_oth=rcfd1563;
    	label ln_oth='Other loans (Including Loans to Non-Dep Financial Institutions)' ;

	/* All Other Loans*/
    /* Date range: 1984q1 to present */
       	ln_allother=.;
       	ln_allother=sum(ln_lease,ln_oth,ln_agr,ln_othre,ln_dep,ln_fgovt);
    	label ln_allother='All Other Loans)' ;

	/* Loans to lease financing receivables (net of unearned income) */
	/* Date range: 1973q2 to present */
       	ln_lease = . ;
       	ln_lease = rcfd2165;
       	label ln_lease='Lease financing receivables';

	/*Lease Financing Receivables - BHCs only*/
		ln_lease_bhc=ln_lease;
		label ln_lease_bhc = 'Lease Financing Receivables - BHCs Only';

    /* ****AFS SECURITIES PORTFOLIO**** */
		afssec=rcfd1773;
		htmsec=rcfd1754;

  	/* U.S. Treasury Securities */;
    	afs_ust=rcfd1287;

	/* U.S. Agency Securities */;
	   	afs_agency=sum(rcfd1293,rcfd1298);

                	if dt < 19940331 then
	agency_htm= .;
	else agency_htm = sum(rcfd1289, rcfd1294) ;
	label agency_htm='Agency Securities (HTM)' ;

        	/*HTM (Amortized Cost) + AFS (Fair Value) + Trading Securities*/;
	if dt<19940331 then
	agency_sec=.;
	else agency_sec = sum(agency_trad, afs_agency, agency_htm);
	label agency_sec='U.S. Agency Securities';

        	/*Other Securities*/;

	if dt < 19940331 then
	oth_trad_sec = .;
	else if dt>=19940331 & dt<20080331 then
	oth_trad_sec = rcon3537;
	else if dt>=20080331 & dt<20090630 then
	oth_trad_sec = rcfd3537;
	else oth_trad_sec = sum(rcfdg383, rcfdg384, rcfdg385, rcfdg386) ;
	label oth_trad_sec='Other debt securities (trading) ' ;



        afs_agency_mbs = . ;
        if dt >= 19940331 & dt <= 20090331 then
        afs_agency_mbs = sum(rcfd1702,rcfd1707,rcfd1717,rcfd1732);
    	else if dt >= 20090630 & dt < 20110331 then
        afs_agency_mbs = sum(rcfdg303,rcfdg307,rcfdg315,rcfdg319);
        else if dt>=20110331 then
        afs_agency_mbs = sum(rcfdg303,rcfdg307,rcfdg315,rcfdg319,rcfdk145,rcfdk149);
        label afs_agency_mbs = 'GNMA-, FNMA-, FHLMC- issued, secured, etc., AFS MBS';

        if dt < 19810630 then
        afs_agency_nonmbs = .;
        else if dt >= 19810630 & dt < 19940331 then
        afs_agency_nonmbs = rcfd0600 ;
        else if dt >= 19940331 & dt < 20010331 then
        afs_agency_nonmbs = rcfd8495;
        else afs_agency_nonmbs = sum(rcfd1293,rcfd1298) ;
        label afs_agency_nonmbs ='U.S. Agency Non-MBS AFS Securities' ;

	/* RMBS/CMBS breakdown */
        agency_rmbs = sum(rcfdg300,rcfdg304,rcfdg312,rcfdg316,rcfdg303,rcfdg307,rcfdg315,rcfdg319);
        nonagency_rmbs = sum(rcfdg308,rcfdg320,rcfdg311,rcfdg323);
        if dt < 20110331 then do;
		afs_agency_cmbs = .;
		afs_nonagency_cmbs = .;
		total_cmbs = sum(rcfdg324,rcfdg328,rcfdg327,rcfdg331);
		end;
        else do;
		afs_agency_cmbs = rcfdk145;
		afs_nonagency_cmbs = rcfdk153;
		total_cmbs = sum(rcfdk142,rcfdk146,rcfdk150,rcfdk154,rcfdk145,rcfdk149,rcfdk153,rcfdk157);
        end;
        afs_state = rcfd8499;

        /*Agency MBS*/;

	/*Note: in 200906, commercial mbs is broken out, but not broken out into agency and nonagency - put all of this in nonagency*/
	/*In 2011, commercial mbs broken out into agency and nonagency*/;

	if dt < 19940331 then
	agency_mbs_trad = .;
	else if dt>=19940331 & dt <20080331 then
	agency_mbs_trad = sum(rcon3534, rcon3535);
	else if dt>=20080331 & dt <20090630 then
	agency_mbs_trad=sum(rcfd3534, rcfd3535);
	else if dt>=20090630 & dt <20110331 then
	agency_mbs_trad=sum(rcfdg379, rcfdg380);
	else agency_mbs_trad = sum(rcfdg379, rcfdg380, rcfdk197) ;
	label agency_mbs_trad='Agency MBS (trading)' ;

        	/*Note: in 2009, commercial mbs is broken out, but not broken out into agency and nonagency - put all of this in nonagency*/
	/*In 2011, commercial mbs broken out into agency and nonagency*/;
	if dt < 19940331 then
	agency_mbs_afs= .;
	else if dt>=19940331 & dt <20090630 then
	agency_mbs_afs = sum( rcfd1702, rcfd1707, rcfd1717, rcfd1732);
	else if dt>=20090630 & dt<20110331 then
	agency_mbs_afs = sum(rcfdg303, rcfdg307, rcfdg315,rcfdg319) ;
	else agency_mbs_afs = sum(rcfdg303, rcfdg307, rcfdg315,rcfdg319, rcfdk145, rcfdk153);
	label agency_mbs_afs='Agency MBS (AFS)' ;

	/*Note: in 2009, commercial mbs is broken out, but not broken out into agency and nonagency - put all of this in nonagency*/
	/*In 2011, commercial mbs broken out into agency and nonagency*/;
	if dt < 19940331 then
	agency_mbs_htm= .;
	else if dt>=19940331 & dt <20090630 then
	agency_mbs_htm = sum(rcfd1698, rcfd1703, rcfd1714, rcfd1718);
	else if dt>=20090630 & dt <20110331 then
	agency_mbs_htm = sum(rcfdg300, rcfdg304, rcfdg312, rcfdg316);
	else agency_mbs_htm = sum(rcfdg300, rcfdg304, rcfdg312, rcfdg316, rcfdk142, rcfdk150) ;
	label agency_mbs_htm='Agency MBS (HTM)' ;

        	/*HTM (Amortized Cost) + AFS (Fair Value) + Trading Securities*/;
	if dt<19940331 then
	agency_mbs=.;
	else agency_mbs = sum(agency_mbs_trad, agency_mbs_afs, agency_mbs_htm);
	label agency_mbs='Agency MBS';

        	/*Non-agency MBS*/;

	if dt < 19940331 then
	nonagency_mbs_trad = .;
	else if dt>=19940331 & dt <20080331 then
	nonagency_mbs_trad = rcon3536;
	else if dt>=20080331 & dt <20090630 then
	nonagency_mbs_trad=rcfd3536;
	else if dt>=20090630 & dt <20110331 then
	nonagency_mbs_trad=sum(rcfdg381,rcfdg382);
	else nonagency_mbs_trad = sum(rcfdg381, rcfdk198) ;
	label nonagency_mbs_trad='Non-Agency MBS (trading) ' ;

        	if dt < 19940331 then
	nonagency_mbs_afs= .;
	else if dt>=19940331 & dt <20090630 then
	nonagency_mbs_afs = sum(rcfd1713, rcfd1736);
	else if dt>=20090630 & dt <20110331 then
	nonagency_mbs_afs = sum(rcfdg311, rcfdg323, rcfdg327, rcfdg331);
	else nonagency_mbs_afs = sum(rcfdg311, rcfdg323, rcfdk149, rcfdk157) ;
	label nonagency_mbs_afs='Non-Agency MBS (AFS)' ;

        	/*Note: in 2009, commercial mbs is broken out, but not broken out into agency and nonagency - put all of this in nonagency*/
	/*In 2011, commercial mbs broken out into agency and nonagency*/;

	if dt < 19940331 then
	nonagency_mbs_htm= .;
	else if dt>=19940331 & dt <20090630 then
	nonagency_mbs_htm = sum(rcfd1709, rcfd1733) ;
	else if dt>=20090630 & dt <20110331 then
	nonagency_mbs_htm = sum(rcfdg308, rcfdg320, rcfdg324, rcfdg328);
	else nonagency_mbs_htm = sum(rcfdg308, rcfdg320, rcfdk146, rcfdk154) ;
	label nonagency_mbs_htm='Non-Agency MBS (HTM)' ;

	/*HTM (Amortized Cost) + AFS (Fair Value) + Trading Securities*/;
	if dt<19940331 then
	nonagency_mbs=.;
	else nonagency_mbs = sum(nonagency_mbs_trad, nonagency_mbs_afs, nonagency_mbs_htm);
	label nonagency_mbs='Non-Agency MBS';


	if dt<19940331 then
	oth_afs_sec = .;
	else oth_afs_sec=sum(afssec,-afs_agency,-muni_afs,-agency_mbs_afs, -nonagency_mbs_afs);

	if dt<19940331 then
	oth_htm_sec = .;
	else oth_htm_sec=sum(htmsec,-agency_htm,-muni_htm,-agency_mbs_htm, -nonagency_mbs_htm);



	/*HTM (Amortized Cost) + AFS (Fair Value) + Trading Securities*/;

	if dt<19940331 then
	oth_sec = .;
	else oth_sec=sum(oth_trad_sec,oth_htm_sec,oth_afs_sec);
	label oth_sec='Other Securities';


	/***********************************/;
	/**********Schedule RC-D************/;
	/***********************************/;


	/***********/;
	/**Assets**/;
	/***********/;

	/*U.S. Treasury Securities*/
	if dt<19940331 then
		trad_ust_sec =.;
	else if dt>=19940331 & dt<20080331 then
		trad_ust_sec =rcon3531;
	else trad_ust_sec = rcfd3531;
	label trad_ust_sec = 'U.S. Treasury Securities';


	/*U.S. government agency obligations (exclude mortgage-backed securities)*/
	if dt<19940331 then
		trad_us_gobl=.;
	else if dt>=19940331 & dt<20080331 then
		trad_us_gobl =rcon3532;
	else trad_us_gobl=rcfd3532;
	label trad_us_gobl = 'U.S. government agency obligations (exclude mortgage-backed securities)';

	/*Securities issued by states and political subdivisions in the U.S. */
	if dt<19940331 then
		trad_sec_sts =.;
	else if dt>=19940331 & dt<20080331 then
		trad_sec_sts = rcon3533;
	else trad_sec_sts=rcfd3533;
	label trad_sec_sts = 'Securities issued by states and political subdivisions in the U.S.';

	/*Agency pass through MBS - includes residential AND commercial*/
	if dt<19940331 then
		trad_agpth_mbs=.;
	else if dt>=19940331 & dt<20080331 then
		trad_agpth_mbs = rcon3534;
	else if dt>=20080331 & dt<20090630 then
		trad_agpth_mbs = rcfd3534;
	else trad_agpth_mbs=.;
	label trad_agpth_mbs = 'Agency Pass Through MBS';

	/*Residential Agency Pass Through MBS */
	if dt<20090630 then
		trad_ragpth_mbs=.;
	else trad_ragpth_mbs=rcfdg379;
	label trad_ragpth_mbs = 'Residential Agency Pass Through MBS';

	/*Agency CMOS*/
	if dt<19940331 then
		trad_ag_cmos =.;
	else if dt>=19940331 & dt<20080331 then
		trad_ag_cmos = rcon3535;
	else if dt>=20080331 & dt<20090630 then
		trad_ag_cmos = rcfd3535;
	else trad_ag_cmos=.;
	label trad_ag_cmos = 'Agency CMOS';

	/*Residential Agency CMOS*/
	if dt<20090630 then
		trad_rag_cmos=.;
	else trad_rag_cmos = rcfdg380;

	/*Non-agency MBS */
	if dt<19940331 then
		trad_nag_mbs=.;
	else if dt>=19940331 & dt<20080331 then
		trad_nag_mbs=rcon3536;
	else if dt>=20080331 & dt<20090630 then
		trad_nag_mbs = rcfd3536;
	else trad_nag_mbs =.;
	label trad_nag_mbs= 'Non-agency MBS';

	/*Residential Non-Agency MBS*/
	if dt<20090630 then
		trad_rnag_mbs=.;
	else trad_rnag_mbs = rcfdg381;
	label trad_rnag_mbs = 'Residential Non-Agency MBS';

	/*Commercial MBS - includes agency AND non-agency MBS*/
	if dt<20090630 then
		trad_com_mbs=.;
	else if dt>=20090630 & dt<20110331 then
		trad_com_mbs=rcfdg382;
	else trad_com_mbs = sum(rcfdk197, rcfdk198);
	label trad_com_mbs = 'Commercial MBS';

	/*Commercial MBS - issued by government agencies: includes both pass through AND CMOs*/
	if dt<20110331 then
		trad_com_ag_mbs =.;
	else trad_com_ag_mbs = rcfdk197;
	label trad_com_ag_mbs = 'Commercial MBS issued or guaranteed by FNMA, GHLMC, or GNMA';

	/*Non-agency Commercial MBS - All Other Commercial MBS*/
	if dt<20110331 then
		trad_com_nag_mbs=.;
	else trad_com_nag_mbs = rcfdk198;
	label trad_com_nag_mbs = 'All Other Commercial MBS - Non-agency Commercial MBS';

	/*Other Debt Securities*/
	if dt<19940331 then
		trad_othdsec=.;
	else if dt>=19940331 & dt<20080331 then
		trad_othdsec=rcon3537;
	else if dt>=20080331 & dt<20090630 then
		trad_othdsec=rcfd3537;
	else trad_othdsec = sum(rcfdg383, rcfdg384, rcfdg385, rcfdg386);
	label trad_othdsec = 'Other Debt Securities';

	/*Certificates of deposit in domestic offices*/
	if dt<19940331 then
		trad_certdp_d=.;
	else if dt>=19940331 & dt<19980331 then
		trad_certdp_d = rcon3538;
	else trad_certdp_d=.;
	label trad_certd_d = 'Certificates of deposit in domestic offices';

	/*Commercial Paper in domestic offices*/
	if dt<19940331 then
		trad_comppr_d=.;
	else if dt>=19940331 & dt<19980331 then
		trad_comppr_d =rcon3539;
	else trad_comppr_d =.;
	label trad_comppr_d = 'Commercial paper in domestic offices';

	/*Bankers acceptances in domestic offices*/
	if dt<19940331 then
		trad_bkccp_d=.;
	else if dt>=19940331 & dt<19980331 then
		trad_bkccp_d=rcon3540;
	else trad_bkccp_d = .;
	label trad_bkccp_d = 'Banker acceptances in domestic offices';

	/*Other trading assets - Called Other trading assets in domestic offices prior to 2008Q1 */
	if dt<19940331 then
	 	trad_othass=.;
	else if dt>=19940331 & dt<20080331 then
		trad_othass=rcon3541;
	else trad_othass=rcfd3541;
	label trad_othass = 'Other Trading Assets';

	/*Trading Assets in foreign offices*/
	if dt<19940331 then
		trad_assfor=.;
	else if dt>=19940331 & dt<20080331 then
		trad_assfor=rcfn3542;
	else trad_assfor=.;
	label trad_assfor = 'Trading Assets in foreign offices';

	/*Derivatives with a positive fair value (also known as Revaluation gains on interest rate, foreign exchange rate, equity, commodity and other contracts)*/
	if dt<19940331 then
		trad_dpos_fv=.;
	else if dt>=19940331 & dt<20080331 then
		trad_dpos_fv = sum(rcon3543, rcfn3543);
	else trad_dpos_fv=rcfd3543;
	label trad_dpos_fv = 'Derivatives with a positive fair value';

	/*Loans Secured by Real Estate*/
	if dt<20080331 then
		trad_ln_res=.;
	else trad_ln_res=rcfdf610;
	label trad_ln_res = 'Loans Secured by Real Estate';

	/*Commercial and Industrial Loans*/
	if dt<20080331 then
		trad_ln_comind=.;
	else trad_ln_comind = rcfdf614;
	label trad_ln_comind = 'Commercial and Industrial Loans';

	/*Credit Cards - Loans to individuals for household, family, and other personal expenditures*/
	if dt<20080331 then
		trad_ln_cc=.;
	else trad_ln_cc=rcfdf615;
	label trad_ln_cc = 'Credit Cards - Loans to individuals for household, family and other personal expenditures';

	/*Other Revolving Card Plans - Loans to individuals for household, family, and other personal expenditures*/
	if dt<20080331 then
		trad_ln_othrcp=.;
	else trad_ln_othrcp = rcfdf616;
	label trad_ln_othrcp = 'Other Revolving Card Plans - Loans to individuals for household, family, and other personal expenditures';

	/*Other Consumer Loans - Loans to individuals for household, family, and other personal expenditures*/
	if dt<20080331 then
		trad_ln_othcons=.;
	else if dt>=20080331 & dt<20110331 then
		trad_ln_othcons = rcfdf617;
	else trad_ln_othcons = rcfdk210;
	label trad_ln_othcons = 'Other Consumer Loans - Loans to individuals for household, family, and other personal expenditures';

	/*Automobile Loans - Loans to individuals for household, family, and other personal expenditures*/
	if dt<20110331 then
		trad_ln_autom=.;
	else trad_ln_autom=rcfdk199;
	label trad_ln_autom = 'Automobile Loans - Loans to individuals for household, family, and other personal expenditures';

	/*Other Loans*/
	if dt<20080331 then
		trad_ln_oth=.;
	else trad_ln_oth=rcfdf618;
	label trad_ln_oth = 'Other Loans';


	/*****************/
	/**Liabilities**/
	/*****************/

	/*Liability for short positions (also known as Revaluation losses on interest rate, foreign exchange rate, equity, commodity and other contracts*/
	if dt<19940331 then
		liab_trad_shpos=.;
	else liab_trad_shpos=rcfd3546;
	label liab_trad_shpos = 'Liability for short positions';

	/*All other trading liabilites*/
	if dt<20080331 then
		liab_trad_alloth=.;
	else liab_trad_alloth=rcfdf624;
	label liab_trad_alloth = 'All other Trading liabilities';

	/*Derivatives with a negative fair value*/
	if dt<19940331 then
		liab_trad_dneg_fv=.;
	else liab_trad_dneg_fv=rcfd3547;
	label liab_trad_dneg_fv = 'Derivatives with a negative fair value';




    /**************************************************************
	**********     SCHEDULE RC-R: REGULATORY CAPITAL     **********
    **************************************************************/

	/* Tier 1 Risk-based Capital */
	/* Date range: 1994q1 to present */
       	tier1_rbc_b1 = .;
       	tier1_rbc_b1 = rcfd8274;
       	label tier1_rbc_b1='Tier 1 Risk-based Capital - Basel I';

    /* Total Risk-based Capital (Sum of Tier 1, Tier 2, and Tier 3 minus deductions)*/
    /* Date range: 1990q1 to present */
       	total_rbc_b1 = .;
       	total_rbc_b1 = rcfd3792;
       	label total_rbc_b1='Total Risk-based Capital - Basel I';

	/* Risk Weighted Assets */
	/* Date range: 1996q1 to present */
       	rwa_b1 = .;
       	rwa_b1 = rcfda223;
       	label rwa_b1='Risk Weighted Assets - Basel I';

	/* Average Assets for Leverage Ratio */
	/* Date range: 1996q1 to present */

		if dt < 19960331 then  asset_forlev_b1 = .;
		else if dt < 20120331 then asset_forlev_b1 =  rcfda224;
		else if dt >= 20120331 then asset_forlev_b1 = rcfdl138;
		label asset_forlev_b1='Average Assets for Leverage Ratio - Basel I';

		t1_unrealized_regafs=.;
		t1_unrealized_eqafs=.;
		t1_accgains_cashhedges=.;
		t1_nonqual_prefstock=.;
		t1_qual_noncon_minint=.;
		t1_disallowed_gwill=.;
		t1_cumchange_fvalue=.;
		t1_disallowed_service_ass=. ;
		t1_disallowed_tax_ass=.;
		t1_otherchanges=.;
		t1_unrealized_regafs=rcfd8434;
		t1_unrealized_eqafs=rcfda221;
		t1_accgains_cashhedges=rcfd4336;
		t1_nonqual_prefstock=rcfdb588;
		t1_qual_noncon_minint=rcfdb589;
		t1_disallowed_gwill=rcfdb590;
		t1_cumchange_fvalue=rcfdf264;
		t1_disallowed_service_ass=rcfdb591 ;
		t1_disallowed_tax_ass=rcfd5610 ;
		t1_otherchanges=rcfdb592;

  	/* Deferred Tax Assets Variables*/;

		if dt < 20010331 then
		t1_netdef_taxassets = .;
		else if dt >=20010331 then
		t1_netdef_taxassets=rcfd2148;

		if dt < 20010331 then
		t1_netdef_taxliab = .;
		else if dt >=20010331 then
		t1_netdef_taxliab=rcfd3049;

		t1_regcap_subtot=rcfdc227;

    /* Tier 1 Common Equity */;
    /* Date range: 2001q1 to present */;
	    if dt < 20010331 then
    	tier1_comm = .;
	    else if dt >=20010331 then
    	tier1_comm = sum(rcfd8274,-rcfd3838,rcfdb588,-rcfdb589);
	    label tier1_comm = 'Tier 1 Common Equity';

            /*******************/
            /* Part 1.B. Items */
            /*******************/;

            /* Create regulatory variables for Basel III regulations */;
            if dt < 20140331 then do ;
                tier1_rbc_b3 = . ;
                cet1 = . ;
                end ;
            else do ;
                tier1_rbc_b3=rcfa8274;
                cet1=rcfap859;
                end ;
            label tier1_rbc_b3 = 'Tier 1 Risk-Based Capital - Basel III - RCFA8274' ;
            label cet1 = 'Common Equity Tier 1 - RCFAP859' ;


            /* Delete these items */;
            /* Regulatory Capital Items */;
            if dt<20140331 then do;
                cet1=.;
                tier1_rbc_b3=.;
                total_rbc_b3=.;
                rwa_b3=.;
                asset_forlev_b3=.;
                end;
            else do;


                total_rbc_b3=rcfa3792;
                rwa_b3=rcfaa223;
                asset_forlev_b3=rcfaa224;
                end;
            label cet1='Common Equity Tier 1 - Basel III';
            label tier1_rbc_b3='Tier 1 Risk-Based Capital - Basel III';
            label total_rbc_b3='Total Risk-Based Capital - Basel III';
            label rwa_b3='Total Risk-Weighted ASsets - Basel III';
            label asset_forlev_b3='Total Assets for the Leverage Ratio - Basel III';

            /* End of delete these items */;

               /* CET1 Components before adjustments and deductions */;
                if dt<20140331 then do;
                    t1_comm_stock_net=.;
                    t1_retain_earn_bank=.;
                    t1_aoci=.;
                    t1_aoci_opt=.;
                    t1_cet1_minint=.;
                    t1_cet1_before_adj=.;
                    end;
                else do;
                    t1_comm_stock_net=rcfap742;
                    t1_retain_earn_bank=rcfd3632;
                    t1_aoci=rcfab530;
                    t1_aoci_opt=rcoap838;
                    t1_cet1_minint=rcfap839;
                    t1_cet1_before_adj=rcfap840;
                    end;
                label t1_comm_stock_net='Common stock plus related surplus, net of treasury stock and ESOP shares-rcfab742';
                label t1_retain_earn='Retained earnings-rcfdb530';
                label t1_aoci='AOCI-rcfab530';
                label t1_aoci_opt='AOCI opt-out election-rcoap838';
                label t1_cet1_minint='CET1 Minority interest includable in CET1 capital-rcfap839';
                label t1_cet1_before_adj='CET1 capital before adjustments and deductions-rcfap840';

                /* Additional Tier 1 Capital */;
                if dt<20140331 then do;
                    t1_addtional_t1_cap=.;
                    end;
                else do;
                    t1_addtional_t1_cap=rcfap865;
                    end;
                label t1_addtional_t1_cap='Additional tier 1 capital - RCFAP865';

                /* Tier 2 Capital */;
                if dt<20140331 then do;
                    t2_cap=.;
                    t2_cap_aa=.;
                    end;
                else do;
                    t2_cap=rcfa5311;
                    t2_cap_aa=rcfw5311;
                    end;
                label t2_cap='Tier 2 Capital - RCFA5311';
                label t2_cap_aa='Tier 2 Capital (Advanced Approaches) - RCFW5311';

                   /* Total Capital */;
                if dt<20140331 then do;
                    total_rbc_b3 = . ;
                    total_rbc_b3_aa=.;
                    end;
                else do;
                    total_rbc_b3 = rcfa3792;
                    total_rbc_b3_aa=rcfw3792;
                    end;
                label total_rbc_b3 = 'Total Risk-Based Capital - Basel III - RCFA3792' ;
                label total_rbc_b3_aa='Total Risk-Based Capital (Advanced Approaches) RCFW3792';



                /* Total Assets for the leverage ratio */;
                  if dt<20140331 then do;
                      asset_avg_b3=. ;
                      t1_cap_deduc=.;
                      oth_deduct_avg_ass=.;
                      asset_forlev_b3_aa = . ;
                      end;
                  else do;
                      asset_avg_b3=rcfd3368;
                      t1_cap_deduc=rcfap875;
                      oth_deduct_avg_ass=rcfab596;
                      asset_forlev_b3 = rcfaa224 ;
                      end;
                  label asset_avg_b3='Average total consolidated assets-Basel III-rcfd3368';
                  label t1_cap_deduc='Deductions from cet1 capital and addtioanl tier 1 capital-rcfap875';
                  label oth_deduct_avg_ass='Other deductions from (additions to) assets for leverage ratio purposes-rcfab596';
                  label asset_forlev_b3 = 'Average Assets for Leverage Ratio - Basel III - RCFAA224' ;

                  /* Total Risk-Weighted Assets */;
                if dt<20140331 then do;
                    rwa_b3 = . ;
                    rwa_b3_aa = . ;
                    end;
                else do;
                    rwa_b3 = rcfaa223 ;
                    rwa_b3_aa = rcfwa223 ;
                    end;
                label rwa_b3 = 'Total risk-weighted assets - Basel III - RCFAA223' ;
                label rwa_b3_aa = 'Total risk-weighted (Advanced Approaches) - Basel III - RCFA223' ;




                /* There is no analogous average asset variable in HC-R Part 1.A. Therefore, the combined variable is only equal to advanced approaches firms. */;
                 /* Combined Variables */;
                 if cet1 ne . then do;
                     comm_t1 = cet1 ;
                     tier1_rbc=tier1_rbc_b3;
                     total_rbc=total_rbc_b3;
                     rwa=rwa_b3;
                     asset_forlev=asset_forlev_b3;
                     asset_avg=asset_avg_b3;
                     end;
                 else do;
                     comm_t1 = tier1_comm ;
                     tier1_rbc=tier1_rbc_b1;
                     total_rbc=total_rbc_b1;
                     rwa=rwa_b1;
                     asset_forlev=asset_forlev_b1;
                     end;
                 label comm_t1='Tier 1 Common Equity and CET1, if available';
                 label tier1_rbc='Tier 1 Risk-Based Capital - Combined Basel I&III' ;
                 label total_rbc='Total Risk-Based Capital - Combined Basel I&III';
                 label rwa = 'Total Weighted Assets - Combined Basel I&III' ;
                 label asset_forlev='Average Assets for Leverage Ratio - Combined Basel I&III';
                 label asset_avg_b3='Average total consolidated assets-Combined';

        /****************************************************************
        **********     SCHEDULE RC-N: NON-PERFORMING LOANS     **********
 	**********      (PAST DUE AND NON-ACCRUAL LOANS)       **********
 	****************************************************************/

	/* Mortgage servicing assets */;
		if dt<20010331 then
	 	srv_ass_mrt = .;
		else srv_ass_mrt = rcfd3164;
		label srv_ass_mrt = 'Mortgage Servicing Assets';

	/* Estimated Fair Value of Mortgage Servicing Assets */;
	 	if dt<20010331 then
	 	srv_ass_mrt_fv = .;
		else srv_ass_mrt_fv = rcfda590;
		label srv_ass_mrt_fv = 'Estimated Fair Value of Mortgage Servicing Assets';

	/* Purchased credit card relationships and nonmortgage servicing assets */;
	if dt<20010331 then
		srv_ass_non_mrt = .;
		else srv_ass_non_mrt = rcfdb026;
		label srv_ass_non_mrt = 'Credit Card Relationships and Nonmortgage Servicing Assets';



    /****************************************************************
    **********     SCHEDULE RC-L: Derivatives and off      **********
    **********      balance sheet itmes                    **********
    ****************************************************************/

                    	if dt<19900331 then
	unused_commit=.;
	else if dt>=19900331 & dt<19910331 then
	unused_commit=sum(rcfd3814, rcfd3815, rcfd3816, rcfd3818);
	else if dt>=19910331 & dt<20070331 then
	unused_commit=sum(rcfd3814, rcfd3815, rcfd3816, rcfd6550, rcfd3818);
	else if dt>=20070331 & dt<20100331 then
	unused_commit=sum(rcfd3814, rcfd3815, rcfdf164, rcfdf165, rcfd6550, rcfd3818);
	else unused_commit = sum(rcfd3814, rcfd3815, rcfdf164, rcfdf165, rcfd6550, rcfdj457, rcfdj458, rcfdj459);

	if dt<19900331 then
	standby_loc=.;
	else standby_loc = sum(rcfd3819, rcfd3821, rcfd3411);

	if dt < 19900331 then
	sec_underw = .;
	else sec_underw = rcfd3817 ;
	label sec_underw='Securities underwriting' ;

	if dt < 19900331 then
	sec_lent = .;
	else sec_lent = rcfd3433 ;
	label sec_lent='Security lent' ;


            /* 1. Unused Commitments */;

    if dt<19900331 then
     	uc_loans_fam=. ;
    else uc_loans_fam=rcfd3814 ;
    label uc_loans_fam = 'Unused Commitments: Revolving open-end loans secured by 1-4 family residential properties' ;

    if dt<20100331 then
	uc_lnfam_hecm=.;
    else uc_lnfam_hecm=rconj477;
    label uc_lnfam_hecm = 'Unused Commitments: Revolving open-end loans secured by 1-4 family residential properties: Unused... (j477)' ;

    if dt<20100331 then
	uc_lnfam_prop=.;
    else uc_lnfam_prop=rconj478;
    label uc_lnfam_prop = 'Unused Commitments: Revolving open-end loans secured by 1-4 family residential properties: ...(j478)' ;

    if dt<19900331 then
	uc_cc_lines=.;
    else uc_cc_lines=rcfd3815;
    label uc_cc_lines = 'Unused Commitments: Credit card lines';

    if dt<20100331 then
     	uc_cons_cc_lines=. ;
    else uc_cons_cc_lines=rcfdj455;
    label uc_cons_cc_lines = 'Unused Commitments: Unused consumer credit card lines' ;

    if dt<20100331 then
     	uc_oth_cc_lines=.;
    else uc_oth_cc_lines=rcfd456;
    label uc_oth_cc_lines = 'Unused Commitments: Other unused credit card lines' ;

    if dt<20070331 then
     	uc_res_constr_loans=. ;
    else uc_res_constr_loans=rcfdf164;
    label uc_res_constr_loans = 'Unused commitments: 1-4 family residential construction loan commitments' ;

    if dt<20070331 then
     	uc_com_constr_ld_loans=.;
    else uc_com_constr_ld_loans=rcfdf165;
    label uc_com_constr_ld_loans = 'Unused commitments: Commercial real estate, other construction loan, and land development loan commitments' ;

    if dt<19910331 then
       uc_loans_not_sec_by_restate=. ;
    else uc_loans_nsec_by_restate=rcfd6550;
    label uc_loans_nsec_by_restate = 'Unused commitments: Commitments to fund commercial real estate, construction, and land development loans NOT secured by real estate' ;


    if dt<20100331 then
       uc_othun_cilns=.;
    else uc_othun_cilns=rcfdj457;
    label uc_othun_cilns = 'Unused commitments: Other unused commitments - commercial and industrial loans' ;

    if dt<20100331 then
       uc_othun_filns=.;
    else uc_othun_filns=rcfdj458;
    label uc_othun_filns = 'Unused commitments: Other unused commitments - loans to financial institutions' ;

    if dt<20100331 then
       uc_othun_alloth=.;
    else uc_othun_alloth=rcfdj459;
    label uc_othun_alloth = 'Unused commitments: Other unused commitments - all other unused commitments' ;


     /* 7. Credit Derivatives */;

     /*Notional amounts*/

    if dt<20060331 then
       cd_na_cdfsw_sold=.;
    else cd_na_cdfsw_sold=rcfd968;
    label cd_na_cdfsw_sold = 'Credit Derivatives: Notional amounts - sold credit default swaps' ;

    if dt<20060331 then
       cd_na_trswaps_sold=.;
    else cd_na_trswaps_sold =rcfd970;
    label cd_na_trswaps_sold = 'Credit Derivatives: Notional amounts - sold total return swaps' ;

    if dt<20060331 then
       cd_na_copt_sold=.;
    else cd_na_copt_sold=rcfd972;
    label cd_na_copt_sold = 'Credit Derivatives: Notional amounts - sold credit options' ;

    if dt<20060331 then
       cd_na_other_sold=.;
    else cd_na_other_sold=rcfd974;
    label cd_na_other_sold = 'Credit Derivatives: Notional amounts - sold other credit derivatives' ;

    if dt<19970331 then
	cd_na_sold=.;
    else if dt>=19970331 & dt<=20051231 then cd_na_sold=rcfda534;
    else if dt>20051231 then cd_na_sold=sum(rcfdc968, rcfd970, rcfd972, rcfdc974);

    if dt<20060331 then
       cd_na_cdfsw_purch=.;
    else cd_na_cdfsw_purch=rcfd969;
    label cd_na_cdfsw_purch = 'Credit Derivatives: Notional amounts - purchased credit default swaps' ;

    if dt<20060331 then
       cd_na_trswaps_purch=.;
    else cd_na_trswaps_purch=rcfd971;
    label cd_na_trswaps_purch='Credit Derivatives: Notional amounts - purchased total return swaps' ;


    if dt<20060331 then
       cd_na_copt_purch=rcfd973;
    else cd_na_copt_purch=rcfd973;
    label cd_na_copt_purch = 'Credit Derivatives: Notional amounts - purchased credit options' ;

    if dt<20060331 then
      cd_na_other_purch=.;
    else cd_na_other_purch=rcfd975;
    label cd_na_other_purch='Credit Derivatives: Notional amounts - purchased other credit derivatives' ;

    if dt<19970331 then
	cd_na_purch=.;
    else if dt>=19970331 & dt<=20051231 then cd_na_purch=rcfda535;
    else if dt>20051231 then cd_na_purch=sum(rcfdc969, rcfdc971, rcfdc973, rcfdc975);

     /* Gross Fair Values */;
     if dt<20020331 then
         cd_gfv_pos_sold = . ;
     else cd_gfv_pos_sold=rcfdc219;
     label cd_gfv_pos_sold = 'Credit Derivatives: Gross positive fair value - sold protection (C219)';

     if dt<20020331 then
         cd_gfv_neg_sold = . ;
     else cd_gfv_neg_sold=rcfdc220;
     label cd_gfv_neg_sold = 'Credit Derivatives: Gross negative fair value - sold protection (C220)';

     if dt<20020331 then
         cd_gfv_pos_purch = . ;
     else cd_gfv_pos_purch=rcfdc221;
     label cd_gfv_pos_purch = 'Credit Derivatives: Gross positive fair value - purchased protection (C221)';

     if dt<20020331 then
         cd_gfv_neg_purch = . ;
     else cd_gfv_neg_purch=rcfdc222;
     label cd_gfv_neg_purch = 'Credit Derivatives: Gross negative fair value - purchased protection (C222)';

     /*Notional amounts by regulatory capital treatments*/

     if dt<20090630 then
	cd_nrct_pos_sold=.;
     else cd_nrct_pos_sold=rcfdg401;
     label cd_nrct_pos_sold = 'Credit Derivatives: Notional amounts by regulatory capital treatment - positions covered under Market Risk Rule - Sold Protection';

     if dt<20090630 then
	cd_nrct_pos_purch=.;
     else cd_nrct_pos_purch=rcfdg402;
     label cd_nrct_pos_purch = 'Credit Derivatives: Notional amounts by regulatory capital treatment - positions covered under Market Risk Rule - Purchased Protection';

     if dt<20090630 then
	cd_nrct_aop_sold=.;
     else cd_nrct_aop_sold = rcfd403;
     label cd_nrct_aop_sold = 'Credit Derivatives: Notional amounts by regulatory capital treatment - All other positions - sold protection';

     if dt<20090630 then
	cd_nrct_aop_grc=.;
     else cd_nrct_aop_grc=rcfd404;
     label cd_nrct_aop_grc = 'Credit Derivatives: Notional amounts by regulatory capital treatment - purchased protection that is recognized as a guarantee for regulatory capital purposes';

     if dt<20090630 then
        cd_nrct_aop_ngrc=.;
     else cd_nrct_aop_ngrc=rcfdg405;
     label cd_nrct_aop_ngrc = 'Credit Derivatives: Notional amounts by regulatory capital treatment - purchased protection that is NOT recognized as a guarantee for regulatory capital purposes';


     /*Notional amounts by remaining maturity*/

     if dt<20090630 then
	cd_nrm_scp_igd_l1=.;
     else cd_nrm_scp_igd_l1=rcfdg406;
     label cd_nrm_scp_igd_l1 = 'Credit Derivatives: Notional amounts by remaining maturity - Sold Credit Investment - Investment grade - One year or less';

     if dt<20090630 then
	cd_nrm_scp_igd_1t5=.;
     else cd_nrm_scp_igd_1t5=rcfdg407;
     label cd_nrm_scp_igd_1t5 = 'Credit Derivatives: Notional amounts by remaining maturity - Sold Credit Investment - Investment grade - Over one year through five years';

     if dt<20090630 then
	cd_nrm_scp_igd_o5=.;
     else cd_nrm_scp_igd_o5=rcfdg408;
     label cd_nrm_scp_igd_o5 = 'Credit Derivatives: Notional amounts by remaining maturity - Sold Credit Investment - Investment grade - Over five years';

     if dt<20090630 then
	cd_nrm_scp_sigd_l1=.;
     else cd_nrm_scp_sigd_l1=rcfdg409;
     label cd_nrm_scp_sigd_l1 = 'Credit Derivatives: Notional amounts by remaining maturity - Sold Credit Investment - Subinvestment grade - One year or less';

     if dt<20090630 then
	cd_nrm_scp_sigd_1t5=.;
     else cd_nrm_scp_sigd_1t5=rcfdg410;
     label cd_nrm_scp_sigd_1t5 = 'Credit Derivatives: Notional amounts by remaining maturity - Sold Credit Investment - Subinvestment grade - Over one year through five years';

     if dt<20090630 then
	cd_nrm_scp_sigd_o5=.;
     else cd_nrm_scp_sigd_o5=rcfdg411;
     label cd_nrm_scp_sigd_o5 = 'Credit Derivatives: Notional amounts by remaining maturity - Sold Credit Investment - Subinvestment grade - Over five years';

     if dt<20090630 then
	cd_nrm_pcp_igd_l1=.;
     else cd_nrm_pcp_igd_l1=rcfdg412;
     label cd_nrm_pcp_igd_l1 = 'Credit Derivatives: Notional amounts by remaining maturity - Purchased Credit Investment - Investment grade - One year or less';

     if dt<20090630 then
	cd_nrm_pcp_igd_1t5=.;
     else cd_nrm_pcp_igd_1t5=rcfdg413;
     label cd_nrm_pcp_igd_1t5 = 'Credit Derivatives: Notional amounts by remaining maturity - Purchased Credit Investment - Investment grade - Over one year through five years';

     if dt<20090630 then
	cd_nrm_pcp_igd_o5=.;
     else cd_nrm_pcp_igd_o5=rcfdg414;
     label cd_nrm_pcp_igd_o5 = 'Credit Derivatives: Notional amounts by remaining maturity - Purchased Credit Investment - Investment grade - Over five years';

     if dt<20090630 then
	cd_nrm_pcp_sigd_l1=.;
     else cd_nrm_pcp_sigd_l1=rcfdg415;
     label cd_nrm_pcp_sigd_l1 = 'Credit Derivatives: Notional amounts by remaining maturity - Purchased Credit Investment - Subinvestment grade - One year or less';

     if dt<20090630 then
	cd_nrm_pcp_sigd_1t5=.;
     else cd_nrm_pcp_sigd_1t5=rcfdg416;
     label cd_nrm_pcp_sigd_1t5 = 'Credit Derivatives: Notional amounts by remaining maturity - Purchased Credit Investment - Subinvestment grade - Over one year through five years';

     if dt<20090630 then
	cd_nrm_pcp_sigd_o5=.;
     else cd_nrm_pcp_sigd_o5=rcfdg417;
     label cd_nrm_pcp_sigd_o5 = 'Credit Derivatives: Notional amounts by remaining maturity - Purchased Credit Investment - Subinvestment grade - Over five years';


     /* 8. Spot Foreign Exchange Contracts */;
     if dt<19950331 then
         spot_fx_contract = . ;
     else spot_fx_contract = rcfd8765;
     label spot_fx_contract = 'Spot foreign exchange contracts (8765)';


     /*12. Gross amounts of derivative contracts */;

	 if dt<19950331 then
	 ir_fut_gamt=.;
	 else ir_fut_gamt=rcfd8693;
	 label ir_fut_gamt = 'Gross amounts of futures contracts - interest rate contracts';

	 if dt<19950331 then
	 fex_fut_gamt=.;
	 else fex_fut_gamt=rcfd8694;
	 label fex_fut_gamt = 'Gross amounts of futures contracts - foreign exchange contracts';

	 if dt<19950331 then
	 eqd_fut_gamt=.;
	 else eqd_fut_gamt=rcfd8695;
	 label eqd_fut_gamt = 'Gross amounts of futures contracts - equity derivative contracts';

	 if dt<19950331 then
	 coc_fut_gamt=.;
	 else coc_fut_gamt=rcfd8696;
	 label coc_fut_gamt = 'Gross amounts of futures contracts - commodity and other contracts';

	 if dt<19950331 then
	 ir_fwd_gamt=.;
	 else ir_fwd_gamt=rcfd8697;
	 label ir_fwd_gamt = 'Gross amounts of forward contracts - interest rate contracts';

     if dt<19950331 then
	 fex_fwd_gamt=.;
	 else fex_fwd_gamt=rcfd8698;
	 label fex_fwd_gamt = 'Gross amounts of forward contracts - foreign exchange contracts';

     if dt<19950331 then
	 eqd_fwd_gamt=.;
	 else eqd_fwd_gamt=rcfd8699;
	 label eqd_fwd_gamt = 'Gross amounts of forward contracts - equity derivative contracts';

     if dt<19950331 then
	 coc_fwd_gamt=.;
	 else coc_fwd_gamt=rcfd8700;
	 label coc_fwd_gamt = 'Gross amounts of forward contracts - commodity and other contracts';

	 if dt<19950331 then
	 ir_ext_wo_gamt=.;
	 else ir_ext_wo_gamt=rcfd8701;
	 label ir_ext_wo_gamt = 'Gross amounts of exchange-traded option contracts - interest rate contracts';

	 if dt<19950331 then
	 fex_ext_wo_gamt=.;
	 else fex_ext_wo_gamt=rcfd8702;
	 label fex_ext_wo_gamt = 'Gross amounts of exchange-traded option contracts - foreign exchange contracts';

	 if dt<19950331 then
	 eqd_ext_wo_gamt=.;
	 else eqd_ext_wo_gamt=rcfd8703;
	 label eqd_ext_wo_gamt = 'Gross amounts of exchange-traded option contracts - equity derivative contracts';

	 if dt<19950331 then
	 coc_ext_wo_gamt=.;
	 else coc_ext_wo_gamt=rcfd8704;
	 label coc_ext_wo_gamt = 'Gross amounts of exchange-traded option contracts - commodity and other contracts';

     if dt<19950331 then
	 ir_ext_po_gamt=.;
	 else ir_ext_po_gamt=rcfd8705;
	 label ir_ext_po_gamt = 'Gross amounts of exchange-traded option contracts - interest rate contracts';

	 if dt<19950331 then
	 fex_ext_po_gamt=.;
	 else fex_ext_po_gamt=rcfd8706;
	 label fex_ext_po_gamt = 'Gross amounts of exchange-traded option contracts - foreign exchange contracts';

	 if dt<19950331 then
	 eqd_ext_po_gamt=.;
	 else eqd_ext_po_gamt=rcfd8707;
	 label eqd_ext_po_gamt = 'Gross amounts of exchange-traded option contracts - equity derivative contracts';

	 if dt<19950331 then
	 coc_ext_po_gamt=.;
	 else coc_ext_po_gamt=rcfd8708;
	 label coc_ext_po_gamt = 'Gross amounts of exchange-traded option contracts - commodity and other contracts';

	 if dt<19950331 then
	 ir_ovc_wo_gamt=.;
	 else ir_ovc_wo_gamt=rcfd8709;
	 label ir_ovc_wo_gamt = 'Gross amounts of exchange-traded option contracts - interest rate contracts';

	 if dt<19950331 then
	 fex_ovc_wo_gamt=.;
	 else fex_ovc_wo_gamt=rcfd8710;
	 label fex_ovc_wo_gamt = 'Gross amounts of exchange-traded option contracts - foreign exchange contracts';

	 if dt<19950331 then
	 eqd_ovc_wo_gamt=.;
	 else eqd_ovc_wo_gamt=rcfd8711;
	 label eqd_ovc_wo_gamt = 'Gross amounts of exchange-traded option contracts - equity derivative contracts';

	 if dt<19950331 then
	 coc_ovc_wo_gamt=.;
	 else coc_ovc_wo_gamt=rcfd8712;
	 label coc_ovc_wo_gamt = 'Gross amounts of exchange-traded option contracts - commodity and other contracts';

	 if dt<19950331 then
	 ir_ovc_po_gamt=.;
	 else ir_ovc_po_gamt=rcfd8713;
	 label ir_ovc_po_gamt = 'Gross amounts of exchange-traded option contracts - interest rate contracts';

	 if dt<19950331 then
	 fex_ovc_po_gamt=.;
	 else fex_ovc_po_gamt=rcfd8714;
	 label fex_ovc_po_gamt = 'Gross amounts of exchange-traded option contracts - foreign exchange contracts';

	 if dt<19950331 then
	 eqd_ovc_po_gamt=.;
	 else eqd_ovc_po_gamt=rcfd8715;
	 label eqd_ovc_po_gamt = 'Gross amounts of exchange-traded option contracts - equity derivative contracts';

	 if dt<19950331 then
	 coc_ovc_wo_gamt=.;
	 else coc_ovc_po_gamt=rcfd8716;
	 label coc_ovc_po_gamt = 'Gross amounts of exchange-traded option contracts - commodity and other contracts';

	 if dt<19950331 then
	 ir_swps_gamt=.;
	 else ir_swps_gamt=rcfd3450;
	 label ir_swps_gamt = 'Gross amounts of swaps - interest rate contracts';

	 if dt<19950331 then
	 fex_swps_gamt=.;
	 else fex_swps_gamt=rcfd3826;
	 label fex_swps_gamt = 'Gross amounts of swaps - foreign exchange contracts';

	 if dt<19950331 then
	 eqd_swps_gamt=.;
	 else eqd_swps_gamt=rcfd8719;
	 label eqd_swps_gamt = 'Gross amounts of swaps - equity derivative contracts';

	 if dt<19950331 then
	 coc_swps_gamt=.;
	 else coc_swps_gamt=rcfd8720;
	 label coc_swps_gamt = 'Gross amounts of swaps - commodity and other contracts';

	 if dt<19950331 then
	 ir_gamt=.;
     else ir_gamt=sum(rcfd8693, rcfd8697, rcfd8701, rcfd8705, rcfd8709, rcfd8713, rcfd3450);
     label ir_gamt = 'Gross amounts of interest rate contracts';

     if dt<19950331 then
	 fex_gamt=.;
     else fex_gamt=sum(rcfd8694, rcfd8698, rcfd8702, rcfd8706, rcfd8710, rcfd8714, rcfd3826);
     label fex_gamt= 'Gross amounts of foreign exchange contracts';

     if dt<19950331 then
	 eqderiv_gamt=.;
     else eqderiv_gamt=sum(rcfd8695, rcfd8699, rcfd8703, rcfd8707, rcfd8711, rcfd8715, rcfd8719);
     label eqderiv_gamt= 'Gross amounts of equity derivative contracts';

     if dt<19950331 then
	 com_and_oth_gamt=.;
     else com_and_oth_gamt=sum(rcfd8696, rcfd8700, rcfd8704, rcfd8708, rcfd8712, rcfd8716, rcfd8720);
     label com_and_oth_gamt= 'Gross amounts of commodity and other contracts';

     /* 13.Total gross notional amount of derivative contracts held for trading */
     if dt<19950331 then
	tot_gna_dcht_ir=.;
     else tot_gna_dcht_ir=rcfda126;
     label tot_gna_dcht_ir = 'Total gross notional amount of derivative contracts held for trading - interest rate contracts';

     if dt<19950331 then
	tot_gna_dcht_fex=.;
     else tot_gna_dcht_fex=rcfda127;
     label tot_gna_dcht_fex = 'Total gross notional amount of derivative contracts held for trading - foreign exchange contracts';


     if dt<19950331 then
	tot_gna_dcht_eqd=.;
     else tot_gna_dcht_eqd=rcfda8723;
     label tot_gna_dcht_eqd = 'Total gross notional amount of derivative contracts held for trading - equity derivative contracts';


     if dt<19950331 then
	tot_gna_dcht_coc=.;
     else tot_gna_dcht_coc=rcfda8724;
     label tot_gna_dcht_coc = 'Total gross notional amount of derivative contracts held for trading - commodity and other contracts contracts';

     /* 14. Total gross notional amount of derivative contracts held for purposes other than trading */
     if dt<19950331 then
	tot_gna_dchot_ir=.;
     else tot_gna_dchot_ir=rcfd8725;
     label tot_gna_dchot_ir = 'Total gross notional amount of derivative contracts held for purposes other than trading - interest rate contracts';

     if dt<19950331 then
	tot_gna_dchot_fex=.;
     else tot_gna_dchot_fex=rcfd8726;
     label tot_gna_dchot_fex = 'Total gross notional amount of derivative contracts held for purposes other than trading - foreign exchange contracts';

     if dt<19950331 then
	tot_gna_dchot_eqd=.;
     else tot_gna_dchot_eqd=rcfd8727;
     label tot_gna_dchot_eqd = 'Total gross notional amount of derivative contracts held for purposes other than trading - equity derivative contracts';

     if dt<19950331 then
	tot_gna_dchot_coc=.;
     else tot_gna_dchot_coc=rcfd8728;
     label tot_gna_dchot_coc = 'Total gross notional amount of derivative contracts held for purposes other than trading - commodity and other contracts';

     if dt<19970630 then
        tot_gna_dchot_irsfr=.;
     else tot_gna_dchot_irsfr=rcfda589;
     label tot_gna_dchot_irsfr = 'Total gross notional amount of derivative contracts held for purposes other than trading - interest rate swaps where the bank has agreed to pay a fixed rate' ;


     /* 15. Gross fair value of derivative contracts */;

     /* Gross fair value of interest rate derivative contracts */;
     if dt<19950331 then
         ir_deriv_gfv_pos_hft = . ;
     else ir_deriv_gfv_pos_hft = rcfd8733;
     label ir_deriv_gfv_pos_hft = 'Gross Positive Fair Value of interest rate contracts held for trading (8733)';

     /* Gross fair value of foreign exchange derivative contracts */;
     if dt<19950331 then
         fx_deriv_gfv_pos_hft = . ;
     else fx_deriv_gfv_pos_hft = rcfd8734;
     label fx_deriv_gfv_pos_hft = 'Gross Positive Fair Value of foreign exchange contracts held for trading (8734)';

     /* Gross fair value of equity derivative contracts */;
     if dt<19950331 then
         eq_deriv_gfv_pos_hft = . ;
     else eq_deriv_gfv_pos_hft = rcfd8735;
     label eq_deriv_gfv_pos_hft = 'Gross Positive Fair Value of equity derivative contracts held for trading (8735)';

     /* Gross fair value of derivative contracts */;
     if dt<19950331 then
         oth_deriv_gfv_pos_hft = . ;
     else oth_deriv_gfv_pos_hft = rcfd8736;
     label oth_deriv_gfv_pos_hft = 'Gross Positive Fair Value of commodity and other contracts held for trading (8736)';

     /* negative fair value for contracts held for trading */;

     /* Gross fair value of interest rate derivative contracts */;
     if dt<19950331 then
         ir_deriv_gfv_neg_hft = . ;
     else ir_deriv_gfv_neg_hft = rcfd8737;
     label ir_deriv_gfv_neg_hft = 'Gross Negative Fair Value of interest rate contracts held for trading (8737)';

     /* Gross fair value of foreign exchange derivative contracts */;
     if dt<19950331 then
         fx_deriv_gfv_neg_hft = . ;
     else fx_deriv_gfv_neg_hft = rcfd8738;
     label fx_deriv_gfv_neg_hft = 'Gross Negative Fair Value of foreign exchange contracts held for trading (8738)';

     /* Gross fair value of equity derivative contracts */;
     if dt<19950331 then
         eq_deriv_gfv_neg_hft = . ;
     else eq_deriv_gfv_neg_hft = rcfd8739;
     label eq_deriv_gfv_neg_hft = 'Gross Negative Fair Value of equity derivative contracts held for trading (8739)';

     /* Gross fair value of derivative contracts */;
     if dt<19950331 then
         oth_deriv_gfv_neg_hft = . ;
     else oth_deriv_gfv_neg_hft = rcfd8740;
     label oth_deriv_gfv_neg_hft = 'Gross Negative Fair Value of commodity and other contracts held for trading (8740)';

     /* Not held for trading */;

     /* Gross fair value of interest rate derivative contracts */;
     if dt<19950331 then
         ir_deriv_gfv_pos_nhft = . ;
     else ir_deriv_gfv_pos_nhft = rcfd8741;
     label ir_deriv_gfv_pos_nhft = 'Gross Positive Fair Value of interest rate contracts not held for trading (8741)';

     /* Gross fair value of foreign exchange derivative contracts */;
     if dt<19950331 then
         fx_deriv_gfv_pos_nhft = . ;
     else fx_deriv_gfv_pos_nhft = rcfd8742;
     label fx_deriv_gfv_pos_nhft = 'Gross Positive Fair Value of foreign exchange contracts not held for trading (8742)';

     /* Gross fair value of equity derivative contracts */;
     if dt<19950331 then
         eq_deriv_gfv_pos_nhft = . ;
     else eq_deriv_gfv_pos_nhft = rcfd8743;
     label eq_deriv_gfv_pos_nhft = 'Gross Positive Fair Value of equity derivative contracts not held for trading (8743)';

     /* Gross fair value of derivative contracts */;
     if dt<19950331 then
         oth_deriv_gfv_pos_nhft = . ;
     else oth_deriv_gfv_pos_nhft = rcfd8744;
     label oth_deriv_gfv_pos_nhft = 'Gross Positive Fair Value of commodity and other contracts not held for trading (8744)';

     /* negative fair value not held for trading */;

     /* Gross fair value of interest rate derivative contracts */;
     if dt<19950331 then
         ir_deriv_gfv_neg_nhft = . ;
     else ir_deriv_gfv_neg_nhft = rcfd8745;
     label ir_deriv_gfv_neg_nhft = 'Gross Negative Fair Value of interest rate contracts not held for trading (8737)';

     /* Gross fair value of foreign exchange derivative contracts */;
     if dt<19950331 then
         fx_deriv_gfv_neg_nhft = . ;
     else fx_deriv_gfv_neg_nhft = rcfd8746;
     label fx_deriv_gfv_neg_nhft = 'Gross Negative Fair Value of foreign exchange contracts not held for trading (8738)';

     /* Gross fair value of equity derivative contracts */;
     if dt<19950331 then
         eq_deriv_gfv_neg_nhft = . ;
     else eq_deriv_gfv_neg_nhft = rcfd8747;
     label eq_deriv_gfv_neg_nhft = 'Gross Negative Fair Value of equity derivative contracts not held for trading (8739)';

     /* Gross fair value of derivative contracts */;
     if dt<19950331 then
         oth_deriv_gfv_neg_nhft = . ;
     else oth_deriv_gfv_neg_nhft = rcfd8748;
     label oth_deriv_gfv_neg_nhft = 'Gross Negative Fair Value of commodity and other contracts not held for trading (8740)';


     /* 15. Over-the-counter derivatives */

     /* Net current credit exposure */
     if dt<20090630 then
	ocd_ncrex_bksec=.;
     else ocd_ncrex_bksec=rcfdg418;
     label ocd_ncrex_bksec = 'Over-the counter derivatives - net current credit exposure of banks and securities firms';

     if dt<20090630 then
	ocd_ncrex_mfing=.;
     else ocd_ncrex_mfing=rcfdg419;
     label ocd_ncrex_mfing = 'Over-the counter derivatives - net current credit exposure of monoline financial guarantors';

     if dt<20090630 then
	ocd_ncrex_hfun=.;
     else ocd_ncrex_hfun=rcfdg420;
     label ocd_ncrex_hfun = 'Over-the counter derivatives - net current credit exposure of hedge funds';

     if dt<20090630 then
	ocd_ncrex_svgov=.;
     else ocd_ncrex_svgov=rcfdg421;
     label ocd_ncrex_svgov = 'Over-the counter derivatives - net current credit exposure of sovereign governments';

     if dt<20090630 then
	ocd_ncrex_calocp=.;
     else ocd_ncrex_calocp=rcfdg422;
     label ocd_ncrex_calocp = 'Over-the counter derivatives - net current credit exposure of corporations and all other counterparties';

     /* Fair value of collateral */

     /* Banks and Securities Firms */
     if dt<20090630 then
	ocd_fvoc_usd_bksec=.;
     else ocd_fvoc_usd_bksec=rcfdg423;
     label ocd_fvoc_usd_bksec = 'Over-the counter derivatives - fair value of collateral of cash - U.S. dollar of banks and securities firms';

     if dt<20090630 then
	ocd_fvoc_oc_bksec=.;
     else ocd_fvoc_oc_bksec=rcfdg428;
     label ocd_fvoc_oc_bksec = 'Over-the counter derivatives - fair value of collateral of cash - other currencies of banks and securities firms';

     if dt<20090630 then
	ocd_fvoc_tsec_bksec=.;
     else ocd_fvoc_tsec_bksec=rcfdg433;
     label ocd_fvoc_tsec_bksec = 'Over-the counter derivatives - fair value of collateral of U.S. Treasury securities of banks and securities firms';

     if dt<20090630 then
	ocd_fvoc_usgds_bksec=.;
     else ocd_fvoc_usgds_bksec=rcfdg438;
     label ocd_fvoc_usgds_bksec = 'Over-the counter derivatives - fair value of collateral of U.S. Government agency and U.S. Government-sponsored agency debt securities of banks and securities firms';

     if dt<20090630 then
	ocd_fvoc_cbds_bksec=.;
     else ocd_fvoc_cbds_bksec=rcfdg443;
     label ocd_fvoc_cbds_bksec = 'Over-the counter derivatives - fair value of collateral of corporate bonds of banks and securities firms';

     if dt<20090630 then
	ocd_fvoc_eqs_bksec=.;
     else ocd_fvoc_eqs_bksec=rcfdg448;
     label ocd_fvoc_eqs_bksec = 'Over-the counter derivatives - fair value of collateral of equity securities of banks and securities firms';

     if dt<20090630 then
	ocd_fvoc_othc_bksec=.;
     else ocd_fvoc_othc_bksec=rcfdg453;
     label ocd_fvoc_othc_bksec = 'Over-the counter derivatives - fair value of collateral of all other collateral of banks and securities firms';

     if dt<20090630 then
	ocd_fvoc_tot_bksec=.;
     else ocd_fvoc_tot_bksec=rcfdg458;
     label ocd_fvoc_tot_bksec = 'Over-the counter derivatives - total fair value of collateral of banks and securities firms';

     /* Monoline Financial Guarantors */
     if dt<20090630 then
	ocd_fvoc_usd_mfing=.;
     else ocd_fvoc_usd_mfing=rcfdg424;
     label ocd_fvoc_usd_mfing = 'Over-the counter derivatives - fair value of collateral of cash - U.S. dollar of monoline financial guarantors';

     if dt<20090630 then
	ocd_fvoc_oc_mfing=.;
     else ocd_fvoc_oc_mfing=rcfdg429;
     label ocd_fvoc_oc_mfing = 'Over-the counter derivatives - fair value of collateral of cash - other currencies of monoline financial guarantors';

     if dt<20090630 then
	ocd_fvoc_tsec_mfing=.;
     else ocd_fvoc_tsec_mfing=rcfdg434;
     label ocd_fvoc_tsec_mfing = 'Over-the counter derivatives - fair value of collateral of U.S. Treasury securities of monoline financial guarantors';

     if dt<20090630 then
	ocd_fvoc_usgds_mfing=.;
     else ocd_fvoc_usgds_mfing=rcfdg439;
     label ocd_fvoc_usgds_mfing = 'Over-the counter derivatives - fair value of collateral of U.S. Government agency and U.S. Government-sponsored agency debt securities of monoline financial guarantors';

     if dt<20090630 then
	ocd_fvoc_cbds_mfing=.;
     else ocd_fvoc_cbds_mfing=rcfdg444;
     label ocd_fvoc_cbds_mfing = 'Over-the counter derivatives - fair value of collateral of corporate bonds of monoline financial guarantors';

     if dt<20090630 then
	ocd_fvoc_eqs_mfing=.;
     else ocd_fvoc_eqs_mfing=rcfdg449;
     label ocd_fvoc_eqs_mfing = 'Over-the counter derivatives - fair value of collateral of equity securities of monoline financial guarantors';

     if dt<20090630 then
	ocd_fvoc_othc_mfing=.;
     else ocd_fvoc_othc_mfing=rcfdg454;
     label ocd_fvoc_othc_mfing = 'Over-the counter derivatives - fair value of collateral of all other collateral of monoline financial guarantors';

     if dt<20090630 then
	ocd_fvoc_tot_mfing=.;
     else ocd_fvoc_tot_mfing=rcfdg459;
     label ocd_fvoc_tot_mfing = 'Over-the counter derivatives - total fair value of collateral of monoline financial guarantors';

     /* Hedge Funds */
     if dt<20090630 then
	ocd_fvoc_usd_hun=.;
     else ocd_fvoc_usd_hun=rcfdg425;
     label ocd_fvoc_usd_hun = 'Over-the counter derivatives - fair value of collateral of cash - U.S. dollar of hedge funds';

     if dt<20090630 then
	ocd_fvoc_oc_hun=.;
     else ocd_fvoc_oc_hun=rcfdg430;
     label ocd_fvoc_oc_hun = 'Over-the counter derivatives - fair value of collateral of cash - other currencies of hedge funds';

     if dt<20090630 then
	ocd_fvoc_tsec_hun=.;
     else ocd_fvoc_tsec_hun=rcfdg435;
     label ocd_fvoc_tsec_hun = 'Over-the counter derivatives - fair value of collateral of U.S. Treasury securities of hedge funds';

     if dt<20090630 then
	ocd_fvoc_usgds_hun=.;
     else ocd_fvoc_usgds_hun=rcfdg440;
     label ocd_fvoc_usgds_hun = 'Over-the counter derivatives - fair value of collateral of U.S. Government agency and U.S. Government-sponsored agency debt securities of hedge funds';

     if dt<20090630 then
	ocd_fvoc_cbds_hun=.;
     else ocd_fvoc_cbds_hun=rcfdg445;
     label ocd_fvoc_cbds_hun = 'Over-the counter derivatives - fair value of collateral of corporate bonds of hedge funds';

     if dt<20090630 then
	ocd_fvoc_eqs_hun=.;
     else ocd_fvoc_eqs_hun=rcfdg450;
     label ocd_fvoc_eqs_hun = 'Over-the counter derivatives - fair value of collateral of equity securities of hedge funds';

     if dt<20090630 then
	ocd_fvoc_othc_hun=.;
     else ocd_fvoc_othc_hun=rcfdg455;
     label ocd_fvoc_othc_hun = 'Over-the counter derivatives - fair value of collateral of all other collateral of hedge funds';

     if dt<20090630 then
	ocd_fvoc_tot_hun=.;
     else ocd_fvoc_tot_hun=rcfdg460;
     label ocd_fvoc_tot_hun = 'Over-the counter derivatives - total fair value of collateral of hedge funds';


     /* Sovereign Governments */
     if dt<20090630 then
	ocd_fvoc_usd_svgov=.;
     else ocd_fvoc_usd_svgov=rcfdg426;
     label ocd_fvoc_usd_svgov = 'Over-the counter derivatives - fair value of collateral of cash - U.S. dollar of sovereign governments';

     if dt<20090630 then
	ocd_fvoc_oc_svgov=.;
     else ocd_fvoc_oc_svgov=rcfdg431;
     label ocd_fvoc_oc_svgov = 'Over-the counter derivatives - fair value of collateral of cash - other currencies of sovereign governments';

     if dt<20090630 then
	ocd_fvoc_tsec_svgov=.;
     else ocd_fvoc_tsec_svgov=rcfdg436;
     label ocd_fvoc_tsec_svgov = 'Over-the counter derivatives - fair value of collateral of U.S. Treasury securities of sovereign governments';

     if dt<20090630 then
	ocd_fvoc_usgds_svgov=.;
     else ocd_fvoc_usgds_svgov=rcfdg441;
     label ocd_fvoc_usgds_svgov = 'Over-the counter derivatives - fair value of collateral of U.S. Government agency and U.S. Government-sponsored agency debt securities of sovereign governments';

     if dt<20090630 then
	ocd_fvoc_cbds_svgov=.;
     else ocd_fvoc_cbds_svgov=rcfdg446;
     label ocd_fvoc_cbds_svgov = 'Over-the counter derivatives - fair value of collateral of corporate bonds of sovereign governments';

     if dt<20090630 then
	ocd_fvoc_eqs_svgov=.;
     else ocd_fvoc_eqs_svgov=rcfdg451;
     label ocd_fvoc_eqs_svgov = 'Over-the counter derivatives - fair value of collateral of equity securities of sovereign governments';

     if dt<20090630 then
	ocd_fvoc_othc_svgov=.;
     else ocd_fvoc_othc_svgov=rcfdg456;
     label ocd_fvoc_othc_svgov = 'Over-the counter derivatives - fair value of collateral of all other collateral of sovereign governments';

     if dt<20090630 then
	ocd_fvoc_tot_svgov=.;
     else ocd_fvoc_tot_svgov=rcfdg461;
     label ocd_fvoc_tot_svgov = 'Over-the counter derivatives - total fair value of collateral of sovereign governments';

     /* Corporations and all other counterparties */
     if dt<20090630 then
	ocd_fvoc_usd_calocp=.;
     else ocd_fvoc_usd_calocp=rcfdg427;
     label ocd_fvoc_usd_calocp = 'Over-the counter derivatives - fair value of collateral of cash - U.S. dollar of corporations and all other counterparties';

     if dt<20090630 then
	ocd_fvoc_oc_calocp=.;
     else ocd_fvoc_oc_calocp=rcfdg432;
     label ocd_fvoc_oc_calocp = 'Over-the counter derivatives - fair value of collateral of cash - other currencies of corporations and all other counterparties';

     if dt<20090630 then
	ocd_fvoc_tsec_calocp=.;
     else ocd_fvoc_tsec_calocp=rcfdg437;
     label ocd_fvoc_tsec_calocp = 'Over-the counter derivatives - fair value of collateral of U.S. Treasury securities of corporations and all other counterparties';

     if dt<20090630 then
	ocd_fvoc_usgds_calocp=.;
     else ocd_fvoc_usgds_calocp=rcfdg442;
     label ocd_fvoc_usgds_calocp = 'Over-the counter derivatives - fair value of collateral of U.S. Government agency and U.S. Government-sponsored agency debt securities of corporations and all other counterparties';

     if dt<20090630 then
	ocd_fvoc_cbds_calocp=.;
     else ocd_fvoc_cbds_calocp=rcfdg447;
     label ocd_fvoc_cbds_calocp = 'Over-the counter derivatives - fair value of collateral of corporate bonds of corporations and all other counterparties';

     if dt<20090630 then
	ocd_fvoc_eqs_calocp=.;
     else ocd_fvoc_eqs_calocp=rcfdg452;
     label ocd_fvoc_eqs_calocp = 'Over-the counter derivatives - fair value of collateral of equity securities of corporations and all other counterparties';

     if dt<20090630 then
	ocd_fvoc_othc_calocp=.;
     else ocd_fvoc_othc_calocp=rcfdg457;
     label ocd_fvoc_othc_calocp = 'Over-the counter derivatives - fair value of collateral of all other collateral of corporations and all other counterparties';

     if dt<20090630 then
	ocd_fvoc_tot_calocp=.;
     else ocd_fvoc_tot_calocp=rcfdg462;
     label ocd_fvoc_tot_calocp = 'Over-the counter derivatives - total fair value of collateral of corporations and all other counterparties';



	/************************************************************************
	*************************************************************************
	*****    SCHEDULE RC-M - MEMORANDA     *****
	*************************************************************************
	************************************************************************/;

        	if dt < 19940331 then
	obm_less1yr = .;
	else if dt>=19940331 & dt<20010331 then
	obm_less1yr = rcfd2332;
	else obm_less1yr = sum(rcfd2651, rcfdb571) ;
	label obm_less1yr='Other Borrowed Money - less than 1 year' ;

    /****************************************************************
    **********     SCHEDULE RC-N: NON-PERFORMING LOANS     **********
    **********      (PAST DUE AND NON-ACCRUAL LOANS)       **********
    ****************************************************************/

	/* Non-performing Loans, Total */
  	/* Date range (rcfd1406,rcfd1407,rcfd1403): 1982q4 to present */
		pdl_tot_3089 = .;
		pdl_tot_90   = .;
		pdl_tot_non  = .;
		pdl_tot_3089 = rcfd1406;
		pdl_tot_90   = rcfd1407;
		pdl_tot_non  = rcfd1403;

       	npl_tot = .;
       	npl_tot = sum(rcfd1407,rcfd1403);
       	label npl_tot='Non-performing Loans, Total';

	/* Real Estate */
	/* Non-performing Real Estate Loans */
 	/* It is my understanding that Loans secured by Real Estate before
    2001 includes loans in domestic and foreign offices, so we should
    include b572-b574 from 2001q1 forward for consistency. Someone
    should check this in the future. - Matt Botsch, 17May2007 */
 	/* Prior to 2001q1, variable defs for forms FFIEC 031,032
    differ from FFIEC 033,034:
    031,032: (rcfd1245+1248, rcfd1246+1249, rcfd1247+1250)
    033,034: (rcon1210,rcon1211,rcon1212)            */
  	/* Date range 2001q1 to present (forms FFIEC 031, 041):
    (rcfd1421+rcfnb572,rcfd1422+rcfnb573,rcfd1423+rcfnb574) */
		pdl_re_3089 = .;
       	pdl_re_90 = .;
       	pdl_re_non = .;
       	if dt >= 19840331 & dt <= 20001231 then do;
        pdl_re_3089 = sum(rcfd1245,rcfd1248,rcon1210);
        pdl_re_90 = sum(rcfd1246,rcfd1249,rcon1211);
        pdl_re_non = sum(rcfd1247,rcfd1250,rcon1212);
       	end;
       	if dt >20001231 then do;
        pdl_re_3089 = sum(rcfd1421,rcfnb572);
        pdl_re_90 = sum(rcfd1422,rcfnb573);
        pdl_re_non = sum(rcfd1423,rcfnb574);
        end;

       	npl_re = .;
      	npl_re = sum(pdl_re_90,pdl_re_non) ;

   		label pdl_re_3089='Real Estate Loans -- 30-89 Days Past Due' ;
   		label pdl_re_90='Real Estate Loans -- 90 + Days Past Due' ;
   		label pdl_re_non='Real Estate Loans -- Nonaccrual' ;
   		label npl_re='Non-Performing Real Estate Loans' ;

	/* Non-performing 1-4 Family Residential Real Estate (Mortgages + HE) */
	/* Date range (rcon5398,rcon5399,rcon5400,rcon5401,rcon5402,rcon5403): 1991q1 to present */
	/* Date range (rcon5430,rcon5431,rcon5432,rcon5433,rcon5434,rcon5435): 1991q1 to 2000q4 */
	/* Date range (rconc236,rconc237,rconc229,rconc238,rconc239,rconc230): 2002q1 to present */
		pdl_rre_3089 = .;
		pdl_rre_90 = .;
		pdl_rre_non = .;
		pdl_heloc_3089 = .;
		pdl_heloc_90 = .;
		pdl_heloc_non = .;
		pdl_closedlien_3089 = .;
		pdl_closedlien_90 = .;
		pdl_closedlien_non = .;
		pdl_firstlien_3089 = .;
		pdl_firstlien_90 = .;
		pdl_firstlien_non = .;
		pdl_jrlien_3089 = .;
		pdl_jrlien_90 = .;
		pdl_jrlien_non = .;
		if dt >=19910331 & dt <=20001231 then do;
      	pdl_rre_3089 = sum(rcon5398,rcon5430,rcon5401,rcon5433);
      	pdl_rre_90 = sum(rcon5399,rcon5431,rcon5402,rcon5434);
      	pdl_rre_non = sum(rcon5400,rcon5432,rcon5403,rcon5435);
      	pdl_heloc_3089 = sum(rcon5398,rcon5430);
      	pdl_heloc_90 = sum(rcon5399,rcon5431);
      	pdl_heloc_non = sum(rcon5400,rcon5432);
      	pdl_closedlien_3089 = rcon5401;
      	pdl_closedlien_90 = rcon5402;
      	pdl_closedlien_non = rcon5403;
	    pdl_firstlien_3089 = .;
	    pdl_firstlien_90 = .;
      	pdl_firstlien_non = .;
      	pdl_jrlien_3089 = .;
      	pdl_jrlien_90 = .;
      	pdl_jrlien_non = .;
      	end;
        if dt > 20001231 & dt < 20020331 then do;
        pdl_rre_3089 = sum(rcon5398,rcon5401);
        pdl_rre_90 = sum(rcon5399,rcon5402);
        pdl_rre_non = sum(rcon5400,rcon5403);
        pdl_heloc_3089 = rcon5398;
        pdl_heloc_90 = rcon5399;
        pdl_heloc_non = rcon5400;
        pdl_closedlien_3089 = sum(rconc236,rconc238);
        pdl_closedlien_90 = sum(rconc237,rconc239);
        pdl_closedlien_non = sum(rconc229,rconc230);
        pdl_firstlien_3089 = rconc236;
        pdl_firstlien_90 = rconc237;
        pdl_firstlien_non = rconc229;
        pdl_jrlien_3089 = rconc238;
        pdl_jrlien_90 = rconc239;
        pdl_jrlien_non = rconc230;
		end;
        else if dt >= 20020331 then do;
		pdl_rre_3089 = sum(rcon5398,rconc236,rconc238);
		pdl_rre_90 = sum(rcon5399,rconc237,rconc239);
		pdl_rre_non = sum(rcon5400,rconc229,rconc230);
		pdl_heloc_3089 = rcon5398;
		pdl_heloc_90 = rcon5399;
      	pdl_heloc_non = rcon5400;
      	pdl_closedlien_3089 = sum(rconc236,rconc238);
      	pdl_closedlien_90 = sum(rconc237,rconc239);
      	pdl_closedlien_non = sum(rconc229,rconc230);
      	pdl_firstlien_3089 = rconc236;
      	pdl_firstlien_90 = rconc237;
      	pdl_firstlien_non = rconc229;
      	pdl_jrlien_3089 = rconc238;
      	pdl_jrlien_90 = rconc239;
      	pdl_jrlien_non = rconc230;
      	end;

       	npl_rre = sum(pdl_rre_90, pdl_rre_non);
       	npl_heloc = sum(pdl_heloc_90, pdl_heloc_non) ;
       	npl_closedlien = sum(pdl_closedlien_90, pdl_closedlien_non) ;
       	npl_firstlien = sum(pdl_firstlien_90, pdl_firstlien_non) ;
       	npl_jrlien = sum(pdl_jrlien_90, pdl_jrlien_non) ;

	   	label pdl_rre_3089='Residential Real Estate -- 30-89 Days Past Due' ;
   		label pdl_rre_90='Residential Real Estate -- 90+ Days Past Due' ;
   		label pdl_rre_non='Residential Real Estate -- Nonaccrual' ;
   		label npl_rre='Non-Performing Residential Estate' ;
       	label pdl_heloc_3089='1-4 Family HELOCs -- 30-89 Days Past Due' ;
       	label pdl_heloc_90='1-4 Family HELOCs -- 90+ Days Past Due' ;
       	label pdl_heloc_non='1-4 Family HELOCs -- Nonaccrual' ;
      	label npl_heloc='Non-Performing 1-4 Family HELOCs' ;
       	label pdl_closedlien_3089='1-4 Family Closed Lien Mortgages -- 30-89 Days Past Due' ;
       	label pdl_closedlien_90='1-4 Family Closed Lien Mortgages -- 90+ Days Past Due' ;
       	label pdl_closedlien_non='1-4 Family Closed Lien Mortgages -- Nonaccrual' ;
       	label npl_closedlien='Non-Performing 1-4 Family Closed Lien Mortgages' ;
       	label pdl_firstlien_3089='1-4 Family 1st Lien Mortgages -- 30-89 Days Past Due' ;
       	label pdl_firstlien_90='1-4 Family 1st Lien Mortgages -- 90+ Days Past Due' ;
       	label pdl_firstlien_non='1-4 Family 1st Lien Mortgages -- Nonaccrual' ;
       	label npl_firstlien='Non-Performing 1-4 Family 1st Lien Mortgages' ;
       	label pdl_jrlien_3089='1-4 Family Jr Lien Mortgages -- 30-89 Days Past Due' ;
       	label pdl_jrlien_90='1-4 Family Jr Lien Mortgages -- 90+ Days Past Due' ;
       	label pdl_jrlien_non='1-4 Family Jr Lien Mortgages -- Nonaccrual' ;
       	label npl_jrlien='Non-Performing 1-4 Family Jr Lien Mortgages' ;

	/*Construction*/
	/*Date Range: 1991q1 to present*/
	/*Residential and Other Construction/Land Development, Date Range: 2007q1 to present*/
		pdl_const_3089 = .;
		pdl_const_90 = .;
		pdl_const_non = .;
		pdl_resconst_3089 = .;
		pdl_resconst_90 = .;
		pdl_resconst_non = .;
		pdl_crelandconst_3089 = .;
		pdl_crelandconst_90 = .;
		pdl_crelandconst_non = .;
		if dt>=19910331 & dt<20070331 then do;
		pdl_const_3089 = rcon2759;
		pdl_const_90 = rcon2769;
		pdl_const_non = rcon3492;
		pdl_resconst_3089 = .;
		pdl_resconst_90 = .;
		pdl_resconst_non = .;
		pdl_crelandconst_3089 = .;
		pdl_crelandconst_90 = .;
		pdl_crelandconst_non = .;
		end;
       	else if dt>=20070331 then do;
		pdl_const_3089 = rcon2759;
		pdl_resconst_3089 = rconf172;
		pdl_crelandconst_3089 = rconf173;
		pdl_const_90 = rcon2769;
		pdl_resconst_90 = rconf174;
		pdl_crelandconst_90 = rconf175;
		pdl_const_non = rcon3492;
		pdl_resconst_non = rconf176;
 	 	pdl_crelandconst_non = rconf177;
       	end;
   		npl_const =  sum(pdl_const_90, pdl_const_non);
   		npl_resconst = sum(pdl_resconst_90, pdl_resconst_non);
   		npl_crelandconst = sum(pdl_crelandconst_90, pdl_crelandconst_non);

       	label pdl_const_3089 = 'Construction loans -- 30-89 days past due';
       	label pdl_const_90 = 'Construction loans -- 90+ days past due';
       	label pdl_const_non = 'Construction loans -- Nonaccrual';
       	label npl_const = 'Non-Performing construction loans';
       	label pdl_resconst_3089 = '1-4 Residential Construction loans -- 30-89 days past due';
       	label pdl_resconst_90 = '1-4 Residential Construction loans -- 90+ days past due';
       	label pdl_resconst_non = '1-4 Residential Construction loans -- Nonaccrual';
       	label npl_resconst = 'Non-Performing 1-4 Residential Construction loans';
       	label pdl_crelandconst_3089 = 'Other Construction and Land loans -- 30-89 days past due';
       	label pdl_crelandconst_90 = 'Other Construction and Land loans -- 90+ days past due';
       	label pdl_crelandconst_non = 'Other Construction and Land loans -- Nonaccrual';
       	label npl_crelandconst = 'Non-Performing Other Construction and Land loans';

	/* Multifamily */
	/* Date Range: 1991q1 to present */
		pdl_multi_3089 = .;
		pdl_multi_90 = .;
		pdl_multi_non = .;
		if dt>=19910331 then do;
		pdl_multi_3089 = rcon3499;
		pdl_multi_90 = rcon3500;
		pdl_multi_non = rcon3501;
        end;

		npl_multi = sum(pdl_multi_90, pdl_multi_non);

		label pdl_multi_3089='Multifamily Real Estate Loans -- 30-89 days past due';
		label pdl_multi_90='Multifamily Real Estate Loans -- 90 days past due';
		label pdl_multi_non='Multifamily Real Estate Loans -- Nonaccrual';
		label npl_multi='Non-Performing Multifamily Real Estate Loans';

	/* Nonfarm, nonresidential */
	/* Date Range: 1991q1 to present */
	/* Owner-occupied and Other date range: 2007q1 to present */
       	pdl_nfnr_3089 = .;
       	pdl_nfnr_90 = .;
       	pdl_nfnr_non = .;
       	pdl_ownoccnfnr_3089=.;
       	pdl_ownoccnfnr_90=.;
       	pdl_ownoccnfnr_non=.;
       	pdl_othernfnr_3089=.;
       	pdl_othernfnr_90=.;
       	pdl_othernfnr_non=.;
       	if dt>=19910331 & dt<20070331 then do;
       	pdl_nfnr_3089 = rcon3502;
       	pdl_nfnr_90 = rcon3503;
       	pdl_nfnr_non = rcon3504;
       	pdl_ownoccnfnr_3089=.;
       	pdl_ownoccnfnr_90=.;
       	pdl_ownoccnfnr_non=.;
       	pdl_othernfnr_3089=.;
       	pdl_othernfnr_90=.;
       	pdl_othernfnr_non=.;
       	end;
       	else if dt>=20070331 then do;
        pdl_nfnr_3089=rcon3502;
		pdl_ownoccnfnr_3089 = rconf178;
		pdl_othernfnr_3089 = rconf179;
		pdl_nfnr_90 = rcon3503;
		pdl_ownoccnfnr_90 = rconf180;
		pdl_othernfnr_90 = rconf181;
		pdl_nfnr_non = rcon3504;
		pdl_ownoccnfnr_non = rconf182;
		pdl_othernfnr_non = rconf183;
		end;

		npl_nfnr = sum(pdl_nfnr_90,pdl_nfnr_non);
		npl_ownoccnfnr = sum(pdl_ownoccnfnr_90, pdl_ownoccnfnr_non);
		npl_othernfnr = sum(pdl_othernfnr_90,  pdl_othernfnr_non);


       	label pdl_nfnr_3089='Nonfarm, Nonresidential Real Estate Loans -- 30-89 days past due';
       	label pdl_nfnr_90='Nonfarm, Nonresidential Real Estate Loans -- 90 days past due';
       	label pdl_nfnr_non='Nonfarm, Nonresidential Real Estate Loans -- Nonaccrual';
       	label npl_nfnr='Non-Performing Nonfarm, Nonresidential Real Estate Loans';
       	label pdl_ownoccnfnr_3089='Owner-Occupied Nonfarm, Nonresidential Real Estate Loans -- 30-89 days past due';
       	label pdl_ownoccnfnr_90='Owner-Occupied Nonfarm, Nonresidential Real Estate Loans -- 90 days past due';
       	label pdl_ownoccnfnr_non='Owner-Occupied Nonfarm, Nonresidential Real Estate Loans -- Nonaccrual';
       	label npl_ownoccnfnr='Non-Performing Owner-Occupied Nonfarm, Nonresidential Real Estate Loans';
       	label pdl_othernfnr_3089='Nonfarm, Other Nonresidential Real Estate Loans -- 30-89 days past due';
       	label pdl_othernfnr_90='Nonfarm, Other Nonresidential Real Estate Loans -- 90 days past due';
       	label pdl_othernfnr_non='Nonfarm, Other Nonresidential Real Estate Loans -- Nonaccrual';
       	label npl_othernfnr='Non-Performing Other Nonfarm, Nonresidential Real Estate Loans';



       	pdl_cre_3089 = sum(pdl_const_3089,pdl_multi_3089,pdl_nfnr_3089);
       	pdl_cre_90 = sum(pdl_const_90,pdl_multi_90,pdl_nfnr_90);
       	pdl_cre_non = sum(pdl_const_non,pdl_multi_non,pdl_nfnr_non);

		npl_cre = sum(pdl_cre_90, pdl_cre_non);

		label pdl_cre_3089='Commercial Real Estate Loans -- 30-89 Days Past Due' ;
		label pdl_cre_90='Commercial Real Estate Loans -- 90+ Days Past Due' ;
		label pdl_cre_non='Commercial Real Estate Loans -- Nonaccrual' ;
		label npl_cre='Non-Performing Commercial Real Estate Loans' ;


                /* Other Real Estate Loans */;
                if dt<19910331 then
                    npl_othre=.;
                else npl_othre=sum(npl_re,-npl_rre,-npl_cre);
                label npl_othre = 'Other Non-Performing Real Estate Loans';

   	/* Depository Institutions */
	/* Non-performing Loans to Depository Institutions and Acceptances of Other Banks */
	/* Date range (rcfd5377,rcfd5378,rcfd5379,rcfd5380,rcfd5381,rcfd5382): 1991q1 to present  */
	/* Date range (rcfdb834,rcfdb835,rcfdb836): 2001q4 to present */
		pdl_dep_3089 = .;
		pdl_dep_90 = .;
		pdl_dep_non = .;
		if dt >= 19910331 & dt < 20011231 then do;
		pdl_dep_3089 = sum(rcfd5377,rcfd5380);
		pdl_dep_90 = sum(rcfd5378,rcfd5381);
		pdl_dep_non = sum(rcfd5379,rcfd5382);
		end;
		if dt >= 20011231 then do;
		pdl_dep_3089 = rcfdb834;
		pdl_dep_90 = rcfdb835;
		pdl_dep_non = rcfdb836;
		end;

		npl_dep = sum(pdl_dep_90, pdl_dep_non);

		label pdl_dep_3089='Loans to Depository Institutions -- 30-89 Days Past Due' ;
       	label pdl_dep_90='Loans to Depository Institutions -- 90+ Days Past Due' ;
       	label pdl_dep_non='Loans to Depository Institutions -- Nonaccrual' ;
       	label npl_dep='Non-Performing Loans to Depository Institutions' ;

	/* Non-performing Loans to Finance Agricultural Production */
	/* Date range (rcfd1594,rcfd1597,rcfd1583): 1987q1 to present */
	/* Date range (rcon1594,rcon1597,rcon1583): 1982q4 to present */
		pdl_agr_3089 = .;
		pdl_agr_90 = .;
		pdl_agr_non = .;
		if (dt >= 19821231 < 19870331) then do;
		pdl_agr_3089 = rcon1594;
		pdl_agr_90 = rcon1597;
		pdl_agr_non = rcon1583;
		end;
		if dt >= 19870331 then do;
		pdl_agr_3089 = rcfd1594;
		pdl_agr_90 = rcfd1597;
		pdl_agr_non = rcfd1583;
		end;

		npl_agr = sum(pdl_agr_90, pdl_agr_non);

		label pdl_agr_3089='Loans to Finance Agricultural Production -- 30-89 Days Past Due' ;
		label pdl_agr_90='Loans to Finance Agricultural Production - 90 + Days Past Due' ;
		label pdl_agr_non='Loans to Finance Agricultural Production  - Nonaccrual' ;
		label npl_agr='Non-Performing Loans to Finance Agricultural Production' ;



	/* Non-performing C&I Loans */
	/* Date range (rcfd1606,rcfd1607,rcfd1608): 1985q4 to present */
	/* Date range (rcon1222,rcon1223,rcon1224): 1984q1 to 2000q4 */
	/* Date range (rcon1251,rcon1252,rcon1253): 1987q1 to 2000q4 */
	/* Date range (rcon1254,rcon1255,rcon1256): 1987q1 to present */
	/* 1607 and 1608 are for the FFIEC 30 and 1223 and 1224 are for the 0041 */ ;
		pdl_ci_3089 = .;
		pdl_ci_90 = .;
		pdl_ci_non = .;
		if dt < 19870331 then do ;
		pdl_ci_3089 = sum(rcfd1606,rcon1222);
		pdl_ci_90 = sum(rcfd1607,rcon1223);
		pdl_ci_non = sum(rcfd1608,rcon1224);
		end;
		else if (dt >= 19870331 & dt <= 20001231) then do ;
		pdl_ci_3089 = sum(rcon1222,rcon1251,rcon1254);
		pdl_ci_90 = sum(rcon1223,rcon1252,rcon1255);
		pdl_ci_non = sum(rcon1224,rcon1253,rcon1256);
		end;
		else do ;
		pdl_ci_3089 = rcfd1606;
		pdl_ci_90 = rcfd1607;
		pdl_ci_non = rcfd1608 ;
		end ;

		npl_ci = sum(pdl_ci_90, pdl_ci_non);

		label pdl_ci_3089='C&I Loans -- 30-89 Days Past Due' ;
		label pdl_ci_90='C&I Loans -- 90 + Days Past Due' ;
		label pdl_ci_non='C&I Loans -- Nonaccrual' ;
		label npl_ci='Non-Performing C&I Loans' ;

   	/* Loans to individuals for household, family, and other personal expenditures */
	/* Non-performing Consumer Loans */
	/* Date range 1984q1 to Present */
		pdl_cons_3089 = .;
		pdl_cons_90 = .;
		pdl_cons_non = .;
		if (dt >= 19840331 & dt < 19910331) then do;
		pdl_cons_3089 = sum(rcfd1978,rcon1218,rcon1214);
		pdl_cons_90 = sum(rcfd1979,rcon1219,rcon1215);
		pdl_cons_non = sum(rcfd1981,rcon1220,rcon1216);
		end;
		else if (dt >= 19910331 & dt <= 20001231) then do;
		pdl_cons_3089 = sum(rcfd5383,rcfd5386,rcon1218,rcon1214);
		pdl_cons_90 = sum(rcfd5384,rcfd5387,rcon1219,rcon1215);
		pdl_cons_non = sum(rcfd5385,rcfd5388,rcon1220,rcon1216);
		end;
		else if dt <20110331 then do;
		pdl_cons_3089 = sum(rcfdb575,rcfdb578);
		pdl_cons_90 = sum(rcfdb576,rcfdb579);
		pdl_cons_non = sum(rcfdb577,rcfdb580);
		end;
		else do;
		pdl_cons_3089 = sum(rcfdb575,rcfdk213,rcfdk216);
		pdl_cons_90 = sum(rcfdb576,rcfdk214,rcfdk217);
		pdl_cons_non = sum(rcfdb577,rcfdk215,rcfdk218);
		end;

		npl_cons = sum(pdl_cons_90, pdl_cons_non);

		label pdl_cons_3089='Consumer Loans -- 30-89 Days Past Due' ;
		label pdl_cons_90='Consumer Loans -- 90 + Days Past Due' ;
		label pdl_cons_non='Consumer Loans -- Nonaccrual' ;
		label npl_cons='Non-Performing Consumer Loans' ;

	/* Non-performing Credit Card Loans */
	/* Note: NPL CC includes related plans of credit through 2000q4. Afterwards
	related plans of credit are included in other consumer.*/
	/* Date range (rcfdb575,rcfdb576,rcfdb577): 2001q1 to present */
	/* Date range (rcfd5383,rcfd5384,rcfd5385): 1991q1 to 2000q4 */
	/* Date range (rcon1218,rcon1219,rcon1220): 1984q1 to 2000q4 */
		pdl_cc_3089 = .;
		pdl_cc_90 = .;
		pdl_cc_non = .;
		if dt <= 20001231 then do;
		pdl_cc_3089 = sum(rcfd5383,rcon1218);
		pdl_cc_90 = sum(rcfd5384,rcon1219);
		pdl_cc_non = sum(rcfd5385,rcon1220);
		end;
		else if dt > 20001231 then do;
		pdl_cc_3089 = rcfdb575;
		pdl_cc_90 = rcfdb576;
		pdl_cc_non = rcfdb577;
		end;

		npl_cc = sum(pdl_cc_90, pdl_cc_non);

		label pdl_cc_3089='Credit Card Loans -- 30-89 Days Past Due' ;
		label pdl_cc_90='Credit Card Loans -- 90+ Days Past Due' ;
		label pdl_cc_non='Credit Card Loans -- Nonaccrual' ;
		label npl_cc='Non-Perfoming Credit Card Loans' ;

	/* Non-performing Other Consumer Loans */
	/* Date range 1984q1 to Present */
		pdl_othcons_3089 = .;
		pdl_othcons_90 = .;
		pdl_othcons_non = .;
		if dt <= 20001231 then do;
		pdl_othcons_3089 = sum(rcfd5386,rcon1214);
		pdl_othcons_90 = sum(rcfd5387,rcon1215);
		pdl_othcons_non = sum(rcfd5388,rcon1216);
		end;
		else if dt <20110331 then do;
		pdl_othcons_3089 = rcfdb578;
		pdl_othcons_90 = rcfdb579;
		pdl_othcons_non = rcfdb580;
		end;
		else do;
		pdl_othcons_3089 = sum(rcfdk213,rcfdk216);
		pdl_othcons_90 = sum(rcfdk214,rcfdk217);
		pdl_othcons_non = sum(rcfdk215,rcfdk218);
		end;

		npl_othcons = sum(pdl_othcons_90, pdl_othcons_non);

		label pdl_othcons_3089='Other Consumer Loans -- 30-89 Days Past Due' ;
		label pdl_othcons_90='Other Consumer Loans -- 90+ Days Past Due' ;
		label pdl_othcons_non='Other Consumer Loans -- Nonaccrual' ;
		label npl_othcons='Non-Perfoming Other Consumer Loans' ;

   	/* Foreign Govts */
	/* Non-performing Loans to Foreign governments */
	/* Date range (rcfd5389,rcfd5390,rcfd5391): 1991q1 to present */
		pdl_fgovt_3089 = .;
		pdl_fgovt_90 = .;
		pdl_fgovt_non = .;
		if dt >= 19910331 then do;
		pdl_fgovt_3089 = rcfd5389;
		pdl_fgovt_90 = rcfd5390;
		pdl_fgovt_non = rcfd5391;
		end;
		npl_fgovt = sum(pdl_fgovt_90, pdl_fgovt_non);

		label pdl_fgovt_3089='Loans to Foreign Governments -- 30-89 Days Past Due' ;
		label pdl_fgovt_90='Loans to Foreign Governments -- 90 + Days Past Due' ;
		label pdl_fgovt_non='Loans to Foreign Governments -- Nonaccrual' ;
		label npl_fgovt='Non-Performing Loans to Foreign Governments' ;

	/* All Other Non-performing Loans */
	/* Date range (rcfd5459,rcfd5460,rcfd5461): 1991q1 to present */
	/* Date range (rcfd1586,rcfd1587,rcfd1588): 1987q1 to 1990q4 */
		pdl_oth_3089 = .;
		pdl_oth_90 = .;
		pdl_oth_non = .;
		if dt < 19910331 then do;
		pdl_oth_3089 = rcfd1586;
		pdl_oth_90 = rcfd1587;
		pdl_oth_non = rcfd1588;
   		end;
		else if dt >= 19910331 then do;
		pdl_oth_3089 = rcfd5459;
		pdl_oth_90 = rcfd5460;
		pdl_oth_non = rcfd5461;
		end;
		npl_oth = sum(pdl_oth_90, pdl_oth_non);

		label pdl_oth_3089='All Other Loans -- 30-89 Days Past Due' ;
		label pdl_oth_90='All Other Loans -- 90+ Days Past Due' ;
		label pdl_oth_non='All Other Loans -- Nonaccrual' ;
		label npl_oth='Non-Performing, All Other Loans' ;

	/* Non-performing Lease Financing Receivables */
	/* Date range (rcfd1226,rcfd1227,rcfd1228): 2001q1 to present */
	/* Date range (rcfd1257,rcfd1258,rcfd1259,rcfd1271,rcfd1272,rcfd1791): 1987q1 to present */
		pdl_lease_3089 = .;
		pdl_lease_90 = .;
		pdl_lease_non = .;
		if dt < 20010331 then do;
		pdl_lease_3089 = sum(rcfd1257,rcfd1271);
		pdl_lease_90 = sum(rcfd1258,rcfd1272);
		pdl_lease_non = sum(rcfd1259,rcfd1791);
		end;
		else if dt >= 20010331 then do;
        pdl_lease_3089 = rcfd1226;
		pdl_lease_90 = rcfd1227;
        pdl_lease_non = rcfd1228;
	    end;
	    npl_lease = sum(pdl_lease_90, pdl_lease_non);

	    label pdl_lease_3089='Lease Financing Receivables -- 30-89 Days Past Due' ;
		label pdl_lease_90='Lease Financing Receivabless -- 90+ Days Past Due' ;
		label pdl_lease_non='Lease Financing Receivables -- Nonaccrual' ;
		label npl_lease='Non-Performing, Lease Financing Receivables' ;


                /* All Other Non-Performing Loans */ ;
                if dt<19910331 then
                    npl_allother = .;
                else npl_allother = sum(npl_oth,npl_agr,npl_othre,npl_dep,npl_fgovt);
                label npl_allother = 'All Other Non-Performing Loans';

                /* Added 7/25/2104 */;
                /* Date range: 1969q2 to present */
               commstock = . ;
               commstock  = rcfd3230 ;
             label commstock ='Common Stock' ;



                code8786=call8786;
                report_filed=rcon9804;
                label report_filed='Type of Report Filed (RCON9804)';
                scorp=riada530;
                label scorp='Firm is an S Corporation';



                drop rc: te: dt_start dt_end call8786 call8002 call8787 edgc8798 row_loaded_tstmp latest_row_ind D_DT: srce_flag tstmp_ext tstmp_load status_conf rias: rifn: ridm: ibfq: iadx: long;

    run;

* CHECK HERE: the lowcase should be referencing the correct file from above. the stat transfer should reflect the proper conftype;
    %lowcase(call,&ctype.);
%mend conftype;

%conftype(nc);

endrsubmit;
%sysexec st ../../../reg_data_nc/call/call_nc.sas7bdat ../../../reg_data_nc/call/call_nc.dta -y -o;
*%sysexec st ../../data_c/call/call_c.sas7bdat ../../data_c/call/call_c.dta -y -o;
