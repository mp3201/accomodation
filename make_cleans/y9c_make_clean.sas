/************************************************************************************************
 This file creates consistent time series for popular data items from BHC's Y9C filings. The
 program inputs raw data from quarterly Y9C filings [y9c[year][month].sas7bdat], and creates a
 "clean" file for every quarter that has a common set of consistenly defined variables [stored
 as newbhc[year][month].sas7bdat. All RSSD items and newly defined variables are kept.

 If you would like to add your (relatively non-obscure!) data item to this file:
 1) make sure that you have consulted MDRM and understand how the mneumonic/definition of
    the variable has changed over time for BOTH banks and BHCs
 2) add it to the appropriate section of this program (income statements, chargeoffs, etc) --
    pay attention to the style of the existing sas file and keep your entry consistent
 3) add your new variable to the excel master file, with the Label, Variable Name, and date range
 4) add your new variable to the "master keep list" in callpull_clean
 5) rerun-this program for all quarters
    LAST UPDATED: May 9th, 2011 by Peter

**************************************************************************************************/;
/*Submitting through the SAS grid*/;
%include '/data/sasbench/frbny_signon_user.sas';
rsubmit  task1;

*CHECK HERE: the nc should be referenced in the file paths for the call and outcall;
libname cin '/san/RDS/Derived/reg_data/data_c/fr_y9c';
libname cout '/san/RDS/Derived/reg_data/data_c/fr_y9c';
libname ncin '/san/RDS/Derived/reg_data_nc/fr_y9c';
libname ncout '/san/RDS/Derived/reg_data_nc/fr_y9c';

/* Make all variables lowercase */
*options macrogen symbolgen;
%macro lowcase(dsn,ctype);
     %let dsid=%sysfunc(open(&dsn));
     %let num=%sysfunc(attrn(&dsid,nvars));
     %put &num;
     data &ctype.out.&dsn._&ctype;
           set &dsn(rename=(
        %do i = 1 %to &num;
        %let var&i=%sysfunc(varname(&dsid,&i));      /*function of varname returns the name of a SAS data set variable*/
        &&var&i=%sysfunc(lowcase(&&var&i))         /*rename all variables*/
        %end;));
        %let close=%sysfunc(close(&dsid));
  run;
    %mend lowcase;

* CHECK HERE: The data step needs to reference nc in the in and outcall. here is the outcall:;
%macro conftype(ctype);
    data fr_y9c(rename=(dt=date ID_RSSD=entity nm_short=name));

    * CHECK HERE: and here is the incall where the nc also needs to be referenced;
        set &ctype.in.cuv_bhcf_&ctype. ;
        name=rssd9010;
        y9c9802=bhck9802;
        label y9c9802='Top tier filer indicator' ;
                * CHECK HERE: the dt step should delete anything greater than the desired date;
        if dt>20140630 then delete;



/****************************************************/;
/****************************************************/;
/****************************************************/;
/****************************************************/;
/************ INCOME STATEMENT VARIABLES ************/;
/****************************************************/;
/****************************************************/;
/****************************************************/;
/****************************************************/;

	/* Net Income */;
        if dt < 19810630 then
            ytdnetinc = . ;
        else ytdnetinc = bhck4340;
	label ytdnetinc='Net Income' ;

	/* Interest Income */;
        if dt < 19810630 then
            ytdint_inc = . ;
        else ytdint_inc = bhck4107 ;
	label ytdint_inc='Interest Income';

		/* Interest & Fee Income on Loans in Domestic Offices */;
	        if dt < 19860630 then
	           ytdint_inc_lndom = . ;
                else if dt>=19860630 & dt<=19870630 then
                    ytdint_inc_lndom = sum(bhck4393, bhck4057, bhck4065);
                else if dt>19870630 & dt<=19900630 then
                    ytdint_inc_lndom= sum(bhck4393, bhck4503, bhck4504, bhck4065);
                else if dt>19900630 & dt<=20001231 then
                    ytdint_inc_lndom = sum(bhck4393, bhck4503, bhck4504, bhck4505, bhck4307);
                else if dt>20001231 & dt<=20071231 then
                    ytdint_inc_lndom = sum(bhck4010, bhck4065);
                else ytdint_inc_lndom = sum(bhck4435, bhck4436, bhckf821, bhck4065);
	        label ytdint_inc_lndom='Interest & Fee Income on Loans in Domestic Offices' ;

                /* Interest & Fee Income on Loans in Foreign Offices */;
	        if dt < 19860630 then
	            ytdint_inc_lnfor = . ;
	        else ytdint_inc_lnfor = bhck4059;
	        label ytdint_inc_lnfor='Interest & Fee Income on Loans in Foreign Offices' ;

		/* Interest Income from Trading Assets */;
		if dt <198696 then
		    ytdint_inc_trad= . ;
		else ytdint_inc_trad= bhck4069;
		label ytdint_inc_trad='Interest Income From Trading Assets';

                /* Interest Income on Securities (Total) + Interest Income from Depository Institutions*/;
                if dt<19860630 then
                    ytdint_inc_sec= . ;
                else if dt>=19860630 & dt<=19870630 then
                    ytdint_inc_sec = sum(bhck4105,bhck4106,bhck4027,bhck4066,bhck4394
                    ,bhck4395,bhck1638,bhck1639);
                else if dt>=19870930 & dt<=20001231 then
                    ytdint_inc_sec = sum(bhck4105,bhck4106,bhck4027,bhck4506,bhck4507,bhck4394,
                    bhck4395,bhck1638,bhck1639 );
                else ytdint_inc_sec= sum(bhck4115,bhckb488,bhckb489,bhck4060);
                label ytdint_inc_sec='Interest and Dividend Incomd on Securities';

				/*Interest Income on IBB only*/
				if dt<19860630 then
				ytdint_inc_ibb=.;
				else if dt>=19860630 & dt<=20001231 then
				ytdint_inc_ibb=sum(bhck4105,bhck4106);
				else ytdint_inc_ibb=bhck4115;
				label ytdint_inc_ibb='Interest Income on Balances';


                /* Interest Income on Federal Funds and Repos*/;
              	if dt <19860630 then
		        ytdint_inc_ffrepo = . ;
                else ytdint_inc_ffrepo = bhck4020;
     			label ytdint_inc_ffrepo = 'Interest Income on Federal Funds and Repos';

                /* All other Interest Income*/;
                if dt < 19860630 then
                    ytdint_inc_alloth = .;
                else ytdint_inc_alloth= sum(ytdint_inc,-ytdint_inc_lndom,
                    -ytdint_inc_lnfor,-ytdint_inc_trad,-ytdint_inc_sec,
                    -ytdint_inc_ffrepo);
                label ytdint_inc_alloth = 'Interest Income from All Other Asset';

				/*Other Interest Income*/;
				if dt < 19870930 then
					ytdint_inc_other = .;
				else ytdint_inc_other = bhck4518;
				label ytdint_inc_other = 'Other Interest Income';

	/* Interest Expense */;
	if dt <19810630 then
	    ytdint_exp = . ;
	else ytdint_exp = bhck4073;
	label ytdint_exp='Interest Expense' ;

                /* Interest Expense on Domestic Deposits */;
                 if dt <19860630 then
                     ytdint_exp_dom = .;
                 else if dt>=19860630 & dt<=19900630 then
                     ytdint_exp_dom = sum(bhck4174,bhck4176);
                 else if dt>=19900930 & dt<=19961231 then
                     ytdint_exp_dom = sum(bhck4174,bhck6760,bhck6761);
                 else ytdint_exp_dom = sum(bhck6761,bhcka517,bhcka518);
                 label ytdint_exp_dom = 'Interest on Domestic Deposits';

                 /* Interest Expense on Foreign Deposits */;
                 if dt<19860630 then
                     ytdint_exp_for = .;
                 else ytdint_exp_for=bhck4172;
                 label ytdint_exp_for='Interest on Foreign Deposits';

                 /* Interest Expense on Federal Funds purchased and securities
                     sold under agreements to repurchase */;
                 if dt<19860630 then
                     ytdint_exp_ffrepo = .;
                 else ytdint_exp_ffrepo = bhck4180;
                 label ytdint_exp_ffrepo = 'Interest Expense on Federal Funds and Repos';


                 /* Interest on Trading Liabilities and Other Borrowed Money */;
                 if dt<19860630 then
                     ytdint_exp_trad_othbor = .;
                 else if dt>=19860630 & dt<=20001231 then
                     ytdint_exp_trad_othbor = bhck4396;
                 else ytdint_exp_trad_othbor = bhck4185;
                 label ytdint_exp_trad_othbor = 'Interest on Trading Liabilities and Other Borrowed Money';


                 /*Interest on Subordinated Notes and Debentures*/;
                 if dt<19860630 then
                     ytdint_exp_subdebt =.;
                else ytdint_exp_subdebt = bhck4397;
                label ytdint_exp_subdebt = 'Interest on Subordinated Notes and Debentures';

                 /* All Other Interest Expenses */;
                 if dt < 19860630 then
                     ytdint_exp_alloth = .;
                 else ytdint_exp_alloth = sum(ytdint_exp,-ytdint_exp_dom, -ytdint_exp_for,-ytdint_exp_ffrepo,-ytdint_exp_trad_othbor,-ytdint_exp_subdebt);
                 label ytdint_exp_alloth = 'All Other Interest Expenses';

	/* Net Interest Income */;
        if dt < 19810630 then
            ytdint_inc_net = . ;
        else ytdint_inc_net = bhck4074;
        label ytdint_inc_net='Net Interest Income';

/***********************************************************************/;
			/*FOR GRANULAR SECURITIES MODELLING*/;
				/*Interest Income on IBB only*/;
				if dt<19860630 then
				ytdint_inc_ibb_bhc=.;
				else if dt>=19860630 & dt<=20001231 then
				ytdint_inc_ibb_bhc=sum(bhck4105, bhck4106);
				else ytdint_inc_ibb_bhc=bhck4115;
				label ytdint_inc_ibb_bhc='Interest Income on Balances: BHCs Only';


				/*Interest Income on U.S. Treasury and agency securities BHCs Only*/;
				if dt<19810630 then
					ytdint_inc_sec_ust_bhc = .;
				else if dt>=19810630 & dt<20010331 then
					ytdint_inc_sec_ust_bhc = bhck4027;
				else ytdint_inc_sec_ust_bhc = bhckb488;
				label ytdint_inc_sec_ust_bhc = 'Interest Income on U.S. Treasury and Agency Securities: BHCs Only';

				/*Interest Income on MBS BHCs Only*/;
				if dt<20010331 then
					ytdint_inc_sec_mbs_bhc =.;
				else ytdint_inc_sec_mbs_bhc = bhckb489;
				label ytdint_inc_sec_mbs_bhc = 'Interest Income on MBS: BHCs Only';

				/*Interest Income on MBS*/;
				if dt<20010331 then
					ytdint_inc_sec_mbs =.;
				else ytdint_inc_sec_mbs = bhckb489;
				label ytdint_inc_sec_mbs = 'Interest Income on MBS';

				/*Interest Income on All Other Securities*/;
				if dt<19810630 then
					ytdint_inc_sec_oth_bhc =.;
				else if dt>=19810630 & dt<19870930 then
					ytdint_inc_sec_oth_bhc = sum(bhck4394, bhck4395, bhck1638, bhck1639);
				else if dt>=19870930 & dt<20010331 then
					ytdint_inc_sec_oth_bhc = sum(bhck4394, bhck4395, bhck1638, bhck1639, bhck4506, bhck4507);
				else ytdint_inc_sec_oth_bhc = bhck4060;
				label ytdint_inc_sec_oth_bhc = 'Interest Income on All Other Securities: BHCs Only';

				/*Interest Income on Securities Less Interest Income on MBS*/;
  				if dt< 19860630 then
                     ytdint_inc_sec_nombs = .;
                 else ytdint_inc_sec_nombs = sum(ytdint_inc_sec, -ytdint_inc_sec_mbs);
                 label ytdint_inc_sec_nombs = 'Interest Income on Securities, No MBS';

			/****************************************/;


	/* Total Noninterest Income */;
		if dt < 19810630 then
           ytdnonint_inc = . ;
		else ytdnonint_inc = bhck4079;
	    label ytdnonint_inc='Total Noninterest Income' ;

	/* Total Noninterest Income Excluding OREO*/;
        /*Net gains (losses) on sales of other real estate owned (oreo) taken out since it started being reported in 1994q1*/;
		if dt < 19810630 then
           ytdnonint_inc_no_oreo = . ;
		else ytdnonint_inc_no_oreo = sum(bhck4079,-bhck8561);
	    label ytdnonint_inc_no_oreo ='Total Noninterest Income, Excluding OREO Since 1994q1' ;


		/*Net gains (losses) on sales of other real estate owned (oreo) as its own variable*/;
		if dt < 19940331 then
			ytdnonint_inc_oreo = . ;
		else ytdnonint_inc_oreo = bhck8561;
		label ytdnonint_inc_oreo = 'Noninterest Income from OREO' ;

		/*Net change in the fair values of financial instruments accounted for under a fair value option as its own variable for 6 large banks only*/;
		if dt < 20070331 then
			ytdnonint_inc_fvo = . ;
		else if ID_RSSD=1039502|ID_RSSD=1073757|ID_RSSD=1120754|ID_RSSD=1951350|ID_RSSD=2162966|ID_RSSD=2380443 then
			ytdnonint_inc_fvo = bhckf229;
		else ytdnonint_inc_fvo = . ;
		label ytdnonint_inc_fvo = 'FVO for Large Banks' ;


		/* Fiduciary Income */;
	        if dt < 19810630 then
	            ytdnonint_inc_fiduc = . ;
	        else ytdnonint_inc_fiduc = bhck4070;
		label ytdnonint_inc_fiduc='Fiduciary Income' ;

                	/* Service Income */;
	        if dt < 19810630 then
	            ytdservice_inc = . ;
	        else ytdservice_inc = bhck4483 ;
		label ytdservice_inc='Service Income' ;

		/* Trading Income */;
	        if dt < 19860630 then
	            ytdtradrev_inc = . ;
	        else if dt >= 19860630 & dt <= 19951231 then
	            ytdtradrev_inc = sum(bhck1655,bhck4077) ;
	        else ytdtradrev_inc = bhcka220 ;
		label ytdtradrev_inc='Trading Revenue (Income)' ;

                /* Subcomponents of Trading Income (for revised CCAR) */
	        if dt < 19950331 then
	            ytdtrad_ir_inc = . ;
	        else ytdtrad_ir_inc = bhck8757 ;
		label ytdtrad_ir_inc='Interest Rate Exposures' ;

	        if dt < 19950331 then
	            ytdtrad_fx_inc = . ;
	        else ytdtrad_fx_inc = bhck8758 ;
		label ytdtrad_fx_inc='Foreign Exchange Exposures' ;

	        if dt < 19950331 then
	            ytdtrad_eq_inc = . ;
	        else ytdtrad_eq_inc = bhck8759 ;
		label ytdtrad_eq_inc='Equity Security or Index Exposures' ;

                	        if dt < 19950331 then
	            ytdtrad_com_oth_inc = . ;
	        else ytdtrad_com_oth_inc = bhck8760 ;
		label ytdtrad_com_oth_inc='Commodity and Other Exposures' ;

 	        if dt < 20070331 then
	            ytdtrad_cred_inc = . ;
	        else ytdtrad_cred_inc = bhckf186 ;
		label ytdtrad_cred_inc='Credit Exposures' ;

  	        if dt < 19950331 then
	            ytdtrad_allother_inc = . ;
	        else if dt <20070331 then ytdtrad_allother_inc = bhck8760 ;
                else ytdtrad_allother_inc = sum(bhck8760,bhckf186);
		label ytdtrad_allother_inc='Commodity and Other Exposures - Incl. Credit Exposures' ;

                	/* Fees and Other Income, INCLUDES OREO */;
		ytdfeeOther_inc = sum(ytdnonint_inc,-ytdtradrev_inc,-ytdservice_inc,-ytdnonint_inc_fiduc) ;
		label ytdfeeOther_inc='Fees and Other Income' ;

			/* Net Securitization Income */;
			 /* Date range: 2001q1 to present */;
		        if dt < 20010331 then
		           ytdsectn_inc = . ;
		        else ytdsectn_inc = bhckb493 ;
			label ytdsectn_inc='Net Securitization Income' ;

			/* Venture Capital Revenue */
			 /* Date range: 2001q1 to present */;
		        if dt < 20010331 then
			    ytdvc_inc = . ;
			else ytdvc_inc = bhckb491;
			label ytdvc_inc='Venture Capital Revenue';

                  /* Service Charges, Commissions, and Fees 2 */ ;
                 /* Includes all Service Charges, Commissions and Fees, but does not include "Other Service Charges, Commissions, and Fees" (BHCK4399), a category from before 2001q1 which is moved into "Other Noninterest Income" (BHCK497) after 2001q1 */;
                 /*if dt<19860630 then
                     ytdnonint_inc_fees = . ;
                 else if dt>=19860630 & dt<=20001231 then
                     ytdnonint_inc_fees = sum(bhck4483) ;
                 else if dt>=20010331 & dt<=20021231 then
                     ytdnonint_inc_fees = sum(bhck4483, bhckb490, bhckb492, bhckb494);
                 else if dt>=20030331 & dt<=20061231 then
                     ytdnonint_inc_fees = sum(bhck4483, bhckb490, bhckb492, bhckc386, bhckc387);
                 else ytdnonint_inc_fees = sum(bhck4483, bhckc886, bhckc888, bhckc887, bhckc386, bhckc387, bhckb492);
                 label ytdnonint_inc_fees = 'Service Charges, Commissions, and Fees'*/;

                 /*Service Charges, Commissions, and Fees 2 separated by business type*/;
                 /*ibank*/;
                 if dt<20010331 then
                     ytdnonint_inc_fees_ibank = .;
                 else if dt>=20010331 & dt<=20021231 then
                     ytdnonint_inc_fees_ibank = sum(bhckb490, bhckb491);
                 else if dt>=20030331 & dt<=20061231 then
                     ytdnonint_inc_fees_ibank = sum(bhckb490, bhckb491);
                 else ytdnonint_inc_fees_ibank = sum(bhckc886, bhckc888, bhckb491);
                 label ytdnonint_inc_fees_ibank = 'Service Charges, Commissions, and Fees IBank';

                 /*Service Charges, Commissions, and Fees 2 separated by business type*/;
                 /*insurance*/;
                 if dt<20010331 then
                     ytdnonint_inc_fees_insur = .;
                 else if dt>=20010331 & dt<=20021231 then
                     ytdnonint_inc_fees_insur = bhckb494;
                 else if dt>=20030331 & dt<=20061231 then
                     ytdnonint_inc_fees_insur = sum(bhckc386, bhckc387);
                 else ytdnonint_inc_fees_insur = sum(bhckc887, bhckc386, bhckc387);
                 label ytdnonint_inc_fees_insur = 'Service Charges, Commissions, and Fees Insurance';

                 /*Service Charges, Commissions, and Fees 2 separated by business type*/;
                 /*bank*/;
                 if dt<20010331 then
                     ytdnonint_inc_fees_bank = .;
                 else if dt>=20010331 & dt<=20021231 then
                     ytdnonint_inc_fees_bank = sum(bhck4483, bhckb492);
                 else if dt>=20030331 & dt<=20061231 then
                     ytdnonint_inc_fees_bank = sum(bhck4483, bhckb492);
                 else ytdnonint_inc_fees_bank = sum(bhck4483, bhckb492);
                 label ytdnonint_inc_fees_bank = 'Service Charges, Commissions, and Fees Bank';

				 /*Securities Brokerage Only*/
				 /*NOTE: CAN ONLY PULL THIS STARTING FROM 2007Q1*/
				 if dt<20061231 then
				 ytdnonint_inc_sb =.;
				 else ytdnonint_inc_sb = bhckc886;
				 label ytdnonint_inc_sb='Fees and Commissions from Securities Brokerage';

				 /*Service Charges on Deposits*/;
                 if dt<19900331 then
                     ytdnonint_inc_srv_chrg_dep = .;
                 else ytdnonint_inc_srv_chrg_dep = sum(bhck4483);
                 label ytdnonint_inc_srv_chrg_dep = 'Service Charges on Deposits';

                 /*Net Servicing Fees*/;
                 if dt<20010331 then
                     ytdnonint_inc_net_srv_fees = .;
                 else ytdnonint_inc_net_srv_fees = bhckb492;
                 label ytdnonint_inc_net_srv_fees = 'Net Servicing Fees';

				 /*Fiduciary, Annuity, and Insurance Fees*/;
                 if dt<20010331 then
                     ytdnonint_inc_fiduc_insur_anu = .;
                 else if dt>=20010331 & dt<=20021231 then
                     ytdnonint_inc_fiduc_insur_anu = sum(bhckb494, bhck4070);
                 else if dt>=20030331 & dt<=20061231 then
                     ytdnonint_inc_fiduc_insur_anu = sum(bhckc386, bhckc387, bhck4070);
                 else ytdnonint_inc_fiduc_insur_anu = sum(bhckc887, bhckc386, bhckc387, bhck4070);
                 label ytdnonint_inc_fiduc_insur_anu = 'Fiduciary, Annuity, and Insurance Fees';


                 /* All Other Noninterest Income*/ ;
                 /* Includes all other forms on noninterest income, including other service charges, commissions and fees */ ;
                 /*if dt<19860630 then
                     ytdnonint_inc_alloth = . ;
                 else ytdnonint_inc_alloth = sum(ytdnonint_inc,-ytdnonint_inc_fiduc,-ytdtradrev_inc,-ytdnonint_inc_fees_ibank,-ytdnonint_inc_fees_insur,-ytdnonint_inc_fees_bank);
                 label ytdnonint_inc_alloth = 'Other Noninterest Income' */;


                  /* All Other Noninterest Income*/ ;
                 /* Includes all other forms on noninterest income, including oreo, other service charges, commissions and fees */ ;
                 /*Does not include VC*/;
				 if dt<20010331 then
                 ytdnonint_inc_alloth2 = . ;
                 else ytdnonint_inc_alloth2 = sum(ytdnonint_inc,-ytdtradrev_inc,-ytdnonint_inc_fees_ibank,-ytdnonint_inc_srv_chrg_dep,-ytdnonint_inc_net_srv_fees,-ytdnonint_inc_fiduc_insur_anu);
                 label ytdnonint_inc_alloth2 = 'Other Noninterest Income 2' ;

                  /* All Other Noninterest Income Excluding OREO  since it started being reported in 1994q1*/ ;
                 /* Includes all other forms on noninterest income, including other service charges, commissions and fees */ ;
                 /*Does not include VC*/;
				 if dt<20010331 then
                 ytdnonint_inc_alloth2_no_oreo = . ;
                 else ytdnonint_inc_alloth2_no_oreo = sum(ytdnonint_inc_no_oreo,-ytdtradrev_inc,-ytdnonint_inc_fees_ibank,-ytdnonint_inc_srv_chrg_dep,-ytdnonint_inc_net_srv_fees,-ytdnonint_inc_fiduc_insur_anu);
                 label ytdnonint_inc_alloth2_no_oreo = 'Other Noninterest Income 2, Excluding OREO Since 1994q1' ;

				 /*All Other Noninterest Income constructed as sum, rather than as residual*/;
					if dt<20010331 then
						ytdnonint_inc_alloth3 = .;
					else ytdnonint_inc_alloth3 = sum(bhckb493,bhck8560,bhck8561,bhckb496,bhckb497);

				  /*Other noninterest income line item as its own variable*/;
				  	if dt<20010331 then
						ytdnonint_inc_oth = .;
					else ytdnonint_inc_oth = bhckb497;
					label ytdnonint_inc_oth='Other Noninterest Income (BHCKB497)';

	/*Other Noninterest Income GRANULAR (Schedule HI: Memoranda Item 6)*/;
	/*Note: these items are reported if greater than $25,000 and greater than 3% of Other Noninterest Income since 2008*/;
	/*Prior to this, the reporting criteria may have been different*/;
		/*Income and fees from the printing and sale of checks*/;
		if dt<20020331 then
			ytdnonint_inc_oth_checks = .;
		else ytdnonint_inc_oth_checks = bhckc013;
		label ytdnonint_inc_oth_checks='Other NII: Fees from printing and sale of checks (BHCKC013)';
		/*Earnings on/increase in value of cash surrender value of life insurance*/;
		if dt<20020331 then
			ytdnonint_inc_oth_life_insur = .;
		else ytdnonint_inc_oth_life_insur = bhckc014;
		label ytdnonint_inc_oth_life_insur='Other NII: Earnings on cash surrender value of life insurance (BHCKC014)';
		/*Income and fees from automated teller machines (ATMs)*/;
		if dt<20020331 then
			ytdnonint_inc_oth_atms = .;
		else ytdnonint_inc_oth_atms = bhckc016;
		label ytdnonint_inc_oth_atms='Other NII: Income and fees from automated teller machines (ATMs) (BHCKC016)';
		/*Rent and other income from other real estate owned*/;
		if dt<20020331 then
			ytdnonint_inc_oth_memo_oreo = .;
		else ytdnonint_inc_oth_memo_oreo = bhck4042;
		label ytdnonint_inc_oth_memo_oreo='Other NII: Rent and other income from other real estate owned (BHCK4042)';
		/*Safe deposit box rent*/;
		if dt<20020331 then
			ytdnonint_inc_oth_safe_dep_box = .;
		else ytdnonint_inc_oth_safe_dep_box = bhckc015;
		label ytdnonint_inc_oth_safe_dep_box='Other NII: Safe deposit box rent (BHCKC015)';
		/*Net change in the fair values of financial instruments accounted for under a fair value option*/;
			/*Note: this is also pulled above as ytd_nonint_inc_fvo, but only for the 6 largest BHCs*/;
		if dt<20070331 then
			ytdnonint_inc_oth_fvo = .;
		else ytdnonint_inc_oth_fvo = bhckf229;
		label ytdnonint_inc_oth_fvo='Other NII: Net change in value of instruments accounted under FVO (BHCKF229)';
		/*Bank card and credit card interchange fees*/;
		if dt <20080331 then
			ytdnonint_inc_oth_cc_fees = .;
		else ytdnonint_inc_oth_cc_fees = bhckf555;
		label ytdnonint_inc_oth_cc_fees='Other NII: Bank Card and Credit Card Interchange Fees (BHCKF555)';
		/*Gains on bargain purchases*/;
		if dt<20091231 then
			ytdnonint_inc_oth_barg_purch = .;
		else ytdnonint_inc_oth_barg_purch = bhckj447;
		label ytdnonint_inc_oth_barg_purch='Other NII: Gains on bargain purchases (BHCKJ447)';

	/*Write-in TEXT Items*/;
		/*Text Item 1: Number*/;
		if dt<19940331 then
			nonint_inc_oth_write_in_1 = .;
		else nonint_inc_oth_write_in_1 = bhck8562;
		label nonint_inc_oth_write_in_1 = 'Other NII: Write-in Item 1 (BHCK8562)';
		/*Text Item 1: Text*/;
	    length nonint_inc_oth_txt_string1 $200;
	    if dt>=19940331 then nonint_inc_oth_txt_string1 = text8562 ;
		label nonint_inc_oth_txt_string1 = 'Other NII: Write-in Text 1 (TEXT8562)';
		/*Text Item 2: Number*/;
		if dt<19940331 then
			nonint_inc_oth_write_in_2 = .;
		else nonint_inc_oth_write_in_2 = bhck8563;
		label nonint_inc_oth_write_in_2 = 'Other NII: Write-in Item 2 (BHCK8563)';
		/*Text Item 2: Text*/;
	    length nonint_inc_oth_txt_string2 $200;
	    if dt>=19940331 then nonint_inc_oth_txt_string2 = text8563 ;
		label nonint_inc_oth_txt_string2 = 'Other NII: Write-in Text 2 (TEXT8563)';
		/*Text Item 3: Number*/;
		if dt<19940331 then
			nonint_inc_oth_write_in_3 = .;
		else nonint_inc_oth_write_in_3 = bhck8564;
		label nonint_inc_oth_write_in_3 = 'Other NII: Write-in Item 3 (BHCK8564)';
		/*Text Item 3: Text*/;
	    length nonint_inc_oth_txt_string3 $200;
	    if dt>=19940331 then nonint_inc_oth_txt_string3 = text8564 ;
		label nonint_inc_oth_txt_string3 = 'Other NII: Write-in Text 3 (TEXT8564)';

	/* Noninterest Expense */;
        if dt < 19810630 then
            ytdnonint_exp = . ;
        else ytdnonint_exp = bhck4093;
	label ytdnonint_exp='Total Noninterest Expense' ;

	        /* Noninterest Expense from Salaries and Employee Benefits*/
	         /* Date range: 1981q2 to present */
	         if dt <19810630 then
		    ytdnonint_exp_comp = . ;
		 else ytdnonint_exp_comp = bhck4135;
		 label ytdnonint_exp_comp ='NIE of Salaries and Employee Benefits';

	        /* Noninterest Expense associated with fixed assets*/
	         /* Date range: 1981q2 to present */
	         if dt <19810630 then
	             ytdnonint_exp_fass = .;
	         else ytdnonint_exp_fass = bhck4217;
	         label ytdnonint_exp_fass ='NIE of Premises and Fixed Assets';

	        /* ALL OTHER Noninterest Expense (DERIVED)*/
	         /* Date range: 1981q2 to present */
	         if dt <19810630 then
	             ytdnonint_exp_allother = .;
	         else ytdnonint_exp_allother = sum(ytdnonint_exp, -ytdnonint_exp_comp, -ytdnonint_exp_fass) ;
	         label ytdnonint_exp_allother ='All Other Noninterest Expense';

	        /* Goodwill Impairment Losses*/
	         /* Date range: 2002q1 to present */
		 if dt <20020331 then
		    ytdnonint_exp_gwill = . ;
		 else ytdnonint_exp_gwill = bhckc216;
		 label ytdnonint_exp_gwill = 'NIE Goodwill Impairment Losses';

	        /* Amortization Expense and Impairment Losses for Other Intangible Assets*/
	         /* Date range: 2002q1 to present */
		 if dt <20020331 then
		    ytdnonint_exp_amort = . ;
		 else ytdnonint_exp_amort = bhckc232;
		 label ytdnonint_exp_amort = 'NIE Amotization and Impairment Losses for Other Intangible Assets';

	        /* Other Noninterest Expense */
		 /* Date range: */
		 if dt <19810630 then
		    ytdnonint_exp_oth = . ;
		 else ytdnonint_exp_oth = bhck4092;
		 label ytdnonint_exp_oth = 'Other NIE (Includes Goodwill and Amortization until 2002)';

	/*Other Noninterest Expense GRANULAR (Schedule HI: Memoranda Item 7)*/;
	/*Note: these items are reported if greater than $25,000 and greater than 3% of Other Noninterest Income*/;
		/*Data processing expenses*/;
		if dt<20020331 then
			ytdnonint_exp_oth_a = .;
		else ytdnonint_exp_oth_a = bhckc017;
		label ytdnonint_exp_oth_a = 'Other NIE: Data Processing Expenses (BHCKC017)';
		/*Advertising and marketing expenses*/;
		if dt<20020331 then
			ytdnonint_exp_oth_b = .;
		else ytdnonint_exp_oth_b = bhck0497;
		label ytdnonint_exp_oth_b = 'Other NIE: Advertising and marketing expenses (BHCK0497)';
		/*Directors' fees*/;
		if dt<20020331 then
			ytdnonint_exp_oth_c = .;
		else ytdnonint_exp_oth_c = bhck4136;
		label ytdnonint_exp_oth_c = 'Other NIE: Directors fees (BHCK4136)';
		/*Printing, stationary, and supplies*/;
		if dt<20020331 then
			ytdnonint_exp_oth_d = .;
		else ytdnonint_exp_oth_d = bhckc018;
		label ytdnonint_exp_oth_d = 'Other NIE: Printing, stationary, and supplies (BHCKC018)';
		/*Postage*/;
		if dt<20020331 then
		 	ytdnonint_exp_oth_e = .;
		else ytdnonint_exp_oth_e = bhck8403;
		label ytdnonint_exp_oth_e = 'Other NIE: Postage (BHCK8403)';
		/*Legal fees and expenses*/;
		if dt<20020331 then
			ytdnonint_exp_oth_f = .;
		else ytdnonint_exp_oth_f = bhck4141;
		label ytdnonint_exp_oth_f = 'Other NIE: Legal fees and expenses (BHCK4141)';
		/*FDIC deposit insurance assesments*/;
		if dt<20020331 then
			ytdnonint_exp_oth_g = .;
		else ytdnonint_exp_oth_g = bhck4146;
		label ytdnonint_exp_oth_g = 'Other NIE: FDIC deposit insurance assessments (BHCK4146)';
		/*Accounting and auditing expenses*/;
		if dt<20080331 then
			ytdnonint_exp_oth_h = .;
		else ytdnonint_exp_oth_h = bhckf556;
		label ytdnonint_exp_oth_h = 'Other NIE: Accounting and auditing expenses (BHCKF556)';
		/*Consulting and advisory expenses*/;
		if dt<20080331 then
			ytdnonint_exp_oth_i = .;
		else ytdnonint_exp_oth_i = bhckf557;
		label ytdnonint_exp_oth_i = 'Other NIE: Consulting and advisory expenses (BHCKf557)';
		/*Automated teller machine (ATM) and interchange expenses*/;
		if dt<20080331 then
			ytdnonint_exp_oth_j = .;
		else ytdnonint_exp_oth_j = bhckf558;
		label ytdnonint_exp_oth_j = 'Other NIE: ATM and interchange expenses (BHCKF558)';
		/*Telecommunications expenses*/;
		if dt<20080331 then
			ytdnonint_exp_oth_k = .;
		else ytdnonint_exp_oth_k = bhckf559;
		label ytdnonint_exp_oth_k = 'Other NIE: Telecommunications expenses (BHCKF559)';

		if dt<19940331 then
			nonint_exp_oth_l = .;
		else nonint_exp_oth_l = bhck8565;
		label nonint_exp_oth_l = 'Other NIE: TEXT 1 (BHCK8565)';

		if dt<19940331 then
			nonint_exp_oth_m = .;
		else nonint_exp_oth_m = bhck8566;
		label nonint_exp_oth_m = 'Other NIE: TEXT 2 (BHCK8566)';

		if dt<19940331 then
			nonint_exp_oth_n = .;
		else nonint_exp_oth_n = bhck8567;
		label nonint_exp_oth_n = 'Other NIE: TEXT 3 (BHCK8567)';

		if dt<20080331 then
		ytdnonint_exp_textclass=.;
		else ytdnonint_exp_textclass=sum(bhck8565, bhck8566, bhck8567);
		label ytdnonint_exp_textclass = 'Write-in NIE Total';

		   /*oth_nint_exp_text_l = "" */;
    length oth_nint_exp_text_l $200;
    if dt>=19940331 then oth_nint_exp_text_l = text8565 ;

    /*oth_nint_exp_text_m = ""*/;
    length oth_nint_exp_text_m $200;
    if dt>=19940331 then oth_nint_exp_text_m = text8566 ;


    /*oth_nint_exp_text_n = ""*/;
    length oth_nint_exp_text_n $200;
    if dt>=19940331 then oth_nint_exp_text_n = text8567 ;


 	/* Provision for Loan & Lease Losses */;
        if dt < 19810630 then
            ytdllprov = . ;
        else ytdllprov = bhck4230 ;
        label ytdllprov='Provision for Loan & Lease Losses' ;

        /* Gains (losses) on Securities Not Held in Trading Accounts */;
        if dt < 19810630 then
             ytdsecur_inc = . ;
        else if dt <= 19931231 then
             ytdsecur_inc = bhck4091 ;
        else ytdsecur_inc = sum(bhck3521,bhck3196) ;
        label ytdsecur_inc='Gains and losses on securities' ;

		/*Gains (losses) on Securities Held-to-Maturity */;
	        if dt < 19810630 then
	             ytdhtmsecur_inc = . ;
	        else ytdhtmsecur_inc = bhck3521;
	        label ytdhtmsecur_inc='Gains and Losses on Securities Held-to-maturity';

		/*Gains (losses) on Securities Available-for-Sale */;
        	if dt <19810630 then
		     ytdafssecur_inc = . ;
		else ytdafssecur_inc = bhck3196;
	        label ytdafssecur_inc='Gains and Losses on Securities Available-for-Sale' ;

	/* Extraordinary Items and Other Adj, Net of Inc. Taxes */;
        if dt < 19810630 then
           ytdextra_inc = . ;
        else ytdextra_inc = bhck4320;
	label ytdextra_inc='Extraordinary Items and Other Adj, Net of Inc. Taxes';

        /* Applicable income taxes */;
        if dt < 19810630 then
           ytdtaxes_inc = . ;
        else ytdtaxes_inc = bhck4302 ;
        label ytdtaxes_inc='Applicable income taxes' ;

         /* Minority Interest */;
        if dt < 19810630 then
            ytdminor_int = . ;
        else if dt <20090331 then
	    ytdminor_int = bhck4484 ;
	else ytdminor_int = bhckg103 ;
        label ytdminor_int='Minority interest' ;

	/*Memoranda Items: Trading Revenue DVA items*/;
		/*Impact on trading revenue of changes in the creditworthiness of the bank holding company's
		derivates counterparties on the bank holding company's derivative assets */;
		if dt<20110331 then
			ytdtrad_dva_derivatives = . ;
		else ytdtrad_dva_derivatives = bhckk090;
		label ytdtrad_dva_derivatives='Trading Revenue: DVA Impact of Derivatives Counterparties';
		/*Impact on trading revenue of changes in the creditworthiness of the bank holding company
		on the bank holding company's derivative liabilities*/;
		if dt<20110331 then
			ytdtrad_dva = . ;
		else ytdtrad_dva = bhckk094;
		label ytdtrad_dva='Trading Revenue: DVA Impact of Firm Creditworthiness';
	/*Memoranda Items: Net gains (losses) recognized in earnings on assets and liabilities
	that are reported at fair value under a fair value option*/;
		/*Net gains (losses) on assets*/;
		if dt<20080331 then
			ytdfvo_assets = .;
		else ytdfvo_assets = bhckf551;
		label ytdfvo_assets='Net gains (losses) on assets under FVO';
		/*Estimated net gains (losses) on loans attributable to changes in instrument-specific credit risk*/;
		if dt<20080331 then
			ytdfvo_loans_credit_risk = .;
		else ytdfvo_loans_credit_risk = bhckf552;
		label ytdfvo_loans_credit_risk='Est net gains (losses) on assets from credit risk under FVO';
		/*Net gains (losses) on liabilities*/;
		if dt<20080331 then
			ytdfvo_liab = .;
		else ytdfvo_liab = bhckf553;
		label ytdfvo_liab='Net gains (losses) on liabilities under FVO';
		/*Estimated net gains (losses) on liabilities attributable to changes in instrument-specific credit risk*/;
		if dt<20080331 then
			ytdfvo_liab_credit_risk = .;
		else ytdfvo_liab_credit_risk = bhckf554;
		label ytdfvo_liab_credit_risk='Est net gains (losses) on liabilities from credit risk under FVO';

	/*Predecessor income*/;
	    if dt<20030331 then
	       predytd_netinc=.;
	    else if dt>=20030331 then
	       predytd_netinc=bhbc4340;
	    label predytd_netinc = 'Predecessor Net Income for the Quarter';


            if dt<20030331 then do ;
                /*NIM VARIABLES*/;
		predytd_int_inc = . ;
		predytd_int_inc_lndom = . ;
		predytd_int_inc_sec = . ;
		predytd_int_exp = . ;
		predytd_int_exp_dom = . ;
		predytd_int_inc_net = . ;
                /*NINT VARIABLES*/
		predytd_nonint_inc = . ;
		predytd_nonint_inc_fid_ins_anu= . ;
		predytd_nonint_inc_fees_ibank = . ;
		predytd_nonint_inc_net_secur = . ;
                /*TRADING REV*/
		predytd_tradrev_inc = . ;
		/*NONINTEREST EXPENSE*/
		predytd_nonint_exp = . ;
		predytd_nonint_exp_comp= . ;
		predytd_nonint_exp_gwill= . ;
               predytd_taxes=.;
                predytd_minint=.;
             	predytd_extra=.;
		predytd_cashdiv=.;
		predytd_ncos=.;
                predytd_llprov=.;
                predytd_secinc=.;
                end;
            else do;
                predytd_int_inc = bhbc4107;
		predytd_int_inc_lndom = bhbc4094;
		predytd_int_inc_sec = bhbc4218;
		predytd_int_exp = bhbc4073;
		predytd_int_exp_dom = bhbc4421;
		predytd_int_inc_net = bhbc4074;
                /*NINT VARIABLES*/
		predytd_nonint_inc = bhbc4079;
		predytd_nonint_inc_fid_ins_anu=sum(bhbc4070, bhbcb494);
		predytd_nonint_inc_fees_ibank = sum(bhbcb490, bhbcb491);
		predytd_nonint_inc_net_secur = bhbcb493;
                /*TRADING REV*/
		predytd_tradrev_inc = bhbca220;
		/*NONINTEREST EXPENSE*/
		predytd_nonint_exp = bhbc4093;
		predytd_nonint_exp_comp=bhbc4135;
		predytd_nonint_exp_gwill=bhbcc216;
                predytd_taxes=bhbc4302;
                predytd_minint=bhbc4484;
             	predytd_extra=bhbc4320;
		predytd_cashdiv=bhbc4475;
		predytd_ncos=bhbc6061;
                predytd_llprov=bhbc4230;
                predytd_secinc=bhbc4091;
            end;
        label predytd_int_inc = 'Total Interest Income - Predecessor Financials - Predecessor Financial Items' ;
        label predytd_int_inc_lndom = 'Interest and Fee Income on Loans - Predecessor Financials - Predecessor Financial Items' ;
        label predytd_int_inc_sec = 'Interest and Dividend Income on Securities - Predecessor Financial Items';
        label predytd_int_exp = 'Total Interest Expense - Predecessor Financial Items';
        label predytd_int_exp_dom = 'Net Interest Expense on Deposits - Predecessor Financial Items';
        label predytd_int_inc_net = 'Net Interest Income - Predecessor Financial Items';
                /*NINT VARIABLES*/
        label predytd_nonint_inc = 'Total Noninterest Income - Predecessor Financial Items';
        label predytd_nonint_inc_fid_ins_anu='Income from Fiduciary Activities and Insurance Commissions and Fees - Predecessor Financial Items';
        label predytd_nonint_inc_fees_ibank = 'Investment Banking, Advisory, Brokerage, and Underwriting Fees and Commissions and Venture Capital Revenue - Predecessor Financial Items';
        label predytd_nonint_inc_net_secur = 'Net Securitization Income - Predecessor Financial Items';
        /*TRADING REV*/
        label predytd_tradrev_inc = 'Trading Revenue - Predecessor Financial Items';
	/*NONINTEREST EXPENSE*/
        label predytd_nonint_exp = 'Total Noninterest Expense - Predecessor Financial Items';
        label predytd_nonint_exp_comp= 'Salaries and Employee Benefits - Predecessor Financial Items';
	label predytd_nonint_exp_gwill= 'Goodwill Impairment Losses - Predecessor Financial Items';
        label predytd_taxes= 'Applicable Income Taxes - Predecessor Financial Items';
        label predytd_minint='Minority Interest - Predecessor Financial Items';
        label predytd_extra='Extraordinary Items and Other Adjustments, Net of Income Taxes - Predecessor Financial Items';
	label predytd_cashdiv='Cash Dividend Declared - Equity Capital - Predecessor Financial Items';
	label predytd_ncos='Loans Charged Off and Recoveries: Total Loans Charged Off & Recoveries Net Charge-Offs - Predecessor Financial Items';
        label predytd_llprov='Provision for Loan and Lease Losses - Predecessor Financial Items';
        label predytd_secinc='Gains (Losses) on HTM and AFS Securities - Predecessor Financial Items';







                 /**************************************************************
     **********     CREDIT CHARGE-OFFS AND RECOVERIES     **********
     **************************************************************/;

          /************
          *** TOTAL ***
          ************/;

               /* Total Charge-offs on Loans & Leases */;
               /* Total Recoveries on Loans & Leases */;
               if dt < 19810630 then do ;
                  ytdxoff_tot = . ;
                  ytdrecov_tot = . ;
               end ;
               else do ;
                    ytdxoff_tot = bhck4635;
                    ytdrecov_tot = bhck4605;
               end ;

               ytdnetxoff_tot = sum(ytdxoff_tot,-ytdrecov_tot) ;

               label ytdxoff_tot='Total Charge-offs on Loans & Leases';
               label ytdrecov_tot='Total Recoveries on Loans & Leases';
               label ytdnetxoff_tot='Total Net Charge-offs on Loans & Leases';

          /******************
          *** REAL ESTATE ***
          ******************/;

          /* Charge-offs on Loans Secured by Real Estate */
               /* Recoveries on Loans Secured by Real Estate */;
               if dt < 19860630 then do ;
                  ytdxoff_re = . ;
                  ytdrecov_re = . ;
               end ;
               else if dt <= 20001231 then do ;
                  ytdxoff_re = sum(bhck4651,bhck4652) ;
                  ytdrecov_re = sum(bhck4661,bhck4662) ;
               end ;
               else if dt >= 20010331 & dt <= 20011231 then do ;
                  ytdxoff_re = sum(bhck3582,bhck3584,bhck5411,bhck5413,bhck3588,bhck3590,bhckb512) ;
                  ytdrecov_re = sum(bhck3583,bhck3585,bhck5412,bhck5414,bhck3589,bhck3591,bhckb513) ;
               end ;
               else if dt >=20020331 & dt <=20071231 then do ;
                  ytdxoff_re = sum(bhck3582,bhck3584,bhck5411,bhckc234,bhckc235,bhck3588,bhck3590,bhckb512) ;
                  ytdrecov_re = sum(bhck3583,bhck3585,bhck5412,bhckc217,bhckc218,bhck3589,bhck3591,bhckb513) ;
               end ;
               else if dt >=20080331 then do ;
                ytdxoff_re = sum(bhckc891,bhckc893,bhck3584,bhck5411,bhckc234,bhckc235,bhck3588,bhckc895,bhckc897,bhckb512);
                ytdrecov_re=sum(bhckc892,bhckc894,bhck3585,bhck5412,bhckc217,bhckc218,bhck3589,bhckc896,bhckc898,bhckb513);
               end;

               ytdnetxoff_re = sum(ytdxoff_re,-ytdrecov_re) ;

               label ytdxoff_re='Charge-offs on Loans Secured by Real Estate';
               label ytdrecov_re='Recoveries on Loans Secured by Real Estate';
               label ytdnetxoff_re='Net Charge-offs on Loans Secured by Real Estate';


                      /******************************
                      *** RESIDENTIAL REAL ESTATE ***
                      ******************************/;

                           /* Charge-offs on 1-4 Family Residential Real Estate */
                           /* Recoveries on 1-4 Family Residential Real Estate */;
                           if dt < 19910331 then do ;
                               ytdxoff_rre=. ;
                               ytdrecov_rre=. ;
                               ytdxoff_heloc=. ;
                               ytdrecov_heloc=. ;
                               ytdxoff_closedlien=. ;
                               ytdrecov_closedlien=. ;
                               ytdxoff_firstlien=. ;
                               ytdrecov_firstlien=. ;
                               ytdxoff_jrlien=. ;
                               ytdrecov_jrlien=. ;
                           end ;
                           if dt >= 19910331 & dt <= 20011231 then do ;
                               ytdxoff_rre  = sum(bhck5413,bhck5411) ;
                               ytdrecov_rre = sum(bhck5414,bhck5412) ;
                               ytdxoff_heloc=bhck5411 ;
                               ytdrecov_heloc=bhck5412 ;
                               ytdxoff_closedlien= bhck5413 ;
                               ytdrecov_closedlien= bhck5414 ;
                               ytdxoff_firstlien=. ;
                               ytdrecov_firstlien=. ;
                               ytdxoff_jrlien=. ;
                               ytdrecov_jrlien=. ;
                           end ;
                           else do ;
                               ytdxoff_rre  = sum(bhckc234,bhckc235,bhck5411) ;
                               ytdrecov_rre = sum(bhckc217,bhckc218,bhck5412) ;
                               ytdxoff_heloc=bhck5411 ;
                               ytdrecov_heloc=bhck5412 ;
                               ytdxoff_closedlien= sum(bhckc234,bhckc235);
                               ytdrecov_closedlien= sum(bhckc217,bhckc218);
                               ytdxoff_firstlien=bhckc234 ;
                               ytdrecov_firstlien=bhckc217 ;
                               ytdxoff_jrlien=bhckc235 ;
                               ytdrecov_jrlien=bhckc218 ;
                           end ;

                           ytdnetxoff_rre= sum(ytdxoff_rre,-ytdrecov_rre) ;
                           ytdnetxoff_heloc= sum(ytdxoff_heloc,-ytdrecov_heloc) ;
                           ytdnetxoff_closedlien= sum(ytdxoff_closedlien,-ytdrecov_closedlien) ;
                           ytdnetxoff_firstlien= sum(ytdxoff_firstlien,-ytdrecov_firstlien) ;
                           ytdnetxoff_jrlien= sum(ytdxoff_jrlien,-ytdrecov_jrlien) ;

                           label ytdxoff_rre='Charge-offs on 1-4 Family Residential Real Estate';
                           label ytdrecov_rre='Recoveries on 1-4 Family Residential Real Estate';
                           label ytdnetxoff_rre='Net Charge-offs on 1-4 Family Residential Real Estate';
                           label ytdxoff_heloc='Charge-offs on 1-4 Family HELOCs';
                           label ytdrecov_heloc='Recoveries on 1-4 Family HELOCs';
                           label ytdnetxoff_heloc='Net Charge-offs on 1-4 Family HELOCs';
                           label ytdxoff_closedlien='Charge-offs on 1-4 Family Closed Lien Mortgages';
                           label ytdrecov_closedlien='Recoveries on 1-4 Family Closed Lien Mortgages';
                           label ytdnetxoff_closedlien='Net Charge-offs on 1-4 Family Closed Lien Mortgages';
                           label ytdxoff_firstlien='Charge-offs on 1-4 Family first Lien Closed Mortgages';
                           label ytdrecov_firstlien='Recoveries on 1-4 Family 1st Lien Closed Mortgages';
                           label ytdnetxoff_firstlien='Net Charge-offs on 1-4 Family 1st Lien Closed Mortgages';
                           label ytdxoff_jrlien='Charge-offs on 1-4 Family Jr Lien Closed Mortgages';
                           label ytdrecov_jrlien='Recoveries on 1-4 Family Jr Lien Closed Mortgages';
                           label ytdnetxoff_jrlien='Net Charge-offs on 1-4 Family Jr Lien Closed Mortgages';


                      /******************************
                      *** COMMERCIAL REAL ESTATE ***
                      ******************************/;
        /* Charge-offs on construction and land development, mutlifamily res. properties, and nonfarm/nonresidential loans*/
         /* Date range: 1990q3 to present */
            if dt<19900930 then do;
                ytdxoff_const=.;
                ytdrecov_const=.;
                ytdxoff_multi=.;
                ytdrecov_multi=.;
                ytdxoff_nfnr=.;
                ytdrecov_nfnr=.;

            end;
            else if dt>=19900930 & dt<20070331 then do;
                ytdxoff_const=bhck3582;
                ytdrecov_const=bhck3583;
                ytdxoff_multi=bhck3588;
                ytdrecov_multi=bhck3589;
                ytdxoff_nfnr=bhck3590;
                ytdrecov_nfnr=bhck3591;

            end;
            else if dt>=20070331 then do;
                ytdxoff_const=sum(bhckc891,bhckc893);
                ytdrecov_const=sum(bhckc892,bhckc894);
                ytdxoff_multi=bhck3588;
                ytdrecov_multi=bhck3589;
                ytdxoff_nfnr=sum(bhckc895,bhckc897);
                ytdrecov_nfnr=sum(bhckc896,bhckc898);
            end;

            ytdnetxoff_const= sum(ytdxoff_const,-ytdrecov_const);
            ytdnetxoff_multi= sum(ytdxoff_multi,-ytdrecov_multi);
            ytdnetxoff_nfnr= sum(ytdxoff_nfnr,-ytdrecov_nfnr);


            ytdxoff_cre=sum(ytdxoff_const,ytdxoff_multi,ytdxoff_nfnr);
            ytdrecov_cre=sum(ytdrecov_const,ytdrecov_multi,ytdrecov_nfnr);
            ytdnetxoff_cre=sum(ytdxoff_cre,-ytdrecov_cre);

               label ytdxoff_const='Charge-offs on construction, land loans';
               label ytdrecov_const='Recoveries on construction, land loans';
               label ytdnetxoff_const='Net charge-offs on construction, land loans';
               label ytdxoff_multi='Charge-offs on multi-family property loans';
               label ytdrecov_multi='Recoveries on multi-family property loans';
               label ytdnetxoff_multi='Net charge-offs on multi-family property loans';
               label ytdxoff_nfnr='Charge-offs on nonfarm, nonres CRE loans';
               label ytdrecov_nfnr='Recoveries on nonfarm, nonres CRE loans';
               label ytdnetxoff_nfnr='Net charge-offs on nonfarm, nonres CRE loans';

               label ytdxoff_cre='Charge-offs on Commercial Real Estate Loans';
               label ytdrecov_cre='Recoveries on Commercial Real Estate Loans';
               label ytdnetxoff_cre='Net Charge-offs on Commercial Real Estate Loans';

                      /**************************
                      *** OTHER REAL ESTATE *****
                      ***************************/;
			/* Charge-offs on Other Real Estate Loans (Non-Residential, Non-Commercial) */
			/* Includes loans secured by farmland and loans from foreign offices */;
			if dt <19910331 then do ;
			    ytdxoff_othre = . ;
			    ytdrecov_othre = . ;
			end ;
			else do ;
			    ytdxoff_othre = sum(ytdxoff_re,-ytdxoff_rre,-ytdxoff_cre) ;
			    ytdrecov_othre = sum(ytdrecov_re,-ytdrecov_rre,-ytdrecov_cre) ;
			end ;

			ytdnetxoff_othre = sum(ytdxoff_othre,-ytdrecov_othre) ;

			label ytdxoff_othre='Charge-offs on Other Real Estate';
			label ytdrecov_othre='Recoveries on Other Real Estate';
			label ytdnetxoff_othre='Net Charge-offs on Other Real Estate (Non-Res, Non-Com)';


       	        	   	    /**************************************
        	         	     *** Real Estate in Foriegn Offices ***
                	    	    **************************************/;
				/* Charge-offs on Real Estate Loans made in Foreign Offices */
				/* Recoveries on Real Estate Loans made in Foreign Offices */;
				if dt < 20010331 then do ;
				    ytdxoff_refor = . ;
				    ytdrecov_refor = . ;
				end ;
				else do ;
				    ytdxoff_refor = bhckb512 ;
				    ytdrecov_refor = bhckb513 ;
				end ;

				ytdnetxoff_refor = sum(ytdxoff_refor,-ytdrecov_refor) ;

				label ytdxoff_refor='Charge-offs on Loans Secured by Real Estate in Foreign Offices' ;
				label ytdrecov_refor='Recoveries on Loans Secured by Real Estate in Foreign Offices' ;
				label ytdnetxoff_refor='Net Charge-offs on Loans Secured by Real Estate in Foreign Offices' ;

 		                     /*********************************************
        		              *** Real Estate Loans Secured by Farmland ***
	       		               *********************************************/;
				/* Charge-offs on Real Estate Loans Secured by Farmland */
				/* Recoveries on Real Estate Loans Secured by Farmland */;
				if dt < 19900930 then do ;
				    ytdxoff_farm = . ;
				    ytdrecov_farm = . ;
				end ;
				else do ;
				    ytdxoff_farm = bhck3584 ;
				    ytdrecov_farm = bhck3585 ;
				end ;

				ytdnetxoff_farm = sum(ytdxoff_farm,-ytdrecov_farm) ;

				label ytdxoff_farm='Charge-offs on Real Estate Loans Secured by Farmland' ;
				label ytdrecov_farm='Recoveries on Real Estate Loans Secured by Farmland' ;
				label ytdnetxoff_farm='Net Charge-offs on Real Estate Loans Secured by Farmland' ;



          /****************
          *** C&I LOANS ***
          *****************/;

               /* Charge-offs on C&I Loans */
               /* Recoveries on C&I Loans */;
               if dt < 19860630 then do ;
                   ytdxoff_ci = . ;
                   ytdrecov_ci = . ;
               end ;
               else do ;
                    ytdxoff_ci = sum(bhck4645,bhck4646) ;
                    ytdrecov_ci = sum(bhck4617,bhck4618) ;
               end ;

               ytdnetxoff_ci = sum(ytdxoff_ci,-ytdrecov_ci) ;

               label ytdxoff_ci='Charge-offs on C&I Loans' ;
               label ytdrecov_ci='Recoveries on C&I Loans' ;
               label ytdnetxoff_ci='Net Charge-offs on C&I Loans' ;

          /******************************
          *** DEPOSITORY INSTITUTIONS ***
          ******************************/;

               /* Charge-offs on Loans to Depository Institutions */
               /* Recoveries on Loans to Depository Institutions */;
               if dt < 19910331 then do ;
                   ytdxoff_dep = . ;
                   ytdrecov_dep = .;
               end ;
               else do ;
                   ytdxoff_dep = sum(bhck4653,bhck4654) ;
                   ytdrecov_dep = sum(bhck4663,bhck4664) ;
               end ;

               ytdnetxoff_dep = sum(ytdxoff_dep,-ytdrecov_dep) ;

               label ytdxoff_dep='Charge-offs on Loans to Depository Institutions' ;
               label ytdrecov_dep='Recoveries on Loans to Depository Institutions';
               label ytdnetxoff_dep='Net Charge-offs on Loans to Depository Institutions' ;

          /******************************
          *** AGRICULTURAL PRODUCTION ***
          ******************************/;

               /* Charge-offs on Loans to Finance Agricultural Production */;
               /* Recoveries on Loans to Finance Agricultural Production */;
               if dt < 19860630 then do ;
                   ytdxoff_agr = .;
                   ytdrecov_agr = .;
               end ;
               else do ;
                   ytdxoff_agr = bhck4655 ;
                   ytdrecov_agr = bhck4665 ;
               end ;

               ytdnetxoff_agr= sum(ytdxoff_agr,-ytdrecov_agr);

               label ytdxoff_agr='Charge-offs on Loans to Finance Agricultural Production' ;
               label ytdrecov_agr='Recoveries on Loans to Finance Agricultural Production' ;
               label ytdnetxoff_agr='Net Charge-offs on Loans to Finance Agricultural Production' ;

          /***************
          *** CONSUMER ***
          ***************/;

                /* Charge-offs on Loans to Consumers */
               /* Recoveries on Loans to Consumers */;
               if dt < 19860630 then do ;
                  ytdxoff_cons = . ;
                  ytdrecov_cons = . ;
               end ;
               else if dt >= 19860630 & dt <= 19901231 then do ;
                  ytdxoff_cons = bhck4639 ;
                  ytdrecov_cons = bhck4609 ;
               end ;
               else if dt >= 19910331 & dt <= 20001231 then do;
                  ytdxoff_cons = sum(bhck4656,bhck4657) ;
                  ytdrecov_cons = sum(bhck4666,bhck4667) ;
               end ;
               else if dt < 20110331 then do ;
                  ytdxoff_cons = sum(bhckb514,bhckb516) ;
                  ytdrecov_cons = sum(bhckb515,bhckb516) ;
               end ;
               else do ;
                  ytdxoff_cons = sum(bhckb514,bhckk129,bhckk205) ;
                  ytdrecov_cons = sum(bhckb515,bhckk133,bhckk206) ;
               end ;

               ytdnetxoff_cons = sum(ytdxoff_cons,-ytdrecov_cons) ;

               label ytdxoff_cons='Charge-offs on Loans to Consumers' ;
               label ytdrecov_cons='Recoveries on Loans to Consumers' ;
               label ytdnetxoff_cons='Net Charge-offs on Loans to Consumers' ;


                /*************************************
                *** CREDIT CARDS & CONSUMER LOANS ***
                *************************************/;

                     /* Charge-offs on Credit Card Loans & Loans to Consumers (Installment)*/
                     /* Recoveries on Credit Card Loans & Loans to Consumers (Installment)*/;
                     if dt < 19910331 then do ;
                        ytdxoff_cc = .;
                        ytdrecov_cc = .;
                        ytdxoff_othcons = .;
                        ytdrecov_othcons = .;
                     end ;
                     else if dt >= 19910331 & dt <= 20001231 then do ;
                        ytdxoff_cc = bhck4656 ;
                        ytdxoff_othcons = bhck4657 ;
                        ytdrecov_cc = bhck4666 ;
                        ytdrecov_othcons = bhck4667 ;
                     end ;
                     else if dt<20110331 then do ;
                        ytdxoff_cc = bhckb514  ;
                        ytdxoff_othcons = bhckb516 ;
                        ytdrecov_cc = bhckb515 ;
                        ytdrecov_othcons = bhckb517 ;
                     end ;
                     else do ;
                        ytdxoff_cc = bhckb514  ;
                        ytdxoff_othcons = sum(bhckk129,bhckk205);
                        ytdrecov_cc = bhckb515 ;
                        ytdrecov_othcons = sum(bhckk133,bhckk206);
                     end ;

                     ytdnetxoff_cc = sum(ytdxoff_cc,-ytdrecov_cc) ;
                     ytdnetxoff_othcons = sum(ytdxoff_othcons,-ytdrecov_othcons) ;

                     label ytdxoff_cc='Charge-offs on Credit Card Loans' ;
                     label ytdxoff_othcons='Charge-offs on Other Loans to Consumers' ;
                     label ytdrecov_cc='Recoveries on Credit Card Loans' ;
                     label ytdrecov_othcons='Recoveries on Other Loans to Consumers';
                     label ytdnetxoff_cc='Net Charge-offs on Credit Card Loans' ;
                     label ytdnetxoff_othcons='Net Charge-offs on Other Loans to Consumers' ;


          /**************************
          *** FOREIGN GOVERNMENTS ***
          **************************/;

               /* Charge-offs on Loans to Foreign Governments */
               /* Recoveries on Loans to Foreign Governments */;
               if dt < 19860630 then do ;
                   ytdxoff_fgovt = .;
                   ytdrecov_fgovt = .;
               end ;
               else do ;
                   ytdxoff_fgovt = bhck4643 ;
                   ytdrecov_fgovt = bhck4627 ;
               end ;

               ytdnetxoff_fgovt = sum(ytdxoff_fgovt,-ytdrecov_fgovt) ;

               label ytdxoff_fgovt='Charge-offs on Loans to Foreign Governments' ;
               label ytdrecov_fgovt='Recoveries on Loans to Foreign Governments' ;
               label ytdnetxoff_fgovt='Net Charge-offs on Loans to Foreign Governments' ;


          /**********************************
          *** LEASE FINANCING RECEIVABLES ***
          **********************************/;

               /* Charge-offs on Lease Financing Receivables */
               /* Recoveries on Lease Financing Receivables */;
               if dt < 19860630 then do ;
                   ytdxoff_lease =.;
                   ytdrecov_lease = .;
               end ;
               else if dt >= 19860630 & dt <= 20061231 then do ;
                   ytdxoff_lease = sum(bhck4658,bhck4659);
                   ytdrecov_lease = sum(bhck4668,bhck4669);
               end ;
               else do ;
                   ytdxoff_lease = sum(bhckf185,bhckc880);
                   ytdrecov_lease = sum(bhckf187,bhckf188);
               end ;

               ytdnetxoff_lease= sum(ytdxoff_lease,-ytdrecov_lease) ;

               label ytdxoff_lease='Charge-offs on Lease Financing Receivables' ;
               label ytdrecov_lease='Recoveries on Lease Financing Receivables' ;
               label ytdnetxoff_lease='Net Charge-offs on Lease Financing Receivables' ;


          /**********************
          *** OTHER LOANS ***
          **********************/;

               /* Charge-offs on Other Loans */
               /* Recoveries on Other Loans */;
               if dt < 19910331 then do ;
                   ytdxoff_oth = . ;
                   ytdrecov_oth = . ;
               end ;
               else do ;
                   ytdxoff_oth = bhck4644 ;
                   ytdrecov_oth = bhck4628 ;
               end ;

               ytdnetxoff_oth = sum(ytdxoff_oth,-ytdrecov_oth) ;

               label ytdxoff_oth='Charge-offs on Other Loans' ;
               label ytdrecov_oth='Recoveries on Other Loans' ;
               label ytdnetxoff_oth='Net Charge-offs on Other Loans' ;


          /**********************
          *** ALL OTHER LOANS ***
          **********************/;

       /* Charge-offs on All Other Loans */
               /* Recoveries on All Other Loans */;
               if dt < 19910331 then do ;
                   ytdxoff_allother = . ;
                   ytdrecov_allother = . ;
               end ;
               else do ;
                   ytdxoff_allother = sum(ytdxoff_lease, ytdxoff_oth,ytdxoff_fgovt,ytdxoff_agr,ytdxoff_dep,ytdxoff_othre) ;
                   ytdrecov_allother = sum(ytdrecov_lease, ytdrecov_oth,ytdrecov_fgovt,ytdrecov_agr,ytdrecov_dep,ytdrecov_othre) ;
               end ;

               ytdnetxoff_allother = sum(ytdxoff_allother,-ytdrecov_allother) ;

               label ytdxoff_oth='Charge-offs on All Other Loans' ;
               label ytdrecov_oth='Recoveries on All Other Loans' ;
               label ytdnetxoff_oth='Net Charge-offs on All Other Loans' ;


/*********************************************************************************/;


         /*******************************
          *** Changes in Equity Capital***
          *******************************/;

        /* Sale, conversion, acquisition, or retirement of capital stock, net */
         /* Date range: 1990q3 to present */
                if dt < 19900930 then do;
                    ytdprefstocksale = .;
                    ytdprefstockconvert = .;
                    ytdcommstocksale = .;
                    ytdcommstockconvert = .;
                end;
                else do;
                    ytdprefstocksale = bhck3577;
                    ytdprefstockconvert = bhck3578;
                    ytdcommstocksale = bhck3579;
                    ytdcommstockconvert = bhck3580;
                end;
                label ytdprefstocksale = 'Sale of preferred stock, gross';
                label ytdprefstockconvert = 'Conversion or retirement of preferred stock, gross';
                label ytdcommstocksale = 'Sale of common stock, gross';
                label ytdcommstockconvert = 'Conversion or retirement of common stock, gross';

        /* Sale, conversion, acquisition, or retirement of capital stock, net */
         /* Date range: 2001q1 to present */
                ytdstocksale = .;
                ytdstocksale = sum(ytdprefstocksale, ytdprefstockconvert, ytdcommstocksale, ytdcommstockconvert);
                label ytdstocksale = 'Sale, conversion, acquisition, retirement of capital stock, net';


         /* Treasury stock sale and purchase, changes incident to business combinations (net) */
                if dt < 19860630 then do;
                    ytdtreasurysale = .;
                    ytdtreasurypurch = .;
                    ytdtreasurystock=.;
                    ytdbuschange = .;
                end;
                else do;
                    ytdtreasurysale = bhck4782;
                    ytdtreasurypurch = bhck4783;
                    ytdtreasurystock = sum(bhck4782,-bhck4783);
                    ytdbuschange = bhck4356;
                end;
                label ytdtreasurysale = 'Sale of treasury stock';
                label ytdtreasurypurch = 'Purchase of treasury stock';
                label ytdtreasurystock = 'Treasury stock transactions, net';
                label ytdbuschange = 'Capital changes due to business combinations, net';

        /* Cash dividends declared */
          /* Date range: 1986q2 to present */
                 if dt < 19860630 then do;
                     ytdprefdividend = .;
                     ytdcommdividend = .;
                 end;
                 else do;
                     ytdprefdividend = bhck4598;
                     ytdcommdividend = bhck4460;
                 end;
                 label ytdprefdividend = 'Cash dividends declared on preferred stock';
                 label ytdcommdividend = 'Cash dividends declared on common stock';

        /* Other comprehensive income */
         /* Date range: 2001q1 to present */
                if dt < 20010331 then ytdcompinc_oth = .;
                else
                ytdcompinc_oth = bhckb511;
                label ytdcompinc_oth = 'Other comprehensive income';

        /* Changes in ESOP offsetting debt */
         /* Date range: 1987q3 to present */
                ytdesop = .;
                ytdesop = bhck4591;
                label ytdesop = 'Change in ESOP offsetting debit';

        /* Other adjustments */
         /* Date range: 1990q3 to present */
                if dt < 19900930 then
                    ytdcapital_oth = .;
                else
                    ytdcapital_oth = bhck3581;
                label ytdcapital_oth = 'Other adjustments to equity capital';


 	/*Current Period Equity Capital After Changes (15)*/

             /* Total Equity Capital */;
                if dt < 19860630 then
                    equity = . ;
                else equity = bhck3210 ;
        	label equity='Total Equity Capital' ;


        /* Total Equity Capital with minority interests in consolidated subsidiaries */;
        if dt<19860630 then
            tot_eq_cap=.;
        else tot_eq_cap=sum(bhck3210,bhck3000);
        label tot_eq_cap='Total Equity Capital with minority interests in consolidated subsidiaries';



/************************************************************************
*************************************************************************
********************     BALANCE SHEET VARIABLES     ********************
*************************************************************************
************************************************************************/;

          /**********************************
          *********** ASSETS **************
          **********************************/;

             /* Total Assets */;
                if dt < 19810630 then
                    assets = . ;
                else assets = bhck2170 ;
        	label assets='Total Assets' ;

	/* Interest Earning Assets*/;
        	if dt < 19810630 then
                    earn_ass = . ;
                else if dt >= 19810630 & dt <= 19880331 then
                    earn_ass = sum(bhck0395,bhck0397,bhck0390,bhck1350,bhck2122,bhck2146) ;
                else if dt >= 19880630 & dt <= 19931231 then
                    earn_ass = sum(bhck0395,bhck0397,bhck0390,bhck0276,bhck0277,bhck2122,bhck2146) ;
                else if dt >= 19940331 & dt <= 19941231 then
                    earn_ass = sum(bhck0395,bhck0397,bhck1754,bhck1773,bhck0276,bhck0277,bhck2122,bhck2146) ;
                else if dt >= 19950331 & dt <= 19961231 then
                    earn_ass = sum(bhck0395,bhck0397,bhck1754,bhck1773,bhck0276,bhck0277,bhck2122,bhck3545) ;
                else if dt >=19970331 & dt<=20011231 then
		    earn_ass = sum(bhck0395,bhck0397,bhck1754,bhck1773,bhck1350,bhck2122,bhck3545) ;
		else earn_ass = sum(bhck0395,bhck0397,bhck1754,bhck1773,bhck2122,bhck3545,bhdmb987,bhckb989) ;
                label earn_ass = 'Interest Earning Assets' ;

        /* Trading Assets */;
	        if dt < 19810630 then
                trad_ass = . ;
            else if dt >=19810630 & dt < 19950331 then
                trad_ass = bhck2146 ;
            else trad_ass = bhck3545 ;
	    label trad_ass = 'Trading Assets' ;

		 /* Trading Assets Alternative (backfilled variable)*/;
            if dt < 19900331 then
                trad_ass_alt = . ;
            else trad_ass_alt = bhck3545 ;
	    label trad_ass_alt = 'Trading Assets Alternative' ;

    /* Premises and other Fixed Assets */;
              if dt < 19810630 then
                  fixed_ass = . ;
              else fixed_ass = bhck2145 ;
              label fixed_ass = 'Fixed Assets' ;

           /* Other Real Estate Owned */
                  if dt < 19810630 then
					  oreo = . ;
                  else if dt >=19810630 & dt<=19900630 then
                      oreo = bhck2150;
                  else if dt > 19900630 & dt<=20001231 then
                      oreo = sum(bhck2744,bhck2745);
                  else oreo = bhck2150 ;
                  label oreo = 'Other Real Estate Owned' ;

             /* Goodwill */ ;
             if dt < 19810630 then
                 goodwill = . ;
             else goodwill = bhck3163 ;
             label goodwill = 'Goodwill' ;

             /* Other intangible assets */ ;
                    if dt < 19850630 then do ;
                        intang_oth = . ;
                    end ;
                    else if dt >= 19850630 & dt < 19920331 then do ;
                        intang_oth = bhck3165 ;
                    end ;
                    else if dt >= 19920331 & dt <= 19981231 then do ;
                    intang_oth = sum(bhck3164,bhck5506,bhck5507) ;
                    end ;
                    else if dt > 19981231 & dt < 20010331 then do ;
                        intang_oth = sum(bhck3164,bhckb026,bhck5507) ;
                    end ;
                    else do ;
                        intang_oth = bhck0426 ;
                    end ;
                    label intang_oth = 'Other Intangible Assets' ;

             /* Total intangible assets */ ;
             intang = sum(intang_oth, goodwill) ;
             label intang = 'Intangible Assets' ;

             /* Investments in Unconsolidated Subsidiaries (assets) */ ;
             if dt<19810630 then
				unconsub_inv = . ;
			 else unconsub_inv = bhck2130;
			 label unconsub_inv = 'Investments in Unconsolidated Subsidiaries (assets)';

             /* Direct and Indirect Investments in RE Ventures (assets) */ ;
			 if dt<19900930 then
			    reven_inv = . ;
			 else reven_inv = bhck3656 ;
			 label reven_inv = 'Investments in RE Ventures (assets)' ;

             /* Other Assets */;
             if dt < 19810630 then
                 oth_assets = . ;
             else if dt<20060331 then oth_assets = sum(bhck2160, bhck2155) ;
             else oth_assets = bhck2160 ;
             label oth_assets = 'Other Assets (consistent)' ;

             /*All Other Assets*/
                 alloth_assets=sum(fixed_ass, oreo, unconsub_inv, goodwill, intang_oth, oth_assets);

              /* Cash & Interest Bearing Balances in Domestic and Foreign Offices */ ;
                if dt < 19810630 then do ;
                   cash = . ;
                   ibb = . ;
                end ;
                else do ;
                    cash = sum(bhck0081,bhck0395,bhck0397) ;
                    ibb = sum(bhck0395,bhck0397) ;
                end ;
                label cash='Cash and Balances Due from Depository Institutions' ;
                label ibb='Interest Bearing Balances' ;

				ibb_bhc=ibb;
				label ibb_bhc='Interest Bearing Balances - BHCs Only';

             /* Federal Funds Sold, Securities Purchased under Agreements to Resell */;
             /* for some reason the disaggregate series is not reported from 1997 to 2002 ?? */;
               if dt < 19810630 then do ;
                   ffrepo_ass = . ;
                   ffsold = . ;
                   repo_purch = . ;
               end ;
               else if dt >= 19810630 & dt < 19880630 then do ;
                   ffrepo_ass = bhck1350 ;
                    ffsold = . ;
                   repo_purch = . ;
               end ;
               else if dt >=19880630 & dt < 19970331 then do ;
                   ffrepo_ass = sum(bhck0276,bhck0277) ;
                   ffsold = bhck0276 ;
                   repo_purch = bhck0277 ;
               end ;
               else if dt >=19970331 & dt < 20020331 then do ;
                   ffrepo_ass = bhck1350 ;
                   ffsold = . ;
                   repo_purch = . ;
               end ;
               else do ;
                   ffrepo_ass = sum(bhdmb987,bhckb989) ;  /* for some reason ff sold only exists for dom offices */ ;
                   ffsold = bhdmb987 ;
                   repo_purch = bhckb989 ;
               end ;
               label ffrepo_ass='FF Sold & Securities Purchased under Agreements to Resell (asset)' ;
               label ffsold='Federal Funds Sold' ;
               label repo_purch='Securities Purchased under Agreements to Resell' ;

               /********************/;
               /* HC-B Securities */;
               /********************/;

               	if dt < 19950331 then
                    agency_htm= .;
                else agency_htm = sum(bhck1289, bhck1294) ;
                label agency_htm='Agency Securities (HTM)' ;

                	if dt < 19950331 then
	  muni_htm= .;
	else if dt>=19950331 & dt <20010331 then
	muni_htm=sum(bhck8531, bhck8535);
	else muni_htm = bhck8496 ;
	label muni_htm='Muni Securities (HTM)' ;

        if dt<20090630 then
            sfp_corp_ln_htm_amort = . ;
        else sfp_corp_ln_htm_amort = bhckg356;
        label sfp_corp_ln_htm_amort = 'Structured financial products by underlying collateral - HTM Corporate and similar loans - BHCKG356';

        if dt<20090630 then
            sfp_corp_ln_afs_amort = . ;
        else sfp_corp_ln_afs_amort = bhckg358;
        label sfp_corp_ln_afs_amort = 'Structured financial products by underlying collateral - AFS Corporate and similar loans - BHCKG358';

     /*****************************************************
     ******* SCHEDULE HC-C: Loans and Leases    ***********
     *****************************************************/;
   /* Total Loans & Net of Unearned Income */
	/* Loans Secured by Real Estate, C&I loans, Loans to Foreign Goverments, Lease Financing Receivables
        /* NOTE: Loans net of unearned income (2122) is net of unearned on LOANS ONLY (2132), not leases -- On Schedule HC
        /*   from 2001Q1 on, 2122 is not reported, it is equal to the sum of loans held for sale (5369) and other net loans
        /*   (B528) */;
          if dt < 19810630 then do ;
	      ln_tot = . ;
              unearned = . ;
              loans_gross = . ;
              llres = . ;
              ln_re = . ;
      	      ln_ci = . ;
      	      ln_fgovt = . ;
              ln_lease = . ;
          end ;
          else do ;
	      ln_tot = bhck2122;
	      unearned = bhck2123;
              loans_gross = sum(ln_tot,unearned) ;
              llres = bhck3123 ;
              ln_re = bhck1410;
			  /*Note: this definition of C&I is technically incorrect
			  	from 1981-1986 since bhck1764 begins in 1986. It is not
			  	an issue though because the y9c starts in 1986 and this pull
			  	begins in 1990*/;
              ln_ci = sum(bhck1763,bhck1764) ;
              ln_fgovt = bhck2081;
              ln_lease = sum(bhck2182,bhck2183) ;
          end ;
          if dt>=20070331 then
           ln_lease = sum(bhckf162, bhckf163);

          label ln_tot='Total Loans Net of Unearned Income' ;
	  	  label unearned='Unearned Income on Loans' ;
          label loans_gross='Total Loans (Gross)' ;
          label llres='Loan Loss Reserve' ;
          label ln_re='Loans Secured by Real Estate' ;
          label ln_ci='C&I Loans' ;
          label ln_fgovt='Loans to Foreign Goverments' ;
          label ln_lease='Lease Financing Receivables' ;


		  /*Lease Financing Receivables - BHCs Only*/
		  ln_lease_bhc =ln_lease;
		  label ln_lease_bhc = 'Lease Financing Receivables - BHCs Only';


        	/* 1-4 Family Residential Real Estate (Domestic) */;
                /* includes senior and junior mortgages and HE lines of credit */
          	  if dt < 19910331 then do;
                      ln_rre = . ;
                      ln_heloc = . ;
                      ln_closedlien = . ;
                      ln_firstlien = . ;
                      ln_jrlien = . ;
                  end ;
                  else do ;
                      ln_rre = sum(bhdm5367,bhdm5368,bhdm1797) ;
                      ln_heloc = bhdm1797 ;
                      ln_closedlien = sum(bhdm5367,bhdm5368) ;
                      ln_firstlien = bhdm5367 ;
                      ln_jrlien = bhdm5368 ;
                  end;

          	  label ln_rre='1-4 Family Residential Real Estate (Domestic)' ;
                  label ln_heloc = '1-4 Family HELOCs (Domestic)' ;
                  label ln_closedlien = '1-4 Family Closed Lien Mortgages (Domestic)';
                  label ln_firstlien = '1-4 Family 1st Lien Mortgages (Domestic)';
                  label ln_jrlien = '1-4 Family Jr Lien Mortgages (Domestic)';

        	/* Commercial Real Estate */;
        /*Construction and land development loans*/
              if dt<19900930 then do ;
                  ln_const=. ;
              end ;
              else if dt>=19900930 & dt<20070331 then do ;
                  ln_const=bhdm1415 ;
              end ;
              else if dt>=20070331 then do ;
                  ln_const=sum(bhckf158,bhckf159) ;
              end ;
              label ln_const='Construction loans' ;


        /*Multi-Family Resid. Property Loans*/
              if dt<19900930 then
                  ln_multi=. ;
              else ln_multi=bhdm1460 ;
              label ln_multi='Multi-Family Property Loans' ;

        /*Non-Farm, Non-Residential Real Estate Loans*/
               if dt<19900930 then do ;
                   ln_nfnr=. ;

               end ;
               else if dt>=19900930 & dt<20070331 then do ;
                   ln_nfnr=bhdm1480 ;
               end ;
               else if dt>=20070331 then do ;
                   ln_nfnr=sum(bhckf160,bhckf161) ;
               end ;
               label ln_nfnr='Non-Farm, Non-Res CRE Loans' ;

        /*Total Commercial Real Estate Loans*/
            ln_cre = sum(ln_const,ln_multi,ln_nfnr);
            label ln_cre = 'Commercial Real Estate Loans, Total';

        /* Other Real Estate loans */;
	/* Includes loans secured by farmland and from foreign offices since 2001 */;
        	   if dt < 19910331 then
                       ln_othre = . ;
                   else ln_othre = sum(ln_re,-ln_rre,-ln_cre) ;
                   label ln_othre = 'Other Real Estate Loans' ;

		/*Loans Secured by Farmland */;
		   if dt <19910331 then
			ln_farm = . ;
		   else ln_farm = bhdm1420 ;
		   label ln_farm='Loans Secured by Farmland';

	/* Loans to Depository Institutions & Acceptances of other banks */;
	/* Acceptances of all other banks (1755) are added back in between 1996Q3 & 2001Q1 */;
           if dt < 19860630 then
               ln_dep = . ;
           else if dt >= 19860630 & dt <= 19901231 then
               ln_dep = sum(bhck1604,bhck1510) ;
           else if dt >= 19910331 & dt <= 19951231 then
               ln_dep = sum(bhck5481,bhck5482) ;
           else if dt>= 19960331 & dt <= 20001231 then
               ln_dep = sum(bhck1292,bhck1296,bhck1755) ;
           else ln_dep = sum(bhck1292,bhck1296) ;
           label ln_dep='Loans to Depository Institutions' ;

	/* Loans to Finance Agricultural Production */;
      	   if dt < 19860630 then
      	       ln_agr = . ;
      	   else ln_agr = bhck1590 ;
      	   label ln_agr='Loans to Finance Agricultural Production' ;

	/* Loans to Consumers */;
    	   if dt < 19810630 then
               ln_cons = . ;
           else if dt >= 19810630 & dt <= 19901231 then
               ln_cons = bhck1975 ;
           else if dt >= 19910331 & dt <= 20001231 then
               ln_cons = sum(bhck2008,bhck2011) ;
           else if dt < 20110331 then
               ln_cons = sum(bhckb538,bhckb539,bhck2011) ;
           else ln_cons = sum(bhckb538,bhckb539,bhckk137,bhckk207) ;
	   label ln_cons='Loans to Consumers' ;

        	/* Credit Cards & Other Revolving Credit Plans */;
                /*Note: Time-series break in 2000q4/2001q1.
                  Nonperforming consumer loans includes related plans of credit with
                  CC through 2000q4, and with other consumer after. We follow the same
                  method here to create consistent NPL ratios.*/

            if dt < 19910331 then do ;
                    ln_cc = . ;
                    ln_othcons = . ;
                end ;
           else if dt >= 19910331 & dt <= 20001231 then do ;
                     ln_cc = bhck2008 ;
                     ln_othcons = bhck2011 ;
                end ;
           else if dt < 20110331 then do ;
                    ln_cc = bhckb538 ;
                    ln_othcons = sum(bhck2011,bhckb539) ;
                end ;
           else do ;
                    ln_cc = bhckb538 ;
                    ln_othcons = sum(bhckb539,bhckk137,bhckk207) ;
            end;
        	label ln_cc = 'Credit Cards';
        	label ln_othcons = 'Other Consumer Loans (Installment Loans)' ;

	/* Other Loans */;
	if dt < 19810630 then
            ln_oth = . ;
        else if dt >=19810630 & dt <= 19870630 then
            ln_oth = bhck1635 ;
        else if dt >= 19870930 & dt <= 20001231 then
            ln_oth = sum(bhck2033,bhck2079,bhck1563) ;
        else if dt >=20010331 & dt <= 20051231 then
            ln_oth = bhck1635 ;
        else if dt >=20060331 & dt <= 20091231 then
            ln_oth = sum(bhck1545, bhck1564);
        else ln_oth = sum(bhckj454, bhck1545, bhckj451);
	label ln_oth='All Other Loans' ;

	/* All Other Loans (constructed) */
	ln_allother=sum(ln_lease, ln_oth, ln_fgovt, ln_agr, ln_dep, ln_othre) ;
	label ln_allother = 'All Other Loans (constructed)' ;

  /* ****AFS SECURITIES PORTFOLIO**** */;

  /* Total Investment Securities - Book Value */ ;
 if dt<19810630 then
    inv_sec = . ;
else if dt>=19810630 & dt<19940331 then
    inv_sec = bhck0390;
else if dt>=19940331 then
    inv_sec=sum(bhck1754,bhck1773);
label inv_sec = 'Total Investment Securities (book value)' ;

/* Held-to-Maturity Securities, Total */ ;
if dt<19940331 then
	htmsec = . ;
else htmsec=bhck1754;
label htmsec = 'Held-to-Maturity Securities (total)' ;

/* Available-for-Sale Securities, Total */ ;
if dt<19940331 then
	afssec = . ;
else afssec=bhck1773 ;
label afssec = 'Available-for-Sale Securities (total)' ;


/******************************************/;
/* HC-D - Trading Assets and Liabilities */;
/******************************************/;

    /* U.S. Treasury Securities */;
           if dt < 19810630 then
                  afs_ust = .;
           else if dt >= 19810630 & dt < 19940331 then
                  afs_ust = bhck0400 ;
           else afs_ust= bhck1287 ;
           label afs_ust='U.S. Treasury AFS Securities' ;

           /* U.S. Treasury Securitise - Consolidated */;
           if dt<20080331 then
               ust_trad_con=.;
           else ust_trad_con = bhcm3531;
           label ust_trad_con = 'U.S. Treasury Securitise - Consolidated';


if dt < 19950331 then
    ust_trad_dom = .;
else ust_trad_dom = bhck3531;
label ust_trad_dom='U.S. Treasury (trading) in domestic offices' ;

if dt < 19950331 then
    ust_htm = .;
else ust_htm = bhck0211 ;
label ust_htm='U.S. Treasury Securities (HTM)' ;

	if dt < 19950331 then
	ust_trad = .;
	else if dt>=19950331 & dt <20080331 then
	ust_trad = bhck3531;
	else ust_trad = bhcm3531 ;
	label ust_trad='U.S. Treasury (trading) in domestic/consolidated offices' ;


	/*HTM (Amortized Cost) + AFS (Fair Value) + Trading Securities*/;
	if dt<19950331 then
	ust_sec=.;
	ust_sec = sum(ust_trad, afs_ust, ust_htm);
	label ust_sec='U.S. Treasury Securities';


	/*US Agency Securities - Domestic*/;

	if dt < 19950331 then
	agency_trad_dom = .;
	else agency_trad_dom = bhck3532;
	label agency_trad_dom='U.S. Govt Agency Obligations (trading) in domestic offices' ;

        /* US Agency Securities - Consolidated */;
        if dt<20080331 then
            agency_trad_con=.;
        else agency_trad_con=bhcm3532;
        label agency_trad_con = 'US Agency Securities - Consolidated';

	/*US Agency Securities*/;
	if dt < 19950331 then
	agency_trad = .;
	else if dt>=19950331 & dt <20080331 then
	agency_trad = bhck3532;
	else agency_trad = bhcm3532 ;
	label agency_trad='U.S. Govt Agency Obligations (trading) in domestic/consolidated offices' ;


        /* State Securities */ ;
        if dt>= 19940331 & dt<20010331 then
         afs_state = sum(bhck8534,bhck8538) ;
     else if dt >=20010331 then
    afs_state = bhck8499 ;
	label afs_state = 'Available-for-Sale Securities Issued by States and Political Subdivisions' ;



	/***********/;
	/**Assets**/;
	/***********/;

	/*U.S. Treasury Securities*/
	if dt<19950331 then
		trad_ust_sec =.;
	else if dt>=19950331 & dt<20080331 then
		trad_ust_sec =bhck3531;
	else trad_ust_sec = bhcm3531;
	label trad_ust_sec = 'U.S. Treasury Securities';


	/*U.S. government agency obligations (exclude mortgage-backed securities)*/
	if dt<19950331 then
		trad_us_gobl=.;
	else if dt>=19950331 & dt<20080331 then
		trad_us_gobl =bhck3532;
	else trad_us_gobl=bhcm3532;
	label trad_us_gobl = 'U.S. government agency obligations (exclude mortgage-backed securities)';

	/*Securities issued by states and political subdivisions in the U.S. */
	if dt<19950331 then
		trad_sec_sts =.;
	else if dt>=19950331 & dt<20080331 then
		trad_sec_sts = bhck3533;
	else trad_sec_sts=bhcm3533;
	label trad_sec_sts = 'Securities issued by states and political subdivisions in the U.S.';

	/*Agency pass through MBS - includes residential AND commercial*/
	if dt<19950331 then
		trad_agpth_mbs=.;
	else if dt>=19950331 & dt<20080331 then
		trad_agpth_mbs = bhck3534;
	else if dt>=20080331 & dt<20090630 then
		trad_agpth_mbs = bhcm3534;
	else trad_agpth_mbs=.;
	label trad_agpth_mbs = 'Agency Pass Through MBS';

	/*Residential Agency Pass Through MBS */
	if dt<20090630 then
		trad_ragpth_mbs=.;
	else trad_ragpth_mbs=bhckg379;
	label trad_ragpth_mbs = 'Residential Agency Pass Through MBS';

	/*Agency CMOS*/
	if dt<19950331 then
		trad_ag_cmos =.;
	else if dt>=19950331 & dt<20080331 then
		trad_ag_cmos = bhck3535;
	else if dt>=20080331 & dt<20090630 then
		trad_ag_cmos = bhcm3535;
	else trad_ag_cmos=.;
	label trad_ag_cmos = 'Agency CMOS';

	/*Residential Agency CMOS*/
	if dt<20090630 then
		trad_rag_cmos=.;
	else trad_rag_cmos = bhckg380;

	/*Non-agency MBS */
	if dt<19950331 then
		trad_nag_mbs=.;
	else if dt>=19950331 & dt<20080331 then
		trad_nag_mbs=bhck3536;
	else if dt>=20080331 & dt<20090630 then
		trad_nag_mbs = bhcm3536;
	else trad_nag_mbs =.;
	label trad_nag_mbs= 'Non-agency MBS';

	/*Residential Non-Agency MBS*/
	if dt<20090630 then
		trad_rnag_mbs=.;
	else trad_rnag_mbs = bhckg381;
	label trad_rnag_mbs = 'Residential Non-Agency MBS';

	/*Commercial MBS - includes agency AND non-agency MBS*/
	if dt<20090630 then
		trad_com_mbs=.;
	else if dt>=20090630 & dt<20110331 then
		trad_com_mbs=bhckg382;
	else trad_com_mbs = sum(bhckk197, bhckk198);
	label trad_com_mbs = 'Commercial MBS';

	/*Commercial MBS - issued by government agencies: includes both pass through AND CMOs*/
	if dt<20110331 then
		trad_com_ag_mbs =.;
	else trad_com_ag_mbs = bhckk197;
	label trad_com_ag_mbs = 'Commercial MBS issued or guaranteed by FNMA, GHLMC, or GNMA';

	/*Non-agency Commercial MBS - All Other Commercial MBS*/
	if dt<20110331 then
		trad_com_nag_mbs=.;
	else trad_com_nag_mbs = bhckk198;
	label trad_com_nag_mbs = 'All Other Commercial MBS - Non-agency Commercial MBS';

	/*Other Debt Securities*/
	if dt<19950331 then
		trad_othdsec=.;
	else if dt>=19950331 & dt<20080331 then
		trad_othdsec=bhck3537;
	else if dt>=20080331 & dt<20090630 then
		trad_othdsec=bhcm3537;
	else trad_othdsec = sum(bhckg383, bhckg384, bhckg385, bhckg386);
	label trad_othdsec = 'Other Debt Securities';

	/*Certificates of deposit in domestic offices*/
	if dt<19950331 then
		trad_certdp_d=.;
	else if dt>=19950331 & dt<19980331 then
		trad_certdp_d = bhck3538;
	else trad_certdp_d=.;
	label trad_certd_d = 'Certificates of deposit in domestic offices';

	/*Commercial Paper in domestic offices*/
	if dt<19950331 then
		trad_comppr_d=.;
	else if dt>=19950331 & dt<19980331 then
		trad_comppr_d = bhck3539;
	else trad_comppr_d =.;
	label trad_comppr_d = 'Commercial paper in domestic offices';

	/*Bankers acceptances in domestic offices*/
	if dt<19950331 then
		trad_bkccp_d=.;
	else if dt>=19950331 & dt<19980331 then
		trad_bkccp_d=bhck3540;
	else trad_bkccp_d = .;
	label trad_bkccp_d = 'Banker acceptances in domestic offices';

	/*Other trading assets - Called Other trading assets in domestic offices prior to 2008Q1 */
	if dt<19950331 then
	 	trad_othass=.;
	else if dt>=19950331 & dt<20080331 then
		trad_othass=bhck3541;
	else trad_othass=bhcm3541;
	label trad_othass = 'Other Trading Assets';

	/*Trading Assets in foreign offices*/
	if dt<19950331 then
		trad_assfor=.;
	else if dt>=19950331 & dt<20080331 then
		trad_assfor=bhck3542;
	else trad_assfor=.;
	label trad_assfor = 'Trading Assets in foreign offices';

	/*Derivatives with a positive fair value (also known as Revaluation gains on interest rate, foreign exchange rate, equity, commodity and other contracts)*/
	if dt<19950331 then
		trad_dpos_fv=.;
	else if dt>=19950331 & dt<20080331 then
		trad_dpos_fv = sum(bhck3543, bhfn3543);
	else trad_dpos_fv=bhcm3543;
	label trad_dpos_fv = 'Derivatives with a positive fair value';

	/*Loans Secured by Real Estate*/
	if dt<20080331 then
		trad_ln_res=.;
	else trad_ln_res=bhckf610;
	label trad_ln_res = 'Loans Secured by Real Estate';

	/*Commercial and Industrial Loans*/
	if dt<20080331 then
		trad_ln_comind=.;
	else trad_ln_comind = bhckf614;
	label trad_ln_comind = 'Commercial and Industrial Loans';

	/*Credit Cards - Loans to individuals for household, family, and other personal expenditures*/
	if dt<20080331 then
		trad_ln_cc=.;
	else trad_ln_cc=bhckf615;
	label trad_ln_cc = 'Credit Cards - Loans to individuals for household, family and other personal expenditures';

	/*Other Revolving Card Plans - Loans to individuals for household, family, and other personal expenditures*/
	if dt<20080331 then
		trad_ln_othrcp=.;
	else trad_ln_othrcp = bhckf616;
	label trad_ln_othrcp = 'Other Revolving Card Plans - Loans to individuals for household, family, and other personal expenditures';

	/*Other Consumer Loans - Loans to individuals for household, family, and other personal expenditures*/
	if dt<20080331 then
		trad_ln_othcons=.;
	else if dt>=20080331 & dt<20110331 then
		trad_ln_othcons = bhckf617;
	else trad_ln_othcons = bhckk210;
	label trad_ln_othcons = 'Other Consumer Loans - Loans to individuals for household, family, and other personal expenditures';

	/*Automobile Loans - Loans to individuals for household, family, and other personal expenditures*/
	if dt<20110331 then
		trad_ln_autom=.;
	else trad_ln_autom=bhckk199;
	label trad_ln_autom = 'Automobile Loans - Loans to individuals for household, family, and other personal expenditures';

	/*Other Loans*/
	if dt<20080331 then
		trad_ln_oth=.;
	else trad_ln_oth=bhckf618;
	label trad_ln_oth = 'Other Loans';

	/*Total Trading Assets*/
	if dt<19950331 then
		trad_totass=.;
	else trad_totass= bhck3545;
	label trad_totass = 'Total Trading Assets';

              	/*HTM (Amortized Cost) + AFS (Fair Value) + Trading Securities*/;
	if dt<19950331 then
	muni_sec=.;
	else muni_sec = sum(trad_sec_sts, afs_state, muni_htm);
	label muni_sec='U.S. Muni Securities';

        	/*Agency MBS*/;

	/*Note: in 200906, commercial mbs is broken out, but not broken out into agency and nonagency - put all of this in nonagency*/
	/*In 2011, commercial mbs broken out into agency and nonagency*/;

	if dt < 19950331 then
	agency_mbs_trad = .;
	else if dt>=19950331 & dt <20080331 then
	 agency_mbs_trad = sum(bhck3534, bhck3535);
	else if dt>=20080331 & dt <20090630 then
	agency_mbs_trad=sum(bhcm3534, bhcm3535);
	else if dt>=20090630 & dt <20110331 then
	 agency_mbs_trad=sum(bhckg379, bhckg380);
	else agency_mbs_trad = sum(bhckg379, bhckg380, bhckk197) ;
	label agency_mbs_trad='Agency MBS (trading)' ;




	/*****************/
	/**Liabilities**/
	/*****************/

	/*Liability for short positions (also known as Revaluation losses on interest rate, foreign exchange rate, equity, commodity and other contracts*/
	if dt<19950331 then
		liab_trad_shpos=.;
	else if dt>=19950331 & dt<20090331 then
		liab_trad_shpos=bhck3546;
	else liab_trad_shpos = sum(bhckg209, bhckg210, bhckg211);
	label liab_trad_shpos = 'Liability for short positions';

	/*All other trading liabilites*/
	if dt<20080331 then
		liab_trad_alloth=.;
	else liab_trad_alloth=bhckf624;
	label liab_trad_alloth = 'All other Trading liabilities';

	/*Derivatives with a negative fair value*/
	if dt<19950331 then
		liab_trad_dneg_fv=.;
	else liab_trad_dneg_fv=bhck3547;
	label liab_trad_dneg_fv = 'Derivatives with a negative fair value';

	/*Total Trading Liabilites*/
	if dt<19950331 then
		liab_trad_tot=.;
	else liab_trad_tot = bhck3548;
	label liab_trad_tot = 'Total Trading Liabilities';


/*******************************************/;
/*FOR GRANULAR SECURITIES MODELLING TEAM*/;
/*******************************************/;

	/*Schedule HC-B: HTM(Amortized Cost) */

	/*HTM U.S. Treasury Securities*/;
	if dt<19940331 then
		htm_ust = .;
	else htm_ust = bhck0211;
	label htm_ust= 'HTM U.S. Treasury Securities';


	/* HTM U.S. government agency and corporation obligations (exclude mortgage-backed securities) Issued by U.S. government agencies*/;
	if dt<19940331 then
		htm_gov_obl=.;
	else htm_gov_obl=bhck1289;
	label htm_gov_obl = 'U.S. government agency obligations (exclude mortgage-backed securities - Issued by U.S. government agencies';


	/* HTM U.S. government agency and corporation obligations (exclude mortgage-backed securities) Issued by U.S. government-sponsored agencies*/;
	if dt<19940331 then
		htm_gvsp_obl=.;
	else htm_gvsp_obl=bhck1294;
	label htm_gvsp_obl = 'U.S. government agency obligations (exclude mortgage-backed securities - Issued by U.S. government-sponsored agencies';

	/* HTM Securities issued by states and political subdivisions in the U.S. */;
	if dt<19940331 then
		htm_sec_sts=.;
	else if dt>=19940331 & dt<20010331 then
		htm_sec_sts=sum(bhck8531, bhck8535);
	else htm_sec_sts=bhck8496;
	label htm_sec_sts = 'HTM securities issued by states and political subdivisions in the U.S.';

	/* Commercial MBS */

	/* Commercial Agency issued pass-through securities */
	if dt<20110331 then
		htm_copth_sag=.;
	else htm_copth_sag=bhckk142;
	label htm_copth_sag = 'Commercial Pass-Through Securities - Issued by FNMA, GGHLMC, or GNMA';

	/* Commercial Agency issued MBS */
	if dt<20110331 then
		htm_cmbs_ag=.;
	else htm_cmbs_ag=bhckk150;
	label htm_cmbs_ag = 'Other Commercial MBS - Issued or guaranteed by FNMA, FHLMC, or GNMA';

	/* Commercial non-agency pass through securities */
	if dt<20110331 then
		htm_copth_oth=.;
	else htm_copth_oth= bhckk146;
	label htm_copth_oth='Commercial pass-through securities - other pass-through securities';

	/* Other non-agency commercial MBS */
	if dt<20110331 then
		htm_aoth_cmbs=.;
	else htm_aoth_cmbs=bhckk154;
	label htm_aoth_cmbs = 'Other Commercial MBS - All Other MBS';

	/* Commercial pass through securities */
	if dt<20090630 then
		htm_copth=.;
	else if dt>=20090630 & dt<20110331 then
		htm_copth=bhckg324;
	else htm_copth = sum(bhckk142, bhckk146);
	label htm_copth = 'Commercial pass through securities';

	/* Other Commercial MBS */
	if dt<20090630 then
		htm_commbs=.;
	else if dt>=20090630 & dt<20110331 then
		htm_commbs= bhckg328;
	else htm_commbs = sum(bhckk150, bhckk154);
	label htm_commbs = 'Other Commercial MBS';

	/* Agency Pass Through MBS */
	if dt<20010331 then
		htm_agpth_mbs = .;
	else if dt>=20010331 & dt<20090630 then
		htm_agpth_mbs = sum(bhck1698, bhck1703);
	else if dt>=20090630 & dt<20110331 then
		htm_agpth_mbs = sum(bhckg300, bhckg304);
	else htm_agpth_mbs = sum(bhckg300, bhckg304, bhckk142);
	label htm_agpth_mbs = 'Agency Pass Through MBS';


	/* Agency CMOs */
	if dt<20010331 then
		htm_ag_cmos=.;
	else if dt>=20010331 & dt<20090630 then
		htm_ag_cmos = sum(bhck1714, bhck1718);
	else if dt>=20090630 & dt<20110331 then
		htm_ag_cmos = sum(bhckg312, bhckg316);
	else htm_ag_cmos = sum(bhckg312, bhckg316, bhckk150);
	label htm_ag_cmos = 'Agency CMOs';

	/* Non Agency MBS */
	if dt<20010331 then
		htm_nag_mbs=.;
	else if dt>=20010331 & dt<20090630 then
		htm_nag_mbs = sum(bhck1709, bhck1733);
	else if dt>=20090630 & dt<20110331 then
		htm_nag_mbs = sum(bhckg308, bhckg320, bhckg324, bhckg328);
	else htm_nag_mbs = sum(bhckg308, bhckg320, bhckk146, bhckk154);
	label htm_nag_mbs = 'Non Agency MBS';


	/* Asset Backed Securities */
	if dt<20010331 then
		htm_ab_sec=.;
	else if dt>=20010331 & dt<20060331 then
		htm_ab_sec = sum(bhckb838, bhckb842, bhckb846, bhckb850, bhckb854, bhckb858);
	else if dt>=20060331 & dt<20090630 then
		htm_ab_sec = bhckc026;
	else htm_ab_sec = sum(bhckc026, bhckg336, bhckg340, bhckg344);
	label htm_ab_sec = 'Asset Backed Securities';

	/*Other Domestic Debt Securities */
	if dt<20010331 then
		htm_odom_dsec=.;
	else htm_odom_dsec =bhck1737;
	label htm_odom_dsec = 'Other Domestic Debt Securities';

	/*Other Foreign Debt Securities */
	if dt<20010331 then
		htm_ofgn_dsec=.;
	else htm_ofgn_dsec = bhck1742;
	label htm_ofgn_dsec = 'Other Foreign Debt Securities';

	/* HTM (Amortized Cost) Total Securities */
	if dt<20010331 then
		htm_tot_sec=.;
	else htm_tot_sec = bhct1754;
	label htm_tot_sec = 'HTM (Amortized Cost) Total Securities';


	/*Schedule HC-B: AFS (Fair Value) */

	/*AFS U.S. Treasury Securities*/;
	if dt<19940331 then
		afs_ust=.;
	else afs_ust=bhck1287;
	label afs_ust= 'AFS U.S. Treasury Securities';

	/* AFS U.S. government agency and corporation obligations (exclude mortgage-backed securities) issued by U.S. government agencies*/;
	if dt<19940331 then
		afs_gov_obl=.;
	else afs_gov_obl=bhck1293;
	label afs_gov_obl = 'U.S. government agency obligations (exclude mortgage-backed securities - Issued by U.S. government agencies';


	/* AFS U.S. government agency and corporation obligations (exclude mortgage-backed securities) issued by U.S. government-sponsored agencies*/;
	if dt<19940331 then
		afs_gvsp_obl=.;
	else afs_gvsp_obl=bhck1298;
	label afs_gvsp_obl = 'U.S. government agency obligations (exclude mortgage-backed securities - Issued by U.S. government-sponsored agencies';

	/* AFS Securities issued by states and political subdivisions in the U.S. */;
	if dt<19940331 then
		afs_sec_sts=.;
	else if dt>=19940331 & dt<20010331 then
		afs_sec_sts=sum(bhck8534, bhck8538);
	else afs_sec_sts= bhck8499;
	label afs_sec_sts = 'AFS securities issued by states and political subdivisions in the U.S.';

	/* Commercial MBS */

	/* Commercial Agency issued pass-through securities */
	if dt<20110331 then
		afs_copth_sag=.;
	else afs_copth_sag=bhckk145;
	label afs_copth_sag = 'Commercial Pass-Through Securities - Issued by FNMA, FHLMC, or GNMA';

	/* Commercial Agency issued MBS */
	if dt<20110331 then
		afs_cmbs_ag=.;
	else afs_cmbs_ag=bhckk153;
	label afs_cmbs_ag = 'Other Commercial MBS - Issued or guaranteed by FNMA, FHLMC, or GNMA';

	/* Commercial non-agency pass through securities */
	if dt<20110331 then
		afs_copth_oth=.;
	else afs_copth_oth= bhckk149;
	label afs_copth_oth = 'Commercial pass-through securities - other pass-through securities';

	/* Other non-agency commercial MBS */
	if dt<20110331 then
		afs_aoth_cmbs=.;
	else afs_aoth_cmbs=bhckk157;
	label afs_aoth_cmbs = 'Other Commercial MBS - All Other MBS';

	/* Commercial pass through securities */
	if dt<20090630 then
		afs_copth=.;
	else if dt>=20090630 & dt<20110331 then
		afs_copth=bhckg327;
	else afs_copth = sum(bhckk145, bhckk149);
	label afs_copth = 'Commercial pass through securities';

	/* Other Commercial MBS */
	if dt<20090630 then
		afs_commbs=.;
	else if dt>=20090630 & dt<20110331 then
		afs_commbs= bhckg331;
	else afs_commbs = sum(bhckk153, bhckk157);
	label afs_commbs = 'Other Commercial MBS';


	/* Agency Pass Through MBS */
	if dt<20010331 then
		afs_agpth_mbs = .;
	else if dt>=20010331 & dt<20090630 then
		afs_agpth_mbs = sum(bhck1702, bhck1707);
	else if dt>=20090630 & dt<20110331 then
		afs_agpth_mbs = sum(bhckg303, bhckg307);
	else afs_agpth_mbs = sum(bhckg303, bhckg307, bhckk145);
	label afs_agpth_mbs = 'Agency Pass Through MBS';


	/* Agency CMOs */
	if dt<20010331 then
		afs_ag_cmos=.;
	else if dt>=20010331 & dt<20090630 then
		afs_ag_cmos = sum(bhck1717, bhck1732);
	else if dt>=20090630 & dt<20110331 then
		afs_ag_cmos = sum(bhckg315, bhckg319);
	else afs_ag_cmos = sum(bhckg315, bhckg319,bhckk153);
	label afs_ag_cmos ='Agency CMOs';

	/* Non Agency MBS */
	if dt<20010331 then
		afs_nag_mbs=.;
	else if dt>=20010331 & dt<20090630 then
		afs_nag_mbs = sum(bhck1713, bhck1736);
	else if dt>=20090630 & dt<20110331 then
		afs_nag_mbs = sum(bhckg311, bhckg323, bhckg327, bhckg331);
	else afs_nag_mbs = sum(bhckg311, bhckg323, bhckk149, bhckk157);
	label afs_nag_mbs = 'Non Agency MBS';


	/* Asset Backed Securities */
	if dt<20010331 then
		afs_ab_sec=.;
	else if dt>=20010331 & dt<20060331 then
		afs_ab_sec = sum(bhckb841, bhckb845, bhckb849, bhckb853, bhckb857, bhckb861);
	else if dt>=20060331 & dt<20090630 then
		afs_ab_sec = bhckc027;
	else afs_ab_sec = sum(bhckc027, bhckg339, bhckg343, bhckg347);
	label afs_ab_sec = 'Asset Backed Securities';

	/*Other Domestic Debt Securities */
	if dt<20010331 then
		afs_odom_dsec=.;
	else afs_odom_dsec = bhck1741;
	label afs_odom_dsec = 'Other Domestic Debt Securities';

	/*Other Foreign Debt Securities */
	if dt<20010331 then
		afs_ofgn_dsec=.;
	else afs_ofgn_dsec = bhck1746;
	label afs_ofgn_dsec = 'Other Foreign Debt Securities';

	/* Investments in mutual funds and other equity securities */
	if dt<20010331 then
		afs_inv_mf_eqsec=.;
	else afs_inv_mf_eqsec = bhcka511;
	label afs_inv_mf_eqsec = 'Investments in mutual funds and other equity securities with readily determinable fair values';

	/* AFS (Fair Value) Total Securities */
	if dt<20010331 then
		afs_tot_sec=.;
	else afs_tot_sec=bhct1773;
	label afs_tot_sec = 'AFS (Fair Value) of total securities';


	/*Schedule HC-B: HTM (Amortized Cost) + AFS (Fair Value)*/;

	/*HTM and AFS U.S. Treasury Securities*/;
	if dt<19940331 then
		htmafs_ust = .;
	else htmafs_ust = sum(bhck0211, bhck1287);
	label htmafs_ust= 'HTM and AFS U.S. Treasury Securities';


	/*HTM and AFS U.S. Treasury Securities*/;
	if dt<19940331 then
		htmafs_ust_bhc = .;
	else htmafs_ust_bhc = sum(bhck0211, bhck1287);
	label htmafs_ust_bhc = 'HTM and AFS U.S. Treasury Securities: BHCs Only';

	/* HTM and AFS U.S. government agency and corporation obligations (exclude mortgage-backed securities) issued by U.S. government agencies*/;
	if dt<19940331 then
		htmafs_gov_obl=.;
	else htmafs_gov_obl=sum(bhck1289, bhck1293);
	label htmafs_gov_obl = 'U.S. government agency obligations (exclude mortgage-backed securities - Issued by U.S. government agencies';

        /* Agency Securities */;
                /*Note: bhck8495 = bhck1293+bhck1298 + bhck1702 + bhck1707 + bhck1717 +bhck1732*/;
	if dt < 19950331 then
	agency_afs= .;
	else agency_afs = sum(bhck1293, bhck1298);
	label agency_afs='Agency Securities (AFS)' ;


	/* HTM and AFS U.S. government agency and corporation obligations (exclude mortgage-backed securities) issued by U.S. government-sponsored agencies*/;
	if dt<19940331 then
		htmafs_gvsp_obl=.;
	else htmafs_gvsp_obl=sum(bhck1294, bhck1298);
	label htmafs_gvsp_obl = 'U.S. government agency obligations (exclude mortgage-backed securities - Issued by U.S. government-sponsored agencies';

	/* HTM and AFS Securities issued by states and political subdivisions in the U.S. */;
	if dt<19940331 then
		htmafs_sec_sts=.;
	else if dt>=19940331 & dt<20010331 then
		htmafs_sec_sts=sum(bhck8531, bhck8535, bhck8534, bhck8538);
	else htmafs_sec_sts=sum(bhck8496, bhck8499);
	label htmafs_sec_sts = 'HTM and AFS securities issued by states and political subdivisions in the U.S.';

	/* Agency Pass Through MBS */
	if dt<20010331 then
		htmafs_agpth_mbs = .;
	else if dt>=20010331 & dt<20090630 then
		htmafs_agpth_mbs = sum(bhck1698, bhck1703, bhck1702, bhck1707);
	else if dt>=20090630 & dt<20110331 then
		htmafs_agpth_mbs = sum(bhckg300, bhckg304, bhckg303, bhckg307);
	else htmafs_agpth_mbs = sum(bhckg300, bhckg304, bhckg303, bhckg307, bhckk142, bhckk145);
	label htmafs_agpth_mbs ='Agency Pass Through MBS';


	/* Agency CMOs */
	if dt<20010331 then
		htmafs_ag_cmos=.;
	else if dt>=20010331 & dt<20090630 then
		htmafs_ag_cmos = sum(bhck1714, bhck1718, bhck1717, bhck1732);
	else if dt>=20090630 & dt<20110331 then
		htmafs_ag_cmos = sum(bhckg312, bhckg316, bhckg315, bhckg319);
	else htmafs_ag_cmos = sum(bhckg312, bhckg316, bhckg315, bhckg319, bhckk150, bhckk153);
	label htmafs_ag_cmos ='Agency CMOs';

	/* Non Agency MBS */
	if dt<20010331 then
		htmafs_nag_mbs=.;
	else if dt>=20010331 & dt<20090630 then
		htmafs_nag_mbs = sum(bhck1709, bhck1733, bhck1713, bhck1736);
	else if dt>=20090630 & dt<20110331 then
		htmafs_nag_mbs = sum(bhckg308, bhckg320, bhckg324, bhckg328, bhckg311, bhckg323, bhckg327, bhckg331);
	else htmafs_nag_mbs = sum(bhckg308, bhckg320, bhckk146, bhckk154, bhckg311, bhckg323, bhckk149, bhckk157);
	label htmafs_nag_mbs ='Non Agency MBS';


	/* Asset Backed Securities */
	if dt<20010331 then
		htmafs_ab_sec=.;
	else if dt>=20010331 & dt<20060331 then
		htmafs_ab_sec = sum(bhckb838, bhckb842, bhckb846, bhckb850, bhckb854, bhckb858, bhckb841, bhckb845, bhckb849, bhckb853, bhckb857, bhckb861);
	else if dt>=20060331 & dt<20090630 then
		htmafs_ab_sec = sum(bhckc026, bhckc027);
	else htmafs_ab_sec = sum(bhckc026, bhckg336, bhckg340, bhckg344, bhckc027, bhckg339, bhckg343, bhckg347);
	label htmafs_ab_sec ='Asset Backed Securities';

	/*Other Domestic Debt Securities */
	if dt<20010331 then
		htmafs_odom_dsec=.;
	else htmafs_odom_dsec = sum(bhck1737, bhck1741);
	label htmafs_odom_dsec ='Other Domestic Debt Securities';

	/*Other Foreign Debt Securities */
	if dt<20010331 then
		htmafs_ofgn_dsec=.;
	else htmafs_ofgn_dsec = sum(bhck1742, bhck1746);
	label htmafs_ofgn_dsec ='Other Foreign Debt Securities';

	/* HTM (Amortized Cost) and AFS (Fair Value) Total Securities */
	if dt<20010331 then
		htmafs_tot_sec=.;
	else htmafs_tot_sec = sum(bhct1754, bhct1773);
	label htmafs_tot_sec ='HTM (Amortized Cost) and AFS (Fair Value) Total Securities';

	/* Less granular HC-B items*/

	/*HTM and AFS MBS*/;
	if dt<19940331 then
		htmafs_mbs = .;
	else if dt>=19940331 & dt<19940930 then
		htmafs_mbs = sum(bhck1698, bhck1703, bhck1702, bhck1707);
	else if dt>=19940930 & dt<20090630 then
		htmafs_mbs = sum(bhck1698, bhck1703, bhck1709, bhck1714, bhck1718, bhck1733, bhck1702, bhck1707, bhck1713, bhck1717, bhck1732, bhck1736 );
	else if dt>=20090630 & dt<20110331 then
		htmafs_mbs = sum(bhckg300, bhckg304, bhckg308, bhckg312, bhckg316, bhckg320, bhckg324, bhckg328, bhckg303, bhckg307, bhckg311, bhckg315, bhckg319, bhckg323, bhckg327, bhckg331);
	else htmafs_mbs = sum(bhckg300, bhckg304, bhckg308, bhckg312, bhckg316, bhckg320, bhckk142, bhckk146, bhckk150, bhckk154, bhckg303, bhckg307, bhckg311, bhckg315, bhckg319, bhckg323, bhckk145, bhckk149, bhckk153, bhckk157);
	label htmafs_mbs ='HTM and AFS MBS';

	/*HTM and AFS MBS*/;
	if dt<19940331 then
		htmafs_mbs_bhc = .;
	else if dt>=19940331 & dt<19940930 then
		htmafs_mbs_bhc = sum(bhck1698, bhck1703, bhck1702, bhck1707);
	else if dt>=19940930 & dt<20090630 then
		htmafs_mbs_bhc = sum(bhck1698, bhck1703, bhck1709, bhck1714, bhck1718, bhck1733, bhck1702, bhck1707, bhck1713, bhck1717, bhck1732, bhck1736 );
	else if dt>=20090630 & dt<20110331 then
		htmafs_mbs_bhc = sum(bhckg300, bhckg304, bhckg308, bhckg312, bhckg316, bhckg320, bhckg324, bhckg328, bhckg303, bhckg307, bhckg311, bhckg315, bhckg319, bhckg323, bhckg327, bhckg331);
	else htmafs_mbs_bhc = sum(bhckg300, bhckg304, bhckg308, bhckg312, bhckg316, bhckg320, bhckk142, bhckk146, bhckk150, bhckk154, bhckg303, bhckg307, bhckg311, bhckg315, bhckg319, bhckg323, bhckk145, bhckk149, bhckk153, bhckk157);
	label htmafs_mbs_bhc ='HTM and AFS MBS: BHCs Only';

	/*HTM All Other Securities*/;
	/*MDRM definition for ABS is wrong, b841 - b861 only available up until 2005q4*/;
	if dt<20010331 then
		htmafs_oth = .;
	else if dt>=20010331 & dt<20060331 then
		htmafs_oth = sum(bhck1737, bhck1742, bhckb838, bhckb842, bhckb846, bhckb850, bhckb854, bhckb858, bhck1741, bhck1746, bhckb841, bhckb845, bhckb849, bhckb853, bhckb857, bhckb861, bhcka511, bhck8496, bhck8499);
	else if dt>=20060331 & dt<20090630 then
		htmafs_oth = sum(bhck1737, bhck1742, bhckc026, bhck1741, bhck1746, bhcka511, bhckc027, bhck8496, bhck8499);
	else htmafs_oth =sum(bhck1737, bhck1742, bhckc026, bhckg336, bhckg340, bhckg344, bhck1741, bhck1746, bhcka511, bhckc027, bhckg339, bhckg343, bhckg347, bhck8496, bhck8499);
	label htmafs_oth ='HTM All Other Securities';


	/*HTM All Other Securities*/;
	/*MDRM definition for ABS is wrong, b841 - b861 only available up until 2005q4*/;
	if dt<20010331 then
		htmafs_oth_bhc = .;
	else if dt>=20010331 & dt<20060331 then
		htmafs_oth_bhc = sum(bhck1737, bhck1742, bhckb838, bhckb842, bhckb846, bhckb850, bhckb854, bhckb858, bhck1741, bhck1746, bhckb841, bhckb845, bhckb849, bhckb853, bhckb857, bhckb861, bhcka511, bhck8496, bhck8499);
	else if dt>=20060331 & dt<20090630 then
		htmafs_oth_bhc = sum(bhck1737, bhck1742, bhckc026, bhck1741, bhck1746, bhcka511, bhckc027, bhck8496, bhck8499);
	else htmafs_oth_bhc =sum(bhck1737, bhck1742, bhckc026, bhckg336, bhckg340, bhckg344, bhck1741, bhck1746, bhcka511, bhckc027, bhckg339, bhckg343, bhckg347, bhck8496, bhck8499);
	label htmafs_oth_bhc ='HTM All Other Securities: BHCs Only';


	if dt<20010331 then
		ibb_inv_sec_nombs = .;
	else ibb_inv_sec_nombs = sum(ibb, htmafs_ust, htmafs_oth);
	label ibb_inv_sec_nombs='IBB + Investment Securities - MBS';

	/*AMORTIZED COST OF AFS AND HTM SECURITIES TOGETHER, BHCs ONLY*/;
	/*Amortized Cost AFS+HTM U.S. Treasury and Agency Securities*/;
	if dt<19940331 then
		amort_cost_htmafs_ust_bhc = .;
	else amort_cost_htmafs_ust_bhc = sum(bhck0211, bhck1289, bhck1294, bhck1286, bhck1291, bhck1297);
	label amort_cost_htmafs_ust_bhc ='Amortized Cost HTM and AFS U.S. Treasury and Agency Securities: BHCs Only';

	/*Amortized Cost AFS+HTM MBS*/;
	if dt<19940331 then
		amort_cost_htmafs_mbs_bhc = .;
	else if dt>=19940331 & dt<19940930 then
		amort_cost_htmafs_mbs_bhc = sum(bhck1698, bhck1703, bhck1701, bhck1706, bhck1711, bhck1716, bhck1731, bhck1735);
	else if dt>=19940930 & dt<20090630 then
		amort_cost_htmafs_mbs_bhc = sum(bhck1698, bhck1703, bhck1709, bhck1714, bhck1718, bhck1733, bhck1701, bhck1706, bhck1711, bhck1716, bhck1731, bhck1735);
	else if dt>=20090630 & dt<20110331 then
		amort_cost_htmafs_mbs_bhc = sum(bhckg300, bhckg304, bhckg308, bhckg312, bhckg316, bhckg320, bhckg324, bhckg328, bhckg302, bhckg306, bhckg310, bhckg314, bhckg318, bhckg322, bhckg326, bhckg330);
	else amort_cost_htmafs_mbs_bhc = sum(bhckg300, bhckg304, bhckg308, bhckg312, bhckg316, bhckg320, bhckk142, bhckk146, bhckk150, bhckk154, bhckg302, bhckg306, bhckg310, bhckg314, bhckg318, bhckg322, bhckk144, bhckk148, bhckk152, bhckk156);
	label amort_cost_htmafs_mbs_bhc ='Amortized Cost HTM and AFS MBS: BHCs Only';

	/*Amortized Cost AFS+HTM All Other Securities*/;
	if dt<20010331 then
		amort_cost_htmafs_oth_bhc = .;
	else if dt>=20010331 & dt<20060331 then
		amort_cost_htmafs_oth_bhc = sum(bhck1737, bhck1742, bhckb838, bhckb842, bhckb846, bhckb850, bhckb854, bhckb858, bhck1739, bhck1744, bhckb840, bhckb844, bhckb848, bhckb852, bhckb856, bhckb860, bhcka510, bhck8496, bhck8498);
	else if dt>=20060331 & dt<20090630 then
		amort_cost_htmafs_oth_bhc = sum(bhck1737, bhck1742, bhckc026, bhck1739, bhck1744, bhcka510, bhckc989, bhck8496, bhck8498);
	else amort_cost_htmafs_oth_bhc =sum(bhck1737, bhck1742, bhckc026, bhckg336, bhckg340, bhckg344, bhck1739, bhck1744, bhcka510, bhckc989, bhckg338, bhckg342, bhckg346, bhck8496, bhck8498);
	label amort_cost_htmafs_oth_bhc ='Amortized Cost HTM and AFS All Other Securities: BHCs Only';




/**********************************************/;




                  /* U.S. Agency Securities */;
                        if dt < 19810630 then
                            afs_agency_nonmbs = .;
                        else if dt >= 19810630 & dt < 19940331 then
                            afs_agency_nonmbs = bhck0600 ;
                        else if dt >= 19940331 & dt < 20010331 then
                            afs_agency_nonmbs = bhck8495;
                        else afs_agency_nonmbs = sum(bhck1293,bhck1298) ;
                        label afs_agency_nonmbs ='U.S. Agency Non-MBS AFS Securities' ;

                                 /*HTM (Amortized Cost) + AFS (Fair Value) + Trading Securities*/;
	if dt<19950331 then
	agency_sec=.;
	else agency_sec = sum(agency_trad, afs_agency_nonmbs, agency_htm);
	label agency_sec='U.S. Agency Securities';




   /* Available-for-Sale Agency MBS */ ;
     if dt>=20110331 then
    afs_agency_mbs = sum(bhckg303,bhckg307,bhckg315,bhckg319,bhckk145,bhckk149) ;
else if dt<20110331 & dt>=20090630 then
    afs_agency_mbs=sum(bhckg303,bhckg307,bhckg315,bhckg319) ;
else if dt<20090630 & dt>=20060331 then
   afs_agency_mbs =sum(bhck1702,bhck1707,bhck1717,bhck1732) ;
else if dt<20060331 & dt>=20010331 then
   afs_agency_mbs =sum(bhck1702,bhck1707,bhck1717,bhck1732) ;
else if dt<20010331 then
    afs_agency_mbs =sum(bhck1702,bhck1707,bhck1717,bhck1732) ;
	label afs_agency_mbs = 'Available-for-Sale Agency MBS (fair value)' ;

   /* CMBS/RMBS - note only defined so far for 2010 */ ;
      agency_rmbs = sum(bhckg300,bhckg304,bhckg312,bhckg316,bhckg303,bhckg307,bhckg315,bhckg319) ;
      nonagency_rmbs = sum(bhckg308,bhckg320,bhckg311,bhckg323) ;
      if dt < 20110331 then do ;
          afs_agency_cmbs = . ;
          afs_nonagency_cmbs = . ;
          total_cmbs = sum(bhckg324,bhckg328,bhckg327,bhckg331) ;
          end;
      else do;
          afs_agency_cmbs = bhckk145 ;
          afs_nonagency_cmbs = bhckk153 ;
          total_cmbs = sum(bhckk142,bhckk146,bhckk150,bhckk154,bhckk145,bhckk149,bhckk153,bhckk157);
          end;
		  label agency_rmbs = 'Agency Residential MBS' ;
		  label nonagency_rmbs = 'Non-Agency Residential MBS' ;
		  label afs_agency_cmbs = 'Available-for-Sale Agency Commercial MBS' ;
		  label afs_nonagency_cmbs = 'Available-for-Sale Non-Agency Commercial MBS' ;
		  label total_cmbs = 'Total Commercial MBS' ;

                  	/*Note: in 2009, commercial mbs is broken out, but not broken out into agency and nonagency - put all of this in nonagency*/
	/*In 2011, commercial mbs broken out into agency and nonagency*/;
	if dt < 19950331 then
	  agency_mbs_afs= .;
	else if dt>=19950331 & dt <20090630 then
	agency_mbs_afs = sum( bhck1702, bhck1707, bhck1717, bhck1732);
	else if dt>=20090630 & dt<20110331 then
	agency_mbs_afs = sum(bhckg303, bhckg307, bhckg315,bhckg319, bhckg327, bhckg331) ;
	else agency_mbs_afs = sum(bhckg303, bhckg307, bhckg315,bhckg319, bhckk145, bhckk153);
	label agency_mbs_afs='Agency MBS (AFS)' ;


                  /*Note: in 2009, commercial mbs is broken out, but not broken out into agency and nonagency - put all of this in nonagency*/
	/*In 2011, commercial mbs broken out into agency and nonagency*/;
	if dt < 19950331 then
	  agency_mbs_htm= .;
	else if dt>=19950331 & dt <20090630 then
	agency_mbs_htm = sum(bhck1698, bhck1703, bhck1714, bhck1718);
	else if dt>=20090630 & dt <20110331 then
	agency_mbs_htm = sum(bhckg300, bhckg304, bhckg312, bhckg316, bhckg324, bhckg328);
	else agency_mbs_htm = sum(bhckg300, bhckg304, bhckg312, bhckg316, bhckk142, bhckk150) ;
	label agency_mbs_htm='Agency MBS (HTM)' ;

        	/*HTM (Amortized Cost) + AFS (Fair Value) + Trading Securities*/;
	if dt<19950331 then
	agency_mbs=.;
	else agency_mbs = sum(agency_mbs_trad, agency_mbs_afs, agency_mbs_htm);
	label agency_mbs='Agency MBS';

        	/*Non-agency MBS*/;

	if dt < 19950331 then
	nonagency_mbs_trad = .;
	else if dt>=19950331 & dt <20080331 then
	 nonagency_mbs_trad = bhck3536;
	else if dt>=20080331 & dt <20090630 then
	nonagency_mbs_trad=bhcm3536;
	else if dt>=20090630 & dt <20110331 then
	 nonagency_mbs_trad=sum(bhckg381,bhckg382);
	else nonagency_mbs_trad = sum(bhckg381, bhckk198) ;
	label nonagency_mbs_trad='Non-Agency MBS (trading) ' ;

        	/*HTM (Amortized Cost) + AFS (Fair Value) + Trading Securities*/;
	if dt<19950331 then
	nonagency_mbs=.;
	else nonagency_mbs = sum(nonagency_mbs_trad, nonagency_mbs_afs, afs_nag_mbs);
	label nonagency_mbs='Non-Agency MBS';


        	if dt<19950331 then
	oth_afs_sec = .;
	else oth_afs_sec=sum(afssec,-agency_afs,-afs_state,-agency_mbs_afs, -nonagency_mbs_afs);

           	if dt<19950331 then
	oth_htm_sec = .;
	else oth_htm_sec=sum(htmsec,-agency_htm,-muni_htm,-agency_mbs_htm, -htm_nag_mbs);

        	/*HTM (Amortized Cost) + AFS (Fair Value) + Trading Securities*/;

	if dt<19950331 then
	oth_sec = .;
	else oth_sec=sum(oth_trad_sec,oth_htm_sec,oth_afs_sec);
	label oth_sec='Other Securities';


	/**********************************************************
     ******* SCHEDULE HC-H: Interest Sensitivity    ***********
     *********************************************************/

            if dt < 19810630 then
                earn_ass_reprice = . ;
            else earn_ass_reprice = bhck3197;

     /**********************************************
     ******* SCHEDULE HC-M: Memoranda    ***********
     **********************************************/;

	 /*Mortgage servicing assets*/ ;
	 if dt<20010331 then
	 	srv_ass_mrt = . ;
	else srv_ass_mrt = bhck3164 ;
	label srv_ass_mrt = 'Mortgage Servicing Assets' ;

	/*Estimated Fair Value of Mortgage Servicing Assets*/;
	 if dt<20010331 then
	 	srv_ass_mrt_fv = .;
	else srv_ass_mrt_fv = bhck6438;
	label srv_ass_mrt_fv = 'Estimated Fair Value of Mortgage Servicing Assets';

	/*Purchased credit card relationships and nonmortgage servicing assets*/;
	if dt<20010331 then
		srv_ass_non_mrt = . ;
	else srv_ass_non_mrt = bhckb026 ;
	label srv_ass_non_mrt = 'Credit Card Relationships and Nonmortgage Servicing Assets' ;

        	/*Debt maturing in one year or less*/;
	if dt<19900930 then
		debt_lt1y = .;
	else debt_lt1y = bhck6555;
	label debt_lt1y = 'Debt Maturing In One Year or Less Issued to Unrelated Third Parties by Bank Subsidiaries';

	/*Debt maturing in more than one year*/;
	if dt<19900930 then
		debt_gt1y = .;
	else debt_gt1y = bhck6556;
	label debt_gt1y = 'Debt Maturing In More Than One Year Issued to Unrelated Third Parties by Bank Subsidiaries';


			if dt < 19810630 then
				com_paper = .;
            else com_paper = bhck2309 ;
        	label com_paper='Commercial Paper' ;

			if dt < 19810630 then
				obm_less1yr = .;
            else obm_less1yr = bhck2332 ;
        	label obm_less1yr='Other Borrowed Money - less than 1 year' ;

			if dt < 19810630 then
				obm_more1yr = .;
            else obm_more1yr = bhck2333 ;
        	label obm_more1yr='Other Borrowed Money - more than 1 year' ;

     /**************************************************************
     *********************   Liabilities   *************************
     **************************************************************/;

	/* Total Liabilities */;
                if dt < 19900331 then
                liab_tot = . ;
                else if dt <19970331 then liab_tot = sum(bhck2948, -bhck3000, bhck3282) ;
		else if dt <20010331 then liab_tot = sum(bhck2948, -bhck3000) ;
		else liab_tot = bhck2948 ;
        	label liab_tot ='Total Liabilities' ;

	/* Interest Bearing Liabilities*/;
	if dt<19860630 then
		bear_liab =. ;
	else if dt>=19860630 & dt<=19880331 then
		bear_liab = sum(bhck2800, bhdm6636, bhfn6636, bhck3548, bhck2309, bhck2332, bhck2910, bhck4062, bhck3282) ;
	else if dt>19880331 & dt<=19961231 then
		bear_liab = sum(bhck0278,bhck0279,bhdm6636, bhfn6636, bhck3548, bhck2309, bhck2332, bhck2333, bhck2910, bhck4062, bhck3282) ;
	else if dt>19961231 & dt<=20001231 then
		bear_liab = sum(bhck2800,bhdm6636,bhfn6636,bhck3548,bhck2309,bhck2332,bhck2333,bhck4062) ;
	else if dt>20001231 & dt<=20011231 then
		bear_liab = sum(bhck2800,bhdm6636,bhfn6636,bhck3548,bhck3190,bhck4062) ;
	else bear_liab = sum(bhdmb993,bhckb995,bhdm6636,bhfn6636,bhck3548,bhck3190,bhck4062) ;
	label bear_liab = 'Interest Bearing Liabilities' ;


	   /* Other Liabilities (consistently from the present day) */;
                if dt < 19900331 then
                liab_oth = . ;
		else if dt <20010331 then liab_oth = sum(bhck2750, bhck2920, bhck3290, bhck3293) ;
		else if dt <20060331 then liab_oth = sum(bhck2750, bhck2920) ;
		else liab_oth = bhck2750 ;
        	label liab_oth ='Other Liabilities' ;

	    /* Minority Interest in Consolidated Subsidiaries and Similar Items */;
		if dt < 19810630 then
		minint_liab = . ;
		else minint_liab = bhck3000 ;
		label minint_liab = 'Minority Interest in Consolidated Subsidiaries' ;

		/* Subordinated Notes Payable to Trusts Issuing Trust Preferred Securities */
		if dt < 20050331 then
		subnotes_trups = . ;
		else subnotes_trups = bhckc699 ;
		label subnotes_trups = 'Subordinated Notes Payable to Trusts Issuing Trust Preferred Securities' ;

       /* Deposits, Domestic & Total */;
                if dt < 19810630 then do ;
                    dom_deposit = . ;
		    	dom_deposit_ib= . ;
			dom_deposit_nib= . ;
		    for_deposit = . ;
			for_deposit_ib = . ;
			for_deposit_nib = . ;
                    tot_deposit = . ;
                end ;
                else do ;
                    dom_deposit = sum(bhdm6631,bhdm6636) ;
		    	dom_deposit_ib= bhdm6636 ;
			dom_deposit_nib= bhdm6631 ;
		    for_deposit = sum(bhfn6631,bhfn6636) ;
			for_deposit_ib = bhfn6636 ;
			for_deposit_nib = bhfn6631 ;
                    tot_deposit = sum(bhdm6631,bhdm6636,bhfn6631,bhfn6636) ;
                end ;
        	label dom_deposit='Domestic Deposits' ;
        	label for_deposit='Foreign Deposits' ;
        	label dom_deposit_ib='Interest Bearing Domestic Deposits' ;
        	label dom_deposit_nib='Non Interest Bearing Domestic Deposits' ;
        	label for_deposit_ib='Interest Bearing Foreign Deposits' ;
        	label for_deposit_nib='Non Interest Bearing Foreign Deposits' ;
        	label tot_deposit='Total Deposits' ;


                if dt < 19810630 then
                    ib_deposit=.;
                else ib_deposit = sum(dom_deposit_ib, for_deposit_ib);
                label ib_deposit='Interest-bearing Deposits' ;

                if dt < 19810630 then
                    nib_deposit=.;
                else nib_deposit = sum(dom_deposit_nib, for_deposit_nib);
                label nib_deposit='Noninterest-bearing Deposits' ;

       /* Trading Liabilities */;
        	if dt < 19940331 then
                    trad_liab = . ;
                else trad_liab = bhck3548 ;
        	label trad_liab = 'Trading Liabilities' ;

        /* Other Borrowed Money */;
            if dt < 19810630 then
                othbor_liab = . ;
            else if dt >=19810630 & dt<19970331 then
	        othbor_liab = sum(bhck2309,bhck2332,bhck2333,bhck2910) ;
            else if dt >=19970331 & dt<20010331 then
	        othbor_liab = sum(bhck2309,bhck2332,bhck2333) ;
            else othbor_liab = bhck3190 ;
            label othbor_liab = 'Other Borrowed Money Liabilities' ;

                /* Subordinated notes and debentures */
		if dt < 19850630 then
		subdebt= . ;
                else if dt>=19850630 & dt <19970331 then subdebt= sum(bhck4062, bhck3282);
		else subdebt = bhck4062;
        	label subdebt = 'Subordinated Debt and Life-Time Pref Stock' ;

             /* Federal Funds Purch, Securities Sold under Agreements to Resell */;
               if dt < 19810630 then do ;
                   ffrepo_liab = . ;
                   ffpurch = . ;
                   repo_sold = . ;
               end ;
               else if dt >= 19810630 & dt < 19880630 then do ;
                   ffrepo_liab = bhck2800 ;
                   ffpurch = . ;
                   repo_sold = . ;
               end ;
               else if dt >=19880630 & dt < 19970331 then do ;
                   ffrepo_liab = sum(bhck0278,bhck0279) ;
                   ffpurch = bhck0278 ;
                   repo_sold = bhck0279 ;
               end ;
               else if dt >=19970331 & dt < 20020331 then do ;
                   ffrepo_liab = bhck2800 ;
                   ffpurch = . ;
                   repo_sold = . ;
               end ;
               else do ;
                   ffrepo_liab = sum(bhdmb993,bhckb995) ;
                   ffpurch = bhdmb993 ;
                   repo_sold = bhckb995 ;
               end ;
               label ffrepo_liab='FF Purchsed & Securities Sold under Agreements to Repo (liability)' ;
               label ffpurch='Federal Funds purchased' ;
               label repo_sold='Securities Sold under Agreements to Repurchase' ;


     /**************************************************************
     ***********   Schedule HC-E Deposit Liabilities   *************
     **************************************************************/;
		 /*Variables Pulled As Groups of Line Items*/;
			/* All Transaction Accounts (TOTAL) Includes Both Commercial and Non-Commercial Bank 1a, 1b, 2a,2b (demand deposits, NOW ATS, and noninterest bearing) */;
				if dt < 19860630 then
				transaction_dep = . ;
				else transaction_dep=sum(bhcb2210,bhod3189,bhcb3187,bhod3187) ;
				label transaction_dep = 'All Transaction Accounts (TOTAL - commercial and non-commercial bank 1a, 1b, 2a,2b demand deposits, NOW ATS, and non-interest bearing)' ;

			/* Money Market Deposit and Other Savings Accounts (TOTAL) */ ;
				if dt < 19860630 then
				savings_dep = . ;
				else savings_dep = sum(bhcb2389,bhod2389) ;
				label savings_dep = 'Money Market Deposit and Other Savings Accounts (TOTAL)' ;

			/* Time Deposits of Less than 100k (TOTAL) */ ;
				if dt < 19860630 then
				time_lt100k_dep = . ;
				else time_lt100k_dep=sum(bhcb6648,bhod6648) ;
				label time_lt100k_dep = 'Time Deposits of Less Than 100k (TOTAL)' ;

			/* Time Deposits of More than 100k (TOTAL) */;
				if dt < 19860630 then
				time_gt100k_dep = . ;
				else time_gt100k_dep = sum(bhcb2604,bhod2604) ;
				label time_gt100k_dep = 'Time Deposits of More than 100k (TOTAL)' ;

			/* Brokered Deposits of Less than 100k (TOTAL) */;
				if dt < 19960331 then
				brokered_lt100k_dep = . ;
				else brokered_lt100k_dep=sum(bhdma243,bhdma164) ;
				label brokered_lt100k_dep = 'Brokered Deposits of Less than 100k (TOTAL)' ;

		/*Variables Pulled As Granular Line Items*/;
			/*Deposits Held in Domestic Offices of Commercial Bank Subsidiaries of the Reporting Holding Company*/ ;
				/*Noninterest-bearing balances*/ ;
				if dt < 19810630 then
				dep_dom_cb_nonintbear = . ;
				else dep_dom_cb_nonintbear = bhcb2210;
				label dep_dom_cb_nonintbear = 'Noninterest-bearing Balances in Domestic Offices of Commercial Bank Subsidiaries';
				/*Interest-bearing Demand Deposits, NOW, ATS, and Other Transaction Accounts*/;
				if dt < 19860630 then
				dep_dom_cb_now_ats_oth = . ;
				else dep_dom_cb_now_ats_oth = bhcb3187;
				label dep_dom_cb_now_ats_oth = 'Interest-bearing Demand Deposits, NOW, ATS, and Other Transaction Accts in Domestic Offices of Commercial Bank Subsidiaries';
				/*Money Market Deposit Accounts and Other Savings Accounts*/ ;
				if dt < 19810630 then
				dep_dom_cb_mmda_osa = . ;
				else dep_dom_cb_mmda_osa = bhcb2389;
				label dep_dom_cb_mmda_osa = 'Money Market Deposit and Other Savings Accts in Domestic Offices of Commercial Bank Subsidiaries';
				/*Time Deposits of Less than $100,000*/ ;
				if dt < 19860630 then
				dep_dom_cb_time_lt100k = . ;
				else dep_dom_cb_time_lt100k = bhcb6648;
				label dep_dom_cb_time_lt100k = 'Time Deposits of Less than $100,000 in Domestic Offices of Commercial Bank Subsidiaries';
				/*Time Deposits of $100,000 or More */ ;
				if dt < 19860630 then
				dep_dom_cb_time_gt100k = . ;
				else dep_dom_cb_time_gt100k = bhcb2604;
				label dep_dom_cb_time_gt100k = 'Time Deposits of $100,000 or More in Domestic Offices of Commercial Bank Subsidiaries';
			/*Deposits Held in Domestic Offices of Other Depository Institutions That Are Subsidiaries of the Reporting Holding Company*/ ;
				/*Noninterest-bearing balances*/ ;
				if dt < 19860630 then
				dep_dom_othsubs_nonintbear = . ;
				else dep_dom_othsubs_nonintbear = bhod3189;
				label dep_dom_othsubs_nonintbear = 'Noninterest-bearing Balances in Domestic Offices of Other Depository Subsidiaries';
				/*Interest-bearing Demand Deposits, NOW, ATS, and Other Transaction Accounts*/;
				if dt < 19860630 then
				dep_dom_othsubs_now_ats_oth = . ;
				else dep_dom_othsubs_now_ats_oth = bhod3187;
				label dep_dom_othsubs_now_ats_oth = 'Interest-bearing Demand Deposits, NOW, ATS, and Other Transaction Accts in Domestic Offices of Other Depository Subsidiaries';
				/*Money Market Deposit Accounts and Other Savings Accounts*/ ;
				if dt < 19860630 then
				dep_dom_othsubs_mmda_osa = . ;
				else dep_dom_othsubs_mmda_osa = bhod2389;
				label dep_dom_othsubs_mmda_osa = 'Money Market Deposit and Other Savings Accts in Domestic Offices of Other Depository Subsidiaries';
				/*Time Deposits of Less than $100,000*/ ;
				if dt < 19860630 then
				dep_dom_othsubs_time_lt100k = . ;
				else dep_dom_othsubs_time_lt100k = bhod6648;
				label dep_dom_othsubs_time_lt100k = 'Time Deposits of Less than $100,000 in Domestic Offices of Other Depository Subsidiaries';
				/*Time Deposits of $100,000 or More */ ;
				if dt < 19860630 then
				dep_dom_othsubs_time_gt100k = . ;
				else dep_dom_othsubs_time_gt100k = bhod2604;
				label dep_dom_othsubs_time_gt100k = 'Time Deposits of $100,000 or More in Domestic Offices of Other Depository Subsidiaries';
		/*Memoranda*/
				/*Brokered Deposits Less Than $100,000 With a Remaining Maturity of One Year or Less*/ ;
				if dt <19960331 then
				dep_brok_lt100k_lt1y = . ;
				else dep_brok_lt100k_lt1y = bhdma243;
				label dep_brok_lt100k_lt1y = 'Brokered Deposits Less Than $100,000 With a Remaining Maturity of One Year or Less';
				/*Brokered Deposits Less Than $100,000 With a Remaining Maturity of More Than One Year*/ ;
				if dt <19960331 then
				dep_brok_lt100k_gt1y = . ;
				else dep_brok_lt100k_gt1y = bhdma164;
				label dep_brok_lt100k_gt1y = 'Brokered Deposits Less Than $100,000 With a Remaining Maturity of More Than One Year';
				/*Time Deposits of $100,000 or More With a Remaining Maturity of One Year or Less*/ ;
				if dt <19960331 then
				dep_time_gt100k_lt1y = . ;
				else dep_time_gt100k_lt1y = bhdma242;
				label dep_time_gt100k_lt1y = 'Time Deposits of $100,000 or More With a Remaining Maturity of One Year or Less';
				/*Foreign Office Time Deposits With a Remaining Maturity of One year or Less*/ ;
				if dt <19960331 then
				dep_for_time_lt1y = . ;
				else dep_for_time_lt1y = bhdma245;
				label dep_for_time_lt1y = 'Foreign Office Time Deposits With a Remaining Maturity of One year or Less';




                /* "Unstable" and "Stable" Constructed Variables */;
                if dt < 19860630 then
                    stable_deposits = . ;
                else stable_deposits = sum(dep_dom_cb_mmda_osa, dep_dom_cb_time_1t100k, dep_dom_othsubs_mmda_osa, dep_dom_othsubs_time_1t100k) ;
                label stable_deposits = 'Domestic CB and Other Subsidiaries MMDA and Time Deposits of Less Than 100k' ;

                if dt < 19860630 then
                    unstable_deposits = . ;
                else unstable_deposits = sum(dep_dom_cb_nonintbear, dep_dom_cb_now_ats_oth, dep_dom_cb_time_gt100k, dep_dom_othsubs_nonintbear, dep_dom_othsubs_now_ats_oth, dep_dom_othsubs_time_gt100k, for_deposit_nib, for_deposit_ib) ;
                label unstable_deposits = 'Foreign Deposits and Domestic Noninterest-Bearing Balances, IBDD, and Time Deposits of 100k or More' ;


     /**************************************************************
     *********************   REGULATORY CAPITAL     ***************
     **************************************************************/;

             /* Tier 1 Risk-Based Capital*/
	 		/*Risk-Weighted Trading Assets Not Subject to Risk-Weighting*/;
			if dt<19960331 then
				rw_trad_ass_na = . ;
			else rw_trad_ass_na = bhce3545 ;
			label rw_trad_ass_na = 'Risk-Weighted Trading Assets Not Subject to Risk-Weighting' ;
	 		/*Risk-Weighted Trading Assets 0% Weighting*/;
			if dt<19960331 then
				rw_trad_ass_0 = . ;
			else rw_trad_ass_0 = bhc03545 ;
			label rw_trad_ass_0 = 'Risk-Weighted Trading Assets 0% Weighting' ;
	 		/*Risk-Weighted Trading Assets 20% Weighting*/ ;
			if dt<19960331 then
				rw_trad_ass_20 = . ;
			else rw_trad_ass_20 = bhc23545 ;
			label rw_trad_ass_20 = 'Risk-Weighted Trading Assets 20% Weighting' ;
	 		/*Risk-Weighted Trading Assets 50% Weighting*/ ;
			if dt<19960331 then
				rw_trad_ass_50 = . ;
			else rw_trad_ass_50 = bhc53545 ;
			label rw_trad_ass_50 = 'Risk-Weighted Trading Assets 50% Weighting' ;
	 		/*Risk-Weighted Trading Assets 100% Weighting*/ ;
			if dt<19960331 then
				rw_trad_ass_100 = . ;
			else rw_trad_ass_100 = bhc93545 ;
			label rw_trad_ass_100 = 'Risk-Weighted Trading Assets 100% Weighting' ;

			/*Market Risk Equivalent Assets*/ ;
			if dt<19980331 then
				mrea = . ;
			else mrea = bhck1651 ;
			label mrea = 'Market Risk Equivalent Assets' ;

             /* Average Assets and Risk Weighted Assets  */ ;
        	if dt < 19960331 then do ;
                    tier1_rbc_b1 = . ;
                    total_rbc_b1 = . ;
                    rwa_b1 = . ;
                    asset_forlev_b1 = . ;
                end ;
                else do ;
                    tier1_rbc_b1 = bhck8274 ;
                    total_rbc_b1 = bhck3792 ;
                    rwa_b1 = bhcka223 ;
                    asset_forlev_b1 = bhcka224 ;
                end ;
        	label tier1_rbc_b1 = 'Tier 1 Risk-Based Capital - Basel I' ;
        	label total_rbc_b1 = 'Total Risk-Based Capital - Basel I' ;
        	label rwa_b1 = 'Total Risk-Weighted Assets - Basel I' ;
        	label asset_forlev_b1 = 'Average Assets for Leverage Ratio - Basel I' ;

                /* Average Total Assets */;
        	if dt < 19810331 then
                    asset_avg_b1 = . ;
                else
                    asset_avg_b1 = bhck3368 ;
        	label asset_avg_b1 = 'Average Total Assets' ;

    /*TIER 1 Capital Calculation */ ;
    if dt < 19940331 then
        t1_unrealized_regafs = . ;
    else t1_unrealized_regafs = bhck8434 ;
	label t1_unrealized_regafs = 'Net Unrealized Holding Gains (Losses) on Available-for-Sale Securities' ;

    if dt < 19960331 then
        t1_unrealized_eqafs=. ;
    else t1_unrealized_eqafs=bhcka221 ;
	label t1_unrealized_eqafs = 'Net Unrealized Loss on Available-for-Sale Equity Securities' ;

    if dt < 19990331 then
        t1_accgains_cashhedges=. ;
    else t1_accgains_cashhedges=bhck4336 ;
	label t1_accgains_cashhedges = 'Accumulated Net Gains (Losses) on Cash Flow Hedges' ;

    if dt < 20010331 then
        t1_nonqual_prefstock = . ;
    else t1_nonqual_prefstock = bhckb588;
	label t1_nonqual_prefstock = 'Less: Non-Qualifying Perpetual Preferred Stock' ;

    if dt < 20010331 then
        t1_qual_noncon_minint=.;
    else if dt>=20010331 & dt<20081231 then
        t1_qual_noncon_minint=bhckb589;
    else t1_qual_noncon_minint = .;
	label t1_qual_noncon_minint = 'Qualifying Minority Interests in Consolidated Subsidiaries' ;

    if dt < 20040331 then
        t1_qual_trups=.;
    else t1_qual_trups=bhckc502;
	label t1_qual_trups = 'Qualifying Trust Preferred Securities' ;

    if dt < 20010331 then
        t1_disallowed_gwill = .;
    else t1_disallowed_gwill = bhckb590;
	label t1_disallowed_gwill = 'Less: Disallowed Goodwill and Other Disallowed Intangible Assets' ;

    if dt < 20070331 then
        t1_cumchange_fvalue = . ;
    else t1_cumchange_fvalue = bhckf264 ;
	label t1_cumchange_fvalue = 'Cumulative Change in FV of All Financial Liabilities Due to Changes in Bank Creditworthiness' ;

    if dt < 20010331 then
        t1_disallowed_service_ass=. ;
    else t1_disallowed_service_ass=bhckb591 ;
	label t1_disallowed_service_ass = 'Less: Disallowed Service Assets and Purchased Credit Card Relationships' ;

    if dt < 19940331 then
        t1_disallowed_tax_ass = . ;
    else t1_disallowed_tax_ass = bhck5610 ;
	label t1_disallowed_tax_ass = 'Deferred Tax Assets Disallowed for Regulatory Capital Purposes' ;

    if dt < 20010331 then
        t1_otherchanges = . ;
    else t1_otherchanges = bhckb592 ;
	label t1_otherchanges = 'Other Additions to (Deductions from) Tier 1 Capital' ;

    if dt < 20090331 then
        t1_qual_minint_classa = . ;
    else t1_qual_minint_classa = bhckg214 ;
	label t1_qual_minint_classa = 'Qualifying Class A Non-Controlling (Minority) Interests in Consolidated Subsidiaries' ;

    if dt < 20090331 then
        t1_qual_core_cap = . ;
    else t1_qual_core_cap = bhckg215 ;
	label t1_qual_core_cap = 'Qualifying Restricted Core Capital Elements' ;

    if dt < 20090331 then
        t1_qual_convert_sec=. ;
    else t1_qual_convert_sec=bhckg216 ;
	label t1_qual_convert_sec = 'Qualifying Mandatory Convertible Preferred Securities of Internationally Active BHCs' ;

    /*TIER 1 Common Variables*/;

	/* Perpetual Preferred Stock (Including Related Surplus) */
	if dt < 19810630 then
	t1_perprefstock = . ;
	else t1_perprefstock = bhck3283;
	label t1_perprefstock = 'Perpetual Preferred Stock (Including Related Surplus)' ;

	t1_rest_core_cap = bhckg914;

	/* Treasury Stock: In the Form of Perpetual Preferred Stock */
	if dt < 19910331 then
	t1_perpref_tstock = . ;
	else t1_perpref_tstock = bhck5483;
	label t1_perpref_tstock = 'Perpetual Preferred Treasury Stock' ;

	/* Deferred Tax Assets Variables*/;

	if dt < 20020331 then
	t1_regcap_subtot = . ;
	else t1_regcap_subtot = bhckc227 ;

	if dt < 20010331 then
	t1_netdef_taxassets = . ;
	else t1_netdef_taxassets = bhck2148 ;

	if dt < 20010331 then
	t1_netdef_taxliab = . ;
	else t1_netdef_taxliab = bhck3049 ;

             /* Tier 1 Common. Updated 8/17/2012 by Dafna at the direction of Adam Weisz */

     /* Manual edits for WFC are on-going */
    /* The historical idiosyncratic changes for BAC and C reflect accounting adjustments for issued instruments. BAC issued a common equivalent security that was accepted as tier 1 qualifying but accounted for as perpetual preferred stock on the Y-9C. The note I have for C is Citi (RSSD ID 1951350) has Other tier 1 common adjustments reported in its 10-Q related to Abu Dhabi TruPS that we need to remove from the Y-9C data. This affects 4Q09 - 2Q11. The guidance for both of these came from Emir Keye, now at FRBSF */
        /* Contact Adam Weisz for details */

            if dt = 20091231 & ID_RSSD = 1073757 then do;
                bhck3283 = bhck3283 - 19290000;
                bhck3240 = bhck3240 + 19290000;
                end ;

            if dt < 20010331 then
                tier1_comm = .;
            if ID_RSSD ne 1120754 then do;
                if dt >=20010331 & dt<20040331 then
                    tier1_comm = sum(bhck8274,-bhck3283,bhckb588,-bhckb589);
                else if dt >=20040331 & dt<20090331 then
                    tier1_comm = sum(bhck8274,-bhck3283,bhckb588,-bhckb589,-bhckc502);
                else if dt >=20090331 & dt<20090630 then
                    tier1_comm = sum(bhck8274,-bhck3283,bhckb588,-bhckg214,-bhckg215,-bhckg216);
                else if dt >=20090630 & dt<20110331 & ID_RSSD ne 2125813 then
                    tier1_comm = sum(bhck8274,-bhck3283,bhckb588,-bhckg214,-bhckg215,-bhckg216,-bhckg914);
                else if dt >=20090630 & dt<20110331 & ID_RSSD = 2125813 then
                    tier1_comm = sum(bhck8274,-bhck3283,bhckb588,-bhckg214,-bhckg215,-bhckg216);
                else if dt>=20110331 then
                    tier1_comm = sum(bhck8274,-bhck3283,bhckb588,-bhckg214,-bhckg215,-bhckg216);
                label tier1_comm = 'Tier 1 Common Equity';
             end ;
             if ID_RSSD = 1120754 then do;
                 if dt >=20010331 & dt<20040331 then
                     tier1_comm = sum(bhck8274,-bhck3283,bhckb588,-bhckb589,bhck2771);
                 else if dt >=20040331 & dt<20090331 then
                     tier1_comm = sum(bhck8274,-bhck3283,bhckb588,-bhckb589,-bhckc502,bhck2771);
                 else if dt >=20090331 & dt<20090630 then
                     tier1_comm = sum(bhck8274,-bhck3283,bhckb588,-bhckg214,-bhckg215,-bhckg216,bhck2771);
                 else if dt >=20090630 & dt<20110331 & ID_RSSD ne 2125813 then
                     tier1_comm = sum(bhck8274,-bhck3283,bhckb588,-bhckg214,-bhckg215,-bhckg216,-bhckg914,bhck2771);
                 else if dt >=20090630 & dt<20110331 & ID_RSSD = 2125813 then
                     tier1_comm = sum(bhck8274,-bhck3283,bhckb588,-bhckg214,-bhckg215,-bhckg216,bhck2771);
                 else if dt>=20110331 then
                     tier1_comm = sum(bhck8274,-bhck3283,bhckb588,-bhckg214,-bhckg215,-bhckg216,bhck2771);
                 label tier1_comm = 'Tier 1 Common Equity';

              end;
              if ID_RSSD = 1951350 & dt>=20091231 & dt<=20110630 then
                  tier1_comm = tier1_comm - 1875000;


              /* 6/6/2014: The definition of tier 1 common has changed for advanced approaches firms. So we need to pull some additional variables */;

              /* Create regulatory variables for Basel III regulations */;
              if dt < 20140331 then do ;
                    tier1_rbc_b3 = . ;
                    cet1 = . ;

                end ;
                else do ;
                    tier1_rbc_b3 = bhca8274;
                    cet1=bhcap859;

                end ;
        	label tier1_rbc_b3 = 'Tier 1 Risk-Based Capital - Basel III - BHCA8274' ;
                label cet1 = 'Common Equity Tier 1 - BHCAP859' ;



                /* We have several more variables that are defined only in Part I.B. of Schedule HC-R. They are defined below */;

                /* CET1 Components before adjustments and deductions */;
                if dt<20140331 then do;
                    t1_comm_stock_net=.;
                    t1_retain_earn=.;
                    t1_aoci=.;
                    t1_aoci_opt=.;
                    t1_cet1_minint=.;
                    t1_cet1_before_adj=.;
                    end;
                else do;
                    t1_comm_stock_net=bhcap742;
                    t1_retain_earn=bhct3247;
                    t1_aoci=bhcab530;
                    t1_aoci_opt=bhcap838;
                    t1_cet1_minint=bhcap839;
                    t1_cet1_before_adj=bhcap840;
                    end;
                label t1_comm_stock_net='Common stock plus related surplus, net of treasury stock and ESOP shares - BHCAP742';
                label t1_retain_earn='Retained Earnings - BHCT3247';
                label t1_aoci='AOCI - BHCAB530';
                label t1_aoci_opt='AOCI opt-out election-bhcap838';
                label t1_cet1_minint='CET1 Minority interest includable in CET1 capital - BHCAP839';
                label t1_cet1_before_adj='CET1 capital before adjustments and deductions - BHCAP840';

                /* CET1 Adjustments and Deductions */;
                if dt<20140331 then do;
                    dta_ol_tax_net=.;
                    dta_temp_diff=.;
                    end;
                else do;
                    dta_ol_tax_net=bhcap843;
                    dta_temp_diff=bhcap855;
                    end;
                label dta_ol_tax_net='DTAs that arise from net operating loss and tax credit carryforwards, net of any related valuation allowances and net of DTLs-BHCAP843';
                label dta_temp_diff='DTAs arising from temporary differences that could not be realized through net operating loss carrybacks - BHCAP855';

                /* Additional Tier 1 Capital */;
                if dt<20140331 then do;
                    t1_addtional_t1_cap=.;
                    end;
                else do;
                    t1_addtional_t1_cap=bhcap865;
                    end;
                label t1_addtional_t1_cap='Additional tier 1 capital - BHCAP865';

                /* Tier 2 Capital */;
                if dt<20140331 then do;
                    t2_cap=.;
                    t2_cap_aa=.;
                    end;
                else do;
                    t2_cap=bhca5311;
                    t2_cap_aa=bhcw5311;
                    end;
                label t2_cap='Tier 2 Capital - BHCA5311';
                label t2_cap_aa='Tier 2 Capital (Advanced Approaches) - BHCW5311';

                /* Total Capital */;
                if dt<20140331 then do;
                    total_rbc_b3 = . ;
                    total_rbc_b3_aa=.;
                    end;
                else do;
                    total_rbc_b3 = bhca3792;
                    total_rbc_b3_aa=bhcw3792;
                    end;
                label total_rbc_b3 = 'Total Risk-Based Capital - Basel III - BHCA3792' ;
                label total_rbc_b3_aa='Total Risk-Based Capital (Advanced Approaches) BHCW3792';



                /* Total Assets for the leverage ratio */;
                  if dt<20140331 then do;
                      asset_avg_b3=. ;
                      t1_cap_deduc=.;
                      oth_deduct_avg_ass=.;
                      asset_forlev_b3_aa = . ;
                      end;
                  else do;
                      asset_avg_b3=bhcx3368;
                      t1_cap_deduc=bhcap875;
                      oth_deduct_avg_ass=bhcab596;
                      asset_forlev_b3 = bhcaa224 ;
                      end;
                  label asset_avg_b3='Average total consolidated assets-Basel III-bhcx3368';
                  label t1_cap_deduc='Deductions from cet1 capital and addtioanl tier 1 capital-bhcap875';
                  label oth_deduct_avg_ass='Other deductions from (additions to) assets for leverage ratio purposes-bhcab596';
                  label asset_forlev_b3 = 'Average Assets for Leverage Ratio - Basel III - BHCAA224' ;


                /* Total Risk-Weighted Assets */;
                if dt<20140331 then do;
                    rwa_b3 = . ;
                    rwa_b3_aa = . ;
                    end;
                else do;
                    rwa_b3 = bhcaa223 ;
                    rwa_b3_aa = bhcwa223 ;
                    end;
                label rwa_b3 = 'Total risk-weighted assets - Basel III - BHCAA223' ;
                label rwa_b3_aa = 'Total risk-weighted (Advanced Approaches) - Basel III - BHCWA223' ;


             /* Create combined regulatory capital variables. These will be equal to the items on Part II of Schedule HC-R for advanced approaches firms in 2014Q1, and equal to itmes on Part I of Schedule HC-R for other quarters */;
                if cet1 ne . then do;
                    comm_t1 = cet1 ;
                    tier1_rbc=tier1_rbc_b3;
                    total_rbc=total_rbc_b3;
                    rwa=rwa_b3;
                    asset_forlev=asset_forlev_b3;
                    asset_avg=asset_avg_b3;
                    end;
                else do;
                    comm_t1 = tier1_comm ;
                    tier1_rbc=tier1_rbc_b1;
                    total_rbc=total_rbc_b1;
                    rwa=rwa_b1;
                    asset_forlev=asset_forlev_b1;
                    asset_avg=asset_avg_b1;
                    end;
                label comm_t1='Tier 1 Common Equity and CET1, if available';
                label tier1_rbc='Tier 1 Risk-Based Capital - Combined Basel I&III' ;
                label total_rbc='Total Risk-Based Capital - Combined Basel I&III';
                label rwa = 'Total Weighted Assets - Combined Basel I&III' ;
                label asset_forlev='Average Assets for Leverage Ratio - Combined Basel I&III';
                label asset_avg='Average total consolidated assets-bhcx3368';




      /***********************************************************
      **********  SCHEDULE HC-L: Credit Derivatives     **********
     *************************************************************/;

       /* 1. Unused Commitments */;

     if dt<19900930 then
     	uc_loans_fam=. ;
     else uc_loans_fam=bhck3814 ;
     label uc_loans_fam = 'Unused Commitments: Revolving open-end loans secured by 1-4 family residential properties' ;


     if dt<19900930 then
	uc_cc_lines=.;
     else if dt>=19900930 & dt<=20091231 then uc_cc_lines = bhck3815;
     else if dt>20091231 then uc_cc_lines = sum(bhckj455, bhckj456);
     label uc_cc_lines = 'Unused Commitments: Credit card lines';

     if dt<20100331 then
     	uc_cons_cc_lines=. ;
     else uc_cons_cc_lines=bhckj455;
     label uc_cons_cc_lines = 'Unused Commitments: Unused consumer credit card lines' ;

     if dt<20100331 then
     	uc_oth_cc_lines=.;
     else uc_oth_cc_lines=bhckj456;
     label uc_oth_cc_lines = 'Unused Commitments: Other unused credit card lines' ;

     if dt<19910331 then
     	uc_loans_sec_by_restate=. ;
     else uc_loans_sec_by_restate=bhck3816;
     label uc_loans_sec_by_restate = 'Unused commitments: Commitments to fund commercial real estate, construction, and land development loans secured by real estate' ;

     if dt<20070331 then
     	uc_res_constr_loans=. ;
     else uc_res_constr_loans=bhckf164;
     label uc_res_constr_loans = 'Unused commitments: 1-4 family residential construction loan commitments' ;

     if dt<20070331 then
     	uc_com_constr_ld_loans=.;
    else uc_com_constr_ld_loans=bhckf165;
    label uc_com_constr_ld_loans = 'Unused commitments: Commercial real estate, other construction loan, and land development loan commitments' ;

    if dt<19900930 then
       uc_loans_nsec_by_restate=. ;
    else uc_loans_nsec_by_restate=bhck6550;
    label uc_loans_nsec_by_restate = 'Unused commitments: Commitments to fund commercial real estate, construction, and land development loans NOT secured by real estate' ;

    if dt<19900930 then
       uc_sec_uwrit=. ;
    else uc_sec_uwrit=bhck3817;
    label uc_sec_uwrit = 'Unused commitments: Securities underwriting';

    if dt<20100331 then
       uc_othun_cilns=.;
    else uc_othun_cilns=bhckj457;
    label uc_othun_cilns = 'Unused commitments: Other unused commitments - commercial and industrial loans' ;

    if dt<20100331 then
       uc_othun_filns=.;
    else uc_othun_filns=bhckj458;
    label uc_othun_filns = 'Unused commitments: Other unused commitments - loans to financial institutions' ;

    if dt<20100331 then
       uc_othun_alloth=.;
    else uc_othun_alloth=bhckj459;
    label uc_othun_alloth = 'Unused commitments: Other unused commitments - all other unused commitments' ;

    if dt<19900930 then
	uc_othun_tot=.;
    else if dt>=19900930 & dt<=20091231 then uc_othun_tot=bhck3818;
    else uc_othun_tot=sum (bhckj457, bhckj458, bhckj459);
    label uc_othun_tot = 'Unused commitments: Other unused commitments';

    /* 8/26/2014 This variable was not correct. Use unused_commit instead
    if dt<19860630 then
	uc_tot=.;
    else if dt>=19860630 & dt<=19900630 then uc_tot=bhck3423;
    else if dt>19900630 then uc_tot=sum(bhck3814, bhck3815, bhck3816, bhck6550, bhck3817, bhck3818);
    label uc_tot='Unused Commitments';
    */

    if dt<19900930 then
            unused_commit=.;
        else if dt>=19900930 & dt<20100331 then
                            unused_commit=sum(bhck3814, bhck3816, bhck6550, bhck3815, bhck3818);
                        else unused_commit = sum(bhck3814, bhckj455, bhckj456, bhck3816, bhck6550, bhckj457, bhckj458, bhckj459);

                        	if dt<19810630 then
				standby_loc=.;
			else if dt>=19810630 & dt<20010331 then
				standby_loc = sum(bhck3376, bhck3377, bhck3378, bhck3411);
			else standby_loc = sum(bhck6566, bhck6570, bhck3411);

                        		if dt < 19860630 then
				sec_lent = .;
            else sec_lent = bhck3433 ;
        	label sec_lent='Security lent' ;

     /* 7. Credit Derivatives */;

     /*Notional amounts*/

    if dt<20060331 then
       cd_na_cdfsw_sold=.;
    else cd_na_cdfsw_sold=bhckc968;
    label cd_na_cdfsw_sold = 'Credit Derivatives: Notional amounts - sold credit default swaps' ;

    if dt<20060331 then
       cd_na_trswaps_sold=.;
    else cd_na_trswaps_sold =bhckc970;
    label cd_na_trswaps_sold = 'Credit Derivatives: Notional amounts - sold total return swaps' ;

    if dt<20060331 then
       cd_na_copt_sold=.;
    else cd_na_copt_sold=bhckc972;
    label cd_na_copt_sold = 'Credit Derivatives: Notional amounts - sold credit options' ;

    if dt<20060331 then
       cd_na_other_sold=.;
    else cd_na_other_sold=bhckc974;
    label cd_na_other_sold = 'Credit Derivatives: Notional amounts - sold other credit derivatives' ;

    if dt<19970331 then
	cd_na_sold=.;
    else if dt>=19970331 & dt<=20051231 then cd_na_sold=bhcka534;
    else if dt>20051231 then cd_na_sold=sum(bhckc968, bhckc970, bhckc972, bhckc974);

    if dt<20060331 then
       cd_na_cdfsw_purch=.;
    else cd_na_cdfsw_purch=bhckc969;
    label cd_na_cdfsw_purch = 'Credit Derivatives: Notional amounts - purchased credit default swaps' ;

    if dt<20060331 then
       cd_na_trswaps_purch=.;
    else cd_na_trswaps_purch=bhckc971;
    label cd_na_trswaps_purch='Credit Derivatives: Notional amounts - purchased total return swaps' ;


    if dt<20060331 then
       cd_na_copt_purch=bhckc973;
    else cd_na_copt_purch=bhckc973;
    label cd_na_copt_purch = 'Credit Derivatives: Notional amounts - purchased credit options' ;

    if dt<20060331 then
      cd_na_other_purch=.;
    else cd_na_other_purch=bhckc975;
    label cd_na_other_purch='Credit Derivatives: Notional amounts - purchased other credit derivatives' ;


    if dt<19970331 then
	cd_na_purch=.;
    else if dt>=19970331 & dt<=20051231 then cd_na_purch=bhcka535;
    else if dt>20051231 then cd_na_purch=sum(bhckc969, bhckc971, bhckc973, bhckc975);

     /* Gross Fair Values */;
     if dt<20020331 then
         cd_gfv_pos_sold = . ;
     else cd_gfv_pos_sold=bhckc219;
     label cd_gfv_pos_sold = 'Credit Derivatives: Gross positive fair value - sold protection (C219)';

     if dt<20020331 then
         cd_gfv_neg_sold = . ;
     else cd_gfv_neg_sold=bhckc220;
     label cd_gfv_neg_sold = 'Credit Derivatives: Gross negative fair value - sold protection (C220)';

     if dt<20020331 then
         cd_gfv_pos_purch = . ;
     else cd_gfv_pos_purch=bhckc221;
     label cd_gfv_pos_purch = 'Credit Derivatives: Gross positive fair value - purchased protection (C221)';

     if dt<20020331 then
         cd_gfv_neg_purch = . ;
     else cd_gfv_neg_purch=bhckc222;
     label cd_gfv_neg_purch = 'Credit Derivatives: Gross negative fair value - purchased protection (C222)';

     /*Notional amounts by regulatory capital treatments*/

     if dt<20090630 then
	cd_nrct_pos_sold=.;
     else cd_nrct_pos_sold=bhckg401;
     label cd_nrct_pos_sold = 'Credit Derivatives: Notional amounts by regulatory capital treatment - positions covered under Market Risk Rule - Sold Protection';

     if dt<20090630 then
	cd_nrct_pos_purch=.;
     else cd_nrct_pos_purch=bhckg402;
     label cd_nrct_pos_purch = 'Credit Derivatives: Notional amounts by regulatory capital treatment - positions covered under Market Risk Rule - Purchased Protection';

     if dt<20090630 then
	cd_nrct_aop_sold=.;
     else cd_nrct_aop_sold = bhckg403;
     label cd_nrct_aop_sold = 'Credit Derivatives: Notional amounts by regulatory capital treatment - All other positions - sold protection';

     if dt<20090630 then
	cd_nrct_aop_grc=.;
     else cd_nrct_aop_grc=bhckg404;
     label cd_nrct_aop_grc = 'Credit Derivatives: Notional amounts by regulatory capital treatment - purchased protection that is recognized as a guarantee for regulatory capital purposes';

     if dt<20090630 then
        cd_nrct_aop_ngrc=.;
     else cd_nrct_aop_ngrc=bhckg405;
     label cd_nrct_aop_ngrc = 'Credit Derivatives: Notional amounts by regulatory capital treatment - purchased protection that is NOT recognized as a guarantee for regulatory capital purposes';


     /*Notional amounts by remaining maturity*/

     if dt<20090630 then
	cd_nrm_scp_igd_l1=.;
     else cd_nrm_scp_igd_l1=bhckg406;
     label cd_nrm_scp_igd_l1 = 'Credit Derivatives: Notional amounts by remaining maturity - Sold Credit Investment - Investment grade - One year or less';

     if dt<20090630 then
	cd_nrm_scp_igd_1t5=.;
     else cd_nrm_scp_igd_1t5=bhckg407;
     label cd_nrm_scp_igd_1t5 = 'Credit Derivatives: Notional amounts by remaining maturity - Sold Credit Investment - Investment grade - Over one year through five years';

     if dt<20090630 then
	cd_nrm_scp_igd_o5=.;
     else cd_nrm_scp_igd_o5=bhckg408;
     label cd_nrm_scp_igd_o5 = 'Credit Derivatives: Notional amounts by remaining maturity - Sold Credit Investment - Investment grade - Over five years';

     if dt<20090630 then
	cd_nrm_scp_sigd_l1=.;
     else cd_nrm_scp_sigd_l1=bhckg409;
     label cd_nrm_scp_sigd_l1 = 'Credit Derivatives: Notional amounts by remaining maturity - Sold Credit Investment - Subinvestment grade - One year or less';

     if dt<20090630 then
	cd_nrm_scp_sigd_1t5=.;
     else cd_nrm_scp_sigd_1t5=bhckg410;
     label cd_nrm_scp_sigd_1t5 = 'Credit Derivatives: Notional amounts by remaining maturity - Sold Credit Investment - Subinvestment grade - Over one year through five years';

     if dt<20090630 then
	cd_nrm_scp_sigd_o5=.;
     else cd_nrm_scp_sigd_o5=bhckg411;
     label cd_nrm_scp_sigd_o5 = 'Credit Derivatives: Notional amounts by remaining maturity - Sold Credit Investment - Subinvestment grade - Over five years';

     if dt<20090630 then
	cd_nrm_pcp_igd_l1=.;
     else cd_nrm_pcp_igd_l1=bhckg412;
     label cd_nrm_pcp_igd_l1 = 'Credit Derivatives: Notional amounts by remaining maturity - Purchased Credit Investment - Investment grade - One year or less';

     if dt<20090630 then
	cd_nrm_pcp_igd_1t5=.;
     else cd_nrm_pcp_igd_1t5=bhckg413;
     label cd_nrm_pcp_igd_1t5 = 'Credit Derivatives: Notional amounts by remaining maturity - Purchased Credit Investment - Investment grade - Over one year through five years';

     if dt<20090630 then
	cd_nrm_pcp_igd_o5=.;
     else cd_nrm_pcp_igd_o5=bhckg414;
     label cd_nrm_pcp_igd_o5 = 'Credit Derivatives: Notional amounts by remaining maturity - Purchased Credit Investment - Investment grade - Over five years';

     if dt<20090630 then
	cd_nrm_pcp_sigd_l1=.;
     else cd_nrm_pcp_sigd_l1=bhckg415;
     label cd_nrm_pcp_sigd_l1 = 'Credit Derivatives: Notional amounts by remaining maturity - Purchased Credit Investment - Subinvestment grade - One year or less';

     if dt<20090630 then
	cd_nrm_pcp_sigd_1t5=.;
     else cd_nrm_pcp_sigd_1t5=bhckg416;
     label cd_nrm_pcp_sigd_1t5 = 'Credit Derivatives: Notional amounts by remaining maturity - Purchased Credit Investment - Subinvestment grade - Over one year through five years';

     if dt<20090630 then
	cd_nrm_pcp_sigd_o5=.;
     else cd_nrm_pcp_sigd_o5=bhckg417;
     label cd_nrm_pcp_sigd_o5 = 'Credit Derivatives: Notional amounts by remaining maturity - Purchased Credit Investment - Subinvestment grade - Over five years';


     /* 8. Spot Foreign Exchange Contracts */;
     if dt<19950331 then
         spot_fx_contract = . ;
     else spot_fx_contract = bhck8765;
     label spot_fx_contract = 'Spot foreign exchange contracts (8765)';


     /*11. Gross amounts of derivative contracts */;
     if dt<19950331 then
	 ir_gamt=.;
     else ir_gamt=sum(bhck8693, bhck8697, bhck8701, bhck8705, bhck8709, bhck8713, bhck3450);
     label ir_gamt = 'Gross amounts of interest rate contracts';

     if dt<19950331 then
	 fex_gamt=.;
     else fex_gamt=sum(bhck8694, bhck8698, bhck8702, bhck8706, bhck8710, bhck8714, bhck3826);
     label fex_gamt= 'Gross amounts of foreign exchange contracts';


     if dt<19950331 then
	 eqderiv_gamt=.;
     else eqderiv_gamt=sum(bhck8695, bhck8699, bhck8703, bhck8707, bhck8711, bhck8715, bhck8719);
     label eqderiv_gamt= 'Gross amounts of equity derivative contracts';

     if dt<19950331 then
	 com_and_oth_gamt=.;
     else com_and_oth_gamt=sum(bhck8696, bhck8700, bhck8704, bhck8708, bhck8712, bhck8716, bhck8720);
     label com_and_oth_gamt= 'Gross amounts of commodity and other contracts';

     /* 12.Total gross notional amount of derivative contracts held for trading */
     if dt<19950331 then
	tot_gna_dcht_ir=.;
     else tot_gna_dcht_ir=bhcka126;
     label tot_gna_dcht_ir = 'Total gross notional amount of derivative contracts held for trading - interest rate contracts';

     if dt<19950331 then
	tot_gna_dcht_fex=.;
     else tot_gna_dcht_fex=bhcka127;
     label tot_gna_dcht_fex = 'Total gross notional amount of derivative contracts held for trading - foreign exchange contracts';


     if dt<19950331 then
	tot_gna_dcht_eqd=.;
     else tot_gna_dcht_eqd=bhcka8723;
     label tot_gna_dcht_eqd = 'Total gross notional amount of derivative contracts held for trading - equity derivative contracts';


     if dt<19950331 then
	tot_gna_dcht_coc=.;
     else tot_gna_dcht_coc=bhcka8724;
     label tot_gna_dcht_coc = 'Total gross notional amount of derivative contracts held for trading - commodity and other contracts contracts';

     /* 13. Total gross notional amount of derivative contracts held for purposes other than trading */
     if dt<19950331 then
	tot_gna_dchot_ir=.;
     else tot_gna_dchot_ir=bhck8725;
     label tot_gna_dchot_ir = 'Total gross notional amount of derivative contracts held for purposes other than trading - interest rate contracts';

     if dt<19950331 then
	tot_gna_dchot_fex=.;
     else tot_gna_dchot_fex=bhck8726;
     label tot_gna_dchot_fex = 'Total gross notional amount of derivative contracts held for purposes other than trading - foreign exchange contracts';

     if dt<19950331 then
	tot_gna_dchot_eqd=.;
     else tot_gna_dchot_eqd=bhck8727;
     label tot_gna_dchot_eqd = 'Total gross notional amount of derivative contracts held for purposes other than trading - equity derivative contracts';

     if dt<19950331 then
	tot_gna_dchot_coc=.;
     else tot_gna_dchot_coc=bhck8728;
     label tot_gna_dchot_coc = 'Total gross notional amount of derivative contracts held for purposes other than trading - commodity and other contracts';


     /* 14. Gross fair value of derivative contracts */;

     /* Gross fair value of interest rate derivative contracts */;
     if dt<19950331 then
         ir_deriv_gfv_pos_hft = . ;
     else ir_deriv_gfv_pos_hft = bhck8733;
     label ir_deriv_gfv_pos_hft = 'Gross Positive Fair Value of interest rate contracts held for trading (8733)';

     /* Gross fair value of foreign exchange derivative contracts */;
     if dt<19950331 then
         fx_deriv_gfv_pos_hft = . ;
     else fx_deriv_gfv_pos_hft = bhck8734;
     label fx_deriv_gfv_pos_hft = 'Gross Positive Fair Value of foreign exchange contracts held for trading (8734)';

     /* Gross fair value of equity derivative contracts */;
     if dt<19950331 then
         eq_deriv_gfv_pos_hft = . ;
     else eq_deriv_gfv_pos_hft = bhck8735;
     label eq_deriv_gfv_pos_hft = 'Gross Positive Fair Value of equity derivative contracts held for trading (8735)';

     /* Gross fair value of derivative contracts */;
     if dt<19950331 then
         oth_deriv_gfv_pos_hft = . ;
     else oth_deriv_gfv_pos_hft = bhck8736;
     label oth_deriv_gfv_pos_hft = 'Gross Positive Fair Value of commodity and other contracts held for trading (8736)';

     /* negative fair value for contracts held for trading */;

     /* Gross fair value of interest rate derivative contracts */;
     if dt<19950331 then
         ir_deriv_gfv_neg_hft = . ;
     else ir_deriv_gfv_neg_hft = bhck8737;
     label ir_deriv_gfv_neg_hft = 'Gross Negative Fair Value of interest rate contracts held for trading (8737)';

     /* Gross fair value of foreign exchange derivative contracts */;
     if dt<19950331 then
         fx_deriv_gfv_neg_hft = . ;
     else fx_deriv_gfv_neg_hft = bhck8738;
     label fx_deriv_gfv_neg_hft = 'Gross Negative Fair Value of foreign exchange contracts held for trading (8738)';

     /* Gross fair value of equity derivative contracts */;
     if dt<19950331 then
         eq_deriv_gfv_neg_hft = . ;
     else eq_deriv_gfv_neg_hft = bhck8739;
     label eq_deriv_gfv_neg_hft = 'Gross Negative Fair Value of equity derivative contracts held for trading (8739)';

     /* Gross fair value of derivative contracts */;
     if dt<19950331 then
         oth_deriv_gfv_neg_hft = . ;
     else oth_deriv_gfv_neg_hft = bhck8740;
     label oth_deriv_gfv_neg_hft = 'Gross Negative Fair Value of commodity and other contracts held for trading (8740)';

     /* Not held for trading */;

     /* Gross fair value of interest rate derivative contracts */;
     if dt<19950331 then
         ir_deriv_gfv_pos_nhft = . ;
     else ir_deriv_gfv_pos_nhft = bhck8741;
     label ir_deriv_gfv_pos_nhft = 'Gross Positive Fair Value of interest rate contracts not held for trading (8741)';

     /* Gross fair value of foreign exchange derivative contracts */;
     if dt<19950331 then
         fx_deriv_gfv_pos_nhft = . ;
     else fx_deriv_gfv_pos_nhft = bhck8742;
     label fx_deriv_gfv_pos_nhft = 'Gross Positive Fair Value of foreign exchange contracts not held for trading (8742)';

     /* Gross fair value of equity derivative contracts */;
     if dt<19950331 then
         eq_deriv_gfv_pos_nhft = . ;
     else eq_deriv_gfv_pos_nhft = bhck8743;
     label eq_deriv_gfv_pos_nhft = 'Gross Positive Fair Value of equity derivative contracts not held for trading (8743)';

     /* Gross fair value of derivative contracts */;
     if dt<19950331 then
         oth_deriv_gfv_pos_nhft = . ;
     else oth_deriv_gfv_pos_nhft = bhck8744;
     label oth_deriv_gfv_pos_nhft = 'Gross Positive Fair Value of commodity and other contracts not held for trading (8744)';

     /* negative fair value not held for trading */;

     /* Gross fair value of interest rate derivative contracts */;
     if dt<19950331 then
         ir_deriv_gfv_neg_nhft = . ;
     else ir_deriv_gfv_neg_nhft = bhck8745;
     label ir_deriv_gfv_neg_nhft = 'Gross Negative Fair Value of interest rate contracts not held for trading (8737)';

     /* Gross fair value of foreign exchange derivative contracts */;
     if dt<19950331 then
         fx_deriv_gfv_neg_nhft = . ;
     else fx_deriv_gfv_neg_nhft = bhck8746;
     label fx_deriv_gfv_neg_nhft = 'Gross Negative Fair Value of foreign exchange contracts not held for trading (8738)';

     /* Gross fair value of equity derivative contracts */;
     if dt<19950331 then
         eq_deriv_gfv_neg_nhft = . ;
     else eq_deriv_gfv_neg_nhft = bhck8747;
     label eq_deriv_gfv_neg_nhft = 'Gross Negative Fair Value of equity derivative contracts not held for trading (8739)';

     /* Gross fair value of derivative contracts */;
     if dt<19950331 then
         oth_deriv_gfv_neg_nhft = . ;
     else oth_deriv_gfv_neg_nhft = bhck8748;
     label oth_deriv_gfv_neg_nhft = 'Gross Negative Fair Value of commodity and other contracts not held for trading (8740)';


     /* 15. Over-the-counter derivatives */

     /* Net current credit exposure */
     if dt<20090630 then
	ocd_ncrex_bksec=.;
     else ocd_ncrex_bksec=bhckg418;
     label ocd_ncrex_bksec = 'Over-the counter derivatives - net current credit exposure of banks and securities firms';

     if dt<20090630 then
	ocd_ncrex_mfing=.;
     else ocd_ncrex_mfing=bhckg419;
     label ocd_ncrex_mfing = 'Over-the counter derivatives - net current credit exposure of monoline financial guarantors';

     if dt<20090630 then
	ocd_ncrex_hfun=.;
     else ocd_ncrex_hfun=bhckg420;
     label ocd_ncrex_hfun = 'Over-the counter derivatives - net current credit exposure of hedge funds';

     if dt<20090630 then
	ocd_ncrex_svgov=.;
     else ocd_ncrex_svgov=bhckg421;
     label ocd_ncrex_svgov = 'Over-the counter derivatives - net current credit exposure of sovereign governments';

     if dt<20090630 then
	ocd_ncrex_calocp=.;
     else ocd_ncrex_calocp=bhckg422;
     label ocd_ncrex_calocp = 'Over-the counter derivatives - net current credit exposure of corporations and all other counterparties';

     /* Fair value of collateral */

     /* Banks and Securities Firms */
     if dt<20090630 then
	ocd_fvoc_usd_bksec=.;
     else ocd_fvoc_usd_bksec=bhckg423;
     label ocd_fvoc_usd_bksec = 'Over-the counter derivatives - fair value of collateral of cash - U.S. dollar of banks and securities firms';

     if dt<20090630 then
	ocd_fvoc_oc_bksec=.;
     else ocd_fvoc_oc_bksec=bhckg428;
     label ocd_fvoc_oc_bksec = 'Over-the counter derivatives - fair value of collateral of cash - other currencies of banks and securities firms';

     if dt<20090630 then
	ocd_fvoc_tsec_bksec=.;
     else ocd_fvoc_tsec_bksec=bhckg433;
     label ocd_fvoc_tsec_bksec = 'Over-the counter derivatives - fair value of collateral of U.S. Treasury securities of banks and securities firms';

     if dt<20090630 then
	ocd_fvoc_usgds_bksec=.;
     else ocd_fvoc_usgds_bksec=bhckg438;
     label ocd_fvoc_usgds_bksec = 'Over-the counter derivatives - fair value of collateral of U.S. Government agency and U.S. Government-sponsored agency debt securities of banks and securities firms';

     if dt<20090630 then
	ocd_fvoc_cbds_bksec=.;
     else ocd_fvoc_cbds_bksec=bhckg443;
     label ocd_fvoc_cbds_bksec = 'Over-the counter derivatives - fair value of collateral of corporate bonds of banks and securities firms';

     if dt<20090630 then
	ocd_fvoc_eqs_bksec=.;
     else ocd_fvoc_eqs_bksec=bhckg448;
     label ocd_fvoc_eqs_bksec = 'Over-the counter derivatives - fair value of collateral of equity securities of banks and securities firms';

     if dt<20090630 then
	ocd_fvoc_othc_bksec=.;
     else ocd_fvoc_othc_bksec=bhckg453;
     label ocd_fvoc_othc_bksec = 'Over-the counter derivatives - fair value of collateral of all other collateral of banks and securities firms';

     if dt<20090630 then
	ocd_fvoc_tot_bksec=.;
     else ocd_fvoc_tot_bksec=bhckg458;
     label ocd_fvoc_tot_bksec = 'Over-the counter derivatives - total fair value of collateral of banks and securities firms';

     /* Monoline Financial Guarantors */
     if dt<20090630 then
	ocd_fvoc_usd_mfing=.;
     else ocd_fvoc_usd_mfing=bhckg424;
     label ocd_fvoc_usd_mfing = 'Over-the counter derivatives - fair value of collateral of cash - U.S. dollar of monoline financial guarantors';

     if dt<20090630 then
	ocd_fvoc_oc_mfing=.;
     else ocd_fvoc_oc_mfing=bhckg429;
     label ocd_fvoc_oc_mfing = 'Over-the counter derivatives - fair value of collateral of cash - other currencies of monoline financial guarantors';

     if dt<20090630 then
	ocd_fvoc_tsec_mfing=.;
     else ocd_fvoc_tsec_mfing=bhckg434;
     label ocd_fvoc_tsec_mfing = 'Over-the counter derivatives - fair value of collateral of U.S. Treasury securities of monoline financial guarantors';

     if dt<20090630 then
	ocd_fvoc_usgds_mfing=.;
     else ocd_fvoc_usgds_mfing=bhckg439;
     label ocd_fvoc_usgds_mfing = 'Over-the counter derivatives - fair value of collateral of U.S. Government agency and U.S. Government-sponsored agency debt securities of monoline financial guarantors';

     if dt<20090630 then
	ocd_fvoc_cbds_mfing=.;
     else ocd_fvoc_cbds_mfing=bhckg444;
     label ocd_fvoc_cbds_mfing = 'Over-the counter derivatives - fair value of collateral of corporate bonds of monoline financial guarantors';

     if dt<20090630 then
	ocd_fvoc_eqs_mfing=.;
     else ocd_fvoc_eqs_mfing=bhckg449;
     label ocd_fvoc_eqs_mfing = 'Over-the counter derivatives - fair value of collateral of equity securities of monoline financial guarantors';

     if dt<20090630 then
	ocd_fvoc_othc_mfing=.;
     else ocd_fvoc_othc_mfing=bhckg454;
     label ocd_fvoc_othc_mfing = 'Over-the counter derivatives - fair value of collateral of all other collateral of monoline financial guarantors';

     if dt<20090630 then
	ocd_fvoc_tot_mfing=.;
     else ocd_fvoc_tot_mfing=bhckg459;
     label ocd_fvoc_tot_mfing = 'Over-the counter derivatives - total fair value of collateral of monoline financial guarantors';

     /* Hedge Funds */
     if dt<20090630 then
	ocd_fvoc_usd_hun=.;
     else ocd_fvoc_usd_hun=bhckg425;
     label ocd_fvoc_usd_hun = 'Over-the counter derivatives - fair value of collateral of cash - U.S. dollar of hedge funds';

     if dt<20090630 then
	ocd_fvoc_oc_hun=.;
     else ocd_fvoc_oc_hun=bhckg430;
     label ocd_fvoc_oc_hun = 'Over-the counter derivatives - fair value of collateral of cash - other currencies of hedge funds';

     if dt<20090630 then
	ocd_fvoc_tsec_hun=.;
     else ocd_fvoc_tsec_hun=bhckg435;
     label ocd_fvoc_tsec_hun = 'Over-the counter derivatives - fair value of collateral of U.S. Treasury securities of hedge funds';

     if dt<20090630 then
	ocd_fvoc_usgds_hun=.;
     else ocd_fvoc_usgds_hun=bhckg440;
     label ocd_fvoc_usgds_hun = 'Over-the counter derivatives - fair value of collateral of U.S. Government agency and U.S. Government-sponsored agency debt securities of hedge funds';

     if dt<20090630 then
	ocd_fvoc_cbds_hun=.;
     else ocd_fvoc_cbds_hun=bhckg445;
     label ocd_fvoc_cbds_hun = 'Over-the counter derivatives - fair value of collateral of corporate bonds of hedge funds';

     if dt<20090630 then
	ocd_fvoc_eqs_hun=.;
     else ocd_fvoc_eqs_hun=bhckg450;
     label ocd_fvoc_eqs_hun = 'Over-the counter derivatives - fair value of collateral of equity securities of hedge funds';

     if dt<20090630 then
	ocd_fvoc_othc_hun=.;
     else ocd_fvoc_othc_hun=bhckg455;
     label ocd_fvoc_othc_hun = 'Over-the counter derivatives - fair value of collateral of all other collateral of hedge funds';

     if dt<20090630 then
	ocd_fvoc_tot_hun=.;
     else ocd_fvoc_tot_hun=bhckg460;
     label ocd_fvoc_tot_hun = 'Over-the counter derivatives - total fair value of collateral of hedge funds';


     /* Sovereign Governments */
     if dt<20090630 then
	ocd_fvoc_usd_svgov=.;
     else ocd_fvoc_usd_svgov=bhckg426;
     label ocd_fvoc_usd_svgov = 'Over-the counter derivatives - fair value of collateral of cash - U.S. dollar of sovereign governments';

     if dt<20090630 then
	ocd_fvoc_oc_svgov=.;
     else ocd_fvoc_oc_svgov=bhckg431;
     label ocd_fvoc_oc_svgov = 'Over-the counter derivatives - fair value of collateral of cash - other currencies of sovereign governments';

     if dt<20090630 then
	ocd_fvoc_tsec_svgov=.;
     else ocd_fvoc_tsec_svgov=bhckg436;
     label ocd_fvoc_tsec_svgov = 'Over-the counter derivatives - fair value of collateral of U.S. Treasury securities of sovereign governments';

     if dt<20090630 then
	ocd_fvoc_usgds_svgov=.;
     else ocd_fvoc_usgds_svgov=bhckg441;
     label ocd_fvoc_usgds_svgov = 'Over-the counter derivatives - fair value of collateral of U.S. Government agency and U.S. Government-sponsored agency debt securities of sovereign governments';

     if dt<20090630 then
	ocd_fvoc_cbds_svgov=.;
     else ocd_fvoc_cbds_svgov=bhckg446;
     label ocd_fvoc_cbds_svgov = 'Over-the counter derivatives - fair value of collateral of corporate bonds of sovereign governments';

     if dt<20090630 then
	ocd_fvoc_eqs_svgov=.;
     else ocd_fvoc_eqs_svgov=bhckg451;
     label ocd_fvoc_eqs_svgov = 'Over-the counter derivatives - fair value of collateral of equity securities of sovereign governments';

     if dt<20090630 then
	ocd_fvoc_othc_svgov=.;
     else ocd_fvoc_othc_svgov=bhckg456;
     label ocd_fvoc_othc_svgov = 'Over-the counter derivatives - fair value of collateral of all other collateral of sovereign governments';

     if dt<20090630 then
	ocd_fvoc_tot_svgov=.;
     else ocd_fvoc_tot_svgov=bhckg461;
     label ocd_fvoc_tot_svgov = 'Over-the counter derivatives - total fair value of collateral of sovereign governments';

     /* Corporations and all other counterparties */
     if dt<20090630 then
	ocd_fvoc_usd_calocp=.;
     else ocd_fvoc_usd_calocp=bhckg427;
     label ocd_fvoc_usd_calocp = 'Over-the counter derivatives - fair value of collateral of cash - U.S. dollar of corporations and all other counterparties';

     if dt<20090630 then
	ocd_fvoc_oc_calocp=.;
     else ocd_fvoc_oc_calocp=bhckg432;
     label ocd_fvoc_oc_calocp = 'Over-the counter derivatives - fair value of collateral of cash - other currencies of corporations and all other counterparties';

     if dt<20090630 then
	ocd_fvoc_tsec_calocp=.;
     else ocd_fvoc_tsec_calocp=bhckg437;
     label ocd_fvoc_tsec_calocp = 'Over-the counter derivatives - fair value of collateral of U.S. Treasury securities of corporations and all other counterparties';

     if dt<20090630 then
	ocd_fvoc_usgds_calocp=.;
     else ocd_fvoc_usgds_calocp=bhckg442;
     label ocd_fvoc_usgds_calocp = 'Over-the counter derivatives - fair value of collateral of U.S. Government agency and U.S. Government-sponsored agency debt securities of corporations and all other counterparties';

     if dt<20090630 then
	ocd_fvoc_cbds_calocp=.;
     else ocd_fvoc_cbds_calocp=bhckg447;
     label ocd_fvoc_cbds_calocp = 'Over-the counter derivatives - fair value of collateral of corporate bonds of corporations and all other counterparties';

     if dt<20090630 then
	ocd_fvoc_eqs_calocp=.;
     else ocd_fvoc_eqs_calocp=bhckg452;
     label ocd_fvoc_eqs_calocp = 'Over-the counter derivatives - fair value of collateral of equity securities of corporations and all other counterparties';

     if dt<20090630 then
	ocd_fvoc_othc_calocp=.;
     else ocd_fvoc_othc_calocp=bhckg457;
     label ocd_fvoc_othc_calocp = 'Over-the counter derivatives - fair value of collateral of all other collateral of corporations and all other counterparties';

     if dt<20090630 then
	ocd_fvoc_tot_calocp=.;
     else ocd_fvoc_tot_calocp=bhckg462;
     label ocd_fvoc_tot_calocp = 'Over-the counter derivatives - total fair value of collateral of corporations and all other counterparties';


      /********************************************************************
      **********  SCHEDULE HC-N: PAST DUE & NONACCRUAL Loans     **********
     *********************************************************************/;

	/* Total Loans & Leases - 30-89 Days Past Due */
	/* Note: beginning in 199003 BHCs begins reporting delinquencies, past-due, and non-accruals
	for "debt securites and other assets".  These figures need to be subtracted from the total
	to arrive at these concepts for loans and leases, and for consistency with Call Report
	definition */;
        if dt >= 19850630 & dt < 19900930 then do ;
            pdl_tot_3089 = . ;
            pdl_tot_90 = bhck1407 ;
            pdl_tot_non = bhck1403 ;
        end ;
        else do ;
            pdl_tot_3089 = sum(bhck5524,-bhck3505) ;
            pdl_tot_90 = sum(bhck5525,-bhck3506) ;
            pdl_tot_non = sum(bhck5526,-bhck3507) ;
        end ;

        npl_tot = sum(pdl_tot_90,pdl_tot_non) ;

        label pdl_tot_3089='Total Loans & Leases -- 30-89 Days Past Due' ;
		label pdl_tot_90='Total Loans & Leases -- 90+ Days Past Due' ;
		label pdl_tot_non='Total Loans & Leases -- Nonaccrual' ;
		label npl_tot='Total Non-Performing Loans' ;

	/* Delinquent Real Estate Loans */ ;
	if dt < 19860630 then do ;
            pdl_re_3089 = . ;
            pdl_re_90 = . ;
            pdl_re_non = . ;
        end ;
        else if dt >=19860630 & dt < 19900930 then do ;
            pdl_re_3089 = . ;
            pdl_re_90 = bhck1422 ;
            pdl_re_non = bhck1423 ;
        end ;
        else if dt >= 19900930 & dt < 20010331 then do ;
            pdl_re_3089 = bhck1421 ;
            pdl_re_90 = bhck1422 ;
            pdl_re_non = bhck1423 ;
        end ;
        else if dt >= 20010331 & dt < 20020331 then do ;
            pdl_re_3089 = sum(bhck2759,bhck3493,bhck5398,bhck5401,bhck3499,bhck3502,bhckb572) ;
            pdl_re_90 = sum(bhck2769,bhck3494,bhck5399,bhck5402,bhck3500,bhck3503,bhckb573) ;
            pdl_re_non = sum(bhck3492,bhck3495,bhck5400,bhck5403,bhck3501,bhck3504,bhckb574) ;
        end ;
        else if dt >=20020331 & dt<=20071231 then do ;
            pdl_re_3089 = sum(bhck2759,bhck3493,bhck5398,bhckc236,bhckc238,bhck3499,bhck3502,bhckb572) ;
            pdl_re_90 = sum(bhck2769,bhck3494,bhck5399,bhckc237,bhckc239,bhck3500,bhck3503,bhckb573) ;
            pdl_re_non = sum(bhck3492,bhck3495,bhck5400,bhckc229,bhckc230,bhck3501,bhck3504,bhckb574) ;
        end ;
        else if dt >=20080331 then do ;
            pdl_re_3089 = sum(bhckf172,bhckf173,bhck3493,bhck5398,bhckc236,bhckc238,bhck3499,bhckf178,bhckf179,bhckb572) ;
            pdl_re_90 = sum(bhckf174,bhckf175,bhck3494,bhck5399,bhckc237,bhckc239,bhck3500,bhckf180,bhckf181,bhckb573) ;
            pdl_re_non = sum(bhckf176,bhckf177,bhck3495,bhck5400,bhckc229,bhckc230,bhck3501,bhckf182,bhckf183,bhckb574) ;
        end ;

        npl_re = sum(pdl_re_90,pdl_re_non) ;

	label pdl_re_3089='Real Estate Loans -- 30-89 Days Past Due' ;
	label pdl_re_90='Real Estate Loans -- 90 + Days Past Due' ;
	label pdl_re_non='Real Estate Loans -- Nonaccrual' ;
	label npl_re='Non-Performing Real Estate Loans' ;

        	/* Delinquent Residential Real Estate */;
        	if dt < 19910331 then do ;
                    pdl_rre_3089 = . ;
                    pdl_rre_90 = . ;
                    pdl_rre_non = . ;
                    pdl_heloc_3089 = . ;
                    pdl_heloc_90 = . ;
                    pdl_heloc_non = . ;
                    pdl_closedlien_3089 = . ;
                    pdl_closedlien_90 = . ;
                    pdl_closedlien_non = . ;
                    pdl_firstlien_3089 = . ;
                    pdl_firstlien_90 = . ;
                    pdl_firstlien_non = . ;
                    pdl_jrlien_3089 = . ;
                    pdl_jrlien_90 = . ;
                    pdl_jrlien_non = . ;

                end ;
                else if dt >= 19910331 & dt < 20020331 then do ;
                    pdl_rre_3089 = sum(bhck5398,bhck5401) ;
                    pdl_rre_90 = sum(bhck5399,bhck5402) ;
                    pdl_rre_non = sum(bhck5400,bhck5403) ;
                    pdl_heloc_3089 = bhck5398 ;
                    pdl_heloc_90 = bhck5399 ;
                    pdl_heloc_non = bhck5400 ;
                    pdl_closedlien_3089 = bhck5401 ;
                    pdl_closedlien_90 = bhck5402 ;
                    pdl_closedlien_non = bhck5403 ;
                    pdl_firstlien_3089 = . ;
                    pdl_firstlien_90 = . ;
                    pdl_firstlien_non = . ;
                    pdl_jrlien_3089 = . ;
                    pdl_jrlien_90 = . ;
                    pdl_jrlien_non = . ;
                end ;
                else do ;
                    pdl_rre_3089 = sum(bhck5398,bhckc236,bhckc238) ;
                    pdl_rre_90 = sum(bhck5399,bhckc237,bhckc239) ;
        	    pdl_rre_non = sum(bhck5400,bhckc229,bhckc230) ;
                    pdl_heloc_3089 = bhck5398 ;
                    pdl_heloc_90 = bhck5399 ;
                    pdl_heloc_non = bhck5400 ;
                    pdl_closedlien_3089 = sum(bhckc236,bhckc238) ;
                    pdl_closedlien_90 = sum(bhckc237,bhckc239) ;
                    pdl_closedlien_non = sum(bhckc229,bhckc230) ;
                    pdl_firstlien_3089 = bhckc236 ;
                    pdl_firstlien_90 = bhckc237 ;
                    pdl_firstlien_non = bhckc229 ;
                    pdl_jrlien_3089 = bhckc238 ;
                    pdl_jrlien_90 = bhckc239 ;
                    pdl_jrlien_non = bhckc230 ;
        	end ;

        	npl_rre = sum(pdl_rre_90,pdl_rre_non) ;
        	npl_heloc = sum(pdl_heloc_90,pdl_heloc_non) ;
        	npl_closedlien = sum(pdl_closedlien_90,pdl_closedlien_non) ;
        	npl_firstlien = sum(pdl_firstlien_90,pdl_firstlien_non) ;
        	npl_jrlien = sum(pdl_jrlien_90,pdl_jrlien_non) ;

        	label pdl_rre_3089='Residential Real Estate -- 30-89 Days Past Due' ;
         	label pdl_rre_90='Residential Real Estate -- 90+ Days Past Due' ;
        	label pdl_rre_non='Residential Real Estate -- Nonaccrual' ;
        	label npl_rre='Non-Performing Residential Real Estate' ;
        	label pdl_heloc_3089='1-4 Family HELOCs -- 30-89 Days Past Due' ;
         	label pdl_heloc_90='1-4 Family HELOCs -- 90+ Days Past Due' ;
        	label pdl_heloc_non='1-4 Family HELOCs -- Nonaccrual' ;
        	label npl_heloc='Non-Performing 1-4 Family HELOCs' ;
        	label pdl_closedlien_3089='1-4 Family Closed Lien Mortgages -- 30-89 Days Past Due' ;
         	label pdl_closedlien_90='1-4 Family Closed Lien Mortgages -- 90+ Days Past Due' ;
        	label pdl_closedlien_non='1-4 Family Closed Lien Mortgages -- Nonaccrual' ;
        	label npl_closedlien='Non-Performing 1-4 Family Closed Lien Mortgages' ;
        	label pdl_firstlien_3089='1-4 Family 1st Lien Mortgages -- 30-89 Days Past Due' ;
                label pdl_firstlien_90='1-4 Family first Lien Mortgages -- 90+ Days Past Due' ;
        	label pdl_firstlien_non='1-4 Family 1st Lien Mortgages -- Nonaccrual' ;
        	label npl_firstlien='Non-Performing 1-4 Family 1st Lien Mortgages' ;
        	label pdl_jrlien_3089='1-4 Family Jr Lien Mortgages -- 30-89 Days Past Due' ;
         	label pdl_jrlien_90='1-4 Family Jr Lien Mortgages -- 90+ Days Past Due' ;
        	label pdl_jrlien_non='1-4 Family Jr Lien Mortgages -- Nonaccrual' ;
        	label npl_jrlien='Non-Performing 1-4 Family Jr Lien Mortgages' ;

                /*Delinquent Construction Loans*/
                if dt<1990930 then do ;
                    pdl_const_3089 = . ;
                    pdl_const_90 = . ;
                    pdl_const_non = . ;
                    pdl_resconst_3089 = . ;
                    pdl_resconst_90 = . ;
                    pdl_resconst_non = . ;
                    pdl_crelandconst_3089 = . ;
                    pdl_crelandconst_90 = . ;
                    pdl_crelandconst_non = . ;
                    end;
                if dt >= 19900930 & dt < 20070331 then do ;
                    pdl_const_3089 = bhck2759 ;
                    pdl_const_90 = bhck2769 ;
                    pdl_const_non = bhck3492  ;
                    pdl_resconst_3089 = . ;
                    pdl_resconst_90 = . ;
                    pdl_resconst_non = . ;
                    pdl_crelandconst_3089 = . ;
                    pdl_crelandconst_90 = . ;
                    pdl_crelandconst_non = . ;
                    end;
                if dt >= 20070331 then do ;
                    pdl_const_3089 = sum(bhckf172,bhckf173) ;
                    pdl_const_90 = sum(bhckf174,bhckf175) ;
                    pdl_const_non = sum(bhckf176,bhckf177) ;
                    pdl_resconst_3089 = bhckf172 ;
                    pdl_resconst_90 = bhckf174 ;
                    pdl_resconst_non = bhckf176 ;
                    pdl_crelandconst_3089 = bhckf173 ;
                    pdl_crelandconst_90 = bhckf175 ;
                    pdl_crelandconst_non = bhckf177 ;
                    end ;
               npl_const = sum(pdl_const_90,pdl_const_non) ;
               npl_resconst = sum(pdl_resconst_90,pdl_resconst_non) ;
               npl_crelandconst = sum(pdl_crelandconst_90,pdl_crelandconst_non) ;

               label pdl_const_3089 = 'Construction loans -- 30-89 days past due' ;
               label pdl_const_90 = 'Construction loans -- 90+ days past due' ;
               label pdl_const_non = 'Construction loans -- Nonaccrual' ;
               label npl_const = 'Non-Performing construction loans' ;
               label pdl_resconst_3089 = '1-4 Residential Construction loans -- 30-89 days past due' ;
               label pdl_resconst_90 = '1-4 Residential Construction loans -- 90+ days past due' ;
               label pdl_resconst_non = '1-4 Residential Construction loans -- Nonaccrual' ;
               label npl_resconst = 'Non-Performing 1-4 Residential Construction loans' ;
               label pdl_crelandconst_3089 = 'Other Construction and Land loans -- 30-89 days past due' ;
               label pdl_crelandconst_90 = 'Other Construction and Land loans -- 90+ days past due' ;
               label pdl_crelandconst_non = 'Other Construction and Land loans -- Nonaccrual' ;
               label npl_crelandconst = 'Non-Performing Other Construction and Land loans' ;

               /* Delinquent Multifamly Mortgages */
                if dt < 19900930 then do ;
                    pdl_multi_3089 = . ;
                    pdl_multi_90 = . ;
                    pdl_multi_non = . ;
                end;
                if dt >=19900930 then do ;
                    pdl_multi_3089 = bhck3499 ;
                    pdl_multi_90 = bhck3500 ;
                    pdl_multi_non = bhck3501 ;
                    end ;

               npl_multi = sum(pdl_multi_90,pdl_multi_non) ;

               label pdl_multi_3089='Multifamily Real Estate Loans -- 30-89 days past due' ;
               label pdl_multi_90='Multifamily Real Estate Loans -- 90 days past due' ;
               label pdl_multi_non='Multifamily Real Estate Loans -- Nonaccrual' ;
               label npl_multi='Non-Performing Multifamily Real Estate Loans' ;


        	/* Delinquent Nonfarm, Non-Residential Commercial Mortgages */
        	if dt < 19900930 then do ;
                    pdl_nfnr_3089 = . ;
                    pdl_nfnr_90 = . ;
                    pdl_nfnr_non = . ;
                    pdl_ownoccnfnr_3089=. ;
                    pdl_ownoccnfnr_90=. ;
                    pdl_ownoccnfnr_non=. ;
                    pdl_othernfnr_3089=. ;
                    pdl_othernfnr_90=. ;
                    pdl_othernfnr_non=. ;
                end ;
                else if dt >=19900930 & dt <20070331 then do ;
                    pdl_nfnr_3089 = bhck3502 ;
                    pdl_nfnr_90 = bhck3503 ;
        	    pdl_nfnr_non = bhck3504 ;
                    pdl_ownoccnfnr_3089=. ;
                    pdl_ownoccnfnr_90=. ;
                    pdl_ownoccnfnr_non=. ;
                    pdl_othernfnr_3089=. ;
                    pdl_othernfnr_90=. ;
                    pdl_othernfnr_non=. ;
        	end ;
                else if dt >= 20070331 then do;
                    pdl_nfnr_3089 = sum(bhckf178,bhckf179) ;
                    pdl_ownoccnfnr_3089 = bhckf178;
                    pdl_othernfnr_3089 = bhckf179;
                    pdl_nfnr_90 = sum(bhckf180,bhckf181);
                    pdl_ownoccnfnr_90 = bhckf180;
                    pdl_othernfnr_90 = bhckf181;
        	   		pdl_nfnr_non = sum(bhckf182,bhckf183) ;
                    pdl_ownoccnfnr_non = bhckf182;
                    pdl_othernfnr_non = bhckf183;
                end;

        	npl_nfnr = sum(pdl_nfnr_90,pdl_nfnr_non) ;
        	npl_ownoccnfnr = sum(pdl_ownoccnfnr_90,pdl_ownoccnfnr_non) ;
        	npl_othernfnr = sum(pdl_othernfnr_90,pdl_othernfnr_non) ;

        	label pdl_nfnr_3089='Commercial Mortgages -- 30-89 Days Past Due' ;
         	label pdl_nfnr_90='Commercial Mortgages -- 90+ Days Past Due' ;
        	label pdl_nfnr_non='Commercial Mortgages -- Nonaccrual' ;
        	label npl_nfnr='Non-Performing Commercial Mortgages' ;
            label pdl_ownoccnfnr_3089='Owner-Occupied Nonfarm, Nonresidential Real Estate Loans -- 30-89 days past due' ;
            label pdl_ownoccnfnr_90='Owner-Occupied Nonfarm, Nonresidential Real Estate Loans -- 90 days past due' ;
            label pdl_ownoccnfnr_non='Owner-Occupied Nonfarm, Nonresidential Real Estate Loans -- Nonaccrual' ;
            label npl_ownoccnfnr='Non-Performing Owner-Occupied Nonfarm, Nonresidential Real Estate Loans' ;               label pdl_othernfnr_3089='Nonfarm, Other Nonresidential Real Estate Loans -- 30-89 days past due' ;
            label pdl_othernfnr_90='Nonfarm, Other Nonresidential Real Estate Loans -- 90 days past due' ;
            label pdl_othernfnr_non='Nonfarm, Other Nonresidential Real Estate Loans -- Nonaccrual' ;
            label npl_othernfnr='Non-Performing Other Nonfarm, Nonresidential Real Estate Loans' ;

             /*Total Delinquent and Nonaccrual Commercial Real Estate Loans*/
               pdl_cre_3089 = sum(pdl_const_3089,pdl_multi_3089,pdl_nfnr_3089) ;
               pdl_cre_90 = sum(pdl_const_90,pdl_multi_90,pdl_nfnr_90) ;
               pdl_cre_non = sum(pdl_const_non,pdl_multi_non,pdl_nfnr_non) ;

               npl_cre = sum(pdl_cre_90,pdl_cre_non) ;

          	label pdl_cre_3089='Commercial Real Estate Loans -- 30-89 Days Past Due' ;
	       	label pdl_cre_90='Commercial Real Estate Loans -- 90+ Days Past Due' ;
	       	label pdl_cre_non='Commercial Real Estate Loans -- Nonaccrual' ;
	       	label npl_cre='Non-Performing Commercial Real Estate Loans' ;

       /* Delinquent Loans to Depsoitory Institutions */; ;
	if dt < 19910331 then do ;
            pdl_dep_3089 = . ;
            pdl_dep_90 = . ;
            pdl_dep_non = . ;
        end ;
        else do ;
            pdl_dep_3089 = sum(bhck5377,bhck5380) ;
            pdl_dep_90 = sum(bhck5378,bhck5381) ;
            pdl_dep_non = sum(bhck5379,bhck5382) ;
        end ;

        npl_dep = sum(pdl_dep_90,pdl_dep_non) ;

		label pdl_dep_3089='Loans to Depository Institutions -- 30-89 Days Past Due' ;
		label pdl_dep_90='Loans to Depository Institutions -- 90+ Days Past Due' ;
		label pdl_dep_non='Loans to Depository Institutions -- Nonaccrual' ;
        label npl_dep='Non-Performing Loans to Depository Institutions' ;

	/* Delinquent Loans to Finance Agricultural Production */;
	if dt < 19900930 then do ;
            pdl_agr_3089 = . ;
            pdl_agr_90 = . ;
            pdl_agr_non = . ;
        end ;
        else do ;
            pdl_agr_3089 = bhck1594 ;
            pdl_agr_90 = bhck1597 ;
            pdl_agr_non = bhck1583 ;
        end ;

        npl_agr = sum(pdl_agr_90,pdl_agr_non) ;

		label pdl_agr_3089='Loans to Finance Agricultural Production -- 30-89 Days Past Due' ;
		label pdl_agr_90='Loans to Finance Agricultural Production - 90 + Days Past Due' ;
		label pdl_agr_non='Loans to Finance Agricultural Production  - Nonaccrual' ;
        label npl_agr='Non-Performing Loans to Finance Agricultural Production' ;

	/* Delinquent C&I Loans */;
	if dt < 19900930 then do ;
            pdl_ci_3089 = . ;
            pdl_ci_90 = . ;
            pdl_ci_non = . ;
        end ;
        else do ;
            pdl_ci_3089 = bhck1606 ;
            pdl_ci_90 = bhck1607 ;
            pdl_ci_non = bhck1608 ;
        end ;

        npl_ci = sum(pdl_ci_90,pdl_ci_non) ;

	label pdl_ci_3089='C&I Loans -- 30-89 Days Past Due' ;
	label pdl_ci_90='C&I Loans -- 90 + Days Past Due' ;
	label pdl_ci_non='C&I Loans -- Nonaccrual' ;
	label npl_ci='Non-Performing C&I Loans' ;

	/* Delinquent Consumer Loans */ ;
	if dt < 19900930 then do ;
            pdl_cons_3089 = . ;
            pdl_cons_90 = . ;
            pdl_cons_non = . ;
        end ;
        else if dt >= 19900930 & dt < 19910331 then do ;
            pdl_cons_3089 = bhck1978 ;
            pdl_cons_90 = bhck1979 ;
            pdl_cons_non = bhck1981 ;
        end ;
        else if dt >= 19910331 & dt < 20010331 then do ;
            pdl_cons_3089 = sum(bhck5383,bhck5386) ;
            pdl_cons_90 = sum(bhck5384,bhck5387) ;
            pdl_cons_non = sum(bhck5385,bhck5388) ;
        end ;
        else if dt < 20110331 then do ;
            pdl_cons_3089 = sum(bhckb575,bhckb578) ;
            pdl_cons_90 = sum(bhckb576,bhckb579) ;
            pdl_cons_non = sum(bhckb577,bhckb580) ;
        end ;
        else do ;
            pdl_cons_3089 = sum(bhckb575,bhckk213,bhckk216) ;
            pdl_cons_90 = sum(bhckb576,bhckk214,bhckk217) ;
            pdl_cons_non = sum(bhckb577,bhckk218,bhckk215) ;
        end;

        npl_cons = sum(pdl_cons_90,pdl_cons_non) ;

	label pdl_cons_3089='Consumer Loans -- 30-89 Days Past Due' ;
	label pdl_cons_90='Consumer Loans -- 90 + Days Past Due' ;
	label pdl_cons_non='Consumer Loans -- Nonaccrual' ;
        label npl_cons='Non-Performing Consumer Loans' ;

        	/* Delinquent Credit Card Loans & Other Consumer Loans */;
        	if dt < 19910331 then do ;
                    pdl_cc_3089 = . ;
                    pdl_cc_90 = . ;
                    pdl_cc_non = . ;
                    pdl_othcons_3089 = . ;
                    pdl_othcons_90 = . ;
                    pdl_othcons_non = . ;
                end ;
                else if dt >= 19910331 & dt < 20010331 then do ;
                    pdl_cc_3089 = bhck5383 ;
                    pdl_cc_90 = bhck5384 ;
                    pdl_cc_non = bhck5385 ;
                    pdl_othcons_3089 = bhck5386 ;
                    pdl_othcons_90 = bhck5387 ;
                    pdl_othcons_non = bhck5388 ;
                end ;
                else if dt<20110331 then do ;
                    pdl_cc_3089 = bhckb575 ;
                    pdl_cc_90 = bhckb576 ;
                    pdl_cc_non = bhckb577 ;
                    pdl_othcons_3089 = bhckb578 ;
                    pdl_othcons_90 = bhckb579 ;
                    pdl_othcons_non = bhckb580 ;
                end ;
                else do ;
                    pdl_cc_3089 = bhckb575 ;
                    pdl_cc_90 = bhckb576 ;
                    pdl_cc_non = bhckb577 ;
                    pdl_othcons_3089 = sum(bhckk213,bhckk216);
                    pdl_othcons_90 = sum(bhckk214,bhckk217) ;
                    pdl_othcons_non = sum(bhckk215,bhckk218) ;
                end ;

                npl_cc = sum(pdl_cc_90,pdl_cc_non) ;
                npl_othcons = sum(pdl_othcons_90,pdl_othcons_non) ;

        		label pdl_cc_3089='Credit Card Loans -- 30-89 Days Past Due' ;
        		label pdl_cc_90='Credit Card Loans -- 90+ Days Past Due' ;
        		label pdl_cc_non='Credit Card Loans -- Nonaccrual' ;
        		label pdl_othcons_3089='Other Consumer Loans -- 30-89 Days Past Due' ;
        		label pdl_othcons_90='Other Consumer Loans -- 90+ Days Past Due' ;
        		label pdl_othcons_non='Other Consumer Loans -- Nonaccrual' ;
                label npl_cc='Non-Perfoming Credit Card Loans' ;
                label npl_othcons='Non-Perfoming Other Consumer Loans' ;

	/* Delinquent Loans to Foreign Governments */;
	if dt < 19910331 then do ;
            pdl_fgovt_3089 = . ;
            pdl_fgovt_90 = . ;
            pdl_fgovt_non = . ;
        end ;
        else do ;
            pdl_fgovt_3089 = bhck5389 ;
            pdl_fgovt_90 = bhck5390 ;
            pdl_fgovt_non = bhck5391 ;
        end ;

        npl_fgovt = sum(pdl_fgovt_90,pdl_fgovt_non) ;

		label pdl_fgovt_3089='Loans to Foreign Governments -- 30-89 Days Past Due' ;
		label pdl_fgovt_90='Loans to Foreign Governments -- 90 + Days Past Due' ;
		label pdl_fgovt_non='Loans to Foreign Governments -- Nonaccrual' ;
        label npl_fgovt='Non-Performing Loans to Foreign Governments' ;

	/* Delinquent Lease Financing Receivables */;
	if dt < 19900930 then do ;
            pdl_lease_3089 = . ;
            pdl_lease_90 = . ;
            pdl_lease_non = . ;
        end ;
        else if dt < 20070331 then  do ;
            pdl_lease_3089 = bhck1226 ;
            pdl_lease_90 = bhck1227 ;
            pdl_lease_non = bhck1228 ;
        end ;
        else do;
            pdl_lease_3089 = sum(bhckf166,bhckf169) ;
            pdl_lease_90 = sum(bhckf167,bhckf170) ;
            pdl_lease_non = sum(bhckf168,bhckf171) ;
        end ;

        npl_lease = sum(pdl_lease_90,pdl_lease_non) ;

		label pdl_lease_3089='Lease Financing Receivables -- 30-89 Days Past Due' ;
		label pdl_lease_90='Lease Financing Receivables -- 90+ Days Past Due' ;
		label pdl_lease_non='Lease Financing Receivables -- Nonaccrual' ;
        label npl_lease='Non-Performing Lease Financing Receivables' ;

	/* All Other Delinquent Loans */;
	if dt < 19910331 then do ;
	     pdl_oth_3089 = . ;
	     pdl_oth_90 = . ;
             pdl_oth_non = . ;
	end ;
	else do ;
            pdl_oth_3089 = bhck5459 ;
            pdl_oth_90 = bhck5460 ;
            pdl_oth_non = bhck5461 ;
        end ;

        npl_oth = sum(pdl_oth_90,pdl_oth_non) ;

	label pdl_oth_3089='All Other Loans -- 30-89 Days Past Due' ;
	label pdl_oth_90='All Other Loans -- 90+ Days Past Due' ;
	label pdl_oth_non='All Other Loans -- Nonaccrual' ;
	label npl_oth='Non-Performing, All Other Loans' ;

               /* Other Real Estate Loans */;
                if dt<19910331 then
                    npl_othre=.;
                else npl_othre=sum(npl_re,-npl_rre,-npl_cre);
                label npl_othre = 'Other Non-Performing Real Estate Loans';

                /* All Other Non Performing Loans*/;
                  if dt<19910331 then
                    npl_allother = .;
                else npl_allother = sum(npl_oth,npl_agr,npl_othre,npl_dep,npl_fgovt);
                label npl_allother = 'All Other Non-Performing Loans';

                num_employees=bhck4150;
                label num_employees = 'Number of full time equivalent employees (bhck4150)';


        drop bh: te: d_dt: dt_start dt_end row_loaded_tstmp latest_row_ind srce_flag tstmp_ext tstmp_load status_conf ;

run;

* CHECK HERE: the nc should be refenced in the lowcase macro and in the stat transfer;

%lowcase(fr_y9c,&ctype.);
%mend conftype;

%conftype(nc);
endrsubmit;


%sysexec st ../../../reg_data_nc/fr_y9c/fr_y9c_nc.sas7bdat ../../../reg_data_nc/fr_y9c/fr_y9c_nc.dta -y -o;
*%sysexec st ../../data_c/fr_y9c/fr_y9c_c.sas7bdat ../../data_c/fr_y9c/fr_y9c_c.dta -y -o;
