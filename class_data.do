

* Note 11/26/2013: all files that were saved with the "_w_cappr" suffix in the class_data_w_capr.do file (on which this quarters program is based) have had that suffix deleted.  
capture restore
set more off
set type double
clear all

local date="1406"
local date2="217"
global rawy9c="/san/RDS/Derived/reg_data_nc/fr_y9c"
global rawcall="/san/RDS/Derived/reg_data_nc/call"
global datapath="/san/RDS/Derived/fif/class/data/`date'/class_data"
global prgpath="/san/RDS/Derived/fif/class/programs/`date'/data_construction"
global nicpath="/san/RDS/Derived/fif/class/data/`date'/class_data"
global datadepot="/san/RDS/Derived/fif/class/data/`date'/class_data/data_depot"
global savepath="/san/RDS/Work/fif/b1mxp07/Matt/data/regdata/"
* Do not run Quarterize Loop *
* When testing this program, it significantly cuts down on running time if you do not run the class_quarterize program.
local dnr_quarterize = 1

/* List of specific institutions to drop (DTC, ICE, etc) */
local droplist="entity==3822016 | entity==52719 | entity==3212149"

/* CCAR/SCAP Firms */
local ccars "1039502 1068025 1069778 1070345 1073757 1074156 1111435 1119794 1120754 1131787 1951350 2277860 3242838 3587146 2380443 2162966 1562859 1275216"

/* LISCC Firms */
local lisccs "1073757 1951350 1039502 1111435 1120754 2380443 2162966 3587146"

* CAPPR firms
local cappr "1039502 1068025 1069778 1070345 1073757 1074156 1111435 1119794 1120754 1131787 1951350 2277860 3242838 3587146 2380443 2162966 1562859 1275216 1027004 1037003 1068191 1078529 1132449 1199611 1199844 1245415 1378434 3232316 3846375 3981856"

* Forein-Owned CAPPR firms
local cappr_for "1037003 1078529 1132449 1245415 1378434 3232316 3981856"

* QS firms
*local qs "1039502 1068025 1069778 1070345 1073757 1074156 1111435 1119794 1120754 1131787 1951350 2277860 3242838 3587146 2380443 2162966 1562859 1275216"

local flows="qint_inc_net qint_inc qint_exp qllprov qtradrev_inc qnonint_inc qnonint_exp_fass qnonint_exp_gwill qnonint_exp_amort qnonint_exp_oth qnonint_exp_allother"

local flows2="qhtmsecur_inc qafssecur_inc qnonint_exp qminor_int qtaxes_inc qextra_inc qint_trad_inc qnetinc qnonint_exp_comp"

local xoffs="qnetxoff_re qnetxoff_rre qnetxoff_jrlien qnetxoff_firstlien qnetxoff_heloc qnetxoff_const qnetxoff_multi qnetxoff_nfnr qnetxoff_cre qnetxoff_dep qnetxoff_agr qnetxoff_ci qnetxoff_cons qnetxoff_cc"

local xoffs2="qnetxoff_othre qnetxoff_allother qnetxoff_othcons qnetxoff_fgovt qnetxoff_oth qnetxoff_lease qnetxoff_tot qnetxoff_closedlien"

local balances="assets ln_tot llres afssec afs_ust afs_agency_mbs afs_agency_nonmbs trad_ass earn_ass ln_re ln_rre ln_jrlien ln_const ln_multi ln_nfnr ln_cre ln_othre ln_dep ln_agr ln_ci ln_cons ln_cc"

local balances2="ln_othcons ln_fgovt ln_oth ln_lease ln_allother ln_firstlien ln_heloc ln_closedlien cash ibb ffrepo_ass ffsold repo_purch intang goodwill intang_oth fixed_ass npl_tot npl_re npl_ci npl_rre npl_cons npl_othcons npl_cre"

local balances3="npl_jrlien npl_const npl_multi npl_nfnr npl_othre npl_dep npl_agr npl_cc npl_fgovt npl_oth npl_lease npl_allother npl_firstlien npl_heloc npl_closedlien"

local regcap="tier1_rbc total_rbc rwa asset_forlev t1_unrealized_regafs tot_deposit inv_sec equity tier1_comm"
********
* TEST *
********
local regcap_cap="tier1_rbc total_rbc rwa asset_forlev tot_deposit inv_sec equity tier1_comm t1_* htmsec"
************
* END TEST *
************

local eqcap="qstocksale qtreasurystock qbuschange qprefdividend qcommdividend qcompinc_oth qprefstocksale qprefstockconvert qcommstocksale qcommstockconvert qtreasurysale qtreasurypurch qesop qcapital_oth"

local ids="bank_id indy_bank smallbhc_bank hhrssd for_hhrssd rssd9001 forownper_rssd forowntype_rssd"

local liab "liab_tot liab_oth for_deposit dom_deposit transaction_dep savings_dep time_* brokered_* trad_liab ffrepo_liab ffpurch repo_sold subdebt othbor_liab subnotes_trups ustdem_liab minint_liab"
********
* TEST *
********
local liab_cap "liab_tot liab_oth for_deposit transaction_dep savings_dep time_* brokered_* trad_liab ffrepo_liab subdebt othbor_liab subnotes_trups ustdem_liab minint_liab"
************
* END TEST *
************

local preds="qpred*"

local cutoff="200"

*****************************
****Step 0: Prepare SBL
*****************************

use "$location/data/regdata/sbl_cleaned.dta", clear
replace hhrssd = entity if hhrssd==0
collapse (sum) ln_* (max) sbl_dum_max=sbl_dum (min) sbl_dum_min=sbl_dum, by(hhrssd date)
ren hhrssd entity

//Correct for banks not filling out the form when a significant portion of their loans are in small business loans
foreach var of varlist ln_sbl*{
	replace `var'= . if sbl_dum_max==1 & sbl_dum_min==1 
}

foreach var of varlist ln_sbl* {
	replace `var' = . if ((sbl_dum_max==0 & sbl_dum_min==0) | (sbl_dum_max==1 & sbl_dum_min==0)) & `var'==0
}
tempfile sbl_bhc
save `sbl_bhc', replace


/*
***********************************
***Step One: Quarterization
***********************************
if `dnr_quarterize' == 0 {
   do "$prgpath/class_nicfix.do"
   forvalues bhc=0/1 {
      global bhc=`bhc'
      do "$prgpath/class_quarterize.do"
   }
   
   
}
*/
if `dnr_quarterize' == 1 {
   use "$savepath/qclass_bhc.dta", clear
   merge 1:1 entity date using `sbl_bhc'
   drop if _merge==2
   ren _merge merge_sbl 
}


***********************************
***Step Two: Post clean the data
***********************************

*PREDECSESOR FINANCIALS ADJUSTMENT FOR VARIABLES THAT WE HAVE

ren qpred_tradrev qpred_tradrev_inc
ren qpred_netintinc qpred_int_inc_net
ren qpred_intinc qpred_int_inc
ren qpred_intexp qpred_int_exp
ren qpred_nonintinc qpred_nonint_inc
ren qpred_nonintexp qpred_nonint_exp
ren qpred_comp qpred_nonint_exp_comp
ren qpred_goodwill qpred_nonint_exp_gwill

*JP Morgan Chase adjustment for series without Bear Stearns Financials
foreach x in int_inc int_exp int_inc_net nonint_inc tradrev_inc nonint_exp nonint_exp_comp nonint_exp_gwill netinc llprov {
sort entity date
gen quart_`x' = qpred_`x' / 3 if year==2008 & quarter==3 & entity==1039502
egen rep_`x'=rowtotal(q`x' quart_`x') if year==2008 & quarter==3 & entity==1039502
replace q`x'=rep_`x' if year==2008 & quarter==3 & entity==1039502
drop rep_`x' quart_`x'
}

*Wells Fargo Adjustment
foreach x in int_inc int_exp int_inc_net nonint_inc tradrev_inc nonint_exp nonint_exp_comp nonint_exp_gwill netinc llprov {
sort entity date
gen ytd_`x'=q`x' + q`x'[_n-1] + q`x'[_n-2] if year==2008 & quarter==3 & entity==1073551
egen ytdm_`x'=max(ytd_`x')
replace ytdm_`x'=ytdm_`x' * -1
egen quart_`x'= rowtotal(qpred_`x' ytdm_`x') if year==2008 & quarter==4 & entity==1120754
egen rep_`x'=rowtotal(q`x' quart_`x')
replace q`x'=rep_`x' if year==2008 & quarter==4 & entity==1120754
drop rep_`x' quart_`x' ytd_`x' ytdm_`x'
}

*PNC Adjustment
foreach x in int_inc int_exp int_inc_net nonint_inc tradrev_inc nonint_exp nonint_exp_comp nonint_exp_gwill netinc llprov {
sort entity date
gen ytd_`x'=q`x' + q`x'[_n-1] + q`x'[_n-2] if year==2008 & quarter==3 & entity==1069125
egen ytdm_`x'=max(ytd_`x')
replace ytdm_`x'=ytdm_`x' * -1
egen quart_`x'= rowtotal(qpred_`x' ytdm_`x') if year==2008 & quarter==4 & entity==1069778
egen rep_`x'=rowtotal(q`x' quart_`x')
replace q`x'=rep_`x' if year==2008 & quarter==4 & entity==1069778
drop rep_`x' quart_`x' ytd_`x' ytdm_`x'
}


***Step Three: COMBINE BANKS AND BHCS
drop if y9c9802==2
*drop if type_code_rssd==37
keep if type_code_rssd == 28
gen bank_id = 0
label var bank_id "Entity is a Commercial Bank (files Call Report)"
gen indy_bank = 0
label var indy_bank "Entity is an Independent Commercial Bank w/o a BHC parent"
gen smallbhc_bank = 0
label var smallbhc_bank "Entity is a bank with a BHC too small to file a Y9C"

sort date entity
tempfile bhcdata


save `bhcdata', replace

use "$savepath/qclass_bank.dta", clear
merge 1:1 entity date using "$location/data/regdata/sbl_cleaned.dta"
drop if _merge==2
ren _merge merge_sbl
capture drop typerssd
*standard commerical bank definition
keep if type_code_rssd==1
*drop if code8786>2
keep if state_code<57
keep if insurer_code==1 | insurer_code==2 | insurer_code==6 | insurer_code==7 | charter_type==250

*update relevant internal indicators
gen bank_id = 1
gen indy_bank=(hhrssd==0)
gen smallbhc_bank = 0

sort entity
tempfile bankdata
save `bankdata', replace

*create a unique list of all HHs listed by banks (called reghh)
keep hhrssd date
rename hhrssd reghh
drop if reghh==0
duplicates drop reghh date, force
sort reghh date
tempfile bankhh
save `bankhh', replace

*check this list against the list of BHCs in Y9C
use `bhcdata', clear
capture drop typerssd

*if a bhc has no high holder than it is the regulatory high holder
gen reghh=entity if hhrssd==0

* if it has a high holder then that is the regulatory high holder
replace reghh=hhrssd if hhrssd!=0

keep reghh date
duplicates drop reghh date, force
sort reghh date
merge reghh date using `bankhh'

*get a list of entity codes of bank HHs listed in Y9C (to drop to avoid double counting)
keep if _merge==3
drop _merge
sort reghh date
tempfile drop
save `drop', replace

*merging this list back to the Call Report data
use `bankdata', clear
gen reghh=hhrssd
sort reghh date
merge reghh date using `drop'

*drop if there's a match
drop if _merge==3
*if a bank is not independent is on this list, it has a BHC too small to file Y9C (or is foreign)
replace smallbhc_bank=1 if indy_bank==0
drop _merge
sort date entity

*add BHC data
append using `bhcdata'
capture drop reghh pool_flag push_flag push_year push_quarter
sort date entity

drop if quarter!=2
drop if year<1975 | year>2011
save "$savepath/class_rawcore_sbl.dta", replace
