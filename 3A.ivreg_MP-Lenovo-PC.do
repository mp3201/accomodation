*This program runs the regressions at the county level.
*The establishment/employment data from qcew to feed in is the combined 
*SIC and NAICS 3 digit with the bartik instrument created beforehand

/*if "`c(os)'" == "Unix" {
	global location "/san/RDS/Work/fif/b1mxp07/Matt"
	adopath ++ "/san/RDS/Work/fif/b1mxp07/my_ados"
}
else if "`c(os)'" == "Windows" {
	global location "//RB/B1/NYRESAN/RDS/Work/fif/b1mxp07/Matt"
	adopath ++ "//RB/B1/NYRESAN/RDS/Work/fif/b1mxp07/my_ados"
}*/

//Set user
global user Minh

if "${user}"=="Minh"{
	global location "C:/Users/Minh/OneDrive/accomodation"
	global outreg "$location/output/regressions"
	global outsum "$location/output/summaries"
	global outint "$location/accomodation/output/interactions"
	global outfig "$location/accomodation/output/figures"
	global data "$location/data"
	global subcode "$location/code/subcode"
}
else{
	if "`c(os)'" == "Unix" {
		global location "/home/rcemxp06/accomodation"
		global outreg "/home/rcemxp06/accomodation/output/regressions"
		global outsum "/home/rcemxp06/accomodation/output/summaries"
		global outint "/home/rcemxp06/accomodation/output/interactions"
		global outfig "/home/rcemxp06/accomodation/output/figures"
		global data "/home/rcemxp06/accomodation/data"
		global subcode "/home/rcemxp06/accomodation/code/subcode"
		adopath ++ "/home/rcemxp06/ado"
	}
	else if "`c(os)'" == "Windows" {
		global location "R:/accomodation"
		global outreg "R:/accomodation/output/regressions"
		global outsum "R:/accomodation/output/summaries"
		global outint "R:/accomodation/output/interactions"
		global outfig "R:/accomodation/output/figures"
		global data "R:/accomodation/data"
		global subcode "R:/accomodation/code/subcode"
		adopath ++ "R:/ado"
	}
}


set more off

/**********************************************************************/
/***********************Add population****************/
use "$location/data/census/county_population.dta", clear
drop if fipsco=="000"
drop state_fips county_fips areaname region division pop19904
reshape long pop, i(fips state_name county_name fipsst fipsco) j(year)
drop if pop==.
ren fips area
save "$location/data/census/county_population_long", replace

/***********************Add dereg****************/
import excel "$location/data/misc.xlsx", firstrow sheet("fips") clear
merge 1:1 stabb using "$location/data/misc_data/dereg.dta"
drop if _merge!=3
drop _merge
tostring stfips, replace
replace stfips = "0" + stfips if length(stfips)!=2
tempfile dereg
save `dereg', replace

use "$location/data/county_level_qcew/naics_sic_inscnty_3.dta", clear
capture drop _merge
gen quarter = quarter(dofq(date))
gen year = year(dofq(date))
drop if quarter!=2
drop quarter date

merge m:1 area year using "$location/data/sod/sodcnty.dta"
drop if _merge!=3
drop _merge

gen stfips = substr(area, 1, 2)
gen stfipsn = stfips
destring stfipsn, replace
merge m:1 stfips using `dereg' 
assert _merge==3
drop _merge

merge m:1 area year using "$location/data/census/county_population_long"
// Some missing counties assert _merge==3
drop if _merge!=3
drop _merge

sort codetype area year
gen ysmadate = year - madate+1
gen ysintra = year - denovo+1
gen ysinter = year - inter+1
gen ysmbhc = year - mbhc+1

destring(stfips), g(st_num)
save `dereg', replace

collapse (sum) numbr , by(year st_num)

gen rindex_yr=year
drop numbr
tempfile cal 
save `cal', replace

import excel "$location/data/misc_data/interstate.xlsx", sheet("Sheet1") firstrow clear
rename stfips st_num
rename year rindex_yr

keep rindex rindex_yr st_num
merge 1:1 st_num rindex_yr using `cal' 
drop _merge

sort st_num year
bysort st_num (year): replace rindex=rindex[_n-1] if rindex==.


merge 1:m st_num year using `dereg' 
replace rindex=0 if year>=1994 & rindex==. 


//foreach var in ysmadate ysintra ysinter ysmbhc{
	gen yearsince = . 
	forvalues i = -10(1)15 {
		if `i' < 0 {
			local temp = substr("`i'", 2,.)
			local concat n`temp'
		}
		else {
			local concat p`i'
		}
		gen indy_`concat' = (((ysmadate==`i') + (ysintra==`i') + (ysinter==`i') + (ysmbhc==`i'))>0)
		replace yearsince = `i' if (ysmadate==`i') | (ysintra==`i') | (ysinter==`i') | (ysmbhc==`i')
	}
//}



foreach var in ysmadate ysintra ysinter ysmbhc{
	gen ind`var' = (`var'>0)
}

egen ys_min=rowmin(ysmadate ysintra ysinter ysmbhc)


replace ysmadate = 0 if ysmadate<0
replace ysintra = 0 if ysintra<0
replace ysinter = 0 if ysinter<0
replace ysmbhc = 0 if ysmbhc<0


gen sumdates = ysmadate+ysintra+ysinter+ysmbhc
foreach var in ysmadate ysintra ysinter ysmbhc sumdates ys_min{
gen log_`var' = log(`var'+1)
gen sq_`var'=`var'*`var'
}


egen deregindex = rowtotal(indysmadate indysintra indysinter indysmbhc)


ren rat_ex_cnty_br rat_br
ren rat_ex_cnty_dep rat_dep

replace rat_br = log(rat_br+1)
replace rat_dep = log(rat_dep+1)

gen log_hhi_dep=log(hhi_dep)
gen bank_pop=log(numbank/pop)

gen log_at_dep=log_tot_assets - log(br_deposits/1000)

gen rbr=rat_br 

gen ltbr=log_totbr_cnty 
gen lewa=log_ew_assets 
gen ldwa=log_dw_assets_cnty 

*rdp lad lap dwl dwu dwld dwla
gen rdp=rat_dep 
gen lad=log_at_dep
gen lap=log_tot_assets_real-log(pop)
gen dwl=pdw_large
gen dwu=pdw_unit
gen dwld=pct_local_cnty_dep_dw
gen dwla=pct_local_cnty_at_dw



bys codetype year (area): egen uspop = sum(pop)
gen poprat = pop/uspop

foreach var in pop {
bys codetype area: egen min`var'=min(`var')
}

gen pctpop=.
forvalues i = 1980/2011 {
xtile temp2=pop if codetype==3 & year==`i', nq(100)
replace pctpop=temp2 if codetype==3 & year==`i'
drop temp2
}
bys area: egen pctpopmin=min(pctpop)

xtile temp=pop if codetype==3 & year==1980, nq(100)
bys area: egen pctpop80=min(temp)
drop temp
xtile temp=pop if codetype==3 & year==1985, nq(100)
bys area: egen pctpop85=min(temp)
drop temp
xtile temp=cemplq if codetype==3 & year==1985, nq(100)
bys area: egen pctemp85=min(temp)
drop temp 
gen temp=numbr if codetype==3 & year==1985
bys area: egen numbr85=min(temp)
replace numbr85=0 if numbr85==.
drop temp
/*gen temp=numbr if codetype==3 & year==1985
bys area: egen numbr85=min(temp)
drop temp*/


global popmin 33 
global popcond pctpop85>$popmin & pctemp85>$popmin & numbr85>0 
global ycond1 year>1985 & year<2006 & $popcond
global ycond2 year>1990 & year<2006 & $popcond
global ycond3 year>1990 & year<=2011 & $popcond
global ycond4 year>1985 & year<=2011 & $popcond
/*global cond if codetype==3 & pctpopmin>$popmin & year<$yearmax

foreach var in bank_pop log_hhi_dep hhi_dep pew_multi_bank pdw_multi_bank  pew_unit_bank pdw_unit_bank ///
log_ew_assets log_dw_assets pew_large pdw_large {
areg `var' i.year $cond, absorb(area)
predict r_`var' if e(sample), r 
}
*/

*Need to create another winsor level 1% and 2.5%

*Trim 1 and 2.5%
foreach typ in empl estab wage {
*forvalue j = 1/3{
		*gen w1_`typ'_`j'=grtc`typ'q
		*gen w2_`typ'_`j'=grtc`typ'q
		gen w1_`typ'=grtc`typ'q
		gen w2_`typ'=grtc`typ'q
forvalue i = 1/3{
		 xtile temp=grtc`typ'q if codetype==`i' & $ycond4, nq(200)
	/*	 summ grtc`typ'q  if codetype==`i', d
		 local p01 = `r(p1)' if codetype==`i'
		 local p99 = `r(p99)' if codetype==`i' */
		 replace w1_`typ' = . if temp<=2 & codetype==`i' & $ycond4
		 replace w1_`typ' = . if temp>=199 & codetype==`i' & $ycond4
		 replace w2_`typ' = . if temp<=5 & codetype==`i' & $ycond4
		 replace w2_`typ' = . if temp>=196 & codetype==`i' & $ycond4
		 drop temp
	}
}

foreach typ in empl estab wage {
gen vol_`typ'=.
gen sd_`typ'=.
gen mean_`typ'=.
forval i=1985(1)2011{ 
local m=`i'-5
bys codetype area (year): egen temp_sd=sd(grtc`typ'q) if year>=`m' & year<=`i' & codetype==3
bys codetype area (year): egen temp_mean=mean(grtc`typ'q) if year>=`m' & year<=`i' & codetype==3
replace sd_`typ'=temp_sd if year==`i' & codetype==3
replace vol_`typ'=temp_sd/temp_mean if year==`i' & codetype==3
drop temp_sd temp_mean
}
gen log_sd_`typ'=log(sd_`typ')
gen log_vol_`typ'=log(vol_`typ')
}

*rename raw growth
foreach var2 in empl estab wage {
rename grtc`var2'q g_`var2'
}

/*******************************************/
/* negative predictions */
/*******************************************/
foreach pred in empl estab wage {
	if ("`pred'" == "empl"){
		local crhsti "Predicted Employment"
	}
	else if ("`pred'" == "estab"){
		local crhsti "Predicted Establishments"
	}
	else if ("`pred'" == "wage"){
		local crhsti "Predicted Wage"
	}

	gen ptemp = (ins`pred'q>=0 & ins`pred'q!=.)
	gen pos_`pred' = ins`pred'q*ptemp
	label var pos_`pred' "Positive `crhsti'"
	gen neg_`pred'_dum = (ins`pred'q<0 & ins`pred'q!=.)
	gen neg_`pred' = ins`pred'q*neg_`pred'_dum
	label var neg_`pred' "Negative `crhsti'"
	drop ptemp 
}


/*******************************************/
/* within year mean/median*/
/*******************************************/

*First rename shock
foreach var2 in empl estab wage {
rename ins`var2'q pred_`var2'
}

*Second calc within year mean/median shock
foreach var2 in empl estab wage {
bysort year codetype: egen mean_pred_`var2'=mean(pred_`var2') if $popcond
bysort year codetype: egen median_pred_`var2'=median(pred_`var2') if $popcond
}

*Third calc within year pctile
foreach var in empl estab wage {
gen ptile_pred_`var'=.
foreach typ in 1 2 3 {
cap drop tag
gen tag = (pred_`var'~=.)
sort year codetype tag pred_`var'
by year codetype tag: replace ptile_pred_`var' = floor((_n-1)/_N * 100) + 1 if tag~=. & codetype==`typ'
drop tag
}
}


*Define dummies labelling above and below 
foreach var2 in empl estab wage {
	if ("`var2'" == "empl"){
		local crhsti "Predicted Employment"
	}
	else if ("`var2'" == "estab"){
		local crhsti "Predicted Establishments"
	}
	else if ("`var2'" == "wage"){
		local crhsti "Predicted Wage"
	}
	
gen below_ts=.
gen below_ts2=.
gen below_ts33=.

foreach typ in 1 2 3 {
replace below_ts=(pred_`var2'<mean_pred_`var2') if codetype==`typ' & $ycond4
replace below_ts2=(pred_`var2'<median_pred_`var2') if codetype==`typ' & $ycond4
replace below_ts33=(ptile_pred_`var2'<=33) if codetype==`typ' & $ycond4
}

gen n_ts_`var2'_dum=below_ts
gen n_ts_`var2'=pred_`var2'*below_ts
label var n_ts_`var2' "Below Avg `crhsti' within year"

gen n_ts2_`var2'_dum=below_ts2
gen n_ts2_`var2'=pred_`var2'*below_ts2
label var n_ts2_`var2' "Below p50 `crhsti' within year"

gen n_ts33_`var2'_dum=below_ts33
gen n_ts33_`var2'=pred_`var2'*below_ts33
label var n_ts33_`var2' "Below p33 `crhsti' within year"

drop below_* 
}

/*******************************************/
/* within sample below mean */
/*******************************************/

foreach pred in empl estab wage {
	if ("`pred'" == "empl"){
		local crhsti "Predicted Employment"
	}
	else if ("`pred'" == "estab"){
		local crhsti "Predicted Establishments"
	}
	else if ("`pred'" == "wage"){
		local crhsti "Predicted Wage"
	}
	gen below_m=.
	gen above_m=.
	gen below_p=.
	gen above_p=.
	gen below_pts=.
	gen above_pts=.
	
	foreach typ in 1 2 3 {
	summ pred_`pred' if codetype==`typ' & $ycond4
	replace below_m = (pred_`pred'<`r(mean)' & pred_`pred'!=.) if codetype==`typ' & $ycond4
	replace above_m = (pred_`pred'>=`r(mean)' & pred_`pred'!=.) if codetype==`typ' & $ycond4
	areg pred_`pred' if codetype==`typ' & $ycond4, absorb(area)
	predict demean1_`typ' if e(sample), r
	replace below_p = (demean1_`typ'<0 & pred_`pred'!=.) if codetype==`typ' & $ycond4
	replace above_p = (demean1_`typ'>=0 & pred_`pred'!=.) if codetype==`typ' & $ycond4
	areg pred_`pred' i.year if codetype==`typ' & $ycond4, absorb(area)
	predict demean2_`typ' if e(sample), r
	replace below_pts = (demean2_`typ'<0 & pred_`pred'!=.) if codetype==`typ' & $ycond4
	replace above_pts = (demean2_`typ'>=0 & pred_`pred'!=.) if codetype==`typ' & $ycond4
	}
	
	gen p_`pred' = pred_`pred'*above_m
	label var p_`pred' "Above Avg `crhsti'"
	
	gen n_`pred'_dum=below_m
	gen n_`pred' = pred_`pred'*below_m
	label var n_`pred' "Above Avg `crhsti'"
	
	gen p_p_`pred' = pred_`pred'*above_p
	label var p_p_`pred' "Above Avg `crhsti' within area"
	
	gen n_p_`pred'_dum=below_p
	gen n_p_`pred' = pred_`pred'*below_p
	label var n_p_`pred' "Above Avg `crhsti' within area"
	
	gen p_pts_`pred' = pred_`pred'*above_pts
	label var p_pts_`pred' "Above Avg `crhsti' within areaand time fe"
	
	gen n_pts_`pred'_dum=below_pts
	gen n_pts_`pred' = pred_`pred'*below_pts
	label var n_pts_`pred' "Above Avg `crhsti' within area and time fe "
	
	drop demean* above_* below_*
}

gen lsd=log_sumdates
gen drg=deregindex
gen sd=sumdates

foreach var in lsd drg sd {
forvalue j = 1/4 {
gen `var'_`j'=.
foreach typ in 1 2 3 {
sum `var' if codetype==`typ' & $ycond`j'
replace `var'_`j'=`var'-`r(mean)'
}
}
}


/*foreach var in ysmadate ysintra ysinter ysmbhc ///
indysmadate indysintra indysinter indysmbhc log_ysmadate log_ysintra log_ysinter log_ysmbhc ///
sq_ysmadate sq_ysintra sq_ysinter sq_ysmbhc deregindex log_sumdates sumdates {*/
foreach var of varlist drg lsd sd drg_* lsd_* sd_* {
foreach var2 in empl estab wage {
gen `var2'_`var'=`var'*pred_`var2'
foreach var3 in neg n_ts n_ts2 n n_p n_pts {
gen `var3'_`var2'_`var'=`var'*`var3'_`var2'
}
}
}

/**************Checking potential financial sector variables***************/
/*
cd $outreg

qui reg rat_br i.year if codetype==3 & $ycond1 & pred_empl~=., cluster(st_num) robust 
outreg2 using ivtest, excel label addtext(Year FE, Yes, County FE, No, Clustered SE, STATE) replace

foreach var in rat_br rat_dep log_totbr_cnty hhi_dep log_hhi_dep pew_multi_br ///
pdw_multi_bank pew_unit_bank pdw_unit_bank pdw_large log_ew_assets_cnty log_dw_assets_cnty pdw_top5 log_at_dep{

qui reg `var' i.year if codetype==3 & $ycond1 & pred_empl~=., cluster(st_num) robust 
outreg2 using ivtest, excel label addtext(Year FE, Yes, County FE, No, Clustered SE, STATE) append

foreach var2 of varlist drg lsd sd{
qui reg `var' `var2' i.year if codetype==3 & $ycond1 & pred_empl~=., cluster(st_num) robust 
outreg2 `var2' using ivtest, excel label addtext(Year FE, Yes, County FE, No, Clustered SE, STATE) append
}
}


qui areg rat_br i.year if codetype==3 & $ycond1 & pred_empl~=., cluster(st_num) robust absorb(area)
outreg2 using ivtest_fe, excel label addtext(Year FE, Yes, County FE, No, Clustered SE, STATE) replace

foreach var in rat_br rat_dep log_totbr_cnty hhi_dep log_hhi_dep pew_multi_br ///
pdw_multi_bank pew_unit_bank pdw_unit_bank pdw_large log_ew_assets_cnty log_dw_assets_cnty pdw_top5 log_at_dep{

qui areg `var' i.year if codetype==3 & $ycond1 & pred_empl~=., cluster(st_num) robust absorb(area)
outreg2 using ivtest_fe, excel label addtext(Year FE, Yes, County FE, No, Clustered SE, STATE) append

foreach var2 of varlist drg lsd sd{
qui areg `var' `var2' i.year if codetype==3 & $ycond1 & pred_empl~=., cluster(st_num) robust absorb(area) 
outreg2 `var2' using ivtest_fe, excel label addtext(Year FE, Yes, County FE, No, Clustered SE, STATE) append
}
}
*/



foreach var in rdp lad lap dwl dwu dwld dwla {
forvalue j = 1/4 {
gen `var'_`j'=.
foreach typ in 1 2 3 {
sum `var' if codetype==`typ' & $ycond`j'
replace `var'_`j'=`var'-`r(mean)'
}
}
}


/*foreach var in rat_br rat_dep log_totbr_cnty hhi_dep log_hhi_dep pew_multi_br ///
pdw_multi_bank pew_unit_bank pdw_unit_bank pdw_large log_ew_assets_cnty log_dw_assets_cnty pdw_top5{*/

foreach var of varlist rdp* lad* lap* dwl* dwu* {
foreach var2 in empl estab wage {
gen `var2'_`var'=`var'*pred_`var2'
foreach var3 in neg n_ts n_ts2 n n_p n_pts {
gen `var3'_`var2'_`var'=`var'*`var3'_`var2'
}
}
}


//Generate Panel IDs//
tab year, gen(yr_)
forvalue i = 1/3 {
egen temp`i'=group(area) if codetype==`i'
}

gen pid=temp3+3*10000
replace pid=temp2+2*10000 if codetype==2
replace pid=temp1+1*10000 if codetype==1

drop typearea temp1 temp2 temp3 state_name
xtset pid year

cd $data
save panel_temp, replace


/**************************************************************************************/
/**************************************************************************************/
/**************************************************************************************/
/**************************************************************************************/

// Prepare county-level data to merge with county-industry
cd $data
use panel_temp, replace
keep codetype area year stfipsn pred_* g_* br_* numb*  drg lsd sd drg_* lsd_* ///
w1_* w2_* rdp* lad* lap* dwl* dwu* n_p_* pctpop* pctemp* neg_*  empl_* wage_* estab_* 

drop *estab* *_sd_*
drop if codetype==2

sort codetype area year

tempfile county
save `county', replace

// Prepare industry-county-level data to merge with county
use "$location/data/naics_sic_insindcnty.dta", clear
capture drop _merge
gen quarter = quarter(dofq(date))
gen year = year(dofq(date))
drop if quarter!=2
drop quarter date


foreach var in empl estab wage {
rename insind2`var'q pi_`var'
rename grtc2`var'q gi_`var'
rename c2`var'q ind_`var'
}

rename industry2dig ind2
destring(ind2), gen(ind2n) force 

drop if codetype==2
sort codetype area year


// Merge in county-level variables
merge m:1 codetype area year using `county'
drop if _merge!=3
drop _merge

global popmin 33 
global popcond pctpop85>$popmin & pctemp85>$popmin & numbr85>0 
global ycond1 year>1985 & year<2006 & $popcond
global ycond2 year>1990 & year<2006 & $popcond
global ycond3 year>1990 & year<=2011 & $popcond
global ycond4 year>1985 & year<=2011 & $popcond


// Trim at 1% and 2.5% levels
foreach typ in empl estab wage {
		gen w1i_`typ'=gi_`typ'
		gen w2i_`typ'=gi_`typ'
foreach i in 1 3{
		 xtile temp=gi_`typ' if codetype==`i' & $ycond4, nq(200)
		 replace w1i_`typ' = . if temp<=2 & codetype==`i' & $ycond4
		 replace w1i_`typ' = . if temp>=199 & codetype==`i' & $ycond4
		 replace w2i_`typ' = . if temp<=5 & codetype==`i' & $ycond4
		 replace w2i_`typ' = . if temp>=196 & codetype==`i' & $ycond4
		 drop temp
	}
}


/*******************************************/
/* negative predictions */
/*******************************************/
foreach pred in empl estab wage {
	if ("`pred'" == "empl"){
		local crhsti "Predicted Employment"
	}
	else if ("`pred'" == "estab"){
		local crhsti "Predicted Establishments"
	}
	else if ("`pred'" == "wage"){
		local crhsti "Predicted Wage"
	}

	gen ptemp = (pi_`pred'>=0 & pi_`pred'!=.)
	gen pos_i_`pred' = pi_`pred'*ptemp
	label var pos_i_`pred' "Pos. Ind. `crhsti'"
	gen neg_i_`pred'_dum = (pi_`pred'<0 & pi_`pred'!=.)
	gen neg_i_`pred' = pi_`pred'*neg_i_`pred'_dum
	label var neg_i_`pred' "Neg. Ind. `crhsti'"
	drop ptemp 
}


/*******************************************/
/* within year mean/median*/
/*******************************************/

*Second calc within year mean/median shock
foreach var2 in empl estab wage {
bysort year codetype ind2: egen mean_pi_`var2'=mean(pi_`var2') if $popcond
bysort year codetype ind2: egen median_pi_`var2'=median(pi_`var2') if $popcond
}

*Third calc within year pctile
foreach var in empl estab wage {
gen ptile_pi_`var'=.
foreach typ in 1 3 {
cap drop tag
gen tag = (pi_`var'~=.)
sort year codetype ind2 tag pi_`var'
by year codetype ind2 tag: replace ptile_pi_`var' = floor((_n-1)/_N * 100) + 1 if tag~=. & codetype==`typ'
drop tag
}
}


*Define dummies labelling above and below 
foreach var2 in empl estab wage {
	if ("`var2'" == "empl"){
		local crhsti "Predicted Employment"
	}
	else if ("`var2'" == "estab"){
		local crhsti "Predicted Establishments"
	}
	else if ("`var2'" == "wage"){
		local crhsti "Predicted Wage"
	}
	
gen bi_ts=.
gen bi_ts2=.
gen bi_ts33=.

foreach typ in 1 3 {
replace bi_ts=(pi_`var2'<mean_pi_`var2') if codetype==`typ' & $ycond4
replace bi_ts2=(pi_`var2'<median_pi_`var2') if codetype==`typ' & $ycond4
replace bi_ts33=(ptile_pi_`var2'<=33) if codetype==`typ' & $ycond4
}

gen n_ts_i_`var2'_dum=bi_ts
gen n_ts_i_`var2'=pi_`var2'*bi_ts
label var n_ts_i_`var2' "Below Avg `crhsti' within year"

/*
gen n_ts2_i_`var2'_dum=bi_ts2
gen n_ts2_i_`var2'=pi_`var2'*bi_ts2
label var n_ts2_i_`var2' "Below p50 `crhsti' within year"

gen n_ts33_i_`var2'_dum=bi_ts33
gen n_ts33_i_`var2'=pi_`var2'*bi_ts33
label var n_ts33_i_`var2' "Below p33 `crhsti' within year"
*/
drop bi_ts* 
}

/*******************************************/
/* within sample below mean */
/*******************************************/

foreach pred in empl estab wage {
	if ("`pred'" == "empl"){
		local crhsti "Predicted Employment"
	}
	else if ("`pred'" == "estab"){
		local crhsti "Predicted Establishments"
	}
	else if ("`pred'" == "wage"){
		local crhsti "Predicted Wage"
	}
	gen below_m=.
	gen above_m=.
	gen below_p=.
	gen above_p=.
	gen below_i=.
	gen above_i=.
	gen below_pts=.
	gen above_pts=.
	
	foreach typ in 1 3 {
	summ pi_`pred' if codetype==`typ' & $ycond4
	replace below_m = (pi_`pred'<`r(mean)' & pi_`pred'!=.) if codetype==`typ' & $ycond4
	replace above_m = (pi_`pred'>=`r(mean)' & pi_`pred'!=.) if codetype==`typ' & $ycond4
	areg pi_`pred' if codetype==`typ' & $ycond4, absorb(area)
	predict demean1_`typ' if e(sample), r
	replace below_p = (demean1_`typ'<0 & pi_`pred'!=.) if codetype==`typ' & $ycond4
	replace above_p = (demean1_`typ'>=0 & pi_`pred'!=.) if codetype==`typ' & $ycond4
	areg pi_`pred' i.year if codetype==`typ' & $ycond4, absorb(area)
	predict demean2_`typ' if e(sample), r
	replace below_pts = (demean2_`typ'<0 & pi_`pred'!=.) if codetype==`typ' & $ycond4
	replace above_pts = (demean2_`typ'>=0 & pi_`pred'!=.) if codetype==`typ' & $ycond4
	areg pi_`pred' if codetype==`typ' & $ycond4, absorb(ind2)
	predict demean3_`typ' if e(sample), r
	replace below_i = (demean3_`typ'<0 & pi_`pred'!=.) if codetype==`typ' & $ycond4
	replace above_i = (demean3_`typ'>=0 & pi_`pred'!=.) if codetype==`typ' & $ycond4
	
	}
	
	gen p_i_`pred' = pi_`pred'*above_m
	label var p_i_`pred' "Above Avg `crhsti'"
	
	gen n_i_`pred'_dum=below_m
	gen n_i_`pred' = pi_`pred'*below_m
	label var n_i_`pred' "Above Avg `crhsti'"
	
	gen p_p_i_`pred' = pi_`pred'*above_p
	label var p_p_i_`pred' "Above Avg `crhsti' within area"
	
	gen n_p_i_`pred'_dum=below_p
	gen n_p_i_`pred' = pi_`pred'*below_p
	label var n_p_i_`pred' "Above Avg `crhsti' within area"
	
	gen p_pts_i_`pred' = pi_`pred'*above_pts
	label var p_pts_i_`pred' "Above Avg `crhsti' within areaand time fe"
	
	gen n_pts_i_`pred'_dum=below_pts
	gen n_pts_i_`pred' = pi_`pred'*below_pts
	label var n_pts_i_`pred' "Above Avg `crhsti' within area and time fe "
	
	gen p_i_i_`pred' = pi_`pred'*above_p
	label var p_i_i_`pred' "Above Avg `crhsti' within industry"
	
	gen n_i_i_`pred'_dum=below_i
	gen n_i_i_`pred' = pi_`pred'*below_i
	label var n_i_i_`pred' "Above Avg `crhsti' within industry"
	
	drop demean* above_* below_*
}


foreach var of varlist drg lsd drg_* lsd_* {
foreach var2 in empl estab wage {
gen i_`var2'_`var'=`var'*pi_`var2'
foreach var3 in neg_i n_ts_i n_i n_p_i n_pts_i n_i_i {
gen `var3'_`var2'_`var'=`var'*`var3'_`var2'
}
}
}



foreach var of varlist rdp* lad* lap* dwl* dwu* {
foreach var2 in empl estab wage {
gen i_`var2'_`var'=`var'*pi_`var2'
foreach var3 in neg_i n_ts_i n_i n_p_i n_pts_i n_i_i {
gen `var3'_`var2'_`var'=`var'*`var3'_`var2'
}
}
}

//	Generate Panel IDs
tab year, gen(yr_)
foreach i in 1 3 {
egen temp`i'=group(area) if codetype==`i'
}

gen pid=temp3+3*10000
*replace pid=temp2+2*10000 if codetype==2
replace pid=temp1+1*10000 if codetype==1
drop temp*

gen pid_yr= pid+(year-1979)*100000

tab ind2n, gen(ind_)
gen pid_ind= pid+(ind2n)*100000 

drop if codetype==2
compress

// Save industry data, compress panel and resave
cd $data
save panel_temp_ind, replace


use panel_temp
compress
save panel_temp, replace



exit
