cd $outreg

gen end=.
gen iv=.
foreach real in empl wage estab {
gen `real'_end=.
gen `real'_iv=.
}
 
cd $outreg
foreach real in empl wage /*estab*/ {
foreach inst in lsd drg {
foreach num of numlist 1/4 {

replace iv=`inst'_`num'
*Average;
global indvar1 iv
global indvar2 $indvar1 yr_*
ivreg2 ${samp}_`real' $indvar2 if codetype==$type & ${ycond`num'}, $se_opt
outreg2 $indvar1 using 1_`inst'_`num'_`real', cttop("RF IV") excel label addtext(County FE, No, Clustered SE, STATE) replace
*SD;
ivreg2 log_sd_`real' $indvar2 if codetype==$type & ${ycond`num'}, $se_opt
outreg2 $indvar1 using 1_`inst'_`num'_`real', cttop("RF IV") excel label addtext(County FE, No, Clustered SE, STATE) append
*vol;
ivreg2 log_vol_`real' $indvar2 if codetype==$type & ${ycond`num'}, $se_opt
outreg2 $indvar1 using 1_`inst'_`num'_`real', cttop("RF IV") excel label addtext(County FE, No, Clustered SE, STATE) append

*Repeat through endog vars
foreach endog0 in $endoglist{
foreach depvar in ${samp}_`real' log_sd_`real' log_vol_`real' {
global indvar1 
global indvar2 $indvar1 yr_*

replace end= `endog0'_`num' 
global endog end 

replace iv=`inst'_`num'
global instrum iv 

ivreg2 `depvar' $indvar2 $endog if codetype==$type & ${ycond`num'}, $se_opt
outreg2 $indvar1 $endog using 1_`inst'_`num'_`real', cttop("OLS") excel label addtext(County FE, No, Clustered SE, STATE) append

*ivreg2 end $indvar2 $instrum if codetype==$type & ${ycond`num'}, $se_opt
*outreg2 $indvar1 $instrum using 1_`inst'_`num'_`real', cttop("1st: `endog0'") excel label addtext(County FE, No, Clustered SE, STATE) append

ivreg2 `depvar' $indvar2 ($endog = $instrum) if codetype==$type & ${ycond`num'}, $se_opt
outreg2 $indvar1 $endog using 1_`inst'_`num'_`real', cttop("2nd: `endog0'") excel label addtext(County FE, No, Clustered SE, STATE) adds(F-Stat, e(widstat)) append
}
}

*Average;
global indvar1 iv
global indvar2 $indvar1 yr_*
xtivreg2 ${samp}_`real' $indvar2 if codetype==$type & ${ycond`num'}, $se_optfe
outreg2 $indvar1 using 1_`inst'_`num'_`real', cttop("RF IV") excel label addtext(County FE, Yes, Clustered SE, STATE) append
*SD;
xtivreg2 log_sd_`real' $indvar2 if codetype==$type & ${ycond`num'}, $se_optfe
outreg2 $indvar1 using 1_`inst'_`num'_`real', cttop("RF IV") excel label addtext(County FE, Yes, Clustered SE, STATE) append
*vol;
xtivreg2 log_vol_`real' $indvar2 if codetype==$type & ${ycond`num'}, $se_optfe
outreg2 $indvar1 using 1_`inst'_`num'_`real', cttop("RF IV") excel label addtext(County FE, Yes, Clustered SE, STATE) append

foreach endog0 in $endoglist {
foreach depvar in ${samp}_`real' log_sd_`real' log_vol_`real' {
global indvar1 
global indvar2 $indvar1 yr_*

replace end= `endog0'_`num' 
global endog end 
global instrum iv 

xtivreg2 `depvar' $indvar2 $endog if codetype==$type & ${ycond`num'}, $se_optfe
outreg2 $indvar1 $endog using 1_`inst'_`num'_`real', cttop("OLS") excel label addtext(County FE, Yes, Clustered SE, STATE) append

*xtivreg2 end $indvar2 $instrum if codetype==$type & ${ycond`num'}, $se_optfe
*outreg2 $indvar1 $instrum using 1_`inst'_`num'_`real', cttop("1st: `endog0'") excel label addtext(County FE, Yes, Clustered SE, STATE) append

xtivreg2 `depvar' $indvar2 ($endog = $instrum) if codetype==$type & ${ycond`num'}, $se_optfe
outreg2 $indvar1 $endog using 1_`inst'_`num'_`real', cttop("2nd: `endog0'") excel label addtext(County FE, Yes, Clustered SE, STATE) adds(F-Stat, e(widstat)) append
}
}
}
}
}

drop end iv *_end *_iv

