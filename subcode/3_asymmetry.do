

/*****************************/
/********Asymmetry************/
/*****************************/
cd $outint

gen end=.
gen iv=.
foreach real in empl wage estab {
gen `real'_end=.
gen `real'_iv=.
}

gen d_dum=.
gen d_dum_iv=.
gen d_dum_end=.
foreach real in empl wage estab {
gen d_`real'=.
gen d_`real'_end=.
gen d_`real'_iv=.
}
 
foreach real in empl wage /*estab*/ {
foreach inst in lsd drg {
foreach num of numlist 1/4 {


global indvar1 pred_`real'
global indvar2 $indvar1 yr_*
ivreg2 ${samp}_`real' $indvar2 if codetype==$type & ${ycond`num'}, $se_opt
outreg2 $indvar1 using 3_`inst'_`num'_`real', cttop("OLS") excel label addtext(County FE, No, Clustered SE, STATE) replace

foreach asymvar in neg n n_p n_ts {

replace d_`real'=`asymvar'_`real'
replace d_dum=`asymvar'_`real'_dum

global indvar1 pred_`real' d_`real' 
global indvar2 $indvar1 yr_*
ivreg2 ${samp}_`real' $indvar2 if codetype==$type & ${ycond`num'}, $se_opt
outreg2 $indvar1 using 3_`inst'_`num'_`real', cttop("OLS `asymvar'") excel label addtext(County FE, No, Clustered SE, STATE) append

replace iv=`inst'_`num'
replace `real'_iv=iv*pred_`real'
replace d_`real'_iv=d_`real'*iv
replace d_dum_iv=d_dum*iv

global indvar1 pred_`real' d_`real' iv `real'_iv  d_`real'_iv 
global indvar2 $indvar1 yr_*
ivreg2 ${samp}_`real' $indvar2 if codetype==$type & ${ycond`num'}, $se_opt
outreg2 $indvar1 using 3_`inst'_`num'_`real', cttop("OLS `asymvar'") excel label addtext(County FE, No, Clustered SE, STATE) append


foreach endog0 in $endoglist{
global indvar1 pred_`real' d_`real' 
global indvar2 $indvar1 yr_*

replace end= `endog0'_`num' 
replace `real'_end=pred_`real'*end
replace d_`real'_end=d_`real'*end
replace d_dum_end=d_dum*end

global endog end `real'_end d_`real'_end 
global instrum iv `real'_iv d_`real'_iv 

ivreg2 ${samp}_`real' $indvar2 $endog if codetype==$type & ${ycond`num'}, $se_opt
outreg2 $indvar1 $endog using 3_`inst'_`num'_`real', cttop("OLS `asymvar'") excel label addtext(County FE, No, Clustered SE, STATE) append

/*ivreg2 end $indvar2 $instrum if codetype==$type & ${ycond`num'}, $se_opt
outreg2 $indvar1 $instrum using 3_`inst'_`num'_`real', cttop("1st: `asymvar' `endog0'") excel label addtext(County FE, No, Clustered SE, STATE) append

ivreg2 `real'_end $indvar2 $instrum if codetype==$type & ${ycond`num'}, $se_opt
outreg2 $indvar1 $instrum using 3_`inst'_`num'_`real', cttop("1st: `asymvar' `real'_`endog0'") excel label addtext(County FE, No, Clustered SE, STATE) append

ivreg2 d_`real'_end $indvar2 $instrum if codetype==$type & ${ycond`num'}, $se_opt
outreg2 $indvar1 $instrum using 3_`inst'_`num'_`real', cttop("1st: `asymvar' `real'_`endog0'") excel label addtext(County FE, No, Clustered SE, STATE) append*/

ivreg2 ${samp}_`real' $indvar2 ($endog = $instrum) if codetype==$type & ${ycond`num'}, $se_opt
outreg2 $indvar1 $endog using 3_`inst'_`num'_`real', cttop("2nd: `asymvar' `endog0'") excel label addtext(County FE, No, Clustered SE, STATE) adds(F-Stat, e(widstat)) append
}
}

*Fixed Effects
global indvar1 pred_`real'
global indvar2 $indvar1 yr_*
xtivreg2 ${samp}_`real' $indvar2 if codetype==$type & ${ycond`num'}, $se_optfe
outreg2 $indvar1 using 3_`inst'_`num'_`real', cttop("OLS") excel label addtext(County FE, Yes, Clustered SE, STATE) append

foreach asymvar in neg n n_p n_ts {

replace d_`real'=`asymvar'_`real'
replace d_dum=`asymvar'_`real'_dum

global indvar1 pred_`real' d_`real' 
global indvar2 $indvar1 yr_*
xtivreg2 ${samp}_`real' $indvar2 if codetype==$type & ${ycond`num'}, $se_optfe
outreg2 $indvar1 using 3_`inst'_`num'_`real', cttop("OLS `asymvar'") excel label addtext(County FE, Yes, Clustered SE, STATE) append

replace iv=`inst'_`num'
replace `real'_iv=iv*pred_`real'
replace d_`real'_iv=d_`real'*iv
replace d_dum_iv=d_dum*iv

global indvar1 pred_`real' d_`real' iv `real'_iv  d_`real'_iv 
global indvar2 $indvar1 yr_*
xtivreg2 ${samp}_`real' $indvar2 if codetype==$type & ${ycond`num'}, $se_optfe
outreg2 $indvar1 using 3_`inst'_`num'_`real', cttop("OLS `asymvar'") excel label addtext(County FE, Yes, Clustered SE, STATE) append


foreach endog0 in $endoglist{
global indvar1 pred_`real' d_`real' 
global indvar2 $indvar1 yr_*

replace end= `endog0'_`num' 
replace `real'_end=pred_`real'*end
replace d_`real'_end=d_`real'*end
replace d_dum_end=d_dum*end

global endog end `real'_end d_`real'_end
global instrum iv `real'_iv d_`real'_iv

xtivreg2 ${samp}_`real' $indvar2 $endog if codetype==$type & ${ycond`num'}, $se_optfe
outreg2 $indvar1 $endog using 3_`inst'_`num'_`real', cttop("OLS `asymvar'") excel label addtext(County FE, Yes, Clustered SE, STATE) append

/*xtivreg2 end $indvar2 $instrum if codetype==$type & ${ycond`num'}, $se_optfe
outreg2 $indvar1 $instrum using 3_`inst'_`num'_`real', cttop("1st: `asymvar' `endog0'") excel label addtext(County FE, Yes, Clustered SE, STATE) append

xtivreg2 `real'_end $indvar2 $instrum if codetype==$type & ${ycond`num'}, $se_optfe
outreg2 $indvar1 $instrum using 3_`inst'_`num'_`real', cttop("1st: `asymvar' `real'_`endog0'") excel label addtext(County FE, Yes, Clustered SE, STATE) append

xtivreg2 d_`real'_end $indvar2 $instrum if codetype==$type & ${ycond`num'}, $se_optfe
outreg2 $indvar1 $instrum using 3_`inst'_`num'_`real', cttop("1st: `asymvar' `real'_`endog0'") excel label addtext(County FE, Yes, Clustered SE, STATE) append*/

xtivreg2 ${samp}_`real' $indvar2 ($endog = $instrum) if codetype==$type & ${ycond`num'}, $se_optfe
outreg2 $indvar1 $endog using 3_`inst'_`num'_`real', cttop("2nd: `asymvar' `endog0'") excel label addtext(County FE, Yes, Clustered SE, STATE) adds(F-Stat, e(widstat)) append
}
}


}
}
}
drop end iv d_dum *_end *_iv



