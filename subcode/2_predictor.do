
/***********************************************/
/****Interact prediction with instrument********/
/***********************************************/
cd $outreg

gen end=.
gen iv=.
foreach real in empl wage estab {
gen `real'_end=.
gen `real'_iv=.
}

foreach real in empl wage /*estab*/ {
foreach inst in lsd drg {
foreach num of numlist 1/4 {


global indvar1 pred_`real'
global indvar2 $indvar1 yr_*
ivreg2 ${samp}_`real' $indvar2 if codetype==$type & ${ycond`num'}, $se_opt
outreg2 $indvar1 using 2_`inst'_`num'_`real', cttop("OLS") excel label addtext(County FE, No, Clustered SE, STATE) replace

replace iv=`inst'_`num'
replace `real'_iv=iv*pred_`real'

global indvar1 pred_`real' iv `real'_iv 
global indvar2 $indvar1 yr_*
ivreg2 ${samp}_`real' $indvar2 if codetype==$type & ${ycond`num'}, $se_opt
outreg2 $indvar1 using 2_`inst'_`num'_`real', cttop("RF IV") excel label addtext(County FE, No, Clustered SE, STATE) append

foreach endog0 in $endoglist {
global indvar1 pred_`real' 
global indvar2 $indvar1 yr_*

replace end= `endog0'_`num' 
replace `real'_end=pred_`real'*end
global endog end `real'_end
global instrum iv `real'_iv

ivreg2 ${samp}_`real' $indvar2 $endog if codetype==$type & ${ycond`num'}, $se_opt
outreg2 $indvar1 $endog using 2_`inst'_`num'_`real', cttop("OLS") excel label addtext(County FE, No, Clustered SE, STATE) append
/*
ivreg2 end $indvar2 $instrum if codetype==$type & ${ycond`num'}, $se_opt
outreg2 $indvar1 $instrum using 2_`inst'_`num'_`real', cttop("1st: `endog0'") excel label addtext(County FE, No, Clustered SE, STATE) append

ivreg2 `real'_end $indvar2 $instrum if codetype==$type & ${ycond`num'}, $se_opt
outreg2 $indvar1 $instrum using 2_`inst'_`num'_`real', cttop("1st: `real'_`endog0'") excel label addtext(County FE, No, Clustered SE, STATE) append
*/
ivreg2 ${samp}_`real' $indvar2 ($endog = $instrum) if codetype==$type & ${ycond`num'}, $se_opt
outreg2 $indvar1 $endog using 2_`inst'_`num'_`real', cttop("2nd: `endog0'") excel label addtext(County FE, No, Clustered SE, STATE) adds(F-Stat, e(widstat)) append
}


*Repeat with fixed effects
global indvar1 pred_`real'
global indvar2 $indvar1 yr_*
xtivreg2 ${samp}_`real' $indvar2 if codetype==$type & ${ycond`num'}, $se_optfe
outreg2 $indvar1 using 2_`inst'_`num'_`real', cttop("OLS") excel label addtext(County FE, Yes, Clustered SE, STATE) append

global indvar1 pred_`real' iv `real'_iv 
global indvar2 $indvar1 yr_*
xtivreg2 ${samp}_`real' $indvar2 if codetype==$type & ${ycond`num'}, $se_optfe
outreg2 $indvar1 using 2_`inst'_`num'_`real', cttop("RF IV") excel label addtext(County FE, Yes, Clustered SE, STATE) append

foreach endog0 in $endoglist {
global indvar1 pred_`real' 
global indvar2 $indvar1 yr_*

replace end= `endog0'_`num' 
replace `real'_end=pred_`real'*end
global endog end `real'_end
global instrum iv `real'_iv

xtivreg2 ${samp}_`real' $indvar2 $endog if codetype==$type & ${ycond`num'}, $se_optfe
outreg2 $indvar1 $endog using 2_`inst'_`num'_`real', cttop("OLS") excel label addtext(County FE, Yes, Clustered SE, STATE) append
/*
xtivreg2 end $indvar2 $instrum if codetype==$type & ${ycond`num'}, $se_optfe
outreg2 $indvar1 $instrum using 2_`inst'_`num'_`real', cttop("1st: `endog0'") excel label addtext(County FE, Yes, Clustered SE, STATE) append

xtivreg2 `real'_end $indvar2 $instrum if codetype==$type & ${ycond`num'}, $se_optfe
outreg2 $indvar1 $instrum using 2_`inst'_`num'_`real', cttop("1st: `real'_`endog0'") excel label addtext(County FE, Yes, Clustered SE, STATE) append
*/
xtivreg2 ${samp}_`real' $indvar2 ($endog = $instrum) if codetype==$type & ${ycond`num'}, $se_optfe
outreg2 $indvar1 $endog using 2_`inst'_`num'_`real', cttop("2nd: `endog0'") excel label addtext(County FE, Yes, Clustered SE, STATE) adds(F-Stat, e(widstat)) append
}

}
}
}
drop end iv *_end *_iv



