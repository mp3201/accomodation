/*****************************/
/********Industry************/
/*****************************/



/***************/
/*Output Industry composition*/
/***************/

bysort codetype area year: egen tot_empl=total(ind_empl)
bysort codetype area year: egen tot_wage=total(ind_wage)
gen share_empl=ind_empl/tot_empl
gen share_wage=ind_wage/tot_wage
replace share_empl=0 if share_empl==. & codetype==3 & $ycond4
replace share_wage=0 if share_wage==. & codetype==3 & $ycond4

sum lsd if codetype==3 & $ycond4
gen temp=`r(mean)'

cd $outsum
log using industry_table.log, replace text
tabstat share_empl if codetype==3 & $ycond4, by(ind2n) columns(statistics) statistics(n mean median max sd ) format(%8.3fc) save
tabstat share_empl if codetype==3 & $ycond4 & lsd>=temp, by(ind2n) columns(statistics) statistics(n mean median max sd ) format(%8.3fc) save
tabstat share_empl if codetype==3 & $ycond4 & lsd<temp, by(ind2n) columns(statistics) statistics(n mean median max sd ) format(%8.3fc) save
tabstat share_wage if codetype==3 & $ycond4, by(ind2n) columns(statistics) statistics(n mean median max sd ) format(%8.3fc) save
tabstat share_wage if codetype==3 & $ycond4 & lsd>=temp, by(ind2n) columns(statistics) statistics(n mean median max sd ) format(%8.3fc) save
tabstat share_wage if codetype==3 & $ycond4 & lsd<temp, by(ind2n) columns(statistics) statistics(n mean median max sd ) format(%8.3fc) save
log close
drop temp

log using industry_table2.log, replace text
forvalue i = 1/24 {
sum ind2n if ind_`i' & codetype==3 & $ycond4
}
log close








// generate simple recession table at the industry level -- not iving at fixed effects level now
cd $outind
gen recess_nber=(year==1991 | year==2001 | year==2002 | year==2008 | year==2009)
rename ind_estab in_estab
rename ind_wage in_wage
rename ind_empl in_empl

gen recess=.
forvalue i = 1/24 {
gen end_in`i'=.
gen iv_in`i'=.
gen recess_in`i'=.
gen recess_iv_in`i'=.
gen recess_end_in`i'=.
}

foreach real in empl wage /*estab*/ {
foreach inst in lsd drg {
foreach num of numlist 2/4 {


forvalue i = 1/24 {
replace iv_in`i'=`inst'_`num'*ind_`i'
}

global indvar1 iv_in*
global indvar2 $indvar1 yr_* ind_*
ivreg2 ${samp}_`real' $indvar2 if codetype==$type & ${ycond`num'}, $se_opt
outreg2 $indvar1 using 7_`inst'_`num'_`real', cttop("OLS") excel label addtext(Industry FE, Yes, County FE, No, Clustered SE, STATE) replace

foreach rec in recess_nber {
replace recess=`rec'

forvalue i = 1/24 {
replace recess_in`i'=recess*ind_`i'
replace recess_iv_in`i'=recess*iv_in`i'
}

global indvar1 recess_in*
global indvar2 $indvar1 yr_* ind_*
ivreg2 ${samp}_`real' $indvar2 if codetype==$type & ${ycond`num'}, $se_opt
outreg2 $indvar1 using 7_`inst'_`num'_`real', cttop("OLS") excel label addtext(Industry FE, Yes, County FE, No, Clustered SE, STATE) append

global indvar1 recess_in* recess_iv_in* iv_in*
global indvar2 $indvar1 yr_* ind_*
ivreg2 ${samp}_`real' $indvar2 if codetype==$type & ${ycond`num'}, $se_opt
outreg2 $indvar1 using 7_`inst'_`num'_`real', cttop("OLS") excel label addtext(Industry FE, Yes, County FE, No, Clustered SE, STATE) append


/*
foreach endog0 in $endoglist {
global indvar1 recess_in*
global indvar2 $indvar1 yr_* ind_*

forvalue i = 1/24 {
replace end_in`i'=`endog0'_`num'*ind_`i'
replace recess_end_in`i'=recess*end_in`i'
}

global endog end_in* recess_end_in*
global instrum iv_in* recess_iv_in*

ivreg2 ${samp}_`real' $indvar2 $endog if codetype==$type & ${ycond`num'}, $se_opt
outreg2 $indvar1 $endog using 7_`inst'_`num'_`real', cttop("OLS") excel label addtext(Industry FE, Yes, County FE, No, Clustered SE, STATE) append

ivreg2 ${samp}_`real' $indvar2 ($endog = $instrum) if codetype==$type & ${ycond`num'}, $se_opt
outreg2 $indvar1 $endog using 7_`inst'_`num'_`real', cttop("2nd: `endog0'") excel label addtext(Industry FE, Yes, County FE, No, Clustered SE, STATE) append
}
*/
}

*Fixed effects
forvalue i = 1/24 {
replace iv_in`i'=`inst'_`num'*ind_`i'
}

global indvar1 iv_in*
global indvar2 $indvar1 yr_* ind_*
areg ${samp}_`real' $indvar2 if codetype==$type & ${ycond`num'}, $se_optfe
outreg2 $indvar1 using 7_`inst'_`num'_`real', cttop("OLS") excel label addtext(Industry FE, Yes, County FE, Yes, Clustered SE, STATE) append

foreach rec in recess_nber {
replace recess=`rec'

forvalue i = 1/24 {
replace recess_in`i'=recess*ind_`i'
replace recess_iv_in`i'=recess*iv_in`i'
}

global indvar1 recess_in*
global indvar2 $indvar1 yr_* ind_*
areg ${samp}_`real' $indvar2 if codetype==$type & ${ycond`num'}, $se_optfe
outreg2 $indvar1 using 7_`inst'_`num'_`real', cttop("OLS") excel label addtext(Industry FE, Yes, County FE, Yes, Clustered SE, STATE) append

global indvar1 recess_in* recess_iv_in* iv_in*
global indvar2 $indvar1 yr_* ind_*
areg ${samp}_`real' $indvar2 if codetype==$type & ${ycond`num'}, $se_optfe
outreg2 $indvar1 using 7_`inst'_`num'_`real', cttop("OLS") excel label addtext(Industry FE, Yes, County FE, Yes, Clustered SE, STATE) append
}

}
}
}

drop end iv *_end_in* *_iv_in* recess_in* recess



// estimate growth sensitivities at industry level
gen end=.
gen iv=.
foreach real in empl wage /*estab*/ {
gen `real'_end=.
gen `real'_iv=.
}

gen d_dum=.
gen d_dum_iv=.
gen d_dum_end=.

forvalue i = 1/24 {
gen end_in`i'=.
gen iv_in`i'=.
foreach real in empl wage estab {
gen `real'_end_in`i'=.
gen `real'_iv_in`i'=.
gen `real'_in`i'=pi_`real'*ind_`i'
gen d_`real'_in`i'=.
gen d_`real'_end_in`i'=.
gen d_`real'_iv_in`i'=.
}
}


foreach real in empl wage /*estab*/ {
foreach inst in lsd drg {
foreach num of numlist 2/4 {

global indvar1 `real'_in*
global indvar2 $indvar1 yr_* ind_*
ivreg2 ${samp}_`real' $indvar2 if codetype==$type & ${ycond`num'}, $se_opt
outreg2 $indvar1 using 6_`inst'_`num'_`real', cttop("OLS") excel label addtext(Industry FE, Yes, County FE, No, Clustered SE, STATE) replace

forvalue i = 1/24 {
replace iv_in`i'=`inst'_`num'*ind_`i'
replace `real'_iv_in`i'=iv_in`i'*pi_`real'
}

global indvar1 `real'_in* iv_in* `real'_iv_in* 
global indvar2 $indvar1 yr_* ind_*
ivreg2 ${samp}_`real' $indvar2 if codetype==$type & ${ycond`num'}, $se_opt
outreg2 $indvar1 using 6_`inst'_`num'_`real', cttop("RF IV") excel label addtext(Industry FE, Yes, County FE, No, Clustered SE, STATE) append


foreach asymvar in neg_i n_i n_p_i {

forvalue i = 1/24 {
replace d_`real'_in`i'=`asymvar'_`real'*ind_`i'
replace d_`real'_end_in`i'=`asymvar'_`real'*end_in`i'
replace d_`real'_iv_in`i'=`asymvar'_`real'*iv_in`i'
}

global indvar1 `real'_in* d_`real'_in*
global indvar2 $indvar1 yr_* ind_*
ivreg2 ${samp}_`real' $indvar2 if codetype==$type & ${ycond`num'}, $se_opt
outreg2 $indvar1 using 6_`inst'_`num'_`real', cttop("OLS `asymvar'") excel label addtext(Industry FE, Yes, County FE, No, Clustered SE, STATE) append

global indvar1 `real'_in* d_`real'_in* iv_in* `real'_iv_in* d_`real'_iv_in*
global indvar2 $indvar1 yr_* ind_*
ivreg2 ${samp}_`real' $indvar2 if codetype==$type & ${ycond`num'}, $se_opt
outreg2 $indvar1 using 6_`inst'_`num'_`real', cttop("RF OLS `asymvar'") excel label addtext(Industry FE, Yes, County FE, No, Clustered SE, STATE) append
}


// County-year fixed effects
global indvar1 `real'_in*
global indvar2 $indvar1 yr_* ind_*
areg ${samp}_`real' $indvar2 if codetype==$type & ${ycond`num'}, $se_optfe
outreg2 $indvar1 using 6_`inst'_`num'_`real', cttop("OLS") excel label addtext(Industry FE, Yes, County FE, Yes, Clustered SE, STATE) append

forvalue i = 1/24 {
replace iv_in`i'=`inst'_`num'*ind_`i'
replace `real'_iv_in`i'=iv_in`i'*pi_`real'
}

global indvar1 `real'_in* iv_in* `real'_iv_in* 
global indvar2 $indvar1 yr_* ind_*
areg ${samp}_`real' $indvar2 if codetype==$type & ${ycond`num'}, $se_optfe
outreg2 $indvar1 using 6_`inst'_`num'_`real', cttop("RF IV") excel label addtext(Industry FE, Yes, County FE, Yes, Clustered SE, STATE) append


foreach asymvar in neg_i n_i n_p_i {

forvalue i = 1/24 {
replace d_`real'_in`i'=`asymvar'_`real'*ind_`i'
replace d_`real'_end_in`i'=`asymvar'_`real'*end_in`i'
replace d_`real'_iv_in`i'=`asymvar'_`real'*iv_in`i'
}

global indvar1 `real'_in* d_`real'_in*
global indvar2 $indvar1 yr_* ind_*
areg ${samp}_`real' $indvar2 if codetype==$type & ${ycond`num'}, $se_optfe
outreg2 $indvar1 using 6_`inst'_`num'_`real', cttop("OLS `asymvar'") excel label addtext(Industry FE, Yes, County FE, Yes, Clustered SE, STATE) append

global indvar1 `real'_in* d_`real'_in* iv_in* `real'_iv_in* d_`real'_iv_in*
global indvar2 $indvar1 yr_* ind_*
areg ${samp}_`real' $indvar2 if codetype==$type & ${ycond`num'}, $se_optfe
outreg2 $indvar1 using 6_`inst'_`num'_`real', cttop("RF OLS `asymvar'") excel label addtext(Industry FE, Yes, County FE, Yes, Clustered SE, STATE) append
}


}
}
}


drop end iv *_end_in* *_iv_in* d_*_in*

