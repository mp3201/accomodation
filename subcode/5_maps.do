/*
if "`c(os)'" == "Unix" {
	global location "/home/rcemxp06/accomodation"
	global outreg "/home/rcemxp06/accomodation/output/regressions"
	global outsum "/home/rcemxp06/accomodation/output/summaries"
	global outint "/home/rcemxp06/accomodation/output/interactions"
	global outfig "/home/rcemxp06/accomodation/output/figures"
	global data "/home/rcemxp06/accomodation/data"
	global subcode "/home/rcemxp06/accomodation/code/subcode"
	adopath ++ "/home/rcemxp06/ado"
}
else if "`c(os)'" == "Windows" {
	global location "R:/accomodation"
	global outreg "R:/accomodation/output/regressions"
	global outsum "R:/accomodation/output/summaries"
	global outint "R:/accomodation/output/interactions"
	global outfig "R:/accomodation/output/figures"
	global data "R:/accomodation/data"
	global subcode "R:/accomodation/code/subcode"
	adopath ++ "R:/ado"
}

set more off
*/
cd $outsum
insheet using sample_counties.csv, comma clear

rename area location
sort location 
gen insamp=1 

collapse (max) insamp, by(location)
cd $data
cd maps
save map_temp, replace

shp2dta using gz_2010_us_050_00_5m, database("countydb") coordinates("countycoord") genid(_ID) replace
shp2dta using gz_2010_us_040_00_5m, database("statedb") coordinates("statecoord") genid(_ID) replace

global ifstate1 if STATE!="15" & STATE~="02" & STATE~="43" & STATE~="72"

use statecoord, clear
merge m:1 _ID using statedb, keepusing(STATE)
keep $ifstate1
drop _merge
save statecoord2, replace

use countydb, clear
gen state2=real(STATE)
gen county2=real(COUNTY)
gen location=state2*1000+county2
sort location
merge location using map_temp, sort unique
drop _merge


replace insamp=0 if insamp==.

gen insamp2=insamp if insamp==1

/*Full US Maps*/

spmap insamp using countycoord $ifstate1, id(_ID) fcolor(Blues2) ndsize(vvthin) polygon(data(statecoord2) osize(medium) ocolor(black)) legenda(off)
cd $outmap
graph export map_sample.eps, replace






