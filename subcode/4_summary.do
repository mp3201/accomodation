
/****/
*Where does asymmetry come from?
*Idenify episodes of high shock days;
cd $outsum


gen temp=1 if $ycond4 & pred_empl~=. 
bysort codetype year: egen count=total(temp) if $ycond4
drop temp
foreach asymvar in neg n n_p n_ts {
gen `asymvar'_temp=1 if $ycond4 & `asymvar'_empl~=. & `asymvar'_empl~=0
bysort codetype year: egen `asymvar'_count1=total(`asymvar'_temp) if $ycond4
bysort codetype: egen `asymvar'_count2=total(`asymvar'_temp) if $ycond4
gen pct1_`asymvar'=`asymvar'_count1/count
gen pct2_`asymvar'=`asymvar'_count1/`asymvar'_count2
drop `asymvar'_temp
}


log using recess_table.log, replace text
*PCT1 is share of counties with a below year for a given year PCT2 is percent of below average county-years that show up in that year
tabstat pct1_n_p pct2_n_p pct1_n pct2_n if codetype==3 & $ycond4, by(year) 
log close

*Create reces variables using NBER and the percent of below average counties in sample;
gen recess_nber=(year==1991 | year==2001 | year==2002 | year==2008 | year==2009)
gen recess_n_p=(pct1_n_p>.6)

gen popt=pop/1000
gen emplt=cemplq/1000
gen waget=cwageq/1000
/***************/
/*Sum table*/
/***************/
local vlist ${samp}_empl ${samp}_wage pred_empl pred_wage lsd drg $endoglist popt numbank numbr emplt waget
local statlist n mean median max sd 

foreach num of numlist 1/4 {
log using "summary_`num'", replace text

tabstat `vlist' if codetype==$type & ${ycond`num'} & (${samp}_empl~=. | ${samp}_wage~=.), columns(statistics) statistics(`statlist') format(%9.2fc) save

log close
}

/***************/
/*Output counties*/
/***************/

cd $outsum
preserve
gen keepvar=1 if codetype==$type & $ycond4
collapse (sum) keepvar, by(area)
drop if keepvar==. | keepvar==0

outsheet using sample_counties.csv, replace comma

restore


/***************/
/*Analyze instruments*/
/***************/

cd $outfig
set scheme lean1

foreach num of numlist 1/4 {
foreach var in rdp lad dwld {
areg `var' yr_* if codetype==$type & ${ycond`num'}, absorb(pid)
predict res`num'_`var' if e(sample), r
reg `var' yr_* if codetype==$type & ${ycond`num'}
predict res2`num'_`var' if e(sample), r
gen ptile_`num'_`var'=.
cap drop tag
gen tag = (res`num'_`var'~=.)
sort codetype tag res`num'_`var'
by codetype tag: replace ptile_`num'_`var' = floor((_n-1)/_N * 10) + 1 if tag~=. & codetype==$type & ${ycond`num'}
drop tag
gen ptile2_`num'_`var'=.
cap drop tag
gen tag = (res2`num'_`var'~=.)
sort codetype tag res2`num'_`var'
by codetype tag: replace ptile2_`num'_`var' = floor((_n-1)/_N * 10) + 1 if tag~=. & codetype==$type & ${ycond`num'}
drop tag
}
}


gen ptile_lsd=.
foreach typ in 1 2 3 {
cap drop tag
gen tag = (lsd_`typ'~=.)
sort codetype tag lsd
by codetype tag: replace ptile_lsd = floor((_n-1)/_N *100) + 1 if tag~=. & codetype==`typ'
drop tag
}


foreach num of numlist 1/4 {
foreach var in rdp lad dwld {
foreach date in madate inter intra mbhc {
preserve
keep if codetype==$type & ${ycond`num'}
collapse (mean) res`num'_`var' res2`num'_`var', by(ys`date')
lowess res`num'_`var' ys`date'
graph export estplot_`num'_`var'_`date'.eps, replace
twoway scatter res`num'_`var' ys`date'
graph export resplot_`num'_`var'_`date'.eps, replace
lowess res2`num'_`var' ys`date'
graph export estplot2_`num'_`var'_`date'.eps, replace
twoway scatter res2`num'_`var' ys`date'
graph export resplot2_`num'_`var'_`date'.eps, replace
restore
}
}
}

foreach num of numlist 3 {
foreach var in lad rdp dwld {
preserve
keep if codetype==$type & ${ycond`num'}
collapse (mean) lsd res`num'_`var' res2`num'_`var', by(ptile_lsd)
lowess lsd ptile_lsd
graph export estplot_`num'_lsd_psld.eps, replace
lowess res`num'_`var' ptile_lsd
graph export estplot_`num'_`var'_psld.eps, replace
twoway scatter res`num'_`var' ptile_lsd, xtitle(Dereg Percentile) ytitle(Residuals)
graph export resplot_`num'_`var'_plsd.eps, replace
lowess res2`num'_`var' ptile_lsd
graph export estplot2_`num'_`var'_plsd.eps, replace
twoway scatter res2`num'_`var' ptile_lsd, xtitle(Dereg Percentile) ytitle(Residuals)
graph export resplot2_`num'_`var'_plsd.eps, replace
restore
}
}


foreach num of numlist 3 {
foreach var in lad rdp dwld {
preserve
keep if codetype==$type & ${ycond`num'}
collapse (mean) lsd, by(ptile_`num'_`var')
lowess ptile_`num'_`var' lsd
graph export estplot_`num'_`var'_lsd.eps, replace
twoway scatter ptile_`num'_`var' lsd
graph export resplot_`num'_`var'_lsd.eps, replace
restore
preserve
keep if codetype==$type & ${ycond`num'}
collapse (mean) lsd, by(ptile2_`num'_`var')
lowess ptile2_`num'_`var' lsd
graph export estplot2_`num'_`var'_lsd.eps, replace
twoway scatter ptile2_`num'_`var' lsd
graph export resplot2_`num'_`var'_lsd.eps, replace
restore
}
}




*generate simple recession table
cd $outreg

gen end=. 
gen iv=.
gen recess_iv=.
gen recess_end=.

foreach real in empl wage /*estab*/ {
foreach inst in lsd drg {
foreach num of numlist 1/4 {

replace iv=`inst'_`num'

global indvar1 iv 
global indvar2 $indvar1 yr_*
ivreg2 ${samp}_`real' $indvar2 if codetype==$type & ${ycond`num'}, $se_opt
outreg2 $indvar1 using 4_`inst'_`num'_`real', cttop("OLS") excel label addtext(County FE, No, Clustered SE, STATE) replace

foreach rec in recess_nber recess_n_p {

global indvar1 `rec'
global indvar2 $indvar1 yr_*
ivreg2 ${samp}_`real' $indvar2 if codetype==$type & ${ycond`num'}, $se_opt
outreg2 $indvar1 using 4_`inst'_`num'_`real', cttop("OLS") excel label addtext(County FE, No, Clustered SE, STATE) append

replace recess_iv=`rec'*iv

global indvar1 `rec' iv recess_iv
global indvar2 $indvar1 yr_*
ivreg2 ${samp}_`real' $indvar2 if codetype==$type & ${ycond`num'}, $se_opt
outreg2 $indvar1 using 4_`inst'_`num'_`real', cttop("OLS") excel label addtext(County FE, No, Clustered SE, STATE) append

foreach endog0 in $endoglist {
global indvar1 `rec'
global indvar2 $indvar1 yr_*

replace end=`endog0'_`num' 
replace recess_end=`rec'*end
global endog end recess_end
global instrum iv recess_iv

ivreg2 ${samp}_`real' $indvar2 $endog if codetype==$type & ${ycond`num'}, $se_opt
outreg2 $indvar1 $endog using 4_`inst'_`num'_`real', cttop("OLS") excel label addtext(County FE, No, Clustered SE, STATE) append
/*
ivreg2 end $indvar2 $instrum if codetype==$type & ${ycond`num'}, $se_opt
outreg2 $indvar1 $instrum using 2_`inst'_`num'_`real', cttop("1st: `endog0'") excel label addtext(County FE, No, Clustered SE, STATE) append

ivreg2 `real'_end $indvar2 $instrum if codetype==$type & ${ycond`num'}, $se_opt
outreg2 $indvar1 $instrum using 2_`inst'_`num'_`real', cttop("1st: `real'_`endog0'") excel label addtext(County FE, No, Clustered SE, STATE) append
*/
ivreg2 ${samp}_`real' $indvar2 ($endog = $instrum) if codetype==$type & ${ycond`num'}, $se_opt
outreg2 $indvar1 $endog using 4_`inst'_`num'_`real', cttop("2nd: `endog0'") excel label addtext(County FE, No, Clustered SE, STATE) adds(F-Stat, e(widstat)) append
}
}

*Fixed effects
replace iv=`inst'_`num'

global indvar1 iv
global indvar2 $indvar1 yr_*
xtivreg2 ${samp}_`real' $indvar2 if codetype==$type & ${ycond`num'}, $se_optfe
outreg2 $indvar1 using 4_`inst'_`num'_`real', cttop("OLS") excel label addtext(County FE, Yes, Clustered SE, STATE) append


foreach rec in recess_nber recess_n_p {

global indvar1 `rec'
global indvar2 $indvar1 yr_*
xtivreg2 ${samp}_`real' $indvar2 if codetype==$type & ${ycond`num'}, $se_optfe
outreg2 $indvar1 using 4_`inst'_`num'_`real', cttop("OLS") excel label addtext(County FE, Yes, Clustered SE, STATE) append

replace recess_iv=`rec'*iv

global indvar1 `rec' iv recess_iv
global indvar2 $indvar1 yr_*
xtivreg2 ${samp}_`real' $indvar2 if codetype==$type & ${ycond`num'}, $se_optfe
outreg2 $indvar1 using 4_`inst'_`num'_`real', cttop("OLS") excel label addtext(County FE, Yes, Clustered SE, STATE) append

foreach endog0 in $endoglist {
global indvar1 `rec'
global indvar2 $indvar1 yr_*

replace end=`endog0'_`num' 
replace recess_end=`rec'*end
global endog end recess_end
global instrum iv recess_iv

xtivreg2 ${samp}_`real' $indvar2 $endog if codetype==$type & ${ycond`num'}, $se_optfe
outreg2 $indvar1 $endog using 4_`inst'_`num'_`real', cttop("OLS") excel label addtext(County FE, Yes, Clustered SE, STATE) append
/*
xtivreg2 end $indvar2 $instrum if codetype==$type & ${ycond`num'}, $se_optfe
outreg2 $indvar1 $instrum using 4_`inst'_`num'_`real', cttop("1st: `endog0'") excel label addtext(County FE, Yes, Clustered SE, STATE) append

xtivreg2 recess_end $indvar2 $instrum if codetype==$type & ${ycond`num'}, $se_optfe
outreg2 $indvar1 $instrum using 4_`inst'_`num'_`real', cttop("1st: recess_`endog0'") excel label addtext(County FE, Yes, Clustered SE, STATE) append
*/
xtivreg2 ${samp}_`real' $indvar2 ($endog = $instrum) if codetype==$type & ${ycond`num'}, $se_optfe
outreg2 $indvar1 $endog using 4_`inst'_`num'_`real', cttop("2nd: `endog0'") excel label addtext(County FE, Yes, Clustered SE, STATE) adds(F-Stat, e(widstat)) append
}
}

}
}
}

drop end iv *_end *_iv 


