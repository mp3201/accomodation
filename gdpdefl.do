if "`c(os)'" == "Unix" {
	global location "/san/RDS/Work/fif/b1mxp07/Matt"
	adopath ++ "/san/RDS/Work/fif/b1mxp07/my_ados"
}
else if "`c(os)'" == "Windows" {
	global location "//RB/B1/NYRESAN/RDS/Work/fif/b1mxp07/Matt"
	adopath ++ "//RB/B1/NYRESAN/RDS/Work/fif/b1mxp07/my_ados"
}


import excel "$location/data/misc/GDPDEF.xls", clear firstrow
gen date2 = qofd(date)
format %tq date2
drop date
ren date2 date

replace gdpdefl = gdpdefl/100

save "$location/data/misc/GDPDEF.dta", replace
