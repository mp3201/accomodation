*This program cleans the the combined NAICS/SIC data
	*1. Drops data before 1979 (since we start in 1980)
	*2. For any industry that was suppressed at any time drop that county from the time series for that county
	*3. Recalculate national and county values
	*4. Generate the bartik instruments

//This section creates county level instruments 
foreach i in 3 {
	set more off
	global i=3
	use "/san/RDS/Work/fif/b1mxp07/Matt/data/naics_sic_`i'.dta",clear
	drop if date<tq(1979q1) & codetype == 2
	
	merge m:1 date using "/san/RDS/Work/fif/b1mxp07/Matt/data/misc/GDPDEF.dta"
	assert _merge!=1
	drop _merge
	
	replace wageq = wageq/gdpdefl
	
	bys codetype area industry (date): egen supanyt = min(statq)
	label values supanyt status
	
	gen drpav = 1 if supanyt==0 & statq==0
	replace drpav = 2 if supanyt==0 & statq==1
	replace drpav = 3 if supanyt==1
	
	label define drptype 1 "Suppressed & Dropped" 2 "Not Suppressed & Dropped" 3 "Not Dropped"
	label values drpav drptype
	tab supanyt codetype
	
	//Dropping county-quarter-industry observation that was suppressed at any time
	drop if drpav!=3
	
	//Calculate national values 
	preserve
		collapse (sum) nemplq=emplq nestabq=estabq nwageq=wageq, by(date codetype industry)
		//Calculating year on year growth date
		egen typeind = group(codetype industry)
		tsset typeind date
		foreach var in nemplq nestabq nwageq{
			gen grt`var' = (`var' - L4.`var')/L4.`var'*100
		}
		save "/san/RDS/Work/fif/b1mxp07/Matt/data/nat_naics_sic_`i'.dta", replace
	restore
		
	//Keep actual county sums for regressions
	preserve
		collapse (sum) cemplq=emplq cestabq=estabq cwageq=wageq, by(area date codetype)
		egen typearea = group(codetype area)
		tsset typearea date
		foreach var in cemplq cestabq cwageq{
			gen grt`var' = (`var' - L4.`var')/L4.`var'*100
		}
		save "/san/RDS/Work/fif/b1mxp07/Matt/data/cnty_naics_sic_`i'.dta", replace
	restore
	
	//Merging in county and national data
	merge m:1 codetype industry date using "/san/RDS/Work/fif/b1mxp07/Matt/data/nat_naics_sic_`i'.dta"
	drop _merge
	sort codetype date area industry
	merge m:1 codetype area date using "/san/RDS/Work/fif/b1mxp07/Matt/data/cnty_naics_sic_`i'.dta"
	drop _merge
	
	egen typeareaind = group(codetype area industry)
	tsset typeareaind date
	
	foreach var in emplq estabq wageq{
		gen double p`var' = `var'/c`var'
	}
	
	//Generating instruments
	sort typeareaind date
	foreach var in emplq estabq wageq{
		gen double ins`var' = L4.p`var'*grtn`var'
	}
	
	//save "/san/RDS/Work/fif/b1mxp07/Matt/data/clean_naics_sic_`i'", replace
		
	//preserve
		collapse (sum) ins* , by(area date codetype)
		local i = 3
		sort codetype area date
		merge 1:1 codetype are date using "/san/RDS/Work/fif/b1mxp07/Matt/data/cnty_naics_sic_`i'.dta" 
		foreach var in emplq estabq wageq{
			replace ins`var' = . if grtc`var'==.
		}
		save "/san/RDS/Work/fif/b1mxp07/Matt/data/naics_sic_inscnty_`i'.dta", replace
	//restore 
	
	
	//local i = 3
	//use "/san/RDS/Work/fif/b1mxp07/Matt/data/naics_sic_inscnty_`i'.dta", clear
	
	
	preserve
		keep if (codetype==1 & date>=tq(1991q1)) | (codetype==2 & date<tq(1991q1))
		replace codetype = 3 
		tempfile combined
		save `combined', replace
	restore
	
	append using `combined'
	
	
	drop typearea
	egen typearea = group(codetype area)
	
	label def types 1 "NAICS" 2 "SIC" 3 "Combined", modify
	label values codetype types
	
	save, replace
	
}
*

//This section creates county-industry instruments (instruments vary by industry within a county)
foreach i in 3 {
	set more off
	local i=3
	use "/san/RDS/Work/fif/b1mxp07/Matt/data/naics_sic_`i'.dta",clear
	drop if date<tq(1979q1) & codetype == 2
	
	merge m:1 date using "/san/RDS/Work/fif/b1mxp07/Matt/data/misc/GDPDEF.dta"
	assert _merge!=1
	drop _merge
	
	replace wageq = wageq/gdpdefl
		
	bys codetype area industry (date): egen supanyt = min(statq)
	label values supanyt status

	gen drpav = 1 if supanyt==0 & statq==0
	replace drpav = 2 if supanyt==0 & statq==1
	replace drpav = 3 if supanyt==1
	
	label define drptype 1 "Suppressed & Dropped" 2 "Not Suppressed & Dropped" 3 "Not Dropped"
	label values drpav drptype
	tab supanyt codetype
	
	//Dropping county-quarter-industry observation that was suppressed at any time
	drop if drpav!=3
	
	//constructing 2 digit industry
	gen industry2dig = substr(industry, 1, 2) if codetype==1
	replace industry2dig = substr(industry, 5, 2) if codetype==2
	
	//Calculate national wages
	preserve
		collapse (sum) nemplq=emplq nestabq=estabq nwageq=wageq, by(date codetype industry)
		//Calculating year on year growth date
		egen typeind = group(codetype industry)
		tsset typeind date
		foreach var in nemplq nestabq nwageq{
			gen grt`var' = (`var' - L4.`var')/L4.`var'*100
		}
		save "/san/RDS/Work/fif/b1mxp07/Matt/data/nat_naics_sic_`i'.dta", replace
	restore
	
	//Keep actual county sums for regressions
	preserve
		collapse (sum) c2emplq=emplq c2estabq=estabq c2wageq=wageq, by(area date codetype industry2dig)
		egen typeareaind2 = group(codetype area industry2dig)
		tsset typeareaind2 date
		foreach var in c2emplq c2estabq c2wageq{
			gen grt`var' = (`var' - L4.`var')/L4.`var'*100
		}
		drop typeareaind2 
		save "/san/RDS/Work/fif/b1mxp07/Matt/data/cnty_naics_sic_2.dta", replace
	restore
	
	//Merging in county and national data
	merge m:1 codetype industry date using "/san/RDS/Work/fif/b1mxp07/Matt/data/nat_naics_sic_`i'.dta"
	drop _merge
	merge m:1 codetype area industry2dig date using "/san/RDS/Work/fif/b1mxp07/Matt/data/cnty_naics_sic_2.dta"
	drop _merge
	
	egen typeareaind = group(codetype area industry)
	tsset typeareaind date
	
	foreach var in emplq estabq wageq{
		gen double p2`var' = `var'/c2`var'
	}
	
	//Generating instruments
	sort typeareaind date
	foreach var in emplq estabq wageq{
		gen double insind`var' = L4.p2`var'*grtn`var'
	}
	
	collapse (sum) insind* , by(area date codetype industry2dig)
	sort codetype area industry2dig date
	merge 1:1 codetype area industry2dig date using "/san/RDS/Work/fif/b1mxp07/Matt/data/cnty_naics_sic_2.dta" 
	foreach var in emplq estabq wageq{
		replace insind`var' = . if grtc2`var'==.
	}
	save "/san/RDS/Work/fif/b1mxp07/Matt/data/naics_sic_insindcnty.dta", replace
			
	preserve
		keep if (codetype==1 & date>=tq(1991q1)) | (codetype==2 & date<tq(1991q1))
		replace codetype = 3 
		tempfile combined
		save `combined', replace
	restore
	
	append using `combined'
		
	label def types 1 "NAICS" 2 "SIC" 3 "Combined", modify
	label values codetype types
	
	ren insindemplq insind2emplq
	ren insindestabq insind2estabq
	ren insindwageq insind2wageq
	
	order codetype area industry2dig date insind* grtc2* 
	
	save, replace
	
}
*
