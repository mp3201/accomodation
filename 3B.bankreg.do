if "`c(os)'" == "Unix" {
	global location "/san/RDS/Work/fif/b1mxp07/Matt"
	adopath ++ "/san/RDS/Work/fif/b1mxp07/my_ados"
}
else if "`c(os)'" == "Windows" {
	global location "//RB/B1/NYRESAN/RDS/Work/fif/b1mxp07/Matt"
	adopath ++ "//RB/B1/NYRESAN/RDS/Work/fif/b1mxp07/my_ados"
}
set more off

use "$location/data/misc/link.dta", clear
duplicates drop lpermco, force
keep lpermco linkdt linkenddt
ren lpermco permco
gen dt_start_crsp = qofd(linkdt)
gen dt_end_crsp = qofd(linkenddt)
format %tq dt_start_crsp dt_end_crsp
drop linkdt linkenddt
sort permco
tempfile links
save `links', replace

use "$location/data/misc/crsp_20140331.dta", clear
drop notice
drop if name==""
duplicates tag entity, gen(dup)
gsort entity -dt_start
by entity: gen dt_end_l1 = dt_end[_n-1]
replace dt_end = dt_end_l1 if dt_end_l1!=.
drop if dt_end_l1==. & dup==1

tostring dt_start dt_end, replace
gen tmp_start = date(dt_start, "YMD")
replace tmp_start = qofd(tmp_start)
format %tq tmp_start

gen tmp_end = date(dt_end, "YMD")
replace tmp_end = qofd(tmp_end)
format %tq tmp_end

drop dt_start dt_end
ren tmp_start dt_start
ren tmp_end dt_end

merge m:1 permco using `links'
drop if _merge==2
replace dt_start = dt_start_crsp if dt_start==tq(1990q1) & dt_start_crsp<dt_start & dt_start_crsp!=. & _merge==3

ren entity hhrssd_dup
drop dt_end_l1 dup _merge

tempfile public
save `public', replace

use "$location/data/bankloans.dta", clear
capture drop _merge
merge m:1 hhrssd_dup using `public'
drop if _merge==2

//Panel format
egen panelid = group(codetype hhrssd_dup)
xtset panelid year

//Generate pulic dummy
gen public = 0 if _merge==1
replace public = 1 if _merge==3 & date>=dt_start & date<=dt_end
replace public = 0 if public==.
drop _merge

//Generate lhs variable
gen l1_assets = l1.assets
gen log_l1_assets = log(l1.assets)
label var log_l1_assets "Lagged Log Assets"

gen nondeposit = liab_tot - tot_deposit
label var nondeposit "Non Deposit Financing"

gen nondep_perc = (liab_tot - tot_deposit)/l1_assets
replace nondep_perc = 0 if nondep_perc<0 & nondep_perc!=.
label var nondep_perc "Non Deposit Financing/Lagged Assets"

gen log_br_cnt = log(br_cnt+1)
label var log_br_cnt "Log Branch Count"


//Change zero loans to missing to avoid high percentage change
foreach var in ln_tot ln_cc ln_re ln_heloc ln_rre ln_cre ln_ci {     
	replace `var' = . if `var'==0
}

*Changes relative to assets
foreach var in ln_sbl_amt ln_sbl_ci_amt ln_sbl_cre_amt ln_tot ln_cc ln_re ln_heloc ln_rre ln_cre ln_othre ln_ci {     
	gen d_`var'_ass = ((`var'-l1.`var')/abs(l1.assets))*100
	label var d_`var'_ass "Annual Change in `var'/Assets"
}
*

*Percentage change
foreach var in ln_tot ln_cc ln_re ln_heloc ln_rre ln_cre ln_ci ln_othre ln_sbl_amt ln_sbl_ci_a1 ln_sbl_ci_a2 ln_sbl_ci_a3 ln_sbl_ci_amt ln_sbl_cre_a1 ln_sbl_cre_a2 ln_sbl_cre_a3 ln_sbl_cre_amt {     
	gen d_`var'_perc = ((`var'-l1.`var')/abs(l1.`var'))*100
	label var d_`var'_perc "Annual % Change in `var'"
}
*


foreach var in ln_tot ln_cc ln_re ln_heloc ln_rre ln_cre ln_ci ln_sbl_amt ln_sbl_ci_amt ln_sbl_cre_amt {
	gen log_`var' = log(`var')
	gen d_log_`var' = log_`var' - l1.log_`var' 
}
*

*Change in funding
foreach var in tot_deposit assets nondeposit equity {     
	gen d_`var'_perc = ((`var'-l1.`var')/abs(l1.`var'))*100
	label var d_`var'_perc "Annual % Change in `var'"
}
*

/*
bys codetype hhrssd_dup (date): gen rank = _n
foreach var in ln_sbl_ci_a1 ln_sbl_ci_a2 ln_sbl_ci_a3 ln_sbl_amt ln_sbl_ci_amt ln_sbl_cre_a1 ln_sbl_cre_a2 ln_sbl_cre_a3 ln_sbl_cre_amt {
	replace d_`var'_perc = 0 if d_`var'_perc == . & year>1993 
}


foreach var in ln_sbl_ci_a1 ln_sbl_ci_a2 ln_sbl_ci_a3 ln_sbl_amt ln_sbl_ci_amt ln_sbl_cre_a1 ln_sbl_cre_a2 ln_sbl_cre_a3 ln_sbl_cre_amt {
	replace d_`var'_perc = . if rank==1
}


foreach var in ln_sbl_ci_a1 ln_sbl_ci_a2 ln_sbl_ci_a3 ln_sbl_amt ln_sbl_ci_amt ln_sbl_cre_a1 ln_sbl_cre_a2 ln_sbl_cre_a3 ln_sbl_cre_amt {
	replace d_`var'_perc = . if year<=1993
}
*/


//Generate rhs control variables
gen log_assets = log(assets)
label var log_assets "Log Assets"

gen t1_rbc_rat = (tier1_rbc/rwa)*100
label var t1_rbc_rat "T1 Risk Base Capital Rat"

gen t1_comm_rat = (tier1_comm/rwa)*100
label var t1_comm_rat "T1 Common Rat"

gen leverage = (liab_tot/assets)*100
label var leverage "Leverage - Liab/Assets"

gen dep_assets = (tot_deposit/assets)*100
label var dep_assets "Deposits/Assets"

gen ln_assets = (ln_tot/assets)*100
label var ln_assets "Total Loans/Assets"

label var bank_id "Bank Dummy"
label var bankhhi "HHI Exposure"

//Correcting instrument at the beginning of the sample
//LOOK THIS OVER WHY DO I NOT HAVE 1991 for NAICS
drop if year==1991 & codetype==1
drop if year==1980 & codetype==2
drop if year==1980 & codetype==3

//Correcting 
foreach var in insemplqhh insestabqhh inswageqhh{
	replace `var' = . if `var'==0
}

egen cdtypeyear = group(codetype year)

summ cdtypeyear

drop if codetype!=1

/*
// Generate changes in assets
gen abs_d_assets_perc = abs(d_assets_perc)
levelsof year if codetype==1, local(years)
foreach yr of local years{
	summ abs_d_assets_perc if year==`yr', d
	drop if abs_d_assets_perc >= `r(p99)' & year==`yr'
	//drop if d_assets_perc <= `r(p5)' & year==`yr'
}
*/

bys hhrssd_dup (year): egen max_year = max(year)
bys hhrssd_dup (year): egen min_year = min(year)
gen num_obs_pred = max_year - min_year + 1
bys hhrssd_dup (year): gen num_obs_act = _N
gen same_count = (num_obs_pred == num_obs_act)
drop if same_count!=1
drop max_year min_year num_obs*

gen d_ln_re_perc_old =  d_ln_re_perc 
gen d_ln_rre_perc_old =  d_ln_rre_perc
gen d_ln_cre_perc_old = d_ln_cre_perc
gen d_ln_othre_perc_old = d_ln_othre_perc

capture drop temp_ln ratio
egen temp_ln = rowtotal(ln_rre ln_cre ln_ci ln_cc ln_othcons ln_allother)
gen ratio = temp_ln/ln_tot
summ ratio if year>1990, d
summ ratio if year>1996, d
summ ratio if year>2000, d

summ cdtypeyear
//forval i  = 1/`r(max)' {
	foreach type in 1 2 3 { 
			foreach var in insemplqhh insestabqhh inswageqhh d_tot_deposit_perc d_assets_perc d_nondeposit_perc d_equity_perc d_ln_sbl_ass d_ln_sbl_ci_ass d_ln_sbl_cre_ass d_ln_re_ass d_ln_rre_ass d_ln_cre_ass d_ln_othre_ass ///
			d_ln_tot_perc d_ln_cc_perc d_ln_re_perc d_ln_rre_perc d_ln_cre_perc d_ln_othre_perc d_ln_ci_perc d_ln_sbl_amt_perc d_ln_sbl_ci_a1_perc d_ln_sbl_ci_a2_perc d_ln_sbl_ci_a3_perc d_ln_sbl_ci_amt_perc d_ln_sbl_cre_a1_perc d_ln_sbl_cre_a2_perc d_ln_sbl_cre_a3_perc d_ln_sbl_cre_amt_perc ///
			d_log_ln_tot d_log_ln_cc d_log_ln_re d_log_ln_heloc d_log_ln_rre d_log_ln_cre d_log_ln_ci d_log_ln_sbl_amt d_log_ln_sbl_ci_amt d_log_ln_sbl_cre_amt insemplqhh gavg_insemplqhh lavg_insemplqhh insestabqhh gavg_insestabqhh lavg_insestabqhh inswageqhh gavg_inswageqhh lavg_inswageqhh  {
			
				/*
				if(`i'!=1 & `i'!=22){
					capture summ `var' if cdtypeyear==`i' , d
					capture local p01 = `r(p1)'
					capture local p99 = `r(p99)'
					capture replace `var' = . if `var'<`p01' & cdtypeyear==`i' 
					capture replace `var' = . if `var'>`p99' & cdtypeyear==`i'
					dis `i'
				}
				*/	
				
				/*
				
				capture winsor `var' if /*cdtypeyear==`i'*/ codetype==`type', gen(winsortemp) p(0.01)
				capture replace `var' = winsortemp if /*cdtypeyear==`i'*/ codetype==`type'
				capture drop winsortemp
				*/
				
				
				capture summ `var' if codetype==`type' , d
				capture local p01 = `r(p1)'
				capture replace `var' = . if `var' < `p01' & `var' !=. & codetype==`type'
				capture local p99 = `r(p99)'
				capture replace `var' = . if `var' > `p99' & `var'!=. & codetype==`type'
				
				
			}
	}
//}


/*

set more off
foreach pred in empl estab wage {
	if ("`pred'" == "empl"){
		local crhsti "Employment"
	}
	else if ("`pred'" == "estab"){
		local crhsti "Establishments"
	}
	else if ("`pred'" == "wage"){
		local crhsti "Wage"
	}
	
	summ ins`pred'qhh if codetype==1
	gen b_naics = (ins`pred'qhh<`r(mean)' & ins`pred'qhh!=.) if codetype==1
	gen a_naics = (ins`pred'qhh>=`r(mean)' & ins`pred'qhh!=.) if codetype==1
	summ ins`pred'qhh if codetype==2
	gen b_sic = (ins`pred'qhh<`r(mean)' & ins`pred'qhh!=.) if codetype==2
	gen a_sic = (ins`pred'qhh>=`r(mean)' & ins`pred'qhh!=.) if codetype==2
	
	summ ins`pred'qhh if codetype==3
	gen b_naics_sic = (ins`pred'qhh<`r(mean)' & ins`pred'qhh!=.) if codetype==3
	gen a_naics_sic = (ins`pred'qhh>=`r(mean)' & ins`pred'qhh!=.) if codetype==3
	
	gen bins`pred'qhh = b_naics if codetype==1
	replace bins`pred'qhh = b_sic if codetype==2
	replace bins`pred'qhh = b_naics_sic if codetype==3
	label var bins`pred'qhh "Below Average Shock Dummy"
	
	gen ains`pred'qhh = a_naics if codetype==1
	replace ains`pred'qhh = a_sic if codetype==2
	replace ains`pred'qhh = a_naics_sic if codetype==3
	label var ains`pred'qhh "Above Average Shock Dummy"
	
	gen gavg_ins`pred'qhh = ins`pred'qhh*ains`pred'qhh
	label var gavg_ins`pred'qhh "Above Avgx`crhsti'"
	
	gen lavg_ins`pred'qhh = ins`pred'qhh*bins`pred'qhh
	label var lavg_ins`pred'qhh "Below Avgx`crhsti'"
	
	drop b_naics a_naics b_sic a_sic b_naics_sic a_naics_sic
	
}
*/

set more off
//Small Bank Dummy is Time Varying
summ cdtypeyear
gen largebank = .
gen smallbank = .
forval i  = 1/`r(max)' {
	summ log_l1_assets if cdtypeyear==`i' , d
	local plarge = `r(mean)'
	di `i'
	replace largebank = 1 if cdtypeyear==`i' & log_l1_assets>=`plarge' & log_l1_assets!=.
	replace largebank = 0 if cdtypeyear==`i' & largebank==. & log_l1_assets!=.
	replace smallbank = 1 if cdtypeyear==`i' & log_l1_assets<`plarge' & log_l1_assets!=.
	replace smallbank = 0 if cdtypeyear==`i' & smallbank==. & log_l1_assets!=.
}

label var largebank "Large Dummy"

foreach var in insemplqhh insestabqhh inswageqhh {
	gen `var'_public = `var'*public
	label var `var'_public "`var'*Public Dummy"
}

foreach var in insemplqhh insestabqhh inswageqhh {
	gen `var'_multi = `var'*multistate
	label var `var'_multi "`var'*Multistate Dummy"
}

foreach var in insemplqhh insestabqhh inswageqhh {
	gen `var'_nondep = `var'*nondep_perc
	label var `var'_nondep "`var'*Non Deposit Financing"
}

foreach var in insemplqhh insestabqhh inswageqhh {
	gen `var'_logbr = `var'*log_br_cnt
	label var `var'_logbr "`var'*Log[Branches+1]"
}

foreach var in insemplqhh insestabqhh inswageqhh {
	gen `var'_lrg = `var'*largebank
	gen `var'_sml = `var'*smallbank
	label var `var'_lrg "`var'*Large Dummy"
	label var `var'_sml "`var'*Small Dummy"
}

/*
foreach var in insemplqhh insestabqhh inswageqhh {
	gen alrg`var' = a`var'*largebank
	label var alrg`var' "Above Average ShockXLarge Bank"
}	

foreach var in empl estab wage {
	gen gavg_ins`var'qhh_lrg = gavg_ins`var'qhh*largebank
	label var gavg_ins`var'qhh_lrg "Above Average*Large Dummy"
	gen lavg_ins`var'qhh_lrg = lavg_ins`var'qhh*largebank
	label var lavg_ins`var'qhh_lrg "Below Average*Large Dummy"
}

foreach var in empl estab wage {
	gen gavg_ins`var'qhh_pub = gavg_ins`var'qhh*public
	label var gavg_ins`var'qhh_pub "Above Average*Public Dummy"
	gen lavg_ins`var'qhh_pub = lavg_ins`var'qhh*public
	label var lavg_ins`var'qhh_pub "Below Average*Public Dummy"
}

foreach var in empl estab wage {
	gen gavg_ins`var'qhh_nd = gavg_ins`var'qhh*nondep_perc
	label var gavg_ins`var'qhh_nd "Above Average*Nondeposit Financing"
	gen lavg_ins`var'qhh_nd = lavg_ins`var'qhh*nondep_perc
	label var lavg_ins`var'qhh_nd "Below Average*Nondeposit Financing"
}

foreach var in empl estab wage {
	gen gavg_ins`var'qhh_br = gavg_ins`var'qhh*log_br_cnt
	label var gavg_ins`var'qhh_br "Above Average*Log Branches"
	gen lavg_ins`var'qhh_br = lavg_ins`var'qhh*log_br_cnt
	label var lavg_ins`var'qhh_br "Below Average*Log Branches"
}
*/

foreach var in insemplqhh insestabqhh inswageqhh {
	gen `var'_ass = `var'*log_l1_assets
	label var `var'_ass "`var'*Log Assets (Lagged)"
}

foreach var in insemplqhh insestabqhh inswageqhh {
	gen `var'_hhi = `var'*bankhhi
	label var `var'_hhi "`var'*HHI Exposure"
}



//Real estate tend to make about 40% of the loan portfolio
//RRE = 25% (NAICS), 16% (SIC)
//CRE = 16% (NAICS), 8% (SIC)
//RE = 61% (NAICS), 44% (SIC)
//CI = 5% (NAICS), 4% (SIC)
//CC = 0% (NAICS), 5% (SIC)
gen p_ln_rre = ln_rre/ln_tot
gen p_ln_cre = ln_cre/ln_tot
gen p_ln_othre = ln_othre/ln_tot
gen p_ln_re = ln_re/ln_tot
gen p_ln_ci = ln_ci/ln_tot
gen p_ln_cc = ln_cc/ln_tot
gen p_ln_othcons = ln_othcons/ln_tot
gen p_ln_lease = ln_lease/ln_tot
gen p_ln_agr = ln_agr/ln_tot
gen p_ln_dep = ln_dep/ln_tot
gen p_ln_fgovt = ln_fgovt/ln_tot
gen p_ln_oth = ln_oth/ln_tot

tabstat p_ln* if bank_id==1 & codetype==1 [aw=assets], stat(mean sd min p1 p25 med p75 p99 max N) column(statistics)
tabstat insemplqhh insestabqhh inswageqhh gavg_insemplqhh gavg_insestabqhh gavg_inswageqhh lavg_insemplqhh lavg_insestabqhh lavg_inswageqhh bankhhi if codetype==1 & year>1992, stat(mean sd min p1 p25 med p75 p99 max N) column(statistics)

//CRE+RRE = 49%(NAICS) 32%(SIC)
egen ln_cre_rre = rowtotal(ln_rre ln_cre)
gen p_cre_rre = ln_cre_rre/ln_tot

summary 



/*
tabstat insemplqhh if codetype==1, by(year) stat(mean min p25 med p75 max)
tabstat insestabqhh if codetype==1, by(year) stat(mean min p25 med p75 max)
tabstat inswageqhh if codetype==1, by(year) stat(mean min p25 med p75 max)

tabstat insemplqhh if codetype==2, by(year) stat(mean min p25 med p75 max)
tabstat insestabqhh if codetype==2, by(year) stat(mean min p25 med p75 max)
tabstat inswageqhh if codetype==2, by(year) stat(mean min p25 med p75 max)


tabstat d_insemplqhh if codetype==1 , by(year) stat(mean min p25 med p75 max)
tabstat d_insestabqhh if codetype==1, by(year) stat(mean min p25 med p75 max)
tabstat d_inswageqhh if codetype==1, by(year) stat(mean min p25 med p75 max)

tabstat d_insemplqhh if codetype==2, by(year) stat(mean min p25 med p75 max)
tabstat d_insestabqhh if codetype==2, by(year) stat(mean min p25 med p75 max)
tabstat d_inswageqhh if codetype==2, by(year) stat(mean min p25 med p75 max)
*/

xi i.year 

/*
set more off
capture erase "$location/output/bankregs_sbl_changes_bfe.xlm"
capture erase "$location/output/bankregs_sbl_changes_bfe.txt"'
forvalues types = 1/1 {
	if `types'==1{
		local typelab "NAICS"
	}
	else if `types'==2 {
		local typelab "SIC"
	}
	else{
		local typelab "Combined"
	}
	
	
	foreach lhs in ln_sbl_amt ln_sbl_ci_amt ln_sbl_cre_amt ln_sbl_ci_a1 ln_sbl_ci_a2 ln_sbl_ci_a3 ln_sbl_cre_a1 ln_sbl_cre_a2 ln_sbl_cre_a3  { 
		foreach rhs in insemplqhh insestabqhh inswageqhh{
			xtreg d_`lhs'_perc `rhs' /*log_l1_assets*/ /*l1.leverage l1.dep_assets bank_id*/ _I* if codetype==`types', fe cluster(hhrssd_dup)
			outreg2 using "$location/output/bankregs_sbl_changes_bfe", append ctitle("`lhs' - `typelab'") r2 excel label drop(_I*) addtext(Bank FE, YES, Year FE, YES, Clustered SE, Bank)
		}
	}
}

*/

set more off
capture rm "$location/output/bankregs_changes.xlm"
capture rm "$location/output/bankregs_changes.txt"
forvalues types = 1/1 {
	if `types'==1{
		local typelab "NAICS"
	}
	else if `types'==2 {
		local typelab "SIC"
	}
	else{
		local typelab "Combined"
	}
	
	
	foreach lhs in ln_sbl_amt ln_sbl_ci_amt ln_sbl_cre_amt ln_tot ln_re ln_rre ln_cre ln_othre ln_ci { 
		foreach rhs in insemplqhh insestabqhh inswageqhh{
			xtreg d_`lhs'_perc `rhs' /*log_l1_assets*/ /*l1.leverage l1.dep_assets bank_id*/ _I* if codetype==`types', i(panelid) fe cluster(hhrssd_dup)
			outreg2 using "$location/output/bankregs_changes", append ctitle("`lhs' - `typelab'") excel adjr2 label drop(_I*) addtext(Bank FE, YES, Year FE, YES, Clustered SE, Bank)
		}
	}
	
	foreach lhs in ln_sbl_amt ln_sbl_ci_amt ln_sbl_cre_amt ln_tot ln_re ln_rre ln_cre ln_othre ln_ci { 
		foreach rhs in insemplqhh insestabqhh inswageqhh{
			xtreg d_`lhs'_ass `rhs' /*log_l1_assets*/ /*l1.leverage l1.dep_assets bank_id*/ _I* if codetype==`types', i(panelid) fe cluster(hhrssd_dup)
			outreg2 using "$location/output/bankregs_changes", append ctitle("`lhs' - `typelab'") excel adjr2 label drop(_I*) addtext(Bank FE, YES, Year FE, YES, Clustered SE, Bank)
		}
	}
}

s

set more off
capture erase "$location/output/bankregs_changes_assym.xlm"
capture erase "$location/output/bankregs_changes_assym.txt"
forvalues types = 1/1 {
	if `types'==1{
		local typelab "NAICS"
	}
	else if `types'==2 {
		local typelab "SIC"
	}
	else{
		local typelab "Combined"
	}
	
	
	foreach lhs in ln_sbl_amt ln_sbl_ci_amt ln_sbl_cre_amt ln_tot ln_re ln_rre ln_cre ln_othre ln_ci { 
		foreach rhs in insemplqhh insestabqhh inswageqhh {
			xtreg d_`lhs'_perc gavg_`rhs' lavg_`rhs' /*log_l1_assets*/ /*l1.leverage l1.dep_assets bank_id*/ _I* if codetype==`types' & d_ln_othre_perc!=. , i(panelid) fe cluster(hhrssd_dup)
			outreg2 using "$location/output/bankregs_changes_assym", append ctitle("`lhs' - `typelab'") excel adjr2 label drop(_I*) addtext(Bank FE, YES, Year FE, YES, Clustered SE, Bank)
		}
	}
	
	/*
	foreach lhs in ln_sbl_amt ln_sbl_ci_amt ln_sbl_cre_amt ln_tot ln_re ln_rre ln_cre ln_othre ln_ci {
		foreach rhs in insemplqhh insestabqhh inswageqhh {
			xtreg d_`lhs'_ass gavg_`rhs' lavg_`rhs' /*log_l1_assets*/ /*l1.leverage l1.dep_assets bank_id*/ _I* if codetype==`types', i(panelid) fe cluster(hhrssd_dup)
			outreg2 using "$location/output/bankregs_changes_assym", append ctitle("`lhs' - `typelab'") excel adjr2 label drop(_I*) addtext(Bank FE, YES, Year FE, YES, Clustered SE, Bank)
		}
	}
	*/
}
z
set more off
capture erase "$location/output/bankregs_changes_assym_hhi.xlm"
capture erase "$location/output/bankregs_changes_assym_hhi.txt"
forvalues types = 1/3 {
	if `types'==1{
		local typelab "NAICS"
	}
	else if `types'==2 {
		local typelab "SIC"
	}
	else{
		local typelab "Combined"
	}
	
	
	foreach lhs in ln_sbl_amt ln_sbl_ci_amt ln_sbl_cre_amt ln_tot ln_re ln_rre ln_cre ln_ci { 
		foreach rhs in insemplqhh insestabqhh inswageqhh {
			xtreg d_`lhs'_perc gavg_hhi_`rhs' lavg_hhi_`rhs' /*log_l1_assets*/ /*l1.leverage l1.dep_assets bank_id*/ _I* if codetype==`types', i(panelid) fe cluster(hhrssd_dup)
			outreg2 using "$location/output/bankregs_changes_assym_hhi", append ctitle("`lhs' - `typelab'") excel adjr2 label drop(_I*) addtext(Bank FE, YES, Year FE, YES, Clustered SE, Bank)
		}
	}
}
*/

set more off
capture erase "$location/output/bankregs_changes_assym_hhi_l.xlm"
capture erase "$location/output/bankregs_changes_assym_hhi_l.txt"
forvalues types = 1/3 {
	if `types'==1{
		local typelab "NAICS"
	}
	else if `types'==2 {
		local typelab "SIC"
	}
	else{
		local typelab "Combined"
	}
	
	
	foreach lhs in ln_sbl_amt ln_sbl_ci_amt ln_sbl_cre_amt ln_tot ln_re ln_rre ln_cre ln_ci { 
		foreach rhs in insemplqhh insestabqhh inswageqhh {
			xtreg d_`lhs'_perc gavg_hhi_`rhs' lavg_hhi_`rhs' /*log_l1_assets*/ /*l1.leverage l1.dep_assets bank_id*/ _I* if codetype==`types' & largebank, i(panelid) fe cluster(hhrssd_dup)
			outreg2 using "$location/output/bankregs_changes_assym_hhi_l", append ctitle("`lhs' - `typelab'") excel adjr2 label drop(_I*) addtext(Bank FE, YES, Year FE, YES, Clustered SE, Bank)
		}
	}
}
*
s
set more off
capture erase "$location/output/bankregs_changes_size.xlm"
capture erase "$location/output/bankregs_changes_size.txt"
forvalues types = 1/3 {
	if `types'==1{
		local typelab "NAICS"
	}
	else if `types'==2 {
		local typelab "SIC"
	}
	else{
		local typelab "Combined"
	}
	
	
	foreach lhs in ln_sbl_amt ln_sbl_ci_amt ln_sbl_cre_amt ln_tot ln_re ln_rre ln_cre ln_ci { 
		foreach rhs in insemplqhh insestabqhh inswageqhh{
			xtreg d_`lhs'_perc `rhs'_lrg `rhs'_sml largebank /*l1.log_assets l1.leverage l1.dep_assets bank_id*/ _I* if codetype==`types', i(panelid) fe cluster(hhrssd_dup)
			outreg2 using "$location/output/bankregs_changes_size", append ctitle("`lhs' - `typelab'") excel adjr2 label drop(_I*) addtext(Bank FE, YES, Year FE, YES, Clustered SE, Bank)
		}
	}
}
s
/*
set more off
capture erase "$location/output/bankregs_changes_size_c.xlm"
capture erase "$location/output/bankregs_changes_size_c.txt"
forvalues types = 1/2 {
	if `types'==1{
		local typelab "NAICS"
	}
	else{
		local typelab "SIC"
	}
	
	foreach lhs in ln_tot ln_re ln_rre ln_cre ln_ci { 
		foreach rhs in insemplqhh insestabqhh inswageqhh{
			xtreg d_`lhs'_perc `rhs'_ass `rhs' log_l1_assets /*l1.log_assets l1.leverage l1.dep_assets bank_id*/ _I* if codetype==`types', i(panelid) fe cluster(hhrssd_dup)
			outreg2 using "$location/output/bankregs_changes_size_c", append ctitle("`lhs' - `typelab'") excel adjr2 label drop(_I*) addtext(Bank FE, YES, Year FE, YES, Clustered SE, Bank)
		}
	}
}
*/
set more off
capture erase "$location/output/bankregs_changes_size_assym.xlm"
capture erase "$location/output/bankregs_changes_size_assym.txt"
forvalues types = 1/2 {
	if `types'==1{
		local typelab "NAICS"
	}
	else if `types'==2 {
		local typelab "SIC"
	}
	else{
		local typelab "Combined"
	}
	
	foreach lhs in ln_tot ln_re ln_rre ln_cre ln_ci { 
		foreach rhs in insemplqhh insestabqhh inswageqhh{
			xtreg d_`lhs'_perc gavg_`rhs'_lrg  gavg_`rhs' `rhs'_lrg alrg`rhs' `rhs' a`rhs' largebank log_l1_assets /*l1.leverage l1.dep_assets bank_id*/ _I* if codetype==`types', i(panelid) fe cluster(hhrssd_dup)
			outreg2 using "$location/output/bankregs_changes_size_assym", append ctitle("`lhs' - `typelab'") excel adjr2 label drop(_I*) addtext(Bank FE, YES, Year FE, YES, Clustered SE, Bank)
		}
	}
}


set more off
foreach rhs in insemplqhh insestabqhh inswageqhh{
	collin gavg_`rhs'_lrg  gavg_`rhs' `rhs'_lrg alrg`rhs' `rhs' a`rhs' largebank if codetype==2
}

************************
*******FUNDING**********
************************

set more off
capture erase "$location/output/fundreg.xlm"
capture erase "$location/output/fundreg.txt"
forvalues types = 1/2 {
	if `types'==1{
		local typelab "NAICS"
	}
	else{
		local typelab "SIC"
	}
	
	foreach lhs in tot_deposit assets nondeposit equity { 
		foreach rhs in insemplqhh insestabqhh inswageqhh{
			xtreg d_`lhs'_perc `rhs' log_l1_assets _I* if codetype==`types', i(panelid) fe cluster(hhrssd_dup)
			outreg2 using "$location/output/fundreg", append ctitle("`lhs' - `typelab'") excel adjr2 label drop(_I*) addtext(Bank FE, YES, Year FE, YES, Clustered SE, Bank)
		}
	}
}


set more off
capture erase "$location/output/fundreg_assym.xlm"
capture erase "$location/output/fundreg_assym.txt"
forvalues types = 1/2 {
	if `types'==1{
		local typelab "NAICS"
	}
	else{
		local typelab "SIC"
	}
	
	foreach lhs in tot_deposit assets nondeposit equity { 
		foreach rhs in insemplqhh insestabqhh inswageqhh{
			xtreg d_`lhs'_perc gavg_`rhs' `rhs' a`rhs' log_l1_assets _I* if codetype==`types', i(panelid) fe cluster(hhrssd_dup)
			outreg2 using "$location/output/fundreg_assym", append ctitle("`lhs' - `typelab'") excel adjr2 label drop(_I*) addtext(Bank FE, YES, Year FE, YES, Clustered SE, Bank)
		}
	}
}

set more off
capture erase "$location/output/fundreg_size.xlm"
capture erase "$location/output/fundreg_size.txt"
forvalues types = 1/2 {
	if `types'==1{
		local typelab "NAICS"
	}
	else{
		local typelab "SIC"
	}
	
	foreach lhs in tot_deposit assets nondeposit equity { 
		foreach rhs in insemplqhh insestabqhh inswageqhh{
			xtreg d_`lhs'_perc `rhs'_lrg `rhs' log_l1_assets /*l1.log_assets l1.leverage l1.dep_assets bank_id*/ largebank _I*  if codetype==`types', i(panelid) fe cluster(hhrssd_dup)
			outreg2 using "$location/output/fundreg_size", append ctitle("`lhs' - `typelab'") excel adjr2 label drop(_I*) addtext(Bank FE, YES, Year FE, YES, Clustered SE, Bank)
		}
	}
}

set more off
capture erase "$location/output/fundreg_size_c.xlm"
capture erase "$location/output/fundreg_size_c.txt"
forvalues types = 1/2 {
	if `types'==1{
		local typelab "NAICS"
	}
	else{
		local typelab "SIC"
	}
	
	foreach lhs in tot_deposit assets nondeposit equity { 
		foreach rhs in insemplqhh insestabqhh inswageqhh{
			xtreg d_`lhs'_perc `rhs'_ass `rhs' log_l1_assets /*l1.log_assets l1.leverage l1.dep_assets bank_id*/ _I* if codetype==`types', i(panelid) fe cluster(hhrssd_dup)
			outreg2 using "$location/output/fundreg_size_c", append ctitle("`lhs' - `typelab'") excel adjr2 label drop(_I*) addtext(Bank FE, YES, Year FE, YES, Clustered SE, Bank)
		}
	}
}

set more off
capture erase "$location/output/fundreg_size_assym.xlm"
capture erase "$location/output/fundreg_size_assym.txt"
forvalues types = 1/2 {
	if `types'==1{
		local typelab "NAICS"
	}
	else{
		local typelab "SIC"
	}
	
	foreach lhs in tot_deposit assets nondeposit equity { 
		foreach rhs in insemplqhh insestabqhh inswageqhh{
			xtreg d_`lhs'_perc gavg_`rhs'_lrg  gavg_`rhs' `rhs'_lrg alrg_`rhs' `rhs' a`rhs' largebank log_l1_assets /*l1.leverage l1.dep_assets bank_id*/ largebank _I* if codetype==`types', i(panelid) fe cluster(hhrssd_dup)
			outreg2 using "$location/output/fundreg_size_assym", append ctitle("`lhs' - `typelab'") excel adjr2 label drop(_I*) addtext(Bank FE, YES, Year FE, YES, Clustered SE, Bank)
		}
	}
}
*
s
*************************
*******HORSE RACE********
*************************

set more off
capture erase "$location/output/hrreg.xlm"
capture erase "$location/output/hrreg.txt"
forvalues types = 1/2 {
	if `types'==1{
		local typelab "NAICS"
	}
	else{
		local typelab "SIC"
	}
	
	foreach lhs in ln_tot ln_re ln_rre ln_cre ln_ci { 
		foreach rhs in insemplqhh insestabqhh inswageqhh{
			xtreg d_`lhs'_perc `rhs'_public `rhs'_logbr `rhs' public log_br_cnt log_l1_assets /*l1.leverage l1.dep_assets bank_id*/ _I* if codetype==`types', i(panelid) fe cluster(hhrssd_dup)
			outreg2 using "$location/output/hrreg", append ctitle("`lhs' - `typelab'") excel adjr2 label drop(_I*) addtext(Bank FE, YES, Year FE, YES, Clustered SE, Bank)
		}
	}
	
	foreach lhs in ln_tot ln_re ln_rre ln_cre ln_ci { 
		foreach rhs in insemplqhh insestabqhh inswageqhh{
			xtreg d_`lhs'_ass `rhs'_nondep  `rhs'_logbr `rhs' nondep_perc log_br_cnt log_l1_assets /*l1.leverage l1.dep_assets bank_id*/ _I* if codetype==`types', i(panelid) fe cluster(hhrssd_dup)
			outreg2 using "$location/output/hrreg", append ctitle("`lhs' - `typelab'") excel adjr2 label drop(_I*) addtext(Bank FE, YES, Year FE, YES, Clustered SE, Bank)
		}
	}
	
}

set more off
capture erase "$location/output/hrreg_multi.xlm"
capture erase "$location/output/hrreg_multi.txt"
forvalues types = 1/2 {
	if `types'==1{
		local typelab "NAICS"
	}
	else{
		local typelab "SIC"
	}
	
	foreach lhs in ln_tot ln_re ln_rre ln_cre ln_ci { 
		foreach rhs in insemplqhh insestabqhh inswageqhh{
			xtreg d_`lhs'_perc `rhs'_public `rhs'_multi `rhs' public multi log_l1_assets /*l1.leverage l1.dep_assets bank_id*/ _I* if codetype==`types', i(panelid) fe cluster(hhrssd_dup)
			outreg2 using "$location/output/hrreg_multi", append ctitle("`lhs' - `typelab'") excel adjr2 label drop(_I*) addtext(Bank FE, YES, Year FE, YES, Clustered SE, Bank)
		}
	}
	
	foreach lhs in ln_tot ln_re ln_rre ln_cre ln_ci { 
		foreach rhs in insemplqhh insestabqhh inswageqhh{
			xtreg d_`lhs'_perc `rhs'_nondep  `rhs'_multi `rhs' nondep_perc multi log_l1_assets /*l1.leverage l1.dep_assets bank_id*/ _I* if codetype==`types', i(panelid) fe cluster(hhrssd_dup)
			outreg2 using "$location/output/hrreg_multi", append ctitle("`lhs' - `typelab'") excel adjr2 label drop(_I*) addtext(Bank FE, YES, Year FE, YES, Clustered SE, Bank)
		}
		
	}
	
}

set more off
capture erase "$location/output/hrreg_assym.xlm"
capture erase "$location/output/hrreg_assym.txt"
forvalues types = 1/2 {
	if `types'==1{
		local typelab "NAICS"
	}
	else{
		local typelab "SIC"
	}
	
	foreach lhs in ln_tot ln_re ln_rre ln_cre ln_ci { 
		foreach rhs in insemplqhh insestabqhh inswageqhh{
			xtreg d_`lhs'_perc gavg_`rhs'_pub lavg_`rhs'_pub gavg_`rhs'_br lavg_`rhs'_br gavg_`rhs' lavg_`rhs' public log_br_cnt log_l1_assets /*l1.leverage l1.dep_assets bank_id*/ _I* if codetype==`types', i(panelid) fe cluster(hhrssd_dup)
			outreg2 using "$location/output/hrreg_assym", append ctitle("`lhs' - `typelab'") excel adjr2 label drop(_I*) addtext(Bank FE, YES, Year FE, YES, Clustered SE, Bank)
		}
	}
	
	foreach lhs in ln_tot ln_re ln_rre ln_cre ln_ci { 
		foreach rhs in insemplqhh insestabqhh inswageqhh{
			xtreg d_`lhs'_perc gavg_`rhs'_nd lavg_`rhs'_nd gavg_`rhs'_br lavg_`rhs'_br gavg_`rhs' lavg_`rhs' nondep_perc log_br_cnt log_l1_assets /*l1.leverage l1.dep_assets bank_id*/ _I* if codetype==`types', i(panelid) fe cluster(hhrssd_dup)
			outreg2 using "$location/output/hrreg_assym", append ctitle("`lhs' - `typelab'") excel adjr2 label drop(_I*) addtext(Bank FE, YES, Year FE, YES, Clustered SE, Bank)
		}
	}
}
*/



/*
/*Percentage Change*/
set more off
capture erase "$location/output/bankregs_changesp.xlm"
capture erase "$location/output/bankregs_changesp.txt"
forvalues types = 1/2 {
	if `types'==1{
		local typelab "NAICS"
	}
	else{
		local typelab "SIC"
	}
	
	foreach lhs in ln_tot ln_re ln_rre ln_cre ln_ci { 
		foreach rhs in insemplqhh insestabqhh inswageqhh{
			xi: xtreg d_`lhs'_perc `rhs' l1.log_assets /*l1.leverage l1.dep_assets bank_id*/ i.year if codetype==`types', i(panelid) fe cluster(hhrssd_dup)
			outreg2 using "$location/output/bankregs_changesp", append ctitle("`lhs' - `typelab'") excel adjr2 label drop(_I*) addtext(Bank FE, YES, Year FE, YES, Clustered SE, Bank)
		}
	}
}
*

set more off
capture erase "$location/output/bankregs_changesp_size.xlm"
capture erase "$location/output/bankregs_changesp_size.txt"
forvalues types = 1/2 {
	if `types'==1{
		local typelab "NAICS"
	}
	else{
		local typelab "SIC"
	}
	
	foreach lhs in ln_tot ln_re ln_rre ln_cre ln_ci { 
		foreach rhs in insemplqhh insestabqhh inswageqhh{
			xi: xtreg d_`lhs'_perc `rhs'_lrg `rhs' l1.log_assets /*l1.log_assets l1.leverage l1.dep_assets bank_id*/ smallbank i.year if codetype==`types', i(panelid) fe cluster(hhrssd_dup)
			outreg2 using "$location/output/bankregs_changesp_size", append ctitle("`lhs' - `typelab'") excel adjr2 label drop(_I*) addtext(Bank FE, YES, Year FE, YES, Clustered SE, Bank)
		}
	}
}
*/
