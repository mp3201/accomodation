*This program checks if the relationship between the instrument and the actual
*growth rate is as expected (i.e the coefficient is approximately 1)

use "//RB/B1/NYRESAN/RDS/Work/fif/b1mxp07/Matt/data/naics_sic_inscnty_3.dta", replace
set more off
forvalues types = 1/2{
	
	if (`types'==1) local codes "NAICS"
	else if (`types'==2) local codes "SIC"
	
	foreach var in emplq estabq wageq{
		if ("`var'"=="emplq"){
			local ctitles "Employment"
		}
		else if ("`var'"=="estabq"){
			local ctitles "Establishments"
		}
		else{
			local ctitles "Wages"
		}
		
		reg grtc`var' ins`var' if codetype==`types'
		outreg2 using "//RB/B1/NYRESAN/RDS/Work/fif/b1mxp07/Matt/output/checkinst.xls", bdec(4) bracket label drop(_I*) ctitle("`ctitles' `codes'") addtext("Weighted, NO") append
		
		reg grtc`var' ins`var' [aw=c`var'] if codetype==`types'
		outreg2 using "//RB/B1/NYRESAN/RDS/Work/fif/b1mxp07/Matt/output/checkinst.xls", bdec(4) bracket label drop(_I*) ctitle("`ctitles' `codes'") addtext("Weighted, YES") append
	}
}
