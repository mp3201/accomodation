/*******************************************/
/* Merges industry-county data with county */
/******************************************/

if "`c(os)'" == "Unix" {
	global location "/home/rcemxp06/accomodation"
	global outreg "/home/rcemxp06/accomodation/output/regressions"
	global outsum "/home/rcemxp06/accomodation/output/summaries"
	global outint "/home/rcemxp06/accomodation/output/interactions"
	global outfig "/home/rcemxp06/accomodation/output/figures"
	global data "/home/rcemxp06/accomodation/data"
	global subcode "/home/rcemxp06/accomodation/code/subcode"
	adopath ++ "/home/rcemxp06/ado"
}
else if "`c(os)'" == "Windows" {
	global location "R:/accomodation"
	global outreg "R:/accomodation/output/regressions"
	global outsum "R:/accomodation/output/summaries"
	global outint "R:/accomodation/output/interactions"
	global outfig "R:/accomodation/output/figures"
	global data "R:/accomodation/data"
	global subcode "R:/accomodation/code/subcode"
	adopath ++ "R:/ado"
}

set more off

/**********************************************************************/

// Prepare county-level data to merge with county-industry
cd $data
use panel_temp, replace
keep codetype area year stfipsn pred_* g_* br_* numb*  drg lsd sd drg_* lsd_* sd_* ///
w1_* w2_* rdp* lad* lap* dwl* dwu* n_p_* pctpop* pctemp* neg_*  empl_* wage_* estab_*

sort codetype area year

tempfile county
save `county', replace

// Prepare industry-county-level data to merge with county
use "$location/data/naics_sic_insindcnty.dta", clear
capture drop _merge
gen quarter = quarter(dofq(date))
gen year = year(dofq(date))
drop if quarter!=2
drop quarter date


foreach var in empl estab wage {
rename insind2`var'q pi_`var'
rename grtc2`var'q gi_`var'
rename c2`var'q ind_`var'
}

rename industry2dig ind2
destring(ind2), gen(ind2n) force 

sort codetype area year


// Merge in county-level variables
merge m:1 codetype area year using `county'
drop if _merge!=3
drop _merge

global popmin 33 
global popcond pctpop85>$popmin & pctemp85>$popmin & numbr85>0 
global ycond1 year>1985 & year<2006 & $popcond
global ycond2 year>1990 & year<2006 & $popcond
global ycond3 year>1990 & year<=2011 & $popcond
global ycond4 year>1985 & year<=2011 & $popcond


// Trim at 1% and 2.5% levels
foreach typ in empl estab wage {
		gen w1i_`typ'=gi_`typ'
		gen w2i_`typ'=gi_`typ'
forvalue i = 1/3{
		 xtile temp=gi_`typ' if codetype==`i' & $ycond4, nq(200)
		 replace w1i_`typ' = . if temp<=2 & codetype==`i' & $ycond4
		 replace w1i_`typ' = . if temp>=199 & codetype==`i' & $ycond4
		 replace w2i_`typ' = . if temp<=5 & codetype==`i' & $ycond4
		 replace w2i_`typ' = . if temp>=196 & codetype==`i' & $ycond4
		 drop temp
	}
}


/*******************************************/
/* negative predictions */
/*******************************************/
foreach pred in empl estab wage {
	if ("`pred'" == "empl"){
		local crhsti "Predicted Employment"
	}
	else if ("`pred'" == "estab"){
		local crhsti "Predicted Establishments"
	}
	else if ("`pred'" == "wage"){
		local crhsti "Predicted Wage"
	}

	gen ptemp = (pi_`pred'>=0 & pi_`pred'!=.)
	gen pos_i_`pred' = pi_`pred'*ptemp
	label var pos_i_`pred' "Pos. Ind. `crhsti'"
	gen neg_i_`pred'_dum = (pi_`pred'<0 & pi_`pred'!=.)
	gen neg_i_`pred' = pi_`pred'*neg_i_`pred'_dum
	label var neg_i_`pred' "Neg. Ind. `crhsti'"
	drop ptemp 
}


/*******************************************/
/* within year mean/median*/
/*******************************************/

*Second calc within year mean/median shock
foreach var2 in empl estab wage {
bysort year codetype ind2: egen mean_pi_`var2'=mean(pi_`var2') if $popcond
bysort year codetype ind2: egen median_pi_`var2'=median(pi_`var2') if $popcond
}

*Third calc within year pctile
foreach var in empl estab wage {
gen ptile_pi_`var'=.
foreach typ in 1 2 3 {
cap drop tag
gen tag = (pi_`var'~=.)
sort year codetype ind2 tag pi_`var'
by year codetype ind2 tag: replace ptile_pi_`var' = floor((_n-1)/_N * 100) + 1 if tag~=. & codetype==`typ'
drop tag
}
}


*Define dummies labelling above and below 
foreach var2 in empl estab wage {
	if ("`var2'" == "empl"){
		local crhsti "Predicted Employment"
	}
	else if ("`var2'" == "estab"){
		local crhsti "Predicted Establishments"
	}
	else if ("`var2'" == "wage"){
		local crhsti "Predicted Wage"
	}
	
gen bi_ts=.
gen bi_ts2=.
gen bi_ts33=.

foreach typ in 1 2 3 {
replace bi_ts=(pi_`var2'<mean_pi_`var2') if codetype==`typ' & $ycond4
replace bi_ts2=(pi_`var2'<median_pi_`var2') if codetype==`typ' & $ycond4
replace bi_ts33=(ptile_pi_`var2'<=33) if codetype==`typ' & $ycond4
}

gen n_ts_i_`var2'_dum=bi_ts
gen n_ts_i_`var2'=pi_`var2'*bi_ts
label var n_ts_i_`var2' "Below Avg `crhsti' within year"

gen n_ts2_i_`var2'_dum=bi_ts2
gen n_ts2_i_`var2'=pi_`var2'*bi_ts2
label var n_ts2_i_`var2' "Below p50 `crhsti' within year"

gen n_ts33_i_`var2'_dum=bi_ts33
gen n_ts33_i_`var2'=pi_`var2'*bi_ts33
label var n_ts33_i_`var2' "Below p33 `crhsti' within year"

drop bi_ts* 
}

/*******************************************/
/* within sample below mean */
/*******************************************/

foreach pred in empl estab wage {
	if ("`pred'" == "empl"){
		local crhsti "Predicted Employment"
	}
	else if ("`pred'" == "estab"){
		local crhsti "Predicted Establishments"
	}
	else if ("`pred'" == "wage"){
		local crhsti "Predicted Wage"
	}
	gen below_m=.
	gen above_m=.
	gen below_p=.
	gen above_p=.
	gen below_i=.
	gen above_i=.
	gen below_pts=.
	gen above_pts=.
	
	foreach typ in 1 2 3 {
	summ pi_`pred' if codetype==`typ' & $ycond4
	replace below_m = (pi_`pred'<`r(mean)' & pi_`pred'!=.) if codetype==`typ' & $ycond4
	replace above_m = (pi_`pred'>=`r(mean)' & pi_`pred'!=.) if codetype==`typ' & $ycond4
	areg pi_`pred' if codetype==`typ' & $ycond4, absorb(area)
	predict demean1_`typ' if e(sample), r
	replace below_p = (demean1_`typ'<0 & pi_`pred'!=.) if codetype==`typ' & $ycond4
	replace above_p = (demean1_`typ'>=0 & pi_`pred'!=.) if codetype==`typ' & $ycond4
	areg pi_`pred' i.year if codetype==`typ' & $ycond4, absorb(area)
	predict demean2_`typ' if e(sample), r
	replace below_pts = (demean2_`typ'<0 & pi_`pred'!=.) if codetype==`typ' & $ycond4
	replace above_pts = (demean2_`typ'>=0 & pi_`pred'!=.) if codetype==`typ' & $ycond4
	areg pi_`pred' if codetype==`typ' & $ycond4, absorb(ind2)
	predict demean3_`typ' if e(sample), r
	replace below_i = (demean3_`typ'<0 & pi_`pred'!=.) if codetype==`typ' & $ycond4
	replace above_i = (demean3_`typ'>=0 & pi_`pred'!=.) if codetype==`typ' & $ycond4
	
	}
	
	gen p_i_`pred' = pi_`pred'*above_m
	label var p_i_`pred' "Above Avg `crhsti'"
	
	gen n_i_`pred'_dum=below_m
	gen n_i_`pred' = pi_`pred'*below_m
	label var n_i_`pred' "Above Avg `crhsti'"
	
	gen p_p_i_`pred' = pi_`pred'*above_p
	label var p_p_i_`pred' "Above Avg `crhsti' within area"
	
	gen n_p_i_`pred'_dum=below_p
	gen n_p_i_`pred' = pi_`pred'*below_p
	label var n_p_i_`pred' "Above Avg `crhsti' within area"
	
	gen p_pts_i_`pred' = pi_`pred'*above_pts
	label var p_pts_i_`pred' "Above Avg `crhsti' within areaand time fe"
	
	gen n_pts_i_`pred'_dum=below_pts
	gen n_pts_i_`pred' = pi_`pred'*below_pts
	label var n_pts_i_`pred' "Above Avg `crhsti' within area and time fe "
	
	gen p_i_i_`pred' = pi_`pred'*above_p
	label var p_i_i_`pred' "Above Avg `crhsti' within industry"
	
	gen n_i_i_`pred'_dum=below_i
	gen n_i_i_`pred' = pi_`pred'*below_i
	label var n_i_i_`pred' "Above Avg `crhsti' within industry"
	
	drop demean* above_* below_*
}


foreach var of varlist drg lsd sd drg_* lsd_* sd_* {
foreach var2 in empl estab wage {
gen i_`var2'_`var'=`var'*pi_`var2'
foreach var3 in neg_i n_ts_i n_ts2_i n_i n_p_i n_pts_i n_i_i {
gen `var3'_`var2'_`var'=`var'*`var3'_`var2'
}
}
}



foreach var of varlist rdp* lad* lap* dwl* dwu* {
foreach var2 in empl estab wage {
gen i_`var2'_`var'=`var'*pi_`var2'
foreach var3 in neg_i n_ts_i n_ts2_i n_i n_p_i n_pts_i n_i_i {
gen `var3'_`var2'_`var'=`var'*`var3'_`var2'
}
}
}

//Generate Panel IDs//
tab year, gen(yr_)
forvalue i = 1/3 {
egen temp`i'=group(area) if codetype==`i'
}

gen pid=temp3+3*10000
replace pid=temp2+2*10000 if codetype==2
replace pid=temp1+1*10000 if codetype==1

gen pid_yr= pid+(year-1979)*100000

tab ind2n, gen(ind_)
gen pid_ind= pid+(ind2n)*100000 


drop typearea temp1 temp2 temp3 state_name
xtset pid_ind year

cd $data
save panel_temp_ind, replace


exit
