*This program appends onto the NAICS data the SIC data

/*

Versions of classification systems
Please refer to the following links for detailed description on each version of NAICS, as well as its predecessor, the Standard Industry Classificatoin (SIC) system.

Data from 2011 forward are classified under the NAICS 2012 system.
Data from 2007-2010 are classified under the NAICS 2007 system.
Data from 1990-2006 are classified under the NAICS 2002 system. (Data from 1990-2000 were originally classified under the 1987 SIC. As a NAICS reconstruction project, the data had been reclassified under the NAICS 2002)
Data from 1988 to 2000 are classified under the 1987 SIC system.
Data for 1975 to 1987 are classified under the 1972 SIC system. The 1977 SIC revision introduced no significant changes.
Data from before 1975 are coded under earlier versions of the SIC system. There are no machine readable versions of that data available at this time.

*/


set more off
forvalues i = 1975/2000{
	cd "/san/RDS/Work/fif/b1mxp07/Matt/zip/csv/sic.`i'.q1-q4.by_area"
	drop _all
	insheet using "cntysic`i'.csv", clear names comma
	drop if substr(area_fips, 1, 1)=="M"
	drop if area_fips=="area_fips"
	foreach var of varlist _all {
		capture destring `var', replace
	}
	save "sic`i'.dta", replace
}

clear all
use "/san/RDS/Work/fif/b1mxp07/Matt/zip/csv/sic.1975.q1-q4.by_area/sic1975.dta", clear
forvalues i = 1976/2000{
	cd "/san/RDS/Work/fif/b1mxp07/Matt/zip/csv/sic.`i'.q1-q4.by_area"
	append using "sic`i'.dta"
}
save "/san/RDS/Work/fif/b1mxp07/Matt/data/sic19752000.dta", replace

use "/san/RDS/Work/fif/b1mxp07/Matt/data/sic19752000.dta", clear
gen date = yq(year, qtr)
drop year qtr

//Dropping useless variables
drop size_code size_title month1_e* month2_e* own_title agglvl_title area_title industry_title ///
taxable_qtrly_wages qtrly_contributions

//Dropping undefined regions
drop if substr(area_fips, 3, 3)=="999"

//In some cases we have that average weekly wage is "infinity"
//when employment is zero. This is only 380 observations though
count if avg_wkly_wage=="Infinity"
replace avg_wkly_wage="" if avg_wkly_wage=="Infinity"
destring avg_wkly_wage, replace

//Renaming variables to match with naics dataset
ren area_fips area
ren own_code own
ren agglvl_code agglvl
ren qtrly_estabs_count estabq 
ren month3_e* emplq
ren total_qtrly_wages wageq
ren avg_wkly_wage awwq
ren industry_code industry
ren disclosure_code statq

//Average employment
gen aveemplq = wageq/(awwq*13)

//Code type is 2 for SIC
gen codetype = 2
keep if own==5

order area date own industry agglvl statq estabq emplq wageq awwq aveemplq

/***insert code to convert to NAICS here***/

preserve
	keep if agglvl == 30
	save "/san/RDS/Work/fif/b1mxp07/Matt/data/sic4_75to00.dta", replace
restore
	
preserve
	keep if agglvl==3
	save "/san/RDS/Work/fif/b1mxp07/Matt/data/sicnat2_75to00.dta", replace
restore

preserve
	keep if agglvl==4
	save "/san/RDS/Work/fif/b1mxp07/Matt/data/sicnat3_75to00.dta", replace
restore

preserve
	keep if agglvl==28
	save "/san/RDS/Work/fif/b1mxp07/Matt/data/sic2_75to00.dta", replace
restore

preserve
	keep if agglvl==29
	save "/san/RDS/Work/fif/b1mxp07/Matt/data/sic3_75to00.dta", replace
restore





//Clean up NAICS data
foreach i in 3{
	use "/san/RDS/Work/fif/b1mxp07/Matt/data/naics`i'_90to11.dta", clear
	drop state 

	gen date = yq(year, quar)
	drop year quar
	format %tq date

	ren naics industry
	ren avemplq aveemplq

	gen codetype = 1

	append using "/san/RDS/Work/fif/b1mxp07/Matt/data/sic`i'_75to00.dta"

	gen temp = 0 if statq=="3" | statq=="N"
	replace temp = 1 if temp==.
	drop statq
	ren temp statq

	keep if own==5
	order area date own industry agglvl statq estabq emplq wageq awwq aveemplq
	
	/*
	preserve
		keep if (codetype==1 & date>=tq(1991q1)) | (codetype==2 & date<tq(1991q1))
		replace codetype = 3 
		tempfile combined
		save `combined', replace
	restore
	
	append using `combined'
	*/
	
	label def types 1 "NAICS" 2 "SIC" 3 "Combined", modify
	label values codetype types

	label define status 0 "Not Disclosed" 1 "Disclosed"
	label values statq status

	save "/san/RDS/Work/fif/b1mxp07/Matt/data/naics_sic_`i'.dta",replace
}
