cd P:\Matt\output\
use P:\Matt\data\qcew_county_1990_2011_annual2.dta, clear

sort area date naics
by area date: gen temp = annemp if naics=="10"
by area date: egen empl_cnty = max(temp)
label variable empl_cnty "County Employment - Series 10"
by area date: gen temp2 = annestab if naics=="10"
by area date: egen estab_cnty = max(temp2)
label variable estab_cnty "County Establishments - Series 10"
drop temp*

drop if naics=="10"
by area date: egen empl_cnty_est = sum(annemp)
label variable empl_cnty_est "County Employment - Hand Summation"
by area date: egen estab_cnty_est = sum(annestab)
label variable estab_cnty_est "County Establishments - Hand Summation"

gen empl_cnty_rat = empl_cnty_est/empl_cnty
label variable empl_cnty_rat "Employment Ratio - Est/Actual"

gen estab_cnty_rat = estab_cnty_est/estab_cnty
label variable estab_cnty_rat "Establishments Ratio - Est/Actual"

by area date: gen num = _n
replace empl_cnty_rat = . if num!=1
replace estab_cnty_rat = . if num!=1

//drop if date<tq(2001q1)
replace annstat="1" if annstat==""
replace annstat="2" if annstat=="-"
replace annstat="3" if annstat=="N"

destring annstat, replace
label define status 1 "Disclosed" 2 "No Data Available" 3 "Nondisclosed"
label values annstat status

gen empl_cnty_miss = 1 - empl_cnty_rat
gen log_emplq = log(annemp)
reg empl_cnty_miss log_emplq, robust


//Histogram of estimated empl to actual empl
hist empl_cnty_rat, fraction ///
title("Estimated employment to actual - Annual, All time")
graph export "P:\Matt\output\empl_annual_rat_cnty.wmf", replace
hist empl_cnty_rat if date>=tq(2001q1), fraction ///
title("Estimated employment to actual - Annual, Post 2001")
graph export "P:\Matt\output\empl_annualpost2001_rat_cnty.wmf", replace
//Historgram of estimated estabilishment to actual establishments
hist estab_cnty_rat, fraction ///
title("Estimated establishments to actual - Annual, All time")
graph export "P:\Matt\output\estab_annual_rat_cnty.wmf", replace
hist estab_cnty_rat if date>=tq(2001q1), fraction ///
title("Estimated establishments to actual - Annual, Post 2001")
graph export "P:\Matt\output\estab_annualpost2001_rat_cnty.wmf", replace






