/*
Takes Files created by 0_UploadFiles
1_CountyAnalysis.do
1) Merges County Level Deposit, CRA and Shale Data
2) County Level Empirics (including calculating some forms of the instrument -- hat)

2_FirmBuild.do
3) Mergers County Level File with Bank Level Financials and Deposit Data
4) Bank-Location Empirics (including calculating some forms of the isntrument -- blhat)

3_FirmAnalysis.do
4) Collapses the FirmBuild Data by bank to conduct bank level regressions
5) Uses RegControls.do and Output.do to looks at bank level impact of the shock and interactions
*/

/*********GLOBAL ANALYSIS SETTINGS**************/
/*Can Choose filepaths (local or unix)*/
/*Can include the upload files construction*/
/*Can set bhc or cbd level banking financials*/
/*Can set a number of analysis options*/
if "`c(os)'" == "Unix" {
	global location "/home/rcemxp06/accomodation"
	global outreg "/home/rcemxp06/accomodation/output/regressions"
	global outsum "/home/rcemxp06/accomodation/output/summaries"
	global outint "/home/rcemxp06/accomodation/output/interactions"
	global outfig "/home/rcemxp06/accomodation/output/figures"
	global data "/home/rcemxp06/accomodation/data"
	global subcode "/home/rcemxp06/accomodation/code/subcode"
	adopath ++ "/home/rcemxp06/ado"
}
else if "`c(os)'" == "Windows" {
	global location "R:/accomodation"
	global outreg "R:/accomodation/output/regressions"
	global outsum "R:/accomodation/output/summaries"
	global outint "R:/accomodation/output/interactions"
	global outfig "R:/accomodation/output/figures"
	global data "R:/accomodation/data"
	global subcode "R:/accomodation/code/subcode"
	adopath ++ "R:/ado"
}

set more off

cd $outsum
insheet using sample_counties.csv, comma clear

rename area location
sort location 
gen insamp=1 

collapse (max) insamp, by(location)
cd $data
cd maps
save map_temp, replace

shp2dta using gz_2010_us_050_00_5m, database("countydb") coordinates("countycoord") genid(_ID) replace
shp2dta using gz_2010_us_040_00_5m, database("statedb") coordinates("statecoord") genid(_ID) replace

global ifstate1 if STATE!="15" & STATE~="02" & STATE~="43" & STATE~="72"

use statecoord, clear
merge m:1 _ID using statedb, keepusing(STATE)
keep $ifstate1
drop _merge
save statecoord2, replace

use countydb, clear
gen state2=real(STATE)
gen county2=real(COUNTY)
gen location=state2*1000+county2
sort location
merge location using map_temp, sort unique
drop _merge


replace insamp=0 if insamp==.


/*Full US Maps*/

spmap insamp using countycoord $ifstate1, id(_ID) ndsize(vvthin) fcolor(Blues2) polygon(data(statecoord2) osize(medium) ocolor(black)) legtitle(Max. Annual Royalties (mm)) legstyle(2) 


graph export map_roydep_full.eps, replace

spmap tzmatch using countycoord $ifstate1, id(_ID) ndsize(vvthin) fcolor(Blues2) polygon(data(statecoord2) osize(medium) ocolor(black)) clmethod(custom) clbreaks(0 1 2)
graph export map_tz_full.eps, replace

spmap border using countycoord $ifstate1, id(_ID) ndsize(vvthin) fcolor(Blues2) polygon(data(statecoord2) osize(medium) ocolor(black)) clmethod(custom) clbreaks(0 1 2)
graph export map_b_full.eps, replace

/*Split Maps*/

spmap roy2_ml using uscoord $ifstate2, id(_ID) ndsize(vvthin) fcolor(Blues2) polygon(data(statecoord3) osize(medium) ocolor(black)) split legtitle(Max. Annual Royalties (mm))  legstyle(2)
graph export map_roy_split.eps, replace

spmap roy2_dep using uscoord $ifstate2, id(_ID) ndsize(vvthin) fcolor(Blues2) polygon(data(statecoord3) osize(medium) ocolor(black)) split legtitle(CFS/Deposits) legstyle(2)
graph export map_dep_split.eps, replace

spmap border using countycoord $ifstate2b, id(_ID) ndsize(vvthin) fcolor(Blues2) polygon(data(statecoord2) osize(medium) ocolor(black)) clmethod(custom) clbreaks(0 1 2)
graph export map_b_split.eps, replace

spmap gas using uscoord $ifstate3a, id(_ID) ndsize(vvthin) legend(off) fcolor(Blues2) polygon(data(statecoord3a) osize(medthick) ocolor(black))
graph export map_tx.eps, replace

spmap gas using uscoord $ifstate3b, id(_ID) ndsize(vvthin) legend(off) fcolor(Blues2) polygon(data(statecoord3b) osize(thick) ocolor(black))
graph export map_mt.eps, replace

spmap gas using uscoord $ifstate3c, id(_ID) ndsize(vvthin) legend(off) fcolor(Blues2) polygon(data(statecoord3c) osize(thick) ocolor(black))
graph export map_pa.eps, replace

spmap roy2_ml using uscoord $ifstate3a, id(_ID) ndsize(vvthin) clmethod(custom)  clbreaks(0 27 85 184 1800) legtitle(Max. Annual Royalties (mm)) legstyle(2) fcolor(Blues2) polygon(data(statecoord3a) osize(medthick) ocolor(black)) 
graph export map_roy_tx.eps, replace

spmap roy2_ml using uscoord $ifstate3b, id(_ID) ndsize(vvthin) legend(off) fcolor(Blues2) clmethod(custom)  clbreaks(0 27 85 184 1800) polygon(data(statecoord3b) osize(thick) ocolor(black)) 
graph export map_roy_mt.eps, replace

spmap roy2_ml using uscoord $ifstate3c, id(_ID) ndsize(vvthin) legend(on) fcolor(Blues2) clmethod(custom)  clbreaks(0 27 85 184 1800) polygon(data(statecoord3c) osize(thick) ocolor(black)) legend(off)  
graph export map_roy_pa.eps, replace

spmap roy2_dep using uscoord $ifstate3a, id(_ID) ndsize(vvthin) legend(off) fcolor(Blues2) polygon(data(statecoord3a) osize(medthick) ocolor(black))
graph export map_roydep_tx.eps, replace

spmap roy2_dep using uscoord $ifstate3b, id(_ID) ndsize(vvthin) legend(off) fcolor(Blues2) polygon(data(statecoord3b) osize(thick) ocolor(black))
graph export map_roydep_mt.eps, replace

spmap roy2_dep using uscoord $ifstate3c, id(_ID) ndsize(vvthin) legend(off) fcolor(Blues2) polygon(data(statecoord3c) osize(thick) ocolor(black))
graph export map_roydep_pa.eps, replace

